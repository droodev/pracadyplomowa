/**
	@author drew
 */

package pl.edu.agh.strateg.logic;

import static org.mockito.Mockito.*;

import java.util.UUID;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import pl.edu.agh.strateg.logic.async.OperationListener;
import pl.edu.agh.strateg.logic.verification.VerificationStatus;
import pl.edu.agh.strateg.logic.verification.Verifier;
import pl.edu.agh.strateg.model.Document;
import pl.edu.agh.strateg.persistence.DocumentsDAO;
import pl.edu.agh.strateg.persistence.PersistenceException;

import com.google.common.base.Optional;

@RunWith(MockitoJUnitRunner.class)
public class DocumentsManagerImplTest {

	@Mock
	private Verifier verifier;
	@Mock
	private DocumentsDAO dataDAO;
	@Mock
	private Document data;

	@Mock
	private OperationListener<Optional<Document>> listenerDocument;
	@Mock
	private OperationListener<Boolean> listenerBoolean;

	@InjectMocks
	private DocumentsManagerImpl testedImpl;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		when(verifier.verifyDocument(data, Optional.absent())).thenReturn(
				VerificationStatus.OK);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAddDocument() throws PersistenceException {
		testedImpl.addEntity(data, listenerBoolean);
		verifyListener(listenerBoolean, false);
		// verify(verifier).verifyData(data);
		verify(dataDAO).saveIfNotExists(data);
	}

	@Test
	public void Entity() throws PersistenceException {
		testedImpl.deleteEntity(data, listenerBoolean);
		verifyListener(listenerBoolean, false);
		// verify(verifier).verifyData(data);
		verify(dataDAO).deleteIfExists(data);
	}

	@Test
	public void testUpdateDocument() throws PersistenceException {
		testedImpl.updateEntity(data, listenerBoolean);
		verifyListener(listenerBoolean, false);
		// verify(verifier).verifyData(data);
		verify(dataDAO).updateIfExists(data);
	}

	// Test not passes because of stupid semantics of find!!!
	@Test
	public void testGetDocument() throws PersistenceException {
		UUID id = new UUID(12, 12);
		testedImpl.getEntity(id, listenerDocument);
		verifyListener(listenerDocument, null);
		verify(dataDAO).get(id);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void verifyListener(OperationListener<?> listener,
			Object postOperation) {
		verify(listener).preOperation();
		verify(listener).postVerification(VerificationStatus.OK);
		((OperationListener) verify(listener)).postOperation(postOperation);
	}
}
