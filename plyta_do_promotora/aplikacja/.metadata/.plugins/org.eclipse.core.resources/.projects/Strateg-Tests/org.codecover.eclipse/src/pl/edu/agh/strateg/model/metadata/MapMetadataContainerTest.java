/**
 * @author drew
 */
package pl.edu.agh.strateg.model.metadata;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import pl.edu.agh.strateg.model.metadata.description.Descriptor;
import pl.edu.agh.strateg.model.metadata.description.SimpleDescriptor;

import com.google.common.base.Optional;

public class MapMetadataContainerTest {
  static {
    CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.ping();
  }


	static { CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[1]++; }
private static final String DESC_ONE_NAME = "DescOneName";
	static { CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[2]++; }
private static final String DESC_TWO_NAME = "DescTwoName";
	static { CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[3]++; }
private static final String DESC_ONE_VAL = "DescOneVal";
	static { CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[4]++; }
private static final int DESC_TWO_VAL = 2;

	private Descriptor descOne;
	private Descriptor descTwo;

	private SimpleMetadatum metaOne;
	private SimpleMetadatum metaTwo;

	Collection<Metadatum> metadataCollection;
	MapMetadataContainer testedContainer;

	@Before
	public void setUp() throws Exception {
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[5]++;
settingMetadataDeps();
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[6]++;
metadataCollection = new ArrayList<Metadatum>();
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[7]++;
metadataCollection.add(metaOne);

		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[8]++;
testedContainer = new MapMetadataContainer(metadataCollection);
	}

	private void settingMetadataDeps() {
		// Mockito cannot mock methods of superclass. Can't mocking
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[9]++;
descOne = SimpleDescriptor.getCreator().createString(DESC_ONE_NAME);
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[10]++;
descTwo = SimpleDescriptor.getCreator().createInteger(DESC_TWO_NAME);

		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[11]++;
metaOne = new SimpleMetadatum(descOne, DESC_ONE_VAL);
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[12]++;
metaTwo = new SimpleMetadatum(descTwo, DESC_TWO_VAL);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void constructorValid() throws Exception {
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[13]++;
metadataCollection.add(metaTwo);
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[14]++;
MapMetadataContainer cont = new MapMetadataContainer(metadataCollection);
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[15]++;
assertThat(cont.getSize(), is(2));
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructorNull() throws Exception {
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[16]++;
new MapMetadataContainer(null);
	}

	@Test
	public void addingValid() throws Exception {
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[17]++;
testedContainer.addMetadatum(metaTwo);
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[18]++;
assertThat(testedContainer.getSize(), is(2));
	}

	@Test(expected = IllegalArgumentException.class)
	public void addingNull() throws Exception {
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[19]++;
testedContainer.addMetadatum(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void addingExisting() throws Exception {
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[20]++;
testedContainer.addMetadatum(metaOne);
	}

	@Test(expected = IllegalArgumentException.class)
	public void removingNull() throws Exception {
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[21]++;
testedContainer.removeMetadatum(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void removingNonexisting() throws Exception {
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[22]++;
testedContainer.removeMetadatum(descTwo);
	}

	@Test
	public void removingValid() throws Exception {
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[23]++;
testedContainer.removeMetadatum(descOne);
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[24]++;
assertThat(testedContainer.getSize(), is(0));
	}

	@Test
	public void gettingValid() throws Exception {
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[25]++;
Optional<Metadatum> got = testedContainer.getMetadatum(descOne);
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[26]++;
assertThat(got.isPresent(), is(true));
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[27]++;
assertThat(got.get(), is((Metadatum) metaOne));
	}

	@Test
	public void gettingNonexisting() throws Exception {
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[28]++;
Optional<Metadatum> got = testedContainer.getMetadatum(descTwo);
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[29]++;
assertThat(got.isPresent(), is(false));
	}

	/**
	 * Watch out - assuming that getting metadatum checks equality on name only!
	 * THIS COULD BE CHANGED
	 */
	@Test(expected = IllegalArgumentException.class)
	public void gettingExistingWithBadType() throws Exception {
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[30]++;
Descriptor newDesc = SimpleDescriptor.getCreator().createInteger(
				DESC_ONE_NAME);
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[31]++;
testedContainer.getMetadatum(newDesc);
	}

	@Test(expected = IllegalArgumentException.class)
	public void gettingNull() throws Exception {
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[32]++;
testedContainer.getMetadatum(null);
	}

	@Test
	public void updateWithSameType() throws Exception {
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[33]++;
SimpleMetadatum newMetadatum = new SimpleMetadatum(descOne, "newVal");
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[34]++;
testedContainer.updateMetadatum(newMetadatum);
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[35]++;
assertThat(testedContainer.getSize(), is(1));
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[36]++;
Optional<Metadatum> gotMetadatum = testedContainer
				.getMetadatum(descOne);
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[37]++;
assertThat(gotMetadatum.get(), is((Metadatum) newMetadatum));
	}

	/**
	 * Watch out - assuming that getting metadatum checks equality on name only!
	 * THIS COULD BE CHANGED
	 */
	@Test
	public void updateWithOtherType() throws Exception {
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[38]++;
Descriptor newDesc = SimpleDescriptor.getCreator().createInteger(
				DESC_ONE_NAME);
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[39]++;
SimpleMetadatum newMetadatum = new SimpleMetadatum(newDesc,
				DESC_TWO_VAL);
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[40]++;
testedContainer.updateMetadatum(newMetadatum);
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[41]++;
assertThat(testedContainer.getSize(), is(1));
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[42]++;
Optional<Metadatum> gotMetadatum = testedContainer
				.getMetadatum(newDesc);
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[43]++;
assertThat(gotMetadatum.get(), is((Metadatum) newMetadatum));
	}

	@Test
	public void updateTheSame() throws Exception {
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[44]++;
testedContainer.updateMetadatum(metaOne);
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[45]++;
assertThat(testedContainer.getSize(), is(1));
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[46]++;
Optional<Metadatum> gotMetadatum = testedContainer
				.getMetadatum(descOne);
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[47]++;
assertThat(gotMetadatum.get(), is((Metadatum) metaOne));
	}

	@Test(expected = IllegalArgumentException.class)
	public void updateNonexisting() throws Exception {
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[48]++;
testedContainer.updateMetadatum(metaTwo);
	}

	@Test(expected = IllegalArgumentException.class)
	public void updateNull() throws Exception {
		CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx.statements[49]++;
testedContainer.updateMetadatum(null);
	}

}

class CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx extends org.codecover.instrumentation.java.measurement.CounterContainer {

  static {
    org.codecover.instrumentation.java.measurement.ProtocolImpl.getInstance(org.codecover.instrumentation.java.measurement.CoverageResultLogFile.getInstance("E:\\studia\\pracaInz\\aplikacja\\.metadata\\.plugins\\org.eclipse.core.resources\\.projects\\Strateg-Tests\\org.codecover.eclipse\\coverage-logs\\coverage-log-file.clf"), "dd26a587-cbcb-4512-8709-17ef58373fbe").addObservedContainer(new CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx ());
  }
    public static long[] statements = new long[50];

  public CodeCoverCoverageCounter$6y8db4cmhzke4i9q17p13t40y95q634kjh34gyyejnrkx () {
    super("pl.edu.agh.strateg.model.metadata.MapMetadataContainerTest.java");
  }

  public static void ping() {/* nothing to do*/}

  public void reset() {
      for (int i = 1; i <= 49; i++) {
        statements[i] = 0L;
      }
  }

  public void serializeAndReset(org.codecover.instrumentation.measurement.CoverageCounterLog log) {
    log.startNamedSection("pl.edu.agh.strateg.model.metadata.MapMetadataContainerTest.java");
      for (int i = 1; i <= 49; i++) {
        if (statements[i] != 0L) {
          log.passCounter("S" + i, statements[i]);
          statements[i] = 0L;
        }
      }
  }
}

