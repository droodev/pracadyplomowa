/**
	@author drew
 */

package pl.edu.agh.strateg.model.analysis;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.base.Optional;

public class SimpleAnalysisTest {
  static {
    CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.ping();
  }


	private Analysis emptyAnalysis;
	private Analysis filledAnalysis;

	private RowAnalysisHeader strengthRow;
	private RowAnalysisHeader weaknessRow;

	private ColumnAnalysisHeader opportunityColumn;
	private ColumnAnalysisHeader threatColumn;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[1]++;
emptyAnalysis = SimpleAnalysis.createEmptyAnalysisWithNoMetadata();

		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[2]++;
filledAnalysis = SimpleAnalysis.createEmptyAnalysisWithNoMetadata();
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[3]++;
strengthRow = filledAnalysis.addStrength("Strength");
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[4]++;
weaknessRow = filledAnalysis.addWeakness("Weakness");
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[5]++;
opportunityColumn = filledAnalysis.addOpportunity("Opportunity");
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[6]++;
threatColumn = filledAnalysis.addThreat("Threat");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void addOneTest() {
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[7]++;
RowAnalysisHeader newStrength = emptyAnalysis.addStrength("A");
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[8]++;
assertThat(emptyAnalysis.getRows().size(), equalTo(1));
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[9]++;
assertThat(emptyAnalysis.getRows().iterator().next(),
				sameInstance(newStrength));
	}

	@Test
	public void orderingTest() {
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[10]++;
RowAnalysisHeader newStrength = filledAnalysis.addStrength("a");
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[11]++;
RowAnalysisHeader newWeakness = filledAnalysis.addWeakness("B");
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[12]++;
ColumnAnalysisHeader newOpportunity = filledAnalysis
				.addOpportunity("c");
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[13]++;
ColumnAnalysisHeader newThreat = filledAnalysis.addThreat("d");
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[14]++;
assertThat(filledAnalysis.getRows().size(), equalTo(4));
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[15]++;
assertThat(filledAnalysis.getColumns().size(), equalTo(4));

		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[16]++;
Iterator<RowAnalysisHeader> rowsIterator = filledAnalysis.getRows()
				.iterator();
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[17]++;
assertThat(rowsIterator.next(), sameInstance(strengthRow));
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[18]++;
assertThat(rowsIterator.next(), sameInstance(newStrength));
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[19]++;
assertThat(rowsIterator.next(), sameInstance(weaknessRow));
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[20]++;
assertThat(rowsIterator.next(), sameInstance(newWeakness));

		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[21]++;
Iterator<ColumnAnalysisHeader> columnsIterator = filledAnalysis
				.getColumns().iterator();
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[22]++;
assertThat(columnsIterator.next(), sameInstance(opportunityColumn));
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[23]++;
assertThat(columnsIterator.next(), sameInstance(newOpportunity));
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[24]++;
assertThat(columnsIterator.next(), sameInstance(threatColumn));
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[25]++;
assertThat(columnsIterator.next(), sameInstance(newThreat));
	}

	@Test
	public void getSetValueTest() {
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[26]++;
assertThat(filledAnalysis.getCell(strengthRow, threatColumn),
				equalTo(Optional.<SWOTField> absent()));
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[27]++;
SWOTField swotField = SWOTField.createSWOTField(990);
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[28]++;
filledAnalysis.setCell(strengthRow, threatColumn, swotField);
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[29]++;
assertThat(filledAnalysis.getCell(strengthRow, threatColumn).get(),
				equalTo(swotField));
	}

	@Test
	public void setCheckedValueTest() {
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[30]++;
SWOTField swotField = SWOTField.createSWOTField(990);
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[31]++;
filledAnalysis.setCell(strengthRow, threatColumn, swotField);
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[32]++;
assertThat(filledAnalysis.setCellChecked(strengthRow, threatColumn,
				swotField), equalTo(false));
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[33]++;
assertThat(filledAnalysis.setCellChecked(weaknessRow, threatColumn,
				swotField), equalTo(true));
	}

	@Test
	public void removeColumnTest() {
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[34]++;
filledAnalysis.removeColumn(opportunityColumn);
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[35]++;
assertThat(filledAnalysis.getColumns().size(), equalTo(1));
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[36]++;
assertThat(filledAnalysis.getRows().size(), equalTo(2));
	}

	@Test
	public void removeRowTest() {
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[37]++;
filledAnalysis.removeRow(weaknessRow);
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[38]++;
assertThat(filledAnalysis.getColumns().size(), equalTo(2));
		CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx.statements[39]++;
assertThat(filledAnalysis.getRows().size(), equalTo(1));
	}
}

class CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx extends org.codecover.instrumentation.java.measurement.CounterContainer {

  static {
    org.codecover.instrumentation.java.measurement.ProtocolImpl.getInstance(org.codecover.instrumentation.java.measurement.CoverageResultLogFile.getInstance("E:\\studia\\pracaInz\\aplikacja\\.metadata\\.plugins\\org.eclipse.core.resources\\.projects\\Strateg-Tests\\org.codecover.eclipse\\coverage-logs\\coverage-log-file.clf"), "dd26a587-cbcb-4512-8709-17ef58373fbe").addObservedContainer(new CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx ());
  }
    public static long[] statements = new long[40];

  public CodeCoverCoverageCounter$2pbogb14nbbie60l81x53fjk0itr2jm15owx () {
    super("pl.edu.agh.strateg.model.analysis.SimpleAnalysisTest.java");
  }

  public static void ping() {/* nothing to do*/}

  public void reset() {
      for (int i = 1; i <= 39; i++) {
        statements[i] = 0L;
      }
  }

  public void serializeAndReset(org.codecover.instrumentation.measurement.CoverageCounterLog log) {
    log.startNamedSection("pl.edu.agh.strateg.model.analysis.SimpleAnalysisTest.java");
      for (int i = 1; i <= 39; i++) {
        if (statements[i] != 0L) {
          log.passCounter("S" + i, statements[i]);
          statements[i] = 0L;
        }
      }
  }
}

