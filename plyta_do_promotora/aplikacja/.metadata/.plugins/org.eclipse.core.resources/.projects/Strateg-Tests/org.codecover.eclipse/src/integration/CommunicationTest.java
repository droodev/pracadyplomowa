/**
	@author drew
 */

package integration;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.List;
import java.util.UUID;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import pl.edu.agh.strateg.backbone.MainModule;
import pl.edu.agh.strateg.backbone.ServiceManager;
import pl.edu.agh.strateg.communication.LogicBus;
import pl.edu.agh.strateg.logic.async.OperationListener;
import pl.edu.agh.strateg.logic.events.AnalysesEvents;
import pl.edu.agh.strateg.logic.events.DescriptorEvents;
import pl.edu.agh.strateg.logic.events.DocumentsEvents;
import pl.edu.agh.strateg.logic.verification.VerificationStatus;
import pl.edu.agh.strateg.model.Document;
import pl.edu.agh.strateg.model.SimpleDocument;
import pl.edu.agh.strateg.model.analysis.Analysis;
import pl.edu.agh.strateg.model.metadata.MapMetadataContainer;
import pl.edu.agh.strateg.model.metadata.MetadataContainer;
import pl.edu.agh.strateg.model.metadata.SimpleMetadatum;
import pl.edu.agh.strateg.model.metadata.description.Descriptor;
import pl.edu.agh.strateg.model.metadata.description.SimpleDescriptor;
import pl.edu.agh.strateg.persistence.PersistenceException;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.mongodb.DB;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;

public class CommunicationTest {

	private Descriptor causeDesc = SimpleDescriptor.getCreator().createString(
			"Cause");

	private SimpleMetadatum causeMeta;
	private SimpleMetadatum accidentMeta;
	private SimpleMetadatum accidentMeta2;

	private Descriptor accidentDesc;

	private Document firstDocument;
	private Document secondDocument;
	private MapMetadataContainer firstContainer;
	private MapMetadataContainer secondContainer;

	private static Document inDatabaseDocument;

	private static final Injector INJECTOR = Guice
			.createInjector(new MainModule());

	private LogicBus bus = INJECTOR.getInstance(LogicBus.class);

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		INJECTOR.getInstance(ServiceManager.class).initService();
		Mongo mongo = new MongoClient();
		DB db = mongo.getDB("TempBase");
		db.dropDatabase();
		mongo.close();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		causeMeta = new SimpleMetadatum(causeDesc, "Because of");

		MetadataContainer container = new MapMetadataContainer(
				Lists.newArrayList(causeMeta));
		accidentDesc = SimpleDescriptor.getCreator()
				.withDescribingMetada(container)
				.createString("Accident category");

		accidentMeta = new SimpleMetadatum(accidentDesc, "Football penalty");
		accidentMeta2 = new SimpleMetadatum(accidentDesc,
				"Unfortunately accident");

		firstContainer = new MapMetadataContainer(
				Lists.newArrayList(accidentMeta));
		secondContainer = new MapMetadataContainer(
				Lists.newArrayList(accidentMeta2));

		firstDocument = new SimpleDocument();
		firstDocument
				.setData("IKonkaSobieSkaczeWesołIKonkaSobieSkaczeWesołIKonkaSobieSkaczeWesołIKonkaSobieSkaczeWesołIKonkaSobieSkaczeWesołIKonkaSobieSkaczeWesołIKonkaSobieSkaczeWesołIKonkaSobieSkaczeWesołIKonkaSobieSkaczeWesołIKonkaSobieSkaczeWesołIKonkaSobieSkaczeWesołIKonkaSobieSkaczeWesołIKonkaSobieSkaczeWesołIKonkaSobieSkaczeWesołIKonkaSobieSkaczeWesołpppppo"
						.getBytes());
		firstDocument.setDescribingMetadata(firstContainer);

		secondDocument = new SimpleDocument();
		secondDocument.setData("someString".getBytes());
		secondDocument.setDescribingMetadata(secondContainer);
	}

	@Test
	public void addingEvent() throws Exception {
		bus.post(new DescriptorEvents.SaveIfNotExistDescriptorEvent(causeDesc,
				new OperationListener<Boolean>() {

					@Override
					public void preOperation() {
						// TODO Auto-generated method stub

					}

					@Override
					public void postVerification(VerificationStatus status) {
						assertThat(status.isOK(), equalTo(true));

					}

					@Override
					public void postOperation(Boolean returnValue) {
						assertThat(returnValue.booleanValue(), is(true));

					}

					@Override
					public void databaseProblemOccured(PersistenceException e) {
						assertTrue(false);
					}
				}));
		bus.post(new DescriptorEvents.SaveIfNotExistDescriptorEvent(
				accidentDesc, new OperationListener<Boolean>() {

					@Override
					public void preOperation() {
						// TODO Auto-generated method stub

					}

					@Override
					public void postVerification(VerificationStatus status) {
						assertThat(status.isOK(), equalTo(true));

					}

					@Override
					public void postOperation(Boolean returnValue) {
						assertThat(returnValue.booleanValue(), is(true));

					}

					@Override
					public void databaseProblemOccured(PersistenceException e) {
						assertTrue(false);
					}
				}));
		bus.post(new DocumentsEvents.SaveIfNotExistsDocumentEvent(
				firstDocument, new OperationListener<Boolean>() {

					@Override
					public void preOperation() {
					}

					@Override
					public void postVerification(VerificationStatus status) {
						assertThat(status.isOK(), equalTo(true));
					}

					@Override
					public void postOperation(Boolean returnValue) {
						assertThat(returnValue.booleanValue(), is(true));
					}

					@Override
					public void databaseProblemOccured(PersistenceException e) {
					}
				}));
		bus.post(new DocumentsEvents.GetDocumentEvent(firstDocument.getUUID(),
				new OperationListener<Optional<Document>>() {

					@Override
					public void preOperation() {
					}

					@Override
					public void postVerification(VerificationStatus status) {
						assertThat(status, equalTo(VerificationStatus.OK));
					}

					@Override
					public void postOperation(Optional<Document> returnValue) {
						assertThat(returnValue.isPresent(), is(true));
						assertThat(returnValue.get(), equalTo(firstDocument));

					}

					@Override
					public void databaseProblemOccured(PersistenceException e) {
						// TODO Auto-generated method stub

					}
				}));
		inDatabaseDocument = firstDocument;
	}

	@Test
	public void analysesEvent() {
		bus.post(new AnalysesEvents.GetAnalysisEvent(UUID.randomUUID(),
				new OperationListener<Optional<Analysis>>() {

					@Override
					public void preOperation() {
					}

					@Override
					public void postVerification(VerificationStatus status) {
						assertThat(status.isOK(), is(true));

					}

					@Override
					public void postOperation(Optional<Analysis> returnValue) {
						assertThat(returnValue,
								equalTo(Optional.<Analysis> absent()));

					}

					@Override
					public void databaseProblemOccured(PersistenceException e) {
					}
				}));
	}

	@Test
	public void deletionEvents() {
		// *****preconditions********, assuming adding works
		// ASSUMING THIS TEST IS RUN AS LAST!
		// *****preconditions********

		bus.post(new DescriptorEvents.DeleteIfExistsDescriptorEvent(causeDesc,
				new OperationListener<Boolean>() {

					@Override
					public void preOperation() {
						// TODO Auto-generated method stub

					}

					@Override
					public void postVerification(VerificationStatus status) {
						assertThat(status.isOK(), equalTo(true));

					}

					@Override
					public void postOperation(Boolean returnValue) {
						assertThat(returnValue.booleanValue(), is(false));

					}

					@Override
					public void databaseProblemOccured(PersistenceException e) {
						assertTrue(false);

					}
				}));
		bus.post(new DescriptorEvents.DeleteIfExistsDescriptorEvent(
				accidentDesc, new OperationListener<Boolean>() {

					@Override
					public void preOperation() {
						// TODO Auto-generated method stub

					}

					@Override
					public void postVerification(VerificationStatus status) {
						assertThat(status.isOK(), equalTo(true));

					}

					@Override
					public void postOperation(Boolean returnValue) {
						assertThat(returnValue.booleanValue(), is(false));
					}

					@Override
					public void databaseProblemOccured(PersistenceException e) {
						assertTrue(false);

					}
				}));

		bus.post(new DocumentsEvents.DeleteIfExistsDocumentEvent(
				inDatabaseDocument, new OperationListener<Boolean>() {

					@Override
					public void preOperation() {
						// TODO Auto-generated method stub

					}

					@Override
					public void postVerification(VerificationStatus status) {
						assertThat(status, equalTo(VerificationStatus.OK));
					}

					@Override
					public void postOperation(Boolean returnValue) {
						assertThat(returnValue, is(true));
					}

					@Override
					public void databaseProblemOccured(PersistenceException e) {
						assertTrue(false);
					}

				}));
		bus.post(new DescriptorEvents.DeleteIfExistsDescriptorEvent(
				accidentDesc, new OperationListener<Boolean>() {

					@Override
					public void preOperation() {
						// TODO Auto-generated method stub

					}

					@Override
					public void postVerification(VerificationStatus status) {
						assertThat(status.isOK(), equalTo(true));

					}

					@Override
					public void postOperation(Boolean returnValue) {
						assertThat(returnValue.booleanValue(), is(true));
					}

					@Override
					public void databaseProblemOccured(PersistenceException e) {
						assertTrue(false);

					}
				}));
		bus.post(new DescriptorEvents.DeleteIfExistsDescriptorEvent(causeDesc,
				new OperationListener<Boolean>() {

					@Override
					public void preOperation() {
						// TODO Auto-generated method stub

					}

					@Override
					public void postVerification(VerificationStatus status) {
						assertThat(status.isOK(), equalTo(true));

					}

					@Override
					public void postOperation(Boolean returnValue) {
						assertThat(returnValue.booleanValue(), is(true));

					}

					@Override
					public void databaseProblemOccured(PersistenceException e) {
						assertTrue(false);

					}
				}));

		bus.post(new DocumentsEvents.GetAllDocumentsEvent(
				new OperationListener<List<Document>>() {

					@Override
					public void preOperation() {
					}

					@Override
					public void postVerification(VerificationStatus status) {
					}

					@Override
					public void postOperation(List<Document> returnValue) {
						assertThat(returnValue.size(), is(0));
					}

					@Override
					public void databaseProblemOccured(PersistenceException e) {
						assertTrue(false);
					}
				}));

		bus.post(new DescriptorEvents.GetAllDescriptorsEvent(
				new OperationListener<List<Descriptor>>() {

					@Override
					public void preOperation() {
					}

					@Override
					public void postVerification(VerificationStatus status) {
					}

					@Override
					public void postOperation(List<Descriptor> returnValue) {
						assertThat(returnValue.size(), is(0));
					}

					@Override
					public void databaseProblemOccured(PersistenceException e) {
						assertTrue(false);
					}
				}));

	}
}
