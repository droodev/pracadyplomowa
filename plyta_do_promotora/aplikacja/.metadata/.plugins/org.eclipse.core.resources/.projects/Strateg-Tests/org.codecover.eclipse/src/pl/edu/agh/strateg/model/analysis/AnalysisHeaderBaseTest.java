/**
	@author drew
 */

package pl.edu.agh.strateg.model.analysis;

import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class AnalysisHeaderBaseTest {
  static {
    CodeCoverCoverageCounter$46mu0e7yyh2jwv0kgpqmhn0a5xaeayo1tjf7smiyqp.ping();
  }


	{ CodeCoverCoverageCounter$46mu0e7yyh2jwv0kgpqmhn0a5xaeayo1tjf7smiyqp.statements[1]++; }
OpportunityAnalysisHeader firstLower = new OpportunityAnalysisHeader(
			"First", 4);
	{ CodeCoverCoverageCounter$46mu0e7yyh2jwv0kgpqmhn0a5xaeayo1tjf7smiyqp.statements[2]++; }
OpportunityAnalysisHeader firstUpper = new OpportunityAnalysisHeader(
			"First", 9);

	{ CodeCoverCoverageCounter$46mu0e7yyh2jwv0kgpqmhn0a5xaeayo1tjf7smiyqp.statements[3]++; }
OpportunityAnalysisHeader second = new OpportunityAnalysisHeader("Second",
			6);

	{ CodeCoverCoverageCounter$46mu0e7yyh2jwv0kgpqmhn0a5xaeayo1tjf7smiyqp.statements[4]++; }
ThreatAnalysisHeader threat = new ThreatAnalysisHeader("Header", 1);

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void equalityAndHashTest() {
		CodeCoverCoverageCounter$46mu0e7yyh2jwv0kgpqmhn0a5xaeayo1tjf7smiyqp.statements[5]++;
Assert.assertThat(firstLower, CoreMatchers.equalTo(firstUpper));
		CodeCoverCoverageCounter$46mu0e7yyh2jwv0kgpqmhn0a5xaeayo1tjf7smiyqp.statements[6]++;
Assert.assertThat(firstLower.hashCode(),
				CoreMatchers.equalTo(firstUpper.hashCode()));
	}

	@Test
	public void comparingOneTypeTest() {
		CodeCoverCoverageCounter$46mu0e7yyh2jwv0kgpqmhn0a5xaeayo1tjf7smiyqp.statements[7]++;
Assert.assertThat(firstLower.compareTo(second) < 0,
				CoreMatchers.equalTo(true));
		CodeCoverCoverageCounter$46mu0e7yyh2jwv0kgpqmhn0a5xaeayo1tjf7smiyqp.statements[8]++;
Assert.assertThat(firstUpper.compareTo(second) > 0,
				CoreMatchers.equalTo(true));
		CodeCoverCoverageCounter$46mu0e7yyh2jwv0kgpqmhn0a5xaeayo1tjf7smiyqp.statements[9]++;
Assert.assertThat(firstLower.compareTo(firstUpper) == 0,
				CoreMatchers.equalTo(true));
	}

	@Test
	public void comparingTwoTypesTest() {
		CodeCoverCoverageCounter$46mu0e7yyh2jwv0kgpqmhn0a5xaeayo1tjf7smiyqp.statements[10]++;
Assert.assertThat(firstLower.compareTo(threat) < 0,
				CoreMatchers.equalTo(true));
	}
}

class CodeCoverCoverageCounter$46mu0e7yyh2jwv0kgpqmhn0a5xaeayo1tjf7smiyqp extends org.codecover.instrumentation.java.measurement.CounterContainer {

  static {
    org.codecover.instrumentation.java.measurement.ProtocolImpl.getInstance(org.codecover.instrumentation.java.measurement.CoverageResultLogFile.getInstance("E:\\studia\\pracaInz\\aplikacja\\.metadata\\.plugins\\org.eclipse.core.resources\\.projects\\Strateg-Tests\\org.codecover.eclipse\\coverage-logs\\coverage-log-file.clf"), "dd26a587-cbcb-4512-8709-17ef58373fbe").addObservedContainer(new CodeCoverCoverageCounter$46mu0e7yyh2jwv0kgpqmhn0a5xaeayo1tjf7smiyqp ());
  }
    public static long[] statements = new long[11];

  public CodeCoverCoverageCounter$46mu0e7yyh2jwv0kgpqmhn0a5xaeayo1tjf7smiyqp () {
    super("pl.edu.agh.strateg.model.analysis.AnalysisHeaderBaseTest.java");
  }

  public static void ping() {/* nothing to do*/}

  public void reset() {
      for (int i = 1; i <= 10; i++) {
        statements[i] = 0L;
      }
  }

  public void serializeAndReset(org.codecover.instrumentation.measurement.CoverageCounterLog log) {
    log.startNamedSection("pl.edu.agh.strateg.model.analysis.AnalysisHeaderBaseTest.java");
      for (int i = 1; i <= 10; i++) {
        if (statements[i] != 0L) {
          log.passCounter("S" + i, statements[i]);
          statements[i] = 0L;
        }
      }
  }
}

