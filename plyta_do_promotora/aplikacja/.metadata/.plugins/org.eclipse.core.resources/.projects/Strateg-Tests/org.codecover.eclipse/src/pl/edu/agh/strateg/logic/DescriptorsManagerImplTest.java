/**
	@author drew
 */

package pl.edu.agh.strateg.logic;

import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import pl.edu.agh.strateg.logic.async.OperationListener;
import pl.edu.agh.strateg.logic.verification.VerificationStatus;
import pl.edu.agh.strateg.logic.verification.Verifier;
import pl.edu.agh.strateg.model.metadata.description.Descriptor;
import pl.edu.agh.strateg.persistence.AnalysesDAO;
import pl.edu.agh.strateg.persistence.DescriptorsDAO;
import pl.edu.agh.strateg.persistence.DocumentsDAO;
import pl.edu.agh.strateg.persistence.PersistenceException;

import com.google.common.base.Optional;

@RunWith(MockitoJUnitRunner.class)
public class DescriptorsManagerImplTest {

	private static final String GOT_NAME = "Name";

	@Mock
	private OperationListener<Boolean> listenerString;

	@Mock
	private OperationListener<Boolean> listenerDescriptor;

	@Mock
	private OperationListener<Optional<Descriptor>> listenerOptDescriptor;

	@Mock
	private Descriptor descriptor;

	@Mock
	private Verifier verifier;

	@Mock
	private DescriptorsDAO descriptorsDAO;

	@Mock
	private DocumentsDAO documentsDAO;

	@Mock
	private AnalysesDAO analysesDAO;

	@InjectMocks
	private DescriptorsManagerImpl testedManager;

	@Before
	public void setUp() throws Exception {
		when(verifier.verifyDescriptor(descriptor, Optional.absent()))
				.thenReturn(VerificationStatus.OK);
		when(descriptor.getName()).thenReturn("Name");
	}

	@Test
	public void testAddDescriptor() throws PersistenceException {
		when(descriptorsDAO.saveIfNotExists(descriptor)).thenReturn(
				new Boolean(false));
		testedManager.addEntity(descriptor, listenerString);
		verify(descriptorsDAO).saveIfNotExists(descriptor);
		verifyListener(listenerString, false);
	}

	@Test
	public void testDeleteDescriptor() throws PersistenceException {
		when(descriptorsDAO.deleteIfExists(descriptor)).thenReturn(
				new Boolean(false));
		testedManager.deleteEntity(descriptor, listenerDescriptor);
		verify(descriptorsDAO).deleteIfExists(descriptor);
		verifyListener(listenerDescriptor, false);
	}

	@Test
	public void testUpdateDescriptor() throws PersistenceException {
		when(descriptorsDAO.updateIfExists(descriptor)).thenReturn(
				new Boolean(false));
		testedManager.updateEntity(descriptor, listenerDescriptor);
		verify(descriptorsDAO).updateIfExists(descriptor);
		verifyListener(listenerDescriptor, false);
	}

	@Test
	public void testGetDescriptor() throws PersistenceException {
		when(descriptorsDAO.get(GOT_NAME)).thenReturn(
				Optional.<Descriptor> absent());
		testedManager.getEntity(GOT_NAME, listenerOptDescriptor);
		verify(descriptorsDAO).get(GOT_NAME);
		verifyListener(listenerOptDescriptor, Optional.absent());
	}

	@SuppressWarnings("unchecked")
	private void verifyListener(OperationListener<?> listener,
			Object postOperation) {
		verify(listener).preOperation();
		verify(listener).postVerification(VerificationStatus.OK);
		verify((OperationListener<Object>) listener).postOperation(
				postOperation);
	}

}
