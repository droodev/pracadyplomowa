/**
	@author drew
 */

package pl.edu.agh.strateg.logic;

import javax.inject.Inject;

import org.apache.log4j.Logger;

import pl.edu.agh.strateg.communication.LogicBus;
import pl.edu.agh.strateg.helpers.ExtendedPreconditions;
import pl.edu.agh.strateg.helpers.annotations.ServiceShutdown;
import pl.edu.agh.strateg.helpers.annotations.ServiceStart;
import pl.edu.agh.strateg.logic.events.AnalysesEvents.DeleteIfExistsAnalysisEvent;
import pl.edu.agh.strateg.logic.events.AnalysesEvents.GetAllAnalysesEvent;
import pl.edu.agh.strateg.logic.events.AnalysesEvents.GetAnalysisEvent;
import pl.edu.agh.strateg.logic.events.AnalysesEvents.QueryAnalysesByMetadataEvent;
import pl.edu.agh.strateg.logic.events.AnalysesEvents.SaveIfNotExistsAnalysisEvent;
import pl.edu.agh.strateg.logic.events.AnalysesEvents.UpdateIfExistsAnalysisEvent;
import pl.edu.agh.strateg.logic.events.DescriptorEvents.DeleteIfExistsDescriptorEvent;
import pl.edu.agh.strateg.logic.events.DescriptorEvents.GetAllDescriptorsEvent;
import pl.edu.agh.strateg.logic.events.DescriptorEvents.GetDescriptorEvent;
import pl.edu.agh.strateg.logic.events.DescriptorEvents.QueryDescriptorsByMetadataEvent;
import pl.edu.agh.strateg.logic.events.DescriptorEvents.SaveIfNotExistDescriptorEvent;
import pl.edu.agh.strateg.logic.events.DescriptorEvents.UpdateIfExistsDescriptorEvent;
import pl.edu.agh.strateg.logic.events.DocumentsEvents.DeleteIfExistsDocumentEvent;
import pl.edu.agh.strateg.logic.events.DocumentsEvents.GetAllDocumentsEvent;
import pl.edu.agh.strateg.logic.events.DocumentsEvents.GetDocumentEvent;
import pl.edu.agh.strateg.logic.events.DocumentsEvents.QueryDocumentsByMetadataEvent;
import pl.edu.agh.strateg.logic.events.DocumentsEvents.SaveIfNotExistsDocumentEvent;
import pl.edu.agh.strateg.logic.events.DocumentsEvents.UpdateIfExistsDocumentEvent;

import com.google.common.eventbus.Subscribe;

public class BackendProvider {
	private static Logger LOG = Logger.getLogger(Thread.currentThread()
			.getStackTrace()[1].getClassName());
	private DocumentsManager dataManager;
	private DescriptorsManager descriptorsManager;
	private AnalysesManager analysesManager;
	private LogicBus bus;

	@Inject
	public BackendProvider(DocumentsManager dataManager,
			DescriptorsManager descriptorsManager,
			AnalysesManager analysesManager, LogicBus bus) {
		ExtendedPreconditions.checkArgumentsNotNull(dataManager,
				descriptorsManager);
		this.dataManager = dataManager;
		this.descriptorsManager = descriptorsManager;
		this.analysesManager = analysesManager;
		this.bus = bus;
	}

	@ServiceStart
	public void start() {
		LOG.debug(String.format("Starting service"));
		bus.register(this);
	}

	@ServiceShutdown
	public void shutdown() {
		LOG.debug(String.format("Shutting down service"));
	}

	@Subscribe
	public void addDocument(SaveIfNotExistsDocumentEvent event) {
		LOG.debug(String.format("AddDataEvent dispatching"));
		dataManager.addEntity(event.getOperationData().get(),
				event.getListener());
	}

	@Subscribe
	public void deleteDocument(DeleteIfExistsDocumentEvent event) {
		dataManager.deleteEntity(event.getOperationData().get(),
				event.getListener());
	}

	@Subscribe
	public void updateDocument(UpdateIfExistsDocumentEvent event) {
		dataManager.updateEntity(event.getOperationData().get(),
				event.getListener());
	}

	@Subscribe
	public void getDocument(GetDocumentEvent event) {
		dataManager.getEntity(event.getOperationData().get(),
				event.getListener());
	}

	@Subscribe
	public void getAllDocuments(GetAllDocumentsEvent event) {
		System.out.println("getting docs");
		dataManager.getAllEntities(event.getListener());
	}

	@Subscribe
	public void queryDocumentsByMetadatum(QueryDocumentsByMetadataEvent event) {
		dataManager.queryByDescriptors(event.getOperationData().get(),
				event.getListener());
	}

	@Subscribe
	public void addDescriptor(SaveIfNotExistDescriptorEvent event) {
		descriptorsManager.addEntity(event.getOperationData().get(),
				event.getListener());
	}

	@Subscribe
	public void deleteDescriptor(DeleteIfExistsDescriptorEvent event) {
		descriptorsManager.deleteEntity(event.getOperationData().get(),
				event.getListener());
	}

	@Subscribe
	public void updateDescriptor(UpdateIfExistsDescriptorEvent event) {
		descriptorsManager.updateEntity(event.getOperationData().get(),
				event.getListener());
	}

	@Subscribe
	public void getDescriptor(GetDescriptorEvent event) {
		descriptorsManager.getEntity(event.getOperationData().get(),
				event.getListener());
	}

	@Subscribe
	public void getAllDescriptors(GetAllDescriptorsEvent event) {
		descriptorsManager.getAllEntities(event.getListener());
	}

	@Subscribe
	public void queryDescriptorsByMetadatum(
			QueryDescriptorsByMetadataEvent event) {
		descriptorsManager.queryByDescriptors(event.getOperationData().get(),
				event.getListener());
	}

	@Subscribe
	public void saveAnalysis(SaveIfNotExistsAnalysisEvent event) {
		analysesManager.addEntity(event.getOperationData().get(),
				event.getListener());
	}

	@Subscribe
	public void deleteAnalysis(DeleteIfExistsAnalysisEvent event) {
		analysesManager.deleteEntity(event.getOperationData().get(),
				event.getListener());
	}

	@Subscribe
	public void updateAnalysis(UpdateIfExistsAnalysisEvent event) {
		analysesManager.updateEntity(event.getOperationData().get(),
				event.getListener());
	}

	@Subscribe
	public void getAnalysis(GetAnalysisEvent event) {
		analysesManager.getEntity(event.getOperationData().get(),
				event.getListener());
	}

	@Subscribe
	public void getAllAnalyses(GetAllAnalysesEvent event) {
		analysesManager.getAllEntities(event.getListener());
	}

	@Subscribe
	public void queryAnalysesByMetadatum(QueryAnalysesByMetadataEvent event) {
		analysesManager.queryByDescriptors(event.getOperationData().get(),
				event.getListener());
	}
}
