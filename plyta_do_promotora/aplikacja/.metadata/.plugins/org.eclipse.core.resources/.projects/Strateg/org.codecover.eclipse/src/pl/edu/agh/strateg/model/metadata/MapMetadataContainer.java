/**
 * @author drew
 */
package pl.edu.agh.strateg.model.metadata;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import pl.edu.agh.strateg.model.metadata.description.Descriptor;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.gwt.thirdparty.guava.common.collect.ImmutableList;

/*Requires the Description to has equal() and hash() overriden*/
public class MapMetadataContainer implements MetadataContainer {

	private final Map<Descriptor, Metadatum> metadata = new HashMap<Descriptor, Metadatum>();

	public MapMetadataContainer() {
	}

	public MapMetadataContainer(Collection<? extends Metadatum> metadata) {
		Preconditions.checkArgument(metadata != null,
				"Cannot create with null list");
		for (Metadatum metadatum : metadata) {
			addMetadatum(metadatum);
		}

	}

	@Override
	public void addMetadatum(Metadatum metadatum) {
		Preconditions.checkArgument(metadatum != null,
				"Cannot add null metadatum");
		if (metadata.containsKey(metadatum.getDescription())) {
			String message = "Metadata collection contains metadata with descriptor ' "
					+ metadatum.getDescription() + "'";
			throw new IllegalArgumentException(message);
		}
		metadata.put(metadatum.getDescription(), metadatum);

	}

	@Override
	public <T> void removeMetadatum(Descriptor descriptor) {
		Preconditions.checkArgument(descriptor != null,
				"Cannot remove metadatum with null descriptor");
		if (metadata.remove(descriptor) == null) {
			throw new IllegalArgumentException(
					"Cannot remove metadatum which wasn't in container");
		}
	}

	@Override
	public <T> Optional<Metadatum> getMetadatum(Descriptor descriptor) {
		Preconditions.checkArgument(descriptor != null,
				"Cannot get metadatum with null descriptor");
		Metadatum o = metadata.get(descriptor);
		if (o == null) {
			return Optional.absent();
		}
		try {
			descriptor.getValueType().cast(o.getValue());
		} catch (ClassCastException e) {
			throw new IllegalArgumentException("Desriptor value type differs");
		}
		Metadatum toReturn = (Metadatum) o;
		return Optional.of(toReturn);
	}

	@Override
	public void updateMetadatum(Metadatum alteredMetadatum) {
		Preconditions.checkArgument(alteredMetadatum != null,
				"You cannot add null metadatum");
		Descriptor alteredDescriptor = alteredMetadatum.getDescription();
		if (metadata.containsKey(alteredDescriptor) == false) {
			throw new IllegalArgumentException(
					"You cannot update metadatum which is not in collection");
		}
		metadata.put(alteredDescriptor, alteredMetadatum);
	}

	@Override
	public int getSize() {
		return metadata.size();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Collection<Metadatum> getAll() {
		return new ImmutableList.Builder().addAll(metadata.values()).build();
	}

	@Override
	public boolean contains(Descriptor descriptor) {
		return getMetadatum(descriptor).isPresent();
	}
}
