/**
 * @author drew
 */
package pl.edu.agh.strateg.model.metadata;

import pl.edu.agh.strateg.helpers.ExtendedPreconditions;
import pl.edu.agh.strateg.model.metadata.description.Descriptor;

public final class UnmodifiableMetadatum extends SimpleMetadatum {

	private Metadatum underlyingMetaDatum;

	private UnmodifiableMetadatum(Metadatum underlyingMetaDatum) {
		ExtendedPreconditions.checkArgumentNotNull(underlyingMetaDatum);
		this.underlyingMetaDatum = underlyingMetaDatum;
	}

	public static UnmodifiableMetadatum create(Metadatum underlyingMetaDatum) {
		return new UnmodifiableMetadatum(underlyingMetaDatum);
	}

	@Override
	public Object getValue() {
		return underlyingMetaDatum.getValue();
	}

	@Override
	public void setValue(Object newValue) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Descriptor getDescription() {
		return underlyingMetaDatum.getDescription();
	}

}
