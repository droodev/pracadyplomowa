/**
	@author drew
 */

package pl.edu.agh.strateg.persistence.mongo;

import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import pl.edu.agh.strateg.model.analysis.Analysis;
import pl.edu.agh.strateg.persistence.AnalysesDAO;
import pl.edu.agh.strateg.persistence.PersistenceException;
import pl.edu.agh.strateg.persistence.mongo.proxy.AnalysisProxy;
import pl.edu.agh.strateg.persistence.mongo.querying.AnalysesQueryProducer;
import pl.edu.agh.strateg.persistence.querying.MetadatumQuery;

import com.google.code.morphia.dao.BasicDAO;
import com.google.code.morphia.query.QueryResults;

public class MAnalysesDAO extends
		MAdaptingDAOBase<UUID, Analysis, AnalysisProxy> implements AnalysesDAO {

	@Inject
	public MAnalysesDAO(BasicDAO<AnalysisProxy, UUID> forProxyDAO) {
		super(forProxyDAO);
	}

	@Override
	public boolean exists(Analysis modelEntity) throws PersistenceException {
		try {
			return forProxyDAO.get(modelEntity.getUUID()) != null;
		} catch (RuntimeException e) {
			throw new PersistenceException(e);
		}
	}

	@Override
	public AnalysisProxy getProxy(Analysis modelEntity) {
		return AnalysisProxy.proxify(modelEntity);
	}

	@Override
	public Analysis deproxify(AnalysisProxy proxyEntity) {
		return proxyEntity.deproxify();
	}

	@Override
	public List<Analysis> queryByMetadata(MetadatumQuery query)
			throws PersistenceException {
		try {
			AnalysesQueryProducer queryBuilder = new AnalysesQueryProducer(
					forProxyDAO.createQuery());
			query.accept(queryBuilder);
			QueryResults<AnalysisProxy> foundQueryResults = forProxyDAO
					.find(queryBuilder.getQuery());
			return transformQueryResult(foundQueryResults);
		} catch (RuntimeException e) {
			throw new PersistenceException(e);
		}
	}

}
