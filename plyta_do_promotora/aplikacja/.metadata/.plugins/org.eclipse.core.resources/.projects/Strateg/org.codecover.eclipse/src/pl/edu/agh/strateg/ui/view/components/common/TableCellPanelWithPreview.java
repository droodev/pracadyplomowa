package pl.edu.agh.strateg.ui.view.components.common;

import pl.edu.agh.strateg.ui.controller.component.documents.SearchResultsController;
import pl.edu.agh.strateg.ui.model.common.DataModel;
import pl.edu.agh.strateg.ui.view.views.DocumentsView;
import pl.edu.agh.strateg.utils.Icons;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;

public class TableCellPanelWithPreview extends TableCellPanel{

	public TableCellPanelWithPreview(DataModel model, SearchResultsController controller) {
		super(model, controller);
	}
	
	protected Button createButton() {
		
		Button button = new Button(DocumentsView.PODGLAD_DOKUMENTU_BUTTON_CAPTION);
		button.setIcon(new ThemeResource(Icons.PREVIEW));
		
		button.setData(model);
		button.addClickListener(controller);
		
		return button;
	}


}
