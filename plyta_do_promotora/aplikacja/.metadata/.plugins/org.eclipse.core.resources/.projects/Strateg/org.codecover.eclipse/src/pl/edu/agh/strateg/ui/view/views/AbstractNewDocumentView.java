package pl.edu.agh.strateg.ui.view.views;

import pl.edu.agh.strateg.ui.controller.main.AbstractNewDocumentController;
import pl.edu.agh.strateg.ui.view.components.common.TitlePanel;
import pl.edu.agh.strateg.ui.view.components.common.ChooseMetadataPanel;
import pl.edu.agh.strateg.ui.view.components.common.ChosenMetadataPanel;
import pl.edu.agh.strateg.utils.Icons;
import pl.edu.agh.strateg.utils.InputStreamList;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.VerticalLayout;

public abstract class AbstractNewDocumentView extends VerticalLayout implements View {
	
	public static final String STRONA_GLOWNA_BUTTON_CAPTION = "Strona główna";
	public String POTWIERDZ_BUTTON_CAPTION;
	public String ANULUJ_BUTTON_CAPTION;
	
	protected String PAGE_CAPTION;
	protected String NIE_UDALO_SIE_DODAC_CAPTION;
	protected String ICON;
	
	private static final String DODANO_POPRAWNIE_CAPTION = "Dodano poprawnie";
	private static final String NOT_VALID_VALUES_WARNING = "Wprowadzone wartości nie są poprawne";
	private static final String DATABASE_PROBLEM = "Problem przy dostępie do bazy";
	private static final String WYBIERZ_METADANE_CAPTION = "Wybierz metadane";
	private static final String WYBRANE_METADANE_CAPTION = "Wybrane metadane";
	
	
	private Button stronaGlownaButton;
	protected Button potwierdzButton;
	private Button anulujButton;
	
	private ChooseMetadataPanel wybierzMetadanePanel;
	private ChosenMetadataPanel wybraneMetadanePanel;
	private TitlePanel titlePanel;
	
	protected VerticalLayout customLayout;
	
	protected AbstractNewDocumentController controller;
	
	@Override
	public void enter(ViewChangeEvent event) {
		controller.fillChooseMetadataPanel();
		InputStreamList.getInstance().clear();
	}
	
	public AbstractNewDocumentView() {
		setCaptions();
		initComponents();
		initLayouts();
	}
	
	public void successfullyAdded() {
		resetContent();
		Notification.show(DODANO_POPRAWNIE_CAPTION, Type.TRAY_NOTIFICATION);
	}
	
	private void resetContent() {
		wybierzMetadanePanel.clear();
		clearWybraneMetadanePanel();
		controller.fillChooseMetadataPanel();
		
	}

	private void clearWybraneMetadanePanel() {
		wybraneMetadanePanel.removeAllFilters();
	}
	
	protected abstract void setCaptions();
	
	protected void initComponents() {
		stronaGlownaButton = new Button(STRONA_GLOWNA_BUTTON_CAPTION);
		potwierdzButton = new Button(POTWIERDZ_BUTTON_CAPTION);
		anulujButton = new Button(ANULUJ_BUTTON_CAPTION);
		
		stronaGlownaButton.setIcon(new ThemeResource(Icons.MAIN));
		potwierdzButton.setIcon(new ThemeResource(Icons.CONFIRM));
		anulujButton.setIcon(new ThemeResource(Icons.CANCEL));
		
		wybierzMetadanePanel = new ChooseMetadataPanel(WYBIERZ_METADANE_CAPTION);
		wybraneMetadanePanel = new ChosenMetadataPanel(WYBRANE_METADANE_CAPTION, true);
		titlePanel = new TitlePanel(PAGE_CAPTION, ICON, true);
		
		customLayout = new VerticalLayout();
	}
	
	protected void initLayouts() {
		VerticalLayout mainLayout = new VerticalLayout();
		
		HorizontalLayout upperLayout = new HorizontalLayout();
		VerticalLayout bottomLayout = new VerticalLayout();
		
		upperLayout.setSizeFull();
		bottomLayout.setSizeFull();
		mainLayout.setSizeFull();
		
		upperLayout.setSpacing(true);
		bottomLayout.setSpacing(true);
		
		mainLayout.setMargin(true);
		mainLayout.setSpacing(true);
		
		HorizontalLayout buttonLayout = new HorizontalLayout();
		buttonLayout.addComponent(potwierdzButton);
		buttonLayout.addComponent(anulujButton);
		
		upperLayout.addComponent(stronaGlownaButton);
		upperLayout.addComponent(wybierzMetadanePanel);
		upperLayout.addComponent(buttonLayout);
		
		upperLayout.setExpandRatio(stronaGlownaButton, 0.25f);
		upperLayout.setExpandRatio(wybierzMetadanePanel, 0.5f);
		upperLayout.setExpandRatio(buttonLayout, 0.25f);
		
		upperLayout.setComponentAlignment(stronaGlownaButton, Alignment.TOP_LEFT);
		upperLayout.setComponentAlignment(buttonLayout, Alignment.TOP_RIGHT);
		
		buttonLayout.setSpacing(true);

		bottomLayout.addComponent(customLayout);
		bottomLayout.addComponent(wybraneMetadanePanel);
		
		bottomLayout.setComponentAlignment(customLayout, Alignment.BOTTOM_LEFT);
		
		mainLayout.addComponent(titlePanel);
		mainLayout.addComponent(upperLayout);
		mainLayout.addComponent(bottomLayout);
		
		addComponent(mainLayout);
		
	}
	
	public void setControllerToAllComponents(
			AbstractNewDocumentController nowyDokumentController) {
		this.controller = nowyDokumentController;
		nowyDokumentController.setNewDocumentView(this);
		wybierzMetadanePanel.setMainController(controller);
		wybraneMetadanePanel.setMainController(controller);
		setListenerToButtons();
		controller.addNavigator(titlePanel);
	}
	
	private void setListenerToButtons() {
		stronaGlownaButton.addClickListener(controller);
		potwierdzButton.addClickListener(controller);
		anulujButton.addClickListener(controller);
		
	}
	
	public void addingFailed() {
		Notification.show(NIE_UDALO_SIE_DODAC_CAPTION, Type.ERROR_MESSAGE);
		resetContent();
	}

	public void displayNotValidValuesWarning() {
		Notification.show(NOT_VALID_VALUES_WARNING, Type.ERROR_MESSAGE);
		
	}
	
	public void databaseProblemOccured() {
		Notification.show(DATABASE_PROBLEM, Type.ERROR_MESSAGE);
		resetContent();
	}

}
