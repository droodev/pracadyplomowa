package pl.edu.agh.strateg.security;


public class Roles {
	
	public static String SEE_DESCRIPTORS = "see_descriptors";
	public static String ADD_DESCRIPTOR = "add_descriptor";
	public static String EDIT_DESCRIPTOR = "edit_descriptor";
	public static String DELETE_DESCRIPTOR = "delete_descriptor";
	
	public static String SEE_DOCUMENTS = "see_documents";
	public static String ADD_DOCUMENT = "add_document";
	public static String EDIT_DOCUMENT = "edit_document";
	public static String DELETE_DOCUMENT = "delete_document";
	
	public static String SEE_ANALYSES = "see_analyses";
	public static String ADD_ANALYSIS = "add_analysis";
	public static String EDIT_ANALYSIS = "edit_analysis";
	public static String DELETE_ANALYSIS = "delete_analysis";
	
	public static String SEE_REPORTS = "see_reports";
	public static String CREATE_REPORT = "create_report";
	
}
