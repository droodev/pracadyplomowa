package pl.edu.agh.strateg.ui.view.views;

import pl.edu.agh.strateg.security.Roles;
import pl.edu.agh.strateg.ui.controller.main.DescriptorsController;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;
import pl.edu.agh.strateg.ui.view.components.common.ConfirmWindow;
import pl.edu.agh.strateg.ui.view.components.common.TitlePanel;
import pl.edu.agh.strateg.ui.view.components.common.ChooseMetadataPanel;
import pl.edu.agh.strateg.ui.view.components.common.access_controlled.AccessControlledButton;
import pl.edu.agh.strateg.ui.view.components.metadata.EditDescriptorWindow;
import pl.edu.agh.strateg.ui.view.components.metadata.DescriptorDetailsPanel;
import pl.edu.agh.strateg.utils.Icons;
import pl.edu.agh.strateg.utils.InputStreamList;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.VerticalLayout;

public class DescriptorsView extends VerticalLayout implements View{
	
	public static final String STRONA_GLOWNA_BUTTON_CAPTION = "Strona główna";
	public static final String NOWA_METADANA_BUTTON_CAPTION = "Nowa metadana";
	public static final String EDYTUJ_BUTTON_CAPTION = "Edytuj";
	public static final String USUN_BUTTON_CAPTION = "Usuń";
	
	private static final String NIE_UDALO_SIE_ZMODYFIKOWAC_METADANEJ = "Nie udało się zmodyfikować";
	private static final String NIE_UDALO_SIE_USUNAC_METADANEJ = "Nie udało się usunąć";
	private static final String DATABASE_PROBLEM = "Problem przy dostępie do bazy";
	private static final String ZMODYFIKOWANO_METADANA = "Zmodyfikowano poprawnie";
	private static final String USUNIETO_METADANA = "Usunięto";
	private static final String PAGE_CAPTION = "METADANE";
	private static final String NAZWA_LABEL_CAPTION = "Nazwa";
	private static final String PYTANIE_DO_POTWIERDZENIA = "Czy na pewno chcesz usunąć?";
	
	private Button stronaGlownaButton;
	private Button nowaMetadanaButton;
	private Button edytujButton;
	private Button usunButton;
	
	private Label nazwaLabel;
	private Label nazwaValueLabel;
	
	private ChooseMetadataPanel wybierzMetadanePanel;
	private DescriptorDetailsPanel szczegolyMetadanejPanel;
	private TitlePanel titlePanel;
	
	DescriptorsController controller;
	
	public DescriptorsView() {
		initComponents();
		initLayouts();
	}
	
	public void setControllerToAllComponents(DescriptorsController controller) {
		this.controller = controller;
		controller.setDescriptorsView(this);
		wybierzMetadanePanel.setMainController(controller);
		szczegolyMetadanejPanel.setMainController(controller);
		setListenerToButtons();
		controller.addNavigator(titlePanel);
	}

	private void fillPanel(DescriptorsController controller) {
		controller.fillChooseFiltersPanel();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		fillPanel(controller);
		InputStreamList.getInstance().clear();
	}
	
	public void fillName(String nazwa) {
		nazwaValueLabel.setCaption(nazwa);
	}
	
	public void displayEditWindow(MetadatumModel metadana) {
		EditDescriptorWindow edytujMetadanaWindow = new EditDescriptorWindow(metadana);
		edytujMetadanaWindow.setMainController(controller);
		getUI().addWindow(edytujMetadanaWindow);
		edytujMetadanaWindow.setModal(true);
	}
	
	
	private void initComponents() {
		stronaGlownaButton = new Button(STRONA_GLOWNA_BUTTON_CAPTION);
		nowaMetadanaButton = new AccessControlledButton(Roles.ADD_DESCRIPTOR);
		
		edytujButton = new AccessControlledButton(Roles.EDIT_DESCRIPTOR);
		usunButton = new AccessControlledButton(Roles.DELETE_DESCRIPTOR);
		
		edytujButton.setCaption(EDYTUJ_BUTTON_CAPTION);
		usunButton.setCaption(USUN_BUTTON_CAPTION);
		nowaMetadanaButton.setCaption(NOWA_METADANA_BUTTON_CAPTION);
		
		stronaGlownaButton.setIcon(new ThemeResource(Icons.MAIN));
		nowaMetadanaButton.setIcon(new ThemeResource(Icons.NEW));
		edytujButton.setIcon(new ThemeResource(Icons.EDIT));
		usunButton.setIcon(new ThemeResource(Icons.DELETE));
		
		usunButton.setEnabled(false);
		edytujButton.setEnabled(false);
		nowaMetadanaButton.setEnabled(true);
		
		nazwaLabel = new Label("<b>" + NAZWA_LABEL_CAPTION + "</b>", ContentMode.HTML);
		nazwaValueLabel = new Label();
		
		wybierzMetadanePanel = new ChooseMetadataPanel();
		szczegolyMetadanejPanel = new DescriptorDetailsPanel();
		titlePanel = new TitlePanel(PAGE_CAPTION, Icons.TITLE_DESCRIPTORS, true);
		
		wybierzMetadanePanel.setSizeFull();
		szczegolyMetadanejPanel.setSizeFull();
		
	}

	private void initLayouts() {
		VerticalLayout mainLayout = new VerticalLayout();
		HorizontalLayout upperLayout = new HorizontalLayout();
		VerticalLayout bottomLayout = new VerticalLayout();
		
		HorizontalLayout buttonLayout = new HorizontalLayout();
		HorizontalLayout nazwaLayout = new HorizontalLayout();
		
		mainLayout.setSizeFull();
		mainLayout.setSpacing(true);
		mainLayout.setMargin(true);
		
		upperLayout.setSizeFull();
		upperLayout.setSpacing(true);
		
		bottomLayout.setSizeFull();
		bottomLayout.setSpacing(true);
		
		buttonLayout.setSpacing(true);
		nazwaLayout.setSpacing(true);
		
		buttonLayout.addComponent(edytujButton);
		buttonLayout.addComponent(usunButton);
		
		nazwaLayout.addComponent(nazwaLabel);
		nazwaLayout.addComponent(nazwaValueLabel);
		
		upperLayout.addComponent(stronaGlownaButton);
		upperLayout.addComponent(wybierzMetadanePanel);
		upperLayout.addComponent(nowaMetadanaButton);
		
		bottomLayout.addComponent(buttonLayout);
		bottomLayout.addComponent(nazwaLayout);
		bottomLayout.addComponent(szczegolyMetadanejPanel);
		
		upperLayout.setComponentAlignment(stronaGlownaButton, Alignment.TOP_LEFT);
		upperLayout.setComponentAlignment(nowaMetadanaButton, Alignment.TOP_RIGHT);
		
		upperLayout.setExpandRatio(stronaGlownaButton, 0.25f);
		upperLayout.setExpandRatio(wybierzMetadanePanel, 0.5f);
		upperLayout.setExpandRatio(nowaMetadanaButton, 0.25f);
		
		mainLayout.addComponent(titlePanel);
		mainLayout.addComponent(upperLayout);
		mainLayout.addComponent(bottomLayout);
		addComponent(mainLayout);
		
	}

	private void setListenerToButtons() {

		stronaGlownaButton.addClickListener(controller);
		nowaMetadanaButton.addClickListener(controller);
		edytujButton.addClickListener(controller);
		usunButton.addClickListener(controller);
		
	}
	
	public void displayConfirmWindow() {
		ConfirmWindow potwierdzWindow = new ConfirmWindow(controller, PYTANIE_DO_POTWIERDZENIA);
		getUI().addWindow(potwierdzWindow);
		potwierdzWindow.setModal(true);
	}
	
	public void descriptorUpdatedSuccessfully() {
		Notification.show(ZMODYFIKOWANO_METADANA, Type.TRAY_NOTIFICATION);
		
	}
	
	public void descriptorDeletedSuccessfully() {
		Notification.show(USUNIETO_METADANA, Type.TRAY_NOTIFICATION);
		
	}

	public void descriptorUpdatingFailed() {
		Notification.show(NIE_UDALO_SIE_ZMODYFIKOWAC_METADANEJ, Type.ERROR_MESSAGE);
		
	}
	
	public void descriptorDeletingFailed() {
		Notification.show(NIE_UDALO_SIE_USUNAC_METADANEJ, Type.ERROR_MESSAGE);
		
	}
	
	public void modificationCancelled() {
		
		
	}
	
	
	public void databaseProblemOccured() {
		Notification.show(DATABASE_PROBLEM, Type.ERROR_MESSAGE);
	}

	public void enableButtons() {
		edytujButton.setEnabled(true);
		usunButton.setEnabled(true);
	}
	
	public void reset() {
		disableButtons();
		nazwaValueLabel.setCaption("");
	}
	
	private void disableButtons() {
		edytujButton.setEnabled(false);
		usunButton.setEnabled(false);
	}
	
}
