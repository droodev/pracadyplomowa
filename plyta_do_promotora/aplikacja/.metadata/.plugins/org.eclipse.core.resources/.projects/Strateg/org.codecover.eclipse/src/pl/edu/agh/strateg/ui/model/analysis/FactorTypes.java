package pl.edu.agh.strateg.ui.model.analysis;

public enum FactorTypes {
	
	SZANSA("Szansa"),
	ZAGROZENIE("Zagrożenie"),
	SILA("Siła"),
	SLABOSC("Słabość");
	
	private String caption;
	
	private FactorTypes(String caption) {
		this.caption = caption;
	}
	
	public String getCaption() {
		return caption;
	}

}
