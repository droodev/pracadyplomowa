package pl.edu.agh.strateg.ui.controller.main;

import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;

import pl.edu.agh.strateg.ui.Views;
import pl.edu.agh.strateg.ui.view.views.LoginView;

import com.vaadin.navigator.Navigator;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public class LoginController implements ClickListener {
	private static final Logger LOG = Logger.getLogger(Thread.currentThread()
			.getStackTrace()[1].getClassName());

	private Navigator navigator;

	private LoginView loginView;

	public LoginController(Navigator navigator) {
		this.navigator = navigator;
	}

	public void setLoginView(LoginView loginView) {
		this.loginView = loginView;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		try {
			String password = loginView.getPassword();
			String login = loginView.getLogin();
			UsernamePasswordToken token = new UsernamePasswordToken(login,
					password);
			Subject currentUser = SecurityUtils.getSubject();
			currentUser.login(token);
			LOG.debug(String.format("Successfully logged as: %s", token
					.getPrincipal().toString()));
			navigator.navigateTo(Views.MAIN_VIEW);

		} catch (UnknownAccountException uae) {
			loginView.loginError();
		} catch (IncorrectCredentialsException ice) {
			loginView.loginError();
		} catch (LockedAccountException lae) {
			loginView.loginError();
		} catch (AuthenticationException ae) {
			loginView.loginError();
		}

	}

}
