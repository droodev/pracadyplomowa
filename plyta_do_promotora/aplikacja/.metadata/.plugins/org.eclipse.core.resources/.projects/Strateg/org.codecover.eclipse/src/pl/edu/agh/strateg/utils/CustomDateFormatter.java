package pl.edu.agh.strateg.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CustomDateFormatter {
	
	private static final String DATE_FORMAT = "dd.MM.yyyy";
	private static final String SHORT_DATE_FORMAT = "yyyy";
	private static SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
	private static SimpleDateFormat shortSdf = new SimpleDateFormat(SHORT_DATE_FORMAT);
	
	public static String format(Date date) {
		return sdf.format(date);
	}
	
	public static Date parse(String source) throws ParseException {
		return sdf.parse(source);
	}
	
	public static String getDateFormat() {
		return DATE_FORMAT;
	}
	
	public static String formatShort(Date date) {
		return shortSdf.format(date);
	}
	
	public static Date parseShort(String source) throws ParseException {
		return shortSdf.parse(source);
	}
	
	public static String getDateFormatShort() {
		return SHORT_DATE_FORMAT;
	}

}
