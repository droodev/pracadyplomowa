/**
	@author drew
 */

package pl.edu.agh.strateg.model.required;

import com.google.code.morphia.annotations.Entity;

@Entity
public enum Category {
	GENERAL("Ogólna"), OTHERS("Inna"), OBSERVABLE("Miernik"), GOAL("Priorytet");

	private String humanReadableName;

	private Category(String humanReadableName) {
		this.humanReadableName = humanReadableName;
	}

	@Override
	public String toString() {
		return humanReadableName;
	}

	public static String[] toStringArray() {
		Category[] values = Category.values();
		// new String()[values.length];
		String[] toReturn = new String[values.length];
		for (Category cat : values) {
			toReturn[cat.ordinal()] = cat.toString();
		}
		return toReturn;
	}
}
