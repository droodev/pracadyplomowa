package pl.edu.agh.strateg.utils;

import java.text.ParseException;

import com.vaadin.data.Validator;

public class DoubleValidator implements Validator{
	
	private static final String INCORRECT_NUMBER_WARNING = "Niepoprawna liczba";

	@Override
	public void validate(Object value) throws InvalidValueException {
		if (value != "") {
			try {
				CustomDoubleFormatter.parse((String) value);
			}
			catch (ParseException e) {
				throw new InvalidValueException(INCORRECT_NUMBER_WARNING);
			}
		}
		
	}

}
