package pl.edu.agh.strateg.ui.view.views;

import pl.edu.agh.strateg.ui.view.components.common.TitlePanel;
import pl.edu.agh.strateg.utils.Icons;
import pl.edu.agh.strateg.utils.InputStreamList;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.VerticalLayout;

public class AccessDeniedView extends VerticalLayout implements View  {
	
	private static final String PAGE_CAPTION = "BRAK UPRAWNIEŃ";
	
	private TitlePanel titlePanel;
	
	public AccessDeniedView() {
		initComponents();
	}

	private void initComponents() {
		
		titlePanel = new TitlePanel(PAGE_CAPTION, Icons.TITLE_NO_PERMISSION, false);
		VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.setSpacing(true);
		mainLayout.setMargin(true);
		mainLayout.setSizeFull();
		mainLayout.addComponent(titlePanel);
		
		addComponent(mainLayout);
	}


	@Override
	public void enter(ViewChangeEvent event) {
		InputStreamList.getInstance().clear();
		titlePanel.setNavigator(getUI().getNavigator());
	}

}
