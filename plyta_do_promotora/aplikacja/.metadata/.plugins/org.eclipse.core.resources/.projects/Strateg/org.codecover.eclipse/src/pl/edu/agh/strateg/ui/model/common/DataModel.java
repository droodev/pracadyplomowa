package pl.edu.agh.strateg.ui.model.common;

import java.util.List;

import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;

public abstract class DataModel {
	
	private List<MetadatumModel> metadata;
	
	public DataModel() {
		
	}
	
	public DataModel(List<MetadatumModel> metadane) {
		this.metadata = metadane;
	}
	
	public List<MetadatumModel> getMetadata() {
		return metadata;
	}

	public void setMetadata(List<MetadatumModel> metadane) {
		this.metadata = metadane;
	}



}
