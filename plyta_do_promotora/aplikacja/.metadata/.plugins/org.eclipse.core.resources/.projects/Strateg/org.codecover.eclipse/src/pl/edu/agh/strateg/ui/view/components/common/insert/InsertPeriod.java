package pl.edu.agh.strateg.ui.view.components.common.insert;

import java.util.Date;

import pl.edu.agh.strateg.model.metadata.description.Period;
import pl.edu.agh.strateg.utils.CustomDateFormatter;

import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;

public class InsertPeriod implements InsertComponent {
	
	private static final String NIEPOPRAWNY_FORMAT_DATY_CAPTION = "Niepoprawny format daty";
	private static final String NIEPOPRAWNY_TYP_ERROR = "Niepoprawny typ";
	
	private HorizontalLayout layout;
	private DateField beginField;
	private DateField endField;
	
	public InsertPeriod() {
		beginField =  new DateField();
		beginField.setDateFormat(CustomDateFormatter.getDateFormat());
		beginField.setParseErrorMessage(NIEPOPRAWNY_FORMAT_DATY_CAPTION);
		beginField.setValue(new Date());
		beginField.setRequired(true);
		beginField.setImmediate(true);
		beginField.setCaption("Od:");
		
		endField =  new DateField();
		endField.setDateFormat(CustomDateFormatter.getDateFormat());
		endField.setParseErrorMessage(NIEPOPRAWNY_FORMAT_DATY_CAPTION);
		endField.setValue(new Date());
		endField.setRequired(true);
		endField.setImmediate(true);
		endField.setCaption("Do:");
		
		// ValueChangeListener not supported
		
		layout = new HorizontalLayout();
		layout.setSpacing(true);
		layout.addComponent(beginField);
		layout.addComponent(endField);

	}

	@Override
	public Object getValue() throws IllegalStateException, IllegalArgumentException {
		return Period.create(beginField.getValue(), endField.getValue());
	}

	@Override
	public Component getField() {
		return layout;
	}

	@Override
	public boolean isValid() {
		return beginField.isValid() && endField.isValid() && beginField.getValue().before(endField.getValue());
	}
	
	@Override
	public void setValue(Object object) {
		if (!(object instanceof Period)) {
			throw new RuntimeException(NIEPOPRAWNY_TYP_ERROR);
		}
		Period period = (Period) object;
		beginField.setValue(period.getStart());
		endField.setValue(period.getEnd());
	}

}
