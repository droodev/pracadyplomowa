/**
	@author drew
 */

package pl.edu.agh.strateg.logic.verification;

public class NotVerified extends VerificationStatus {

	private String description;

	public NotVerified(String description) {
		this.description = description;
	}

	@Override
	public String getDescription() {
		return description;
	}

}
