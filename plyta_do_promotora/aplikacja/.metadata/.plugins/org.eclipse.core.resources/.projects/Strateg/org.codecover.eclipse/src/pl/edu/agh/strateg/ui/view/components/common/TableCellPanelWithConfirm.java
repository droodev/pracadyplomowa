package pl.edu.agh.strateg.ui.view.components.common;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;

import pl.edu.agh.strateg.ui.controller.component.documents.SearchResultsController;
import pl.edu.agh.strateg.ui.model.common.DataModel;
import pl.edu.agh.strateg.ui.view.views.DocumentsView;
import pl.edu.agh.strateg.utils.Icons;

public class TableCellPanelWithConfirm extends TableCellPanel{

	public TableCellPanelWithConfirm(DataModel model, SearchResultsController controller) {
		super(model, controller);
	}

	@Override
	protected Button createButton() {
		
		Button button = new Button(DocumentsView.WYBIERZ_BUTTON_CAPTION);
		button.setIcon(new ThemeResource(Icons.CONFIRM));
		
		button.setData(model);
		button.addClickListener(controller);
		
		return button;
	}

}
