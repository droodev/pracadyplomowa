package pl.edu.agh.strateg.ui.controller.component.metadata;

import java.util.List;

import pl.edu.agh.strateg.ui.controller.component.common.EditController;
import pl.edu.agh.strateg.ui.model.common.DataTypes;
import pl.edu.agh.strateg.ui.model.metadata.MetadataGroup;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;
import pl.edu.agh.strateg.ui.view.components.common.EditWindow;

public class EditDescriptorController extends EditController{
	
	private MetadatumModel metadatumModel;
	
	public EditDescriptorController(EditWindow edytujWindow, MetadatumModel metadana) {
		super(edytujWindow, DataTypes.METADANA);
		this.metadatumModel = metadana;
	}
	
	@Override
	protected void confirmButtonClicked() {
		super.confirmButtonClicked();
		List<MetadatumModel> metadaneOpisujace = chosenMetadataController.getAllValues();
		metadatumModel.setDescribingMetadata(metadaneOpisujace);
		editWindow.close();
	}

	
	@Override
	public void descriptorsReceived(MetadataGroup grupaMetadanych) {
		super.descriptorsReceived(grupaMetadanych);
		for (MetadatumModel m : metadatumModel.getDescribingMetadata()) {
			chosenData(m);
			chosenMetadataController.setValue(m, m.getSimpleDatumModel().getValue());
		}
	}
	
	public MetadatumModel getMetadatum() {
		return metadatumModel;
	}


}
