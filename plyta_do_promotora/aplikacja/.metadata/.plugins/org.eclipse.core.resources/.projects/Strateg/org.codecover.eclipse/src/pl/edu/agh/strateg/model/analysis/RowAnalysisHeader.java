/**
	@author drew
 */

package pl.edu.agh.strateg.model.analysis;

public abstract class RowAnalysisHeader extends AnalysisHeaderBase {

	RowAnalysisHeader(String name, int orderingNumber) {
		super(name, orderingNumber);
	}

}
