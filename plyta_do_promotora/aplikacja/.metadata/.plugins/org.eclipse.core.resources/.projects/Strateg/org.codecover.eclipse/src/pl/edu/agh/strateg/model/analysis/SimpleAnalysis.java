/**
	@author drew
 */

package pl.edu.agh.strateg.model.analysis;

import static pl.edu.agh.strateg.helpers.ExtendedPreconditions.*;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import pl.edu.agh.strateg.helpers.ExtendedPreconditions;
import pl.edu.agh.strateg.model.metadata.MetadataContainer;

import com.google.common.base.Optional;
import com.google.common.collect.Table;
import com.google.common.collect.TreeBasedTable;

public class SimpleAnalysis implements Analysis {
	private static final int GUARD_ORDERING = 0;
	private Table<RowAnalysisHeader, ColumnAnalysisHeader, Optional<SWOTField>> tableRepresentation;
	private int actualOrdering = 1;
	private UUID uuid = UUID.randomUUID();
	private static String GUARD_STRING = "NULL";
	private static StrengthAnalysisHeader GUARD_ROW = new StrengthAnalysisHeader(
			GUARD_STRING, GUARD_ORDERING);
	private static OpportunityAnalysisHeader GUARD_COLUMN = new OpportunityAnalysisHeader(
			GUARD_STRING, GUARD_ORDERING);
	private MetadataContainer container;

	private SimpleAnalysis(
			Table<RowAnalysisHeader, ColumnAnalysisHeader, Optional<SWOTField>> tableRepresentation,
			MetadataContainer container) {
		ExtendedPreconditions.checkArgumentsNotNull(tableRepresentation,
				container);
		this.tableRepresentation = tableRepresentation;
		this.container = container;
		tableRepresentation.put(GUARD_ROW, GUARD_COLUMN,
				Optional.<SWOTField> absent());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * pl.edu.agh.strateg.model.analysis.Analysis#addStrength(java.lang.String)
	 */
	@Override
	public RowAnalysisHeader addStrength(String name) {

		StrengthAnalysisHeader newRowHeader = new StrengthAnalysisHeader(name,
				getNextOrdering());
		addRow(newRowHeader);
		return newRowHeader;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * pl.edu.agh.strateg.model.analysis.Analysis#addWeakness(java.lang.String)
	 */
	@Override
	public RowAnalysisHeader addWeakness(String name) {
		WeaknessAnalysisHeader newRowHeader = new WeaknessAnalysisHeader(name,
				getNextOrdering());
		addRow(newRowHeader);
		return newRowHeader;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * pl.edu.agh.strateg.model.analysis.Analysis#addOpportunity(java.lang.String
	 * )
	 */
	@Override
	public ColumnAnalysisHeader addOpportunity(String name) {
		OpportunityAnalysisHeader newColumnHeader = new OpportunityAnalysisHeader(
				name, getNextOrdering());
		addColumn(newColumnHeader);
		return newColumnHeader;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * pl.edu.agh.strateg.model.analysis.Analysis#addThreat(java.lang.String)
	 */
	@Override
	public ColumnAnalysisHeader addThreat(String name) {
		ThreatAnalysisHeader newColumnHeader = new ThreatAnalysisHeader(name,
				getNextOrdering());
		addColumn(newColumnHeader);
		return newColumnHeader;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pl.edu.agh.strateg.model.analysis.Analysis#getColumns()
	 */
	@Override
	public Set<ColumnAnalysisHeader> getColumns() {
		Set<ColumnAnalysisHeader> columnKeySet = tableRepresentation
				.columnKeySet();
		columnKeySet.remove(GUARD_COLUMN);
		return columnKeySet;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pl.edu.agh.strateg.model.analysis.Analysis#getRows()
	 */
	@Override
	public Set<RowAnalysisHeader> getRows() {
		Set<RowAnalysisHeader> rowKeySet = tableRepresentation.rowKeySet();
		rowKeySet.remove(GUARD_ROW);
		return rowKeySet;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * pl.edu.agh.strateg.model.analysis.Analysis#setCell(pl.edu.agh.strateg
	 * .model.analysis.RowAnalysisHeaderBase,
	 * pl.edu.agh.strateg.model.analysis.ColumnAnalysisHeaderBase,
	 * pl.edu.agh.strateg.model.analysis.SWOTField)
	 */
	@Override
	public void setCell(RowAnalysisHeader row, ColumnAnalysisHeader column,
			SWOTField value) {
		checkArgumentsNotNull(row, column);
		tableRepresentation.put(row, column, Optional.fromNullable(value));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * pl.edu.agh.strateg.model.analysis.Analysis#clearCell(pl.edu.agh.strateg
	 * .model.analysis.RowAnalysisHeaderBase,
	 * pl.edu.agh.strateg.model.analysis.ColumnAnalysisHeaderBase)
	 */
	@Override
	public void clearCell(RowAnalysisHeader row, ColumnAnalysisHeader column) {
		checkArgumentsNotNull(row, column);
		tableRepresentation.put(row, column, Optional.<SWOTField> absent());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * pl.edu.agh.strateg.model.analysis.Analysis#removeColumn(pl.edu.agh.strateg
	 * .model.analysis.ColumnAnalysisHeaderBase)
	 */
	@Override
	public void removeColumn(ColumnAnalysisHeader column) {
		checkArgumentNotNull(column);
		for (RowAnalysisHeader row : tableRepresentation.rowKeySet()) {
			tableRepresentation.remove(row, column);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * pl.edu.agh.strateg.model.analysis.Analysis#removeRow(pl.edu.agh.strateg
	 * .model.analysis.RowAnalysisHeaderBase)
	 */
	@Override
	public void removeRow(RowAnalysisHeader row) {
		checkArgumentNotNull(row);
		// Used because of ConcurrentModificationException
		HashSet<ColumnAnalysisHeader> columnsSet = new HashSet<ColumnAnalysisHeader>(
				tableRepresentation.columnKeySet());
		for (ColumnAnalysisHeader column : columnsSet) {
			tableRepresentation.remove(row, column);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * pl.edu.agh.strateg.model.analysis.Analysis#setCellChecked(pl.edu.agh.
	 * strateg.model.analysis.RowAnalysisHeaderBase,
	 * pl.edu.agh.strateg.model.analysis.ColumnAnalysisHeaderBase,
	 * pl.edu.agh.strateg.model.analysis.SWOTField)
	 */
	@Override
	public boolean setCellChecked(RowAnalysisHeader row,
			ColumnAnalysisHeader column, SWOTField value) {
		ExtendedPreconditions.checkArgumentsNotNull(row, column, value);
		if (getCell(row, column).isPresent()) {
			return false;
		}
		tableRepresentation.put(row, column, Optional.of(value));
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * pl.edu.agh.strateg.model.analysis.Analysis#getCell(pl.edu.agh.strateg
	 * .model.analysis.RowAnalysisHeaderBase,
	 * pl.edu.agh.strateg.model.analysis.ColumnAnalysisHeaderBase)
	 */
	@Override
	public Optional<SWOTField> getCell(RowAnalysisHeader row,
			ColumnAnalysisHeader column) {
		return tableRepresentation.get(row, column);
	}

	private void addRow(RowAnalysisHeader rowToAdd) {

		for (ColumnAnalysisHeader column : tableRepresentation.columnKeySet()) {
			tableRepresentation.put(rowToAdd, column,
					Optional.<SWOTField> absent());
		}
	}

	private void addColumn(ColumnAnalysisHeader columnToAdd) {
		for (RowAnalysisHeader row : tableRepresentation.rowKeySet()) {
			tableRepresentation.put(row, columnToAdd,
					Optional.<SWOTField> absent());
		}
	}

	private int getNextOrdering() {
		return actualOrdering++;
	}

	public static Analysis createEmptyAnalysisWithNoMetadata() {

		return new SimpleAnalysis(
				TreeBasedTable
						.<RowAnalysisHeader, ColumnAnalysisHeader, Optional<SWOTField>> create(),
				MetadataContainer.EMPTY);
	}

	public static Analysis createEmptyAnalysisWithDescribingMetadata(
			MetadataContainer container) {

		return new SimpleAnalysis(
				TreeBasedTable
						.<RowAnalysisHeader, ColumnAnalysisHeader, Optional<SWOTField>> create(),
				container);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pl.edu.agh.strateg.model.analysis.Analysis#getUUID()
	 */
	@Override
	public UUID getUUID() {
		return uuid;
	}

	/*
	 * Maybe it should be replaced by something else but I don't know what
	 */
	public void setUUID(UUID uuid) {
		this.uuid = uuid;
	}

	@Override
	public MetadataContainer getDescribingMetadata() {
		return container;
	}

	@Override
	public void setDescribingMetadata(MetadataContainer container) {
		ExtendedPreconditions.checkArgumentNotNull(container);
		this.container = container;
	}

}
