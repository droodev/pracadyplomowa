package pl.edu.agh.strateg.ui.view.components.metadata;

import pl.edu.agh.strateg.ui.controller.component.metadata.EditDescriptorController;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;
import pl.edu.agh.strateg.ui.view.components.common.EditWindow;

public class EditDescriptorWindow extends EditWindow{
	
	private static final String WINDOW_CAPTION = "Edytuj metadaną";
	
	public EditDescriptorWindow(MetadatumModel metadana) {
		super(WINDOW_CAPTION);
		controller = new EditDescriptorController(this, metadana);
		super.init();
	}


}
