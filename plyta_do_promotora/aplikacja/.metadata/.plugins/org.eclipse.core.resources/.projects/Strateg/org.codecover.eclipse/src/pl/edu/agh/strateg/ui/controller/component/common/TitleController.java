package pl.edu.agh.strateg.ui.controller.component.common;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import pl.edu.agh.strateg.ui.Views;
import pl.edu.agh.strateg.ui.view.components.common.TitlePanel;

import com.vaadin.navigator.Navigator;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public class TitleController implements ClickListener{
	
	private Navigator navigator;
	
	public void setNavigator(Navigator navigator) {
		this.navigator = navigator;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if (button.getCaption().equals(TitlePanel.LOGIN)) {
			navigator.navigateTo(Views.LOGIN_VIEW);
		}
		else if (button.getCaption().equals(TitlePanel.LOGOUT)) {
			Subject currentUser = SecurityUtils.getSubject();
			currentUser.logout();
			navigator.navigateTo(Views.LOGIN_VIEW);
		}
		
	}

}
