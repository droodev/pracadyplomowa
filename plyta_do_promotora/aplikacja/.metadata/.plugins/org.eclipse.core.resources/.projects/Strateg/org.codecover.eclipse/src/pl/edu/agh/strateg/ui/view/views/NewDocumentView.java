package pl.edu.agh.strateg.ui.view.views;

import pl.edu.agh.strateg.ui.controller.main.NewDocumentController;
import pl.edu.agh.strateg.utils.Icons;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.Receiver;

public class NewDocumentView extends AbstractNewDocumentView {

	private static final String ZALADOWANO_CAPTION = "Załadowano: ";
	private static final String UPLOAD_ERROR = "Nie udało się otworzyć pliku";
	private static final String FILE_NOT_UPLOADED_ERROR = "Nie dodano załącznika";
	private static final String UPLOAD_CAPTION = "Dodaj załącznik";
	private static final String WYBIERZ_CAPTION = "Wybierz";
	
	private Upload upload;
	
	protected void setCaptions() {
		POTWIERDZ_BUTTON_CAPTION = "Dodaj dokument";
		ANULUJ_BUTTON_CAPTION = "Powrót do dokumentów";
		PAGE_CAPTION = "NOWY DOKUMENT";
		NIE_UDALO_SIE_DODAC_CAPTION = "Dodawanie dokumentu nie powiodło się";
		ICON = Icons.TITLE_NEW_DOCUMENT;
	}
	
	public NewDocumentView() {
		super();
	}

	
	public void wyswietlFileNotUploadedWarning() {
		Notification.show(FILE_NOT_UPLOADED_ERROR, Type.ERROR_MESSAGE);
	}

	
	public void wyswietlUploadProblemWarning() {
		Notification.show(UPLOAD_ERROR, Type.ERROR_MESSAGE);
	}
	
	
	public void zaladowanoPoprawnie(String fileName) {
		Notification.show(ZALADOWANO_CAPTION + fileName, Type.TRAY_NOTIFICATION);
	}

	
	protected void initComponents() {
		super.initComponents();
		
		upload = new Upload();
		upload.setButtonCaption(WYBIERZ_CAPTION);
		upload.setCaption(UPLOAD_CAPTION);
		upload.setIcon(new ThemeResource(Icons.ATTACH));
		upload.setImmediate(true);
		
	}

	protected void initLayouts() {
		super.initLayouts();
		customLayout.addComponent(upload);
	}

	public void setControllerToAllComponents(
			NewDocumentController nowyDokumentController) {
		super.setControllerToAllComponents(nowyDokumentController);
		upload.setReceiver((Receiver) controller);
	}
	

}
