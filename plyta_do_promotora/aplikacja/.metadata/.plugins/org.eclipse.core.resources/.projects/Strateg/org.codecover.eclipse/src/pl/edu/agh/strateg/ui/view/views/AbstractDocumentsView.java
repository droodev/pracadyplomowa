package pl.edu.agh.strateg.ui.view.views;

import pl.edu.agh.strateg.ui.controller.main.AbstractDocumentsController;
import pl.edu.agh.strateg.ui.model.documents.DocumentsDataModel;
import pl.edu.agh.strateg.ui.view.components.common.TitlePanel;
import pl.edu.agh.strateg.ui.view.components.documents.PreviewDocumentWindow;
import pl.edu.agh.strateg.ui.view.components.documents.SearchDocumentsLayout;
import pl.edu.agh.strateg.utils.Icons;
import pl.edu.agh.strateg.utils.InputStreamList;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;

public abstract class AbstractDocumentsView extends SearchDocumentsLayout implements View {
	
	public static final String STRONA_GLOWNA_BUTTON_CAPTION = "Strona główna";
	public static final String PODGLAD_DOKUMENTU_BUTTON_CAPTION = "Podgląd";
	public static final String WYBIERZ_BUTTON_CAPTION = "Wybierz";
	public String NOWY_DOKUMENT_BUTTON_CAPTION;
	
	protected static String PAGE_CAPTION;
	protected static String ICON;
	
	protected AbstractDocumentsController dokumentyController;
	
	protected boolean areDataReadOnly;
	
	private Button stronaGlownaButton;
	private Button nowyDokumentButton;
	
	private TitlePanel titlePanel;
	
	public AbstractDocumentsView() {
		super();
	}
	
	
	public void setControllerToAllComponents(AbstractDocumentsController controller) {
		this.dokumentyController = controller;
		super.setControllerToAllComponents(controller);
		controller.setDocumentsView(this);
		controller.addNavigator(titlePanel);
	}
	
	public void displayPreviewWindow(DocumentsDataModel model) {
		PreviewDocumentWindow podgladDokumentuWindow = new PreviewDocumentWindow(areDataReadOnly);
		podgladDokumentuWindow.setMainController((AbstractDocumentsController)controller);
		podgladDokumentuWindow.changeDocumentsData(model);
		getUI().addWindow(podgladDokumentuWindow);
		podgladDokumentuWindow.setModal(true);
	}
	
	
	protected void initComponents() {
		super.initComponents();
		stronaGlownaButton = new Button(STRONA_GLOWNA_BUTTON_CAPTION);
		nowyDokumentButton = new Button(NOWY_DOKUMENT_BUTTON_CAPTION);
		
		stronaGlownaButton.setIcon(new ThemeResource(Icons.MAIN));
		nowyDokumentButton.setIcon(new ThemeResource(Icons.NEW));
		
		titlePanel = new TitlePanel(PAGE_CAPTION, ICON, true);
	}
	
	protected void initLayouts() {
		super.initLayouts();
		super.fillLeftLayout(stronaGlownaButton);
		super.fillRightLayout(nowyDokumentButton);
		super.fillTopLayout(titlePanel);
	}

	protected void setListenerToButtons() {
		super.setListenerToButtons();
		stronaGlownaButton.addClickListener(controller);
		nowyDokumentButton.addClickListener(controller);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		super.fillPanel();
		InputStreamList.getInstance().clear();
	}


}
