package pl.edu.agh.strateg.ui.controller.component.metadata;

import java.io.Serializable;

import pl.edu.agh.strateg.ui.controller.interfaces.HasMetadatumDetails;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;
import pl.edu.agh.strateg.ui.view.components.metadata.DescriptorDetailsPanel;

public class DescriptorDetailsController implements Serializable{
	
	private DescriptorDetailsPanel panel;
	@SuppressWarnings("unused")
	private HasMetadatumDetails mainController;
	
	public DescriptorDetailsController(DescriptorDetailsPanel panel) {
		this.panel = panel;
	}

	public void setMainController(HasMetadatumDetails mainController) {
		this.mainController = mainController;
		mainController.setDescriptorDetailsController(this);
	}
	
	public void displayMetadatum(MetadatumModel metadana) {
		panel.setDatum(metadana);
		panel.displayTable();
	}
	
	public void clear() {
		panel.hideTable();
	}
	

}
