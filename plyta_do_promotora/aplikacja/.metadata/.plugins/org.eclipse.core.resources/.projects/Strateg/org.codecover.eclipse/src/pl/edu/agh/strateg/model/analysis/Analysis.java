/**
	@author drew
 */

package pl.edu.agh.strateg.model.analysis;

import java.util.Set;
import java.util.UUID;

import pl.edu.agh.strateg.model.Describable;

import com.google.common.base.Optional;

public interface Analysis extends Describable {

	public RowAnalysisHeader addStrength(String name);

	public RowAnalysisHeader addWeakness(String name);

	public ColumnAnalysisHeader addOpportunity(String name);

	public ColumnAnalysisHeader addThreat(String name);

	public Set<ColumnAnalysisHeader> getColumns();

	public Set<RowAnalysisHeader> getRows();

	public void setCell(RowAnalysisHeader row,
			ColumnAnalysisHeader column, SWOTField value);

	public void clearCell(RowAnalysisHeader row,
			ColumnAnalysisHeader column);

	public void removeColumn(ColumnAnalysisHeader column);

	public void removeRow(RowAnalysisHeader row);

	public boolean setCellChecked(RowAnalysisHeader row,
			ColumnAnalysisHeader column, SWOTField value);

	public Optional<SWOTField> getCell(RowAnalysisHeader row,
			ColumnAnalysisHeader column);

	public UUID getUUID();

}