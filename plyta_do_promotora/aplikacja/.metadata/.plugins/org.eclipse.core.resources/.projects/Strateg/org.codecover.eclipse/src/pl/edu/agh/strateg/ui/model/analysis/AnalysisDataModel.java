package pl.edu.agh.strateg.ui.model.analysis;

import java.util.List;

import pl.edu.agh.strateg.model.analysis.Analysis;
import pl.edu.agh.strateg.ui.model.common.DataModel;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;

public class AnalysisDataModel extends DataModel{
	
	private Analysis analysis;
	
	public AnalysisDataModel() {
		
	}
	
	public AnalysisDataModel(List<MetadatumModel> metadane) {
		super(metadane);
	}

	public Analysis getAnalysis() {
		return analysis;
	}

	public void setAnalysis(Analysis analiza) {
		this.analysis = analiza;
	}
	

}
