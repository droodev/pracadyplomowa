/**
	@author drew
 */

package pl.edu.agh.strateg.logic.verification;

import com.google.common.base.Optional;

public interface VerifierPart<T> {
	public VerificationStatus verify(T entity, Optional<Object> parent,
			Verifier verifier);
}
