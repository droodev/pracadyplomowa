package pl.edu.agh.strateg.ui.model.communication.comparators;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import pl.edu.agh.strateg.model.metadata.description.Descriptor;

public class DescriptorsComparator implements Comparator<Descriptor> {

	private List<String> mainDescriptors = new ArrayList<String>();

	public DescriptorsComparator() {
		mainDescriptors.add("Nazwa danej");
		mainDescriptors.add("Kategoria");
		mainDescriptors.add("Data");
		mainDescriptors.add("Priorytet");
		mainDescriptors.add("Okres");
	}

	@Override
	public int compare(Descriptor descriptor1, Descriptor descriptor2) {
		String name1 = descriptor1.getName();
		String name2 = descriptor2.getName();
		if (mainDescriptors.contains(name1) && mainDescriptors.contains(name2)) {
			return mainDescriptors.indexOf(name1) < mainDescriptors
					.indexOf(name2) ? -1 : 1;
		} else if (mainDescriptors.contains(name1)) {
			return -1;
		} else if (mainDescriptors.contains(name2)) {
			return 1;
		}
		return name1.compareTo(name2);
	}

}
