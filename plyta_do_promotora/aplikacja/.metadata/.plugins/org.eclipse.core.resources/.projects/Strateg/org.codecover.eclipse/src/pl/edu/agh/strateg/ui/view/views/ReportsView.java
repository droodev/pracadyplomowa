package pl.edu.agh.strateg.ui.view.views;

import pl.edu.agh.strateg.utils.Icons;


public class ReportsView extends AbstractDocumentsView {
	
	public ReportsView() {
		super();
		areDataReadOnly = true;
	}
	
	protected void setCaptions() {
		PYTANIE_DO_POTWIERDZENIA = "Czy na pewno chcesz wyświetlić wszystkie raporty?";
		NOWY_DOKUMENT_BUTTON_CAPTION = "Nowy raport";
		PAGE_CAPTION = "RAPORTY";
		TABLE_COLUMN_NAME = "Raporty";
		NIE_UDALO_SIE_POBRAC_PLIKOW = "Nie udało się pobrać raportów";
		ICON = Icons.TITLE_REPORTS;
	}

}
