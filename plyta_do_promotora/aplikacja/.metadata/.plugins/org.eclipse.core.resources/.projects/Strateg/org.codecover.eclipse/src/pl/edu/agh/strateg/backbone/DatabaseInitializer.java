/**
	@author drew
 */

package pl.edu.agh.strateg.backbone;

import java.util.ArrayList;

import javax.inject.Inject;

import org.apache.log4j.Logger;

import pl.edu.agh.strateg.communication.LogicBus;
import pl.edu.agh.strateg.helpers.ExceptionsHelper;
import pl.edu.agh.strateg.logic.async.OperationListener;
import pl.edu.agh.strateg.logic.events.DescriptorEvents;
import pl.edu.agh.strateg.logic.verification.VerificationStatus;
import pl.edu.agh.strateg.model.metadata.description.Descriptor;
import pl.edu.agh.strateg.model.required.RequiredDescriptors;
import pl.edu.agh.strateg.persistence.PersistenceException;

public class DatabaseInitializer {
	public static final Logger LOG = Logger.getLogger(Thread.currentThread()
			.getStackTrace()[1].getClassName());
	private final LogicBus bus;

	@Inject
	private DatabaseInitializer(LogicBus bus) {
		this.bus = bus;
	}

	public void initDatabase() throws InitializingException {
		LOG.debug(String.format("Starting database init"));

		java.util.logging.Logger mongoLogger = java.util.logging.Logger
				.getLogger("com.mongodb");
		mongoLogger.setLevel(java.util.logging.Level.OFF);

		final ArrayList<Throwable> errors = new ArrayList<Throwable>();
		for (final Descriptor desc : new RequiredDescriptors()) {
			bus.post(new DescriptorEvents.SaveIfNotExistDescriptorEvent(desc,
					new OperationListener<Boolean>() {

						@Override
						public void preOperation() {
							LOG.debug(String.format("Saving %s descriptor",
									desc.getName()));
						}

						@Override
						public void postVerification(VerificationStatus status) {
							if (!status.isOK()) {
								LOG.error(String
										.format("Cannot save %s descriptor. Boot stopped",
												desc.getName()));
							}
						}

						@Override
						public void postOperation(Boolean returnValue) {
							if (returnValue.booleanValue()) {
								LOG.debug(String.format(
										"Saving %s descriptor - SAVED",
										desc.getName()));
							} else {
								LOG.debug(String.format(
										"Saving %s descriptor - PRESENT",
										desc.getName()));
							}

						}

						@Override
						public void databaseProblemOccured(
								PersistenceException e) {
							LOG.error(String
									.format("Database error occured(check if you have access to it) caused by: %s",
											e.getCause().getMessage()));
							errors.add(e.getCause());
						}
					}));
		}

		mongoLogger.setLevel(java.util.logging.Level.WARNING);

		if (errors.isEmpty()) {
			LOG.debug(String.format("Database initialized"));
			return;
		}
		ExceptionsHelper.printErrorsArray(errors);
		throw new InitializingException(
				"Cannot prepare database beggining state");
	}
}
