package pl.edu.agh.strateg.ui.view.views;

import pl.edu.agh.strateg.ui.controller.main.LoginController;
import pl.edu.agh.strateg.ui.view.components.common.TitlePanel;
import pl.edu.agh.strateg.utils.Icons;
import pl.edu.agh.strateg.utils.InputStreamList;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class LoginView extends VerticalLayout implements View {
	
	private static final String BLAD_LOGOWANIA = "Nie udało się zalogować. Spróbuj ponownie";
	private static final String PAGE_CAPTION = "STRONA LOGOWANIA";
	private static final String NAME_LABEL_CAPTION = "Nazwa użytkownika";
	private static final String PASSWORD_LABEL_CAPTION = "Hasło";
	private static final String LOGIN_BUTTON_CAPTION = "Zaloguj";
	
	private TitlePanel titlePanel;
	private TextField nameTextField;
	private PasswordField passwordField;
	private Label nameLabel;
	private Label passwordLabel;
	private Button loginButton;
	
	private LoginController loginController;
	
	public LoginView() {
		initComponents();
		initLayouts();
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		InputStreamList.getInstance().clear();
		
	}

	public void setController(LoginController loginController) {
		this.loginController = loginController;
		loginController.setLoginView(this);
		addListeners();
	}

	private void initComponents() {
		titlePanel = new TitlePanel(PAGE_CAPTION, Icons.TITLE_LOGIN);
		nameTextField = new TextField();
		passwordField = new PasswordField();
		nameLabel = new Label("<b>" + NAME_LABEL_CAPTION + "</b>", ContentMode.HTML);
		passwordLabel = new Label("<b>" + PASSWORD_LABEL_CAPTION + "</b>", ContentMode.HTML);
		loginButton = new Button(LOGIN_BUTTON_CAPTION);
		
		loginButton.setIcon(new ThemeResource(Icons.CONFIRM));
		
	}

	private void addListeners() {
		loginButton.addClickListener(loginController);
		
	}

	private void initLayouts() {
		VerticalLayout mainLayout = new VerticalLayout();
		
		GridLayout componentsLayout = new GridLayout(2, 3);
		
		mainLayout.setMargin(true);
		mainLayout.setSpacing(true);
		mainLayout.setSizeFull();
		
		componentsLayout.setSpacing(true);
		
		componentsLayout.addComponent(nameLabel, 0, 0);
		componentsLayout.addComponent(nameTextField, 1, 0);
		componentsLayout.addComponent(passwordLabel, 0, 1);
		componentsLayout.addComponent(passwordField, 1, 1);
		componentsLayout.addComponent(loginButton, 1, 2);
		
		componentsLayout.setComponentAlignment(loginButton, Alignment.BOTTOM_RIGHT);
		
		mainLayout.addComponent(titlePanel);
		mainLayout.addComponent(componentsLayout);
		
		mainLayout.setComponentAlignment(componentsLayout, Alignment.MIDDLE_CENTER);
		
		addComponent(mainLayout);
		
	}
	
	public String getLogin() {
		return nameTextField.getValue();
	}

	public String getPassword() {
		return passwordField.getValue();
	}


	public void loginError() {
		Notification.show(BLAD_LOGOWANIA, Type.ERROR_MESSAGE);
		
	}


}
