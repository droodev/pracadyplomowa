/**
	@author drew
 */

package pl.edu.agh.strateg.persistence;

import java.util.UUID;

import pl.edu.agh.strateg.model.analysis.Analysis;

public interface AnalysesDAO extends GeneralDAO<Analysis, UUID> {

}
