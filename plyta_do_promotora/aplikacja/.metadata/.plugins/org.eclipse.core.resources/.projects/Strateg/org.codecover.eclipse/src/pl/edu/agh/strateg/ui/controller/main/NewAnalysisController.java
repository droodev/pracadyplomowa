package pl.edu.agh.strateg.ui.controller.main;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pl.edu.agh.strateg.model.analysis.Analysis;
import pl.edu.agh.strateg.ui.Views;
import pl.edu.agh.strateg.ui.controller.component.analysis.AddFactorWindowController;
import pl.edu.agh.strateg.ui.controller.component.analysis.AnalysisTableController;
import pl.edu.agh.strateg.ui.controller.component.common.ChooseMetadataController;
import pl.edu.agh.strateg.ui.controller.component.common.ChosenMetadataController;
import pl.edu.agh.strateg.ui.controller.interfaces.HasAnalysisTable;
import pl.edu.agh.strateg.ui.controller.interfaces.HasChooseMetadata;
import pl.edu.agh.strateg.ui.controller.interfaces.HasChosenMetadata;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.AnalysisCreatedListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.GotAllMetadaneListener;
import pl.edu.agh.strateg.ui.model.analysis.AnalysisDataModel;
import pl.edu.agh.strateg.ui.model.analysis.FactorTypes;
import pl.edu.agh.strateg.ui.model.common.DataTypes;
import pl.edu.agh.strateg.ui.model.communication.Gateway;
import pl.edu.agh.strateg.ui.model.communication.GatewayImpl;
import pl.edu.agh.strateg.ui.model.metadata.MetadataGroup;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;
import pl.edu.agh.strateg.ui.model.metadata.RequiredMetadatum;
import pl.edu.agh.strateg.ui.view.components.common.TitlePanel;
import pl.edu.agh.strateg.ui.view.views.NewAnalysisView;

import com.vaadin.navigator.Navigator;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public class NewAnalysisController implements ClickListener, HasChooseMetadata,
		HasChosenMetadata, HasAnalysisTable, GotAllMetadaneListener,
		AnalysisCreatedListener, Serializable {

	private Navigator navigator;

	private ChooseMetadataController chooseMetadataController;
	private ChosenMetadataController chosenMetadataController;
	private AnalysisTableController analysisTableController;
	@SuppressWarnings("unused")
	private AddFactorWindowController addFactorWindowController;

	private NewAnalysisView newAnalysisView;

	private Gateway gateway;

	public NewAnalysisController(Navigator navigator) {
		this.navigator = navigator;
		gateway = GatewayImpl.getInstance();
	}

	public void setNewAnalysisView(NewAnalysisView nowaAnalizaView) {
		this.newAnalysisView = nowaAnalizaView;
	}

	public void setChooseMetadataController(
			ChooseMetadataController wybierzMetadaneController) {
		this.chooseMetadataController = wybierzMetadaneController;
	}

	public void setChosenMetadataController(
			ChosenMetadataController wybraneMetadaneController) {
		this.chosenMetadataController = wybraneMetadaneController;
	}

	public void setAnalysisTableController(
			AnalysisTableController analizaTableController) {
		this.analysisTableController = analizaTableController;

	}

	public void setAddFactorWindowController(
			AddFactorWindowController dodajCzynnikWindowController) {
		this.addFactorWindowController = dodajCzynnikWindowController;

	}

	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if (button.getCaption().equals(
				NewAnalysisView.STRONA_GLOWNA_BUTTON_CAPTION)) {
			mainPageButtonClicked();
		} else if (button.getCaption().equals(
				NewAnalysisView.POTWIERDZ_BUTTON_CAPTION)) {
			confirmButtonClicked();
		} else if (button.getCaption().equals(
				NewAnalysisView.ANULUJ_BUTTON_CAPTION)) {
			cancelButtonClicked();
		}

	}

	private void mainPageButtonClicked() {
		navigator.navigateTo(Views.MAIN_VIEW);

	}

	private void confirmButtonClicked() {
		if (analysisTableController.isAnalysisEmpty()) {
			newAnalysisView.emptyAnalysis();
			return;
		}
		if (!analysisTableController.areValuesValid()
				|| !chosenMetadataController.areValuesValid()) {
			newAnalysisView.displayNotValidValuesWarning();
			return;
		}
		Analysis analiza = analysisTableController.getAnalysis();
		List<MetadatumModel> metadane = chosenMetadataController.getAllValues();
		AnalysisDataModel daneAnalizy = new AnalysisDataModel(metadane);
		daneAnalizy.setAnalysis(analiza);
		gateway.addNewAnalysis(daneAnalizy, this);

	}

	private void cancelButtonClicked() {
		navigator.navigateTo(Views.ANALYSES_VIEW);

	}

	public void addFactorButtonClicked() {
		newAnalysisView.displayAddFactorWindow();

	}

	@Override
	public void chosenData(MetadatumModel metadana) {
		chosenMetadataController.datumChosen(metadana);
		chooseMetadataController.deleteDatum(metadana);
		addAllRequired(metadana);
	}

	private void addAllRequired(MetadatumModel metadana) {
		if (metadana.getRequirements(DataTypes.ANALIZA) == null) {
			return;
		}
		for (RequiredMetadatum wymaganaMetadana : metadana
				.getRequirements(DataTypes.ANALIZA)) {
			if (!chosenMetadataController.contains(wymaganaMetadana
					.getDescriptorName())) {
				if (wymaganaMetadana.getValue() == null
						|| wymaganaMetadana.getValue().equals(
								metadana.getSimpleDatumModel().getValue())) {
					MetadatumModel wymagana = chooseMetadataController
							.deleteDatum(wymaganaMetadana.getDescriptorName());
					chosenMetadataController.datumChosen(wymagana);
				}
			}
		}

	}

	public void fillChooseMetadataPanel() {
		GatewayImpl.getInstance().getAllDescriptors(this);
	}

	@Override
	public void gettingDescriptorsFailed() {
		System.out.println("Getting descriptors failed");

	}

	@Override
	public void descriptorsReceived(MetadataGroup grupaMetadanych) {
		chooseMetadataController.fillPanelWithData(grupaMetadanych);
		List<MetadatumModel> wymaganeMetadane = new ArrayList<MetadatumModel>();
		getRequiredMetadata(grupaMetadanych, wymaganeMetadane);
		for (MetadatumModel metadana : wymaganeMetadane) {
			chosenData(metadana);
		}
	}

	private void getRequiredMetadata(MetadataGroup grupaMetadanych,
			List<MetadatumModel> wymaganeMetadane) {
		if (grupaMetadanych.getData() != null) {
			for (MetadatumModel metadana : grupaMetadanych.getData()) {
				if (metadana.isRequired(DataTypes.ANALIZA)) {
					wymaganeMetadane.add(metadana);
				}
			}
		}
		if (grupaMetadanych.getGroups() != null) {
			for (MetadataGroup grupa : grupaMetadanych.getGroups()) {
				getRequiredMetadata(grupa, wymaganeMetadane);
			}
		}
	}

	public void addFactor(String nazwa, FactorTypes typ) {
		if (typ == FactorTypes.SILA) {
			analysisTableController.addStrength(nazwa);
		} else if (typ == FactorTypes.SLABOSC) {
			analysisTableController.addWeakness(nazwa);
		} else if (typ == FactorTypes.SZANSA) {
			analysisTableController.addOpportunity(nazwa);
		} else if (typ == FactorTypes.ZAGROZENIE) {
			analysisTableController.addThread(nazwa);
		}

	}

	@Override
	public void analysisCreatedSuccessfully() {
		newAnalysisView.addedSuccessfully();

	}

	@Override
	public void analysisCreationFailed() {
		newAnalysisView.addingFailed();

	}

	@Override
	public void datumValueChanged(MetadatumModel metadana) {
		addAllRequired(metadana);
	}

	@Override
	public boolean canBeDeleted(MetadatumModel metadana) {
		return false;
	}

	@Override
	public void restoreDatum(MetadatumModel metadana) {
		chooseMetadataController.restoreDatum(metadana);
	}

	@Override
	public void databaseProblemOccured() {

	}

	public void addNavigator(TitlePanel panel) {
		panel.setNavigator(navigator);
	}

}
