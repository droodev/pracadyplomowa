/**
	@author drew
 */

package pl.edu.agh.strateg.model.metadata.description;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import pl.edu.agh.strateg.helpers.ExtendedPreconditions;

import com.google.code.morphia.annotations.Embedded;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;

public class Requirements {

	public static final Requirements NONE = new NoneRequirements();

	@Embedded
	private HashMap<Domain, ArrayList<Requirement>> requirementsMap = new HashMap<Domain, ArrayList<Requirement>>();

	private Requirements() {
		// FOR DB mapping
	}

	private Requirements(HashMap<Domain, ArrayList<Requirement>> requirementsMap) {
		ExtendedPreconditions.checkArgumentNotNull(requirementsMap);
		this.requirementsMap = requirementsMap;
	}

	public void addRequirement(Domain domain, Requirement requirement) {
		ArrayList<Requirement> domainRequirements = requirementsMap.get(domain);
		if (domainRequirements == null) {
			domainRequirements = new ArrayList<Requirement>();
			requirementsMap.put(domain, domainRequirements);
		}
		domainRequirements.add(requirement);
	}

	public boolean removeRequirement(Domain domain, Requirement requirement) {
		ArrayList<Requirement> domainRequirements = requirementsMap.get(domain);
		if (domainRequirements == null) {
			return false;
		}
		if (!domainRequirements.contains(requirement)) {
			return false;
		}
		domainRequirements.remove(requirement);
		return true;
	}

	public Optional<List<Requirement>> getAllRequirements(Domain domain) {
		List<Requirement> domainRequirements = requirementsMap.get(domain);
		return Optional.fromNullable(domainRequirements);
	}

	public List<String> getRequirementsForValueAndDomain(Domain domain,
			Object value) {
		Optional<List<Requirement>> requirementsForDomain = getAllRequirements(domain);
		List<String> orderedRequiredMetadata = new ArrayList<String>();
		if (!requirementsForDomain.isPresent()) {
			return orderedRequiredMetadata;
		}
		for (Requirement req : requirementsForDomain.get()) {
			if (req.getOnValue().isPresent()
					&& req.getOnValue().get().equals(value)) {
				orderedRequiredMetadata.add(req.getMetadataName());
			}
		}
		return orderedRequiredMetadata;
	}

	public List<String> getUnconditionalRequirements(Domain domain) {
		List<String> orderedRequiredMetadata = new ArrayList<String>();
		Optional<List<Requirement>> allRequirements = getAllRequirements(domain);
		if (!allRequirements.isPresent()) {
			return orderedRequiredMetadata;
		}
		for (Requirement req : allRequirements.get()) {
			if (!req.getOnValue().isPresent()) {
				orderedRequiredMetadata.add(req.getMetadataName());
			}
		}
		return orderedRequiredMetadata;
	}

	public static class NoneRequirements extends Requirements {

		private NoneRequirements() {
			// private scoping
		}

		@Override
		public void addRequirement(Domain domain, Requirement requirement) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean removeRequirement(Domain domain, Requirement requirement) {
			throw new UnsupportedOperationException();
		}

	}

	public static Builder getBuilder() {
		return new Builder();
	}

	public static class Builder {
		private HashMap<Domain, ArrayList<Requirement>> requirements = new HashMap<Domain, ArrayList<Requirement>>();

		private Builder() {
			// private scoped
		}

		public RequirementBuilder addRequirement(Domain domain) {
			return addRequirement(Collections.singleton(domain));
		}

		public RequirementBuilder addRequirement(Collection<Domain> domain) {
			return new RequirementBuilder(domain);
		}

		public Requirements build() {
			return new Requirements(requirements);
		}

		public class RequirementBuilder {

			private Collection<Domain> domains;

			private RequirementBuilder(Collection<Domain> domains) {
				ExtendedPreconditions.checkArgumentNotNull(domains);
				this.domains = domains;
			}

			public Builder conditional(String metadataName, Object onValue) {
				return conditional(Collections.singleton(metadataName), onValue);
			}

			public Builder conditional(Collection<String> metadataNames,
					Object onValue) {
				ensureAndInitializeArrayList();
				for (String metadataName : metadataNames) {
					addToRequirements(Requirement.createConditional(
							metadataName, onValue));
				}
				return Builder.this;
			}

			public Builder unconditional(String metadataName) {
				ensureAndInitializeArrayList();
				addToRequirements(Requirement.createUnconditional(metadataName));
				return Builder.this;
			}

			private void ensureAndInitializeArrayList() {
				for (Domain domain : domains) {
					if (requirements.get(domain) == null) {
						requirements.put(domain, new ArrayList<Requirement>());
					}
				}
			}

			private void addToRequirements(Requirement requirement) {
				for (Domain domain : domains) {
					ArrayList<Requirement> requirementsForDomain = requirements
							.get(domain);
					Preconditions.checkNotNull(requirementsForDomain);
					requirementsForDomain.add(requirement);
				}
			}
		}

	}

}
