/**
 * @author drew
 */
package pl.edu.agh.strateg.model.metadata.description;

import java.util.Collection;

public interface LimitedDescriptor extends Descriptor {
	public abstract Collection<?> getRelevantValues();
}