/**
	@author drew
 */

package pl.edu.agh.strateg.logic;

import java.util.List;

import pl.edu.agh.strateg.logic.async.OperationListener;

import com.google.common.base.Optional;

public interface EntityManager<K, V> {

	/**
	 * ALL listeners here could be null, then no listener is attached!
	 */

	public void addEntity(V entity, OperationListener<Boolean> listener);

	public void deleteEntity(V enity, OperationListener<Boolean> listener);

	public void updateEntity(V entity, OperationListener<Boolean> listener);

	public void getEntity(K id, OperationListener<Optional<V>> listener);

	public void getAllEntities(OperationListener<List<V>> listener);

}