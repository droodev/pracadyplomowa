package pl.edu.agh.strateg.ui.model.metadata;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MetadataGroup implements Serializable{
	
	private String name;
	private List<MetadatumModel> data;
	private List<MetadataGroup> groups;
	
	public MetadataGroup(String nazwa) {
		this.name = nazwa;
		groups = new ArrayList<MetadataGroup>();
		data = new ArrayList<MetadatumModel>();
	
	}
	
	public void setData(List<MetadatumModel> dane) {
		this.data = dane;
	}
	
	public void addDatum(MetadatumModel dana) {
		data.add(dana);
	}
	
	public void removeDatum(MetadatumModel dana) {
		data.remove(dana);
	}
	
	public void removeAllData() {
		data.clear();
	}
	
	public void setGroups(List<MetadataGroup> grupy) {
		this.groups = grupy;
	}
	
	public void addGroup(MetadataGroup grupa) {
		groups.add(grupa);
	}
	
	public void removeGroup(MetadataGroup grupa) {
		groups.remove(grupa);
	}
	
	public void removeAllGroups() {
		groups.clear();
	}

	public String getName() {
		return name;
	}

	public List<MetadatumModel> getData() {
		return data;
	}
	
	public List<MetadataGroup> getGroups() {
		return groups;
	}
	
	

}
