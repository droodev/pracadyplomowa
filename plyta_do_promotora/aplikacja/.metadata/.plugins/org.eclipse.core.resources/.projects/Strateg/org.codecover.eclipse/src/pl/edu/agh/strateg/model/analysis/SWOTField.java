/**
	@author drew
 */

package pl.edu.agh.strateg.model.analysis;

import java.util.UUID;

import pl.edu.agh.strateg.persistence.mongo.helpers.MorphiaRequirement;

import com.google.common.base.Preconditions;

public class SWOTField {
	private int weight;
	private UUID attachmentUUID;

	@MorphiaRequirement
	private SWOTField() {

	}

	private SWOTField(int weight) {
		Preconditions.checkArgument(weight >= 0);
		this.weight = weight;
	}

	private SWOTField(int weight, UUID uuid) {
		this(weight);
		this.attachmentUUID = uuid;
	}

	public int getWeight() {
		return weight;
	}

	public UUID getAttachmentUUID() {
		return attachmentUUID;
	}

	public static SWOTField createSWOTField(int weight) {
		return new SWOTField(weight);
	}

	public static SWOTField createSWOTField(int weight, UUID uuid) {
		return new SWOTField(weight, uuid);
	}
}
