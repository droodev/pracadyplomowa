/**
	@author drew
 */

package pl.edu.agh.strateg.persistence.mongo.proxy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Logger;

import pl.edu.agh.strateg.helpers.ReflectionHelper;
import pl.edu.agh.strateg.helpers.ReflectionOperationException;
import pl.edu.agh.strateg.model.analysis.Analysis;
import pl.edu.agh.strateg.model.analysis.AnalysisHeaderBase;
import pl.edu.agh.strateg.model.analysis.ColumnAnalysisHeader;
import pl.edu.agh.strateg.model.analysis.RowAnalysisHeader;
import pl.edu.agh.strateg.model.analysis.SWOTField;
import pl.edu.agh.strateg.model.analysis.SimpleAnalysis;
import pl.edu.agh.strateg.persistence.mongo.helpers.Mappings.Analyses;
import pl.edu.agh.strateg.persistence.mongo.helpers.MorphiaRequirement;

import com.google.code.morphia.annotations.Embedded;
import com.google.code.morphia.annotations.Id;
import com.google.code.morphia.annotations.Property;

public class AnalysisProxy {

	private static final Logger LOG = Logger.getLogger(Thread.currentThread()
			.getStackTrace()[1].getClassName());

	@Embedded(value = Analyses.ROW_NAMES)
	private ArrayList<String> rowNames = new ArrayList<String>();

	@Property(value = Analyses.LAST_PRIMARY_ROW)
	private int lastPrimaryRow;

	@Embedded(value = Analyses.COL_NAMES)
	private ArrayList<String> columnNames = new ArrayList<String>();

	@Property(value = Analyses.LAST_PRIMARY_COL)
	private int lastPrimaryColumn;

	@Embedded(value = Analyses.SWOT_FIELDS)
	private ArrayList<ArrayList<SWOTField>> fieldsList = new ArrayList<ArrayList<SWOTField>>();

	@Embedded(value = Analyses.DESCRIBING_METADATA)
	private ContainerProxy container;

	@Id
	private UUID uuid;

	@MorphiaRequirement
	private AnalysisProxy() {

	}

	private AnalysisProxy(Analysis analysis) {
		Set<RowAnalysisHeader> rowHeaders = analysis.getRows();
		Set<ColumnAnalysisHeader> columnHeaders = analysis.getColumns();
		lastPrimaryRow = fillHeaderList(rowHeaders, rowNames);
		lastPrimaryColumn = fillHeaderList(columnHeaders, columnNames);
		int row = 0;
		int column = 0;
		// fields = new SWOTField[rowHeaders.size()][columnHeaders.size()];
		for (RowAnalysisHeader rowHeader : rowHeaders) {
			fieldsList.add(new ArrayList<SWOTField>());
			for (ColumnAnalysisHeader columnHeader : columnHeaders) {
				fieldsList.get(row).add(
						analysis.getCell(rowHeader, columnHeader).orNull());
				// ++column;
			}
			// column = 0;
			++row;
		}
		uuid = analysis.getUUID();
		container = ContainerProxy.proxify(analysis.getDescribingMetadata());
	}

	private int fillHeaderList(
			Collection<? extends AnalysisHeaderBase> headersCollection,
			ArrayList<String> listToFill) {
		boolean primaryZone = true;
		int counter = 0;
		int lastPrimary = 0;
		for (AnalysisHeaderBase header : headersCollection) {
			if (primaryZone && !header.isPrimary()) {
				lastPrimary = counter - 1;
				primaryZone = false;
			}
			listToFill.add(header.getName());
			++counter;
		}
		return lastPrimary;
	}

	public Analysis deproxify() {
		Analysis analysis = SimpleAnalysis.createEmptyAnalysisWithNoMetadata();
		ArrayList<RowAnalysisHeader> rowHeaders = new ArrayList<RowAnalysisHeader>();
		ArrayList<ColumnAnalysisHeader> columnHeaders = new ArrayList<ColumnAnalysisHeader>();
		for (int i = 0; i < rowNames.size(); ++i) {
			String headerName = rowNames.get(i);
			RowAnalysisHeader addedHeader;
			if (i <= lastPrimaryRow) {
				addedHeader = analysis.addStrength(headerName);
			} else {
				addedHeader = analysis.addWeakness(headerName);
			}
			rowHeaders.add(addedHeader);
		}
		for (int i = 0; i < columnNames.size(); ++i) {
			String headerName = columnNames.get(i);
			ColumnAnalysisHeader addedHeader;
			if (i <= lastPrimaryColumn) {
				addedHeader = analysis.addOpportunity(headerName);
			} else {
				addedHeader = analysis.addThreat(headerName);
			}
			columnHeaders.add(addedHeader);
		}
		int row = 0;
		int column = 0;
		for (RowAnalysisHeader rowHeader : rowHeaders) {
			for (ColumnAnalysisHeader columnHeader : columnHeaders) {
				analysis.setCell(rowHeader, columnHeader, fieldsList.get(row)
						.get(column));
				++column;
			}
			column = 0;
			++row;
		}
		try {
			ReflectionHelper.injectIntoDeclaredField("uuid", analysis, uuid);
		} catch (ReflectionOperationException e) {
			LOG.warning("Cannot deproxify Analysis due to weird reflectin error");
		}
		analysis.setDescribingMetadata(container.deproxify());
		return analysis;
	}

	public static AnalysisProxy proxify(Analysis analysis) {
		return new AnalysisProxy(analysis);
	}

}
