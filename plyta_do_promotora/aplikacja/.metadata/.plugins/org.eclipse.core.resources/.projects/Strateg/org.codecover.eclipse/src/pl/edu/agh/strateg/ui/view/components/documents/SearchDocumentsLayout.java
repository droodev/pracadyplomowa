package pl.edu.agh.strateg.ui.view.components.documents;

import pl.edu.agh.strateg.ui.controller.component.documents.SearchDocumentsController;
import pl.edu.agh.strateg.ui.model.common.DataModel;
import pl.edu.agh.strateg.ui.view.components.common.ConfirmWindow;
import pl.edu.agh.strateg.ui.view.components.common.ChooseMetadataPanel;
import pl.edu.agh.strateg.ui.view.components.common.ChosenMetadataPanel;
import pl.edu.agh.strateg.ui.view.components.common.SearchResultsPanel;
import pl.edu.agh.strateg.ui.view.views.DocumentsView;
import pl.edu.agh.strateg.utils.Icons;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class SearchDocumentsLayout extends VerticalLayout {
	
	public static final String SZUKAJ_BUTTON_CAPTION = "Szukaj";
	
	protected String PYTANIE_DO_POTWIERDZENIA = "Czy na pewno chcesz wyświetlić wszystkie dokumenty?";
	protected String NIE_UDALO_SIE_POBRAC_PLIKOW = "Nie udało się pobrać dokumentów";
	private static final String DATABASE_PROBLEM = "Problem przy dostępie do bazy";
	protected String TABLE_COLUMN_NAME = "Dokumenty";
	
	private static final String NOT_VALID_VALUES_WARNING = "Wprowadzone wartości nie są poprawne";
	private static final String WYBIERZ_FILTR_CAPTION = "Wybierz filtr";
	
	private ChooseMetadataPanel wybierzMetadanePanel;
	private ChosenMetadataPanel wybraneMetadanePanel;
	private SearchResultsPanel wynikiWyszukiwaniaPanel;
	
	private VerticalLayout topLayout;
	private VerticalLayout leftLayout;
	private VerticalLayout rightLayout;
	
	private Button szukajButton;
	
	private String buttonCaption;
	
	protected SearchDocumentsController controller;
	
	public SearchDocumentsLayout() {
		this.buttonCaption = DocumentsView.PODGLAD_DOKUMENTU_BUTTON_CAPTION;
		setCaptions();
		initComponents();
		initLayouts();
	}
	
	public SearchDocumentsLayout(String buttonCaption) {
		this.buttonCaption = buttonCaption;
		setCaptions();
		initComponents();
		initLayouts();
	}
	
	protected void initComponents() {
		szukajButton = new Button(SZUKAJ_BUTTON_CAPTION);
		
		szukajButton.setIcon(new ThemeResource(Icons.FIND));
		
		wybierzMetadanePanel = new ChooseMetadataPanel(WYBIERZ_FILTR_CAPTION);
		wybraneMetadanePanel = new ChosenMetadataPanel();
		wynikiWyszukiwaniaPanel = new SearchResultsPanel(TABLE_COLUMN_NAME, buttonCaption);
	}
	
	protected void initLayouts() {
		
		topLayout = new VerticalLayout();
		rightLayout = new VerticalLayout();
		leftLayout = new VerticalLayout();
		
		topLayout.setSizeFull();
		rightLayout.setSizeFull();
		leftLayout.setSizeFull();
		
		VerticalLayout mainLayout = new VerticalLayout();
		HorizontalLayout upperLayout = new HorizontalLayout();
		HorizontalLayout bottomLayout = new HorizontalLayout();
		
		mainLayout.setSizeFull();
		mainLayout.setSpacing(true);
		mainLayout.setMargin(true);
		
		upperLayout.setSizeFull();
		upperLayout.setSpacing(true);
		
		bottomLayout.setSizeFull();
		bottomLayout.setSpacing(true);
		
		
		upperLayout.addComponent(leftLayout);
		upperLayout.addComponent(wybierzMetadanePanel);
		upperLayout.addComponent(rightLayout);
		
		upperLayout.setComponentAlignment(leftLayout, Alignment.TOP_LEFT);
		upperLayout.setComponentAlignment(rightLayout, Alignment.TOP_RIGHT);
		
		upperLayout.setExpandRatio(leftLayout, 0.25f);
		upperLayout.setExpandRatio(wybierzMetadanePanel, 0.5f);
		upperLayout.setExpandRatio(rightLayout, 0.25f);
		
		VerticalLayout wybraneLayout = new VerticalLayout();
		wybraneLayout.setSizeFull();
		wybraneLayout.setSpacing(true);
		
		wybraneLayout.addComponent(wybraneMetadanePanel);
		wybraneLayout.addComponent(szukajButton);
		
		wybraneLayout.setComponentAlignment(szukajButton, Alignment.BOTTOM_RIGHT);
		
		bottomLayout.addComponent(wybraneLayout);
		bottomLayout.addComponent(wynikiWyszukiwaniaPanel);
		
		bottomLayout.setExpandRatio(wybraneLayout, 0.25f);
		bottomLayout.setExpandRatio(wynikiWyszukiwaniaPanel, 0.75f);
		
		mainLayout.addComponent(topLayout);
		mainLayout.addComponent(upperLayout);
		mainLayout.addComponent(bottomLayout);
		
		addComponent(mainLayout);
		
	}
	
	protected void fillLeftLayout(Component component) {
		leftLayout.addComponent(component);
	}
	
	protected void fillRightLayout(Component component) {
		rightLayout.addComponent(component);
		rightLayout.setComponentAlignment(component, Alignment.TOP_RIGHT);
	}
	
	protected void fillTopLayout(Component component) {
		topLayout.addComponent(component);
	}
	
	protected void setControllerToAllComponents(SearchDocumentsController controller) {
		this.controller = controller;
		controller.setSearchDocumentsLayout(this);
		wybierzMetadanePanel.setMainController(controller);
		wybraneMetadanePanel.setMainController(controller);
		wynikiWyszukiwaniaPanel.setMainController(controller);
		setListenerToButtons();
	}
	
	protected void setListenerToButtons() {
		szukajButton.addClickListener(controller);
	}
	
	public void displayConfirmWindow() {
		ConfirmWindow potwierdzWindow = new ConfirmWindow(controller, PYTANIE_DO_POTWIERDZENIA);
		getUI().addWindow(potwierdzWindow);
		potwierdzWindow.setModal(true);
	}
	
	public void displayNotValidValuesWarning() {
		Notification.show(NOT_VALID_VALUES_WARNING, Type.ERROR_MESSAGE);
		
	}
	
	public void findingFilesFailed() {
		Notification.show(NIE_UDALO_SIE_POBRAC_PLIKOW, Type.ERROR_MESSAGE);
		
	}
	
	protected void fillPanel() {
		controller.fillChooseMetadataPanel();
	}
	
	protected void setCaptions() {
		
	}

	public void documentChosen(DataModel daneModel) {
		if (getParent() instanceof Window) {
			((Window)getParent()).close();
		}
		
	}
	
	public void databaseProblemOccured() {
		Notification.show(DATABASE_PROBLEM, Type.ERROR_MESSAGE);
		
	}

}
