/**
	@author drew
 */

package pl.edu.agh.strateg.helpers;

import com.google.common.base.Preconditions;

public class ExtendedPreconditions {

	public static void checkArgumentNotNull(Object o) {
		Preconditions.checkArgument(o != null, String.format(
				"Found null in %s class method usage", Thread.currentThread()
						.getStackTrace()[1].getClassName()));
	}

	public static void checkArgumentNotNull(Object o, String messg) {
		Preconditions.checkArgument(o != null, messg);
	}

	public static void checkArgumentsNotNull(Object... objs) {
		for (Object o : objs) {
			checkArgumentNotNull(o);
		}
	}
}
