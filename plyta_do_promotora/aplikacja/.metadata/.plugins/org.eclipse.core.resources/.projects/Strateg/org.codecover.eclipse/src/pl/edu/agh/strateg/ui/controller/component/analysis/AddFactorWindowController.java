package pl.edu.agh.strateg.ui.controller.component.analysis;

import java.io.Serializable;

import pl.edu.agh.strateg.ui.controller.interfaces.HasAnalysisTable;
import pl.edu.agh.strateg.ui.model.analysis.FactorTypes;
import pl.edu.agh.strateg.ui.view.components.analysis.AddFactorWindow;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public class AddFactorWindowController implements ClickListener, Serializable {
	
	private AddFactorWindow addFactorWindow;
	private HasAnalysisTable mainController;
	
	public AddFactorWindowController(AddFactorWindow dodajCzynnikWindow) {
		this.addFactorWindow = dodajCzynnikWindow;
	}
	
	public void setMainController(HasAnalysisTable mainController) {
		this.mainController = mainController;
		mainController.setAddFactorWindowController(this);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		String nazwa = addFactorWindow.getName();
		if (nazwa.trim().isEmpty()) {
			addFactorWindow.emptyName();
			return;
		}
		FactorTypes typ = addFactorWindow.getType();
		addFactorWindow.reset();
		addFactorWindow.close();
		mainController.addFactor(nazwa, typ);
		
	}

}
