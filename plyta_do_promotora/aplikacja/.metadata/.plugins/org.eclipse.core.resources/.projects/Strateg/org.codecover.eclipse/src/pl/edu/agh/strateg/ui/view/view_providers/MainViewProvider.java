package pl.edu.agh.strateg.ui.view.view_providers;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import pl.edu.agh.strateg.ui.Views;
import pl.edu.agh.strateg.ui.controller.main.MainController;
import pl.edu.agh.strateg.ui.view.views.AccessDeniedView;
import pl.edu.agh.strateg.ui.view.views.MainView;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewProvider;

public class MainViewProvider implements ViewProvider{
	
	private MainController glownyController;
	
	private Navigator.ClassBasedViewProvider provider = 
			new Navigator.ClassBasedViewProvider(Views.MAIN_VIEW, MainView.class);
	
	private Navigator.ClassBasedViewProvider noPermissionProvider = 
			new Navigator.ClassBasedViewProvider(Views.MAIN_VIEW, AccessDeniedView.class);
	
	public MainViewProvider(MainController glownyController) {
		this.glownyController = glownyController;
	}
	
	@Override
	public String getViewName(String viewAndParameters) {
		Subject currentUser = SecurityUtils.getSubject();
		if (currentUser.isAuthenticated()) {
			return provider.getViewName(viewAndParameters);
		}
		else {
			return noPermissionProvider.getViewName(viewAndParameters);
		}
	}
	@Override
	public View getView(String viewName) {
		Subject currentUser = SecurityUtils.getSubject();
		if (currentUser.isAuthenticated()) {
			MainView view = (MainView) provider.getView(viewName);
			view.setControllerToAllComponents(glownyController);
			return view;
		}
		else {
			AccessDeniedView view = (AccessDeniedView) noPermissionProvider.getView(viewName);
			return view;
		}
	}

}
