package pl.edu.agh.strateg.ui.view.components.analysis;

import pl.edu.agh.strateg.ui.controller.component.analysis.EditAnalysisController;
import pl.edu.agh.strateg.ui.controller.interfaces.HasAnalysisTable;
import pl.edu.agh.strateg.ui.model.analysis.AnalysisDataModel;
import pl.edu.agh.strateg.ui.view.components.common.EditWindow;

public class EditAnalysisWindow extends EditWindow{
	
	private static final String WINDOW_CAPTION = "Edytuj analizę";
	private AbstractAnalysisTable analizaTable;
	private AddFactorWindow dodajCzynnikWindow;
	
	private AnalysisDataModel daneAnalizy;
	
	public EditAnalysisWindow(AnalysisDataModel daneAnalizy) {
		super(WINDOW_CAPTION);
		this.daneAnalizy = daneAnalizy;
		controller = new EditAnalysisController(this, daneAnalizy);
		init();
	}
	
	@Override
	protected void init() {
		super.init();
		fillAnalizaTable();
	}
	
	@Override
	protected void initComponents() {
		super.initComponents();
		analizaTable = new EditableAnalysisTable();
		analizaTable.setMainController((HasAnalysisTable) controller);
		
		dodajCzynnikWindow = new AddFactorWindow();
		dodajCzynnikWindow.setMainController((HasAnalysisTable) controller);
	}
	
	@Override
	protected void initLayouts() {
		super.initLayouts();
		customElementsBottomLayout.addComponent(analizaTable);
	}
	
	private void fillAnalizaTable() {
		analizaTable.fillTable(daneAnalizy.getAnalysis());
	}
	
	public void displayAddFactorWindow() {
		getUI().addWindow(dodajCzynnikWindow);
		dodajCzynnikWindow.setModal(true);
	}

}
