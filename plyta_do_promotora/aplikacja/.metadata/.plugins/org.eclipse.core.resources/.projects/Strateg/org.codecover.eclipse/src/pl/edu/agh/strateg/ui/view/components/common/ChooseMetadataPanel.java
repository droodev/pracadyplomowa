package pl.edu.agh.strateg.ui.view.components.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.edu.agh.strateg.ui.controller.component.common.ChooseMetadataController;
import pl.edu.agh.strateg.ui.controller.interfaces.HasChooseMetadata;
import pl.edu.agh.strateg.ui.model.metadata.MetadataGroup;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;

import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Tree;
import com.vaadin.ui.VerticalLayout;

public class ChooseMetadataPanel extends Panel {

	private static final String PANEL_CAPTION = "Wybierz";

	private TabSheet tabsheet = new TabSheet();
	private ChooseMetadataController controller;
	private List<Tree> trees;
	private Map<String, MetadatumModel> metadane;
	private Map<MetadatumModel, Tree> removedMetadane;

	public ChooseMetadataPanel() {
		this(PANEL_CAPTION);
	}

	public ChooseMetadataPanel(String caption) {
		setCaption(caption);
		setSizeFull();
		VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.addComponent(tabsheet);
		trees = new ArrayList<Tree>();
		metadane = new HashMap<String, MetadatumModel>();
		removedMetadane = new HashMap<MetadatumModel, Tree>();
		setContent(mainLayout);

		controller = new ChooseMetadataController(this);
	}

	public void setMainController(HasChooseMetadata mainController) {
		controller.setMainController(mainController);
	}

	public void fillWithData(MetadataGroup dane) {

		clear();

		for (MetadataGroup grupa : dane.getGroups()) {
			final VerticalLayout tab = new VerticalLayout();
			tab.setSpacing(true);
			tab.setMargin(true);

			Tree tree = new Tree();
			trees.add(tree);
			tab.addComponent(tree);
			addComponentsToTree(tree, grupa, true);
			tree.addItemClickListener(controller);

			tabsheet.addTab(tab, grupa.getName());

		}
	}

	private void addComponentsToTree(Tree tree, MetadataGroup grupa,
			boolean czyRoot) {
		for (MetadataGroup g : grupa.getGroups()) {
			tree.addItem(g);
			tree.setItemCaption(g, g.getName());
			if (!czyRoot) {
				tree.setParent(g, grupa);
			}
			addComponentsToTree(tree, g, false);
		}
		for (MetadatumModel dana : grupa.getData()) {
			String nazwa = dana.getSimpleDatumModel().getName();
			metadane.put(nazwa, dana);
			tree.addItem(nazwa);
			tree.setChildrenAllowed(nazwa, false);
			if (!czyRoot) {
				tree.setParent(dana.getSimpleDatumModel().getName(), grupa);
			}
		}

	}

	public void deleteDatum(MetadatumModel dana) {
		for (Tree tree : trees) {
			if (tree.containsId(dana.getSimpleDatumModel().getName())) {
				tree.removeItem(dana.getSimpleDatumModel().getName());
				metadane.remove(dana.getSimpleDatumModel().getName());
				removedMetadane.put(dana, tree);
			}
		}
	}

	public void restoreDatum(MetadatumModel dana) {
		if (!removedMetadane.containsKey(dana)) {
			throw new RuntimeException("Dana nie była usuwana");
		}
		String nazwa = dana.getSimpleDatumModel().getName();
		metadane.put(nazwa, dana);
		Tree tree = removedMetadane.get(dana);
		tree.addItem(nazwa);
		tree.setChildrenAllowed(nazwa, false);
	}

	public void clear() {
		tabsheet.removeAllComponents();
		trees.clear();
	}

	public MetadatumModel getMetadana(String nazwa) {
		return metadane.get(nazwa);
	}

}
