/**
	@author drew
 */

package pl.edu.agh.strateg.logic;

import java.util.List;

import pl.edu.agh.strateg.logic.async.OperationListener;
import pl.edu.agh.strateg.persistence.querying.MetadatumQuery;

public interface MetadatumQuerable<T> {
	public void queryByDescriptors(MetadatumQuery query,
			OperationListener<List<T>> listener);
}
