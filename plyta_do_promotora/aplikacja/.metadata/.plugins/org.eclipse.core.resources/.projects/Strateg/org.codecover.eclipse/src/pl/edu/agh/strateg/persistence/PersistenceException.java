/**
	@author drew
 */

package pl.edu.agh.strateg.persistence;

public class PersistenceException extends Exception {

	public PersistenceException(Throwable e) {
		super("Cannot save infromation to database!", e);
	}

}
