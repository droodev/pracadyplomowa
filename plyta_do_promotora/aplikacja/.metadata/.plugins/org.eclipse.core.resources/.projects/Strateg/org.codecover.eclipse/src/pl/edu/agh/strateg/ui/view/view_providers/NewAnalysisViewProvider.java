package pl.edu.agh.strateg.ui.view.view_providers;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import pl.edu.agh.strateg.security.Roles;
import pl.edu.agh.strateg.ui.Views;
import pl.edu.agh.strateg.ui.controller.main.NewAnalysisController;
import pl.edu.agh.strateg.ui.view.views.AccessDeniedView;
import pl.edu.agh.strateg.ui.view.views.NewAnalysisView;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewProvider;

public class NewAnalysisViewProvider implements ViewProvider {
	
	private NewAnalysisController nowaAnalizaController;
	
	private Navigator.ClassBasedViewProvider provider = 
			new Navigator.ClassBasedViewProvider(Views.NEW_ANALYSIS_VIEW, NewAnalysisView.class);
	
	private Navigator.ClassBasedViewProvider noPermissionProvider = 
			new Navigator.ClassBasedViewProvider(Views.NEW_ANALYSIS_VIEW, AccessDeniedView.class);
	
	public NewAnalysisViewProvider(NewAnalysisController nowaAnalizaController) {
		this.nowaAnalizaController = nowaAnalizaController;
	}
	
	@Override
	public String getViewName(String viewAndParameters) {
		Subject currentUser = SecurityUtils.getSubject();
		if (currentUser.isAuthenticated() && currentUser.hasRole(Roles.ADD_ANALYSIS)) {
			return provider.getViewName(viewAndParameters);
		}
		else {
			return noPermissionProvider.getViewName(viewAndParameters);
		}
	}
	
	@Override
	public View getView(String viewName) {
		Subject currentUser = SecurityUtils.getSubject();
		if (currentUser.isAuthenticated() && currentUser.hasRole(Roles.ADD_ANALYSIS)) {
			NewAnalysisView view = (NewAnalysisView) provider.getView(viewName);
			view.setControllerToAllComponents(nowaAnalizaController);
			return view;
		}
		else {
			AccessDeniedView view = (AccessDeniedView) noPermissionProvider.getView(viewName);
			return view;
		}
	}

}
