package pl.edu.agh.strateg.ui.controller.component.analysis;

import java.io.Serializable;

import pl.edu.agh.strateg.model.analysis.Analysis;
import pl.edu.agh.strateg.ui.controller.interfaces.HasAnalysisTable;
import pl.edu.agh.strateg.ui.model.documents.DocumentsDataModel;
import pl.edu.agh.strateg.ui.view.components.analysis.AbstractAnalysisTable;
import pl.edu.agh.strateg.ui.view.components.documents.ChooseDocumentWindow;

import com.vaadin.data.Validator;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

public class AnalysisTableController implements Serializable, Validator, ClickListener, CloseListener{
	
	private static final String INVALID_VALUE = "Niepoprawna wartość";
	
	protected AbstractAnalysisTable analysisTable;
	private HasAnalysisTable mainController;
	
	public AnalysisTableController(AbstractAnalysisTable analizaTable) {
		this.analysisTable = analizaTable;
	}
	
	public void setMainController(HasAnalysisTable mainController) {
		this.mainController = mainController;
		mainController.setAnalysisTableController(this);
	}
	
	public void addOpportunity(String nazwa) {
		analysisTable.addOpportunity(nazwa);
	}
	
	public void addThread(String nazwa) {
		analysisTable.addThread(nazwa);
	}
	
	public void addStrength(String nazwa) {
		analysisTable.addStrength(nazwa);
	}
	
	public void addWeakness(String nazwa) {
		analysisTable.addWeakness(nazwa);
	}

	public boolean areValuesValid() {
		return analysisTable.areValuesValid();
	}

	public Analysis getAnalysis() {
		return analysisTable.getAnalysis();
	}

	
	public boolean isValid(String text) {
		try {
			int value = Integer.valueOf(text);
			if (value < 0) {
				return false;
			}
			return true;
		}
		catch (NumberFormatException e) {
			return false;
		}
	}

	@Override
	public void validate(Object value) throws InvalidValueException {
		try {
			int liczba = Integer.valueOf((String) value);
			if (liczba < 0) {
				throw new InvalidValueException(INVALID_VALUE);
			}
		}
		catch(NumberFormatException e) {
			throw new InvalidValueException(INVALID_VALUE);
		}
		
	}

	public boolean isAnalysisEmpty() {
		return analysisTable.isAnalysisEmpty();
	}

	@Override
	public void buttonClick(ClickEvent event) {
		mainController.addFactorButtonClicked();
		
	}

	@Override
	public void windowClose(CloseEvent e) {
		Window window = e.getWindow();
		if (window instanceof ChooseDocumentWindow) {
			DocumentsDataModel model = ((ChooseDocumentWindow) window).getChosenDocument();
			if (model != null) {
				analysisTable.addAttachment(model.getUuid());
			}
		}
		
	}
	
}
