/**
	@author drew
 */

package pl.edu.agh.strateg.persistence.mongo.helpers;

public class Mappings {

	private Mappings() {
		// private-scoped
	}

	public static class Data {

		private Data() {
			// private-scoped
		}

		public final static String DATA = "data";
		public final static String CONTAINER = "container";
		// NOT for change, it's Mongo contract!
		public final static String ID = "_id";
	}

	public static class Descriptors {

		private Descriptors() {
			// private-scoped
		}

		public final static String VALUE_TYPE = "valueType";
		public final static String DESCRIBING_METADATA = "describingMeta";
		public final static String REQUIREMENTS = "requirements";
		public final static String OBLIGATORIES = "obligatories";
		public final static String BOUNDING_COLLECTION = "boundings";
		// NOT for change, it's Mongo contract!
		public final static String ID = "_id";
	}

	public static class Analyses {
		private Analyses() {
			// privaet-scoped
		}

		public final static String DESCRIBING_METADATA = "describingMeta";
		public final static String LAST_PRIMARY_ROW = "lastRow";
		public final static String LAST_PRIMARY_COL = "lastCol";
		public final static String ROW_NAMES = "rows";
		public final static String COL_NAMES = "cols";
		public final static String SWOT_FIELDS = "SWOTFileds";
		// NOT for change, it's Mongo contract!
		public final static String ID = "_id";
	}

	public static class Metadata {

		private Metadata() {
			// private-scoped
		}

		public final static String VALUE = "value";
	}

	public static class Container {

		private Container() {
			// private-scoped
		}

		public final static String METADATA = "metadata";
	}

}
