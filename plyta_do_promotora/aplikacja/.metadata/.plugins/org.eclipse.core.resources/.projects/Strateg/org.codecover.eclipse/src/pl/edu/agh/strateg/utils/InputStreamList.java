package pl.edu.agh.strateg.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class InputStreamList {

	private static InputStreamList instance;
	private static List<ResetOnCloseInputStream> streams;

	public void addStream(ResetOnCloseInputStream stream) {
		streams.add(stream);
	}

	public void clear() {
		for (ResetOnCloseInputStream stream : streams) {
			try {
				stream.reallyClose();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		streams.clear();
	}

	public static InputStreamList getInstance() {
		if (instance == null) {
			streams = new ArrayList<ResetOnCloseInputStream>();
			instance = new InputStreamList();
		}
		return instance;
	}

	public String print() {
		return streams.toString();
	}

}
