package pl.edu.agh.strateg.ui.view.components.metadata;

import pl.edu.agh.strateg.ui.controller.component.metadata.DescriptorDetailsController;
import pl.edu.agh.strateg.ui.controller.interfaces.HasMetadatumDetails;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;

import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

public class DescriptorDetailsPanel extends Panel{
	
	private static final String PANEL_CAPTION = "Metadane opisujące";
	private static final String NAZWA_CAPTION = "Nazwa";
	private static final String WARTOSC_CAPTION = "Wartość";
	
	private MetadatumModel dana;
	
	private DescriptorDetailsController controller;
	
	private Table table = new Table();
	private VerticalLayout mainLayout = new VerticalLayout();
	
	public DescriptorDetailsPanel() {
		setCaption(PANEL_CAPTION);
		setSizeFull();
		mainLayout.addComponent(table);
		setContent(mainLayout);
		
		controller = new DescriptorDetailsController(this);
		
		table.addContainerProperty(NAZWA_CAPTION, String.class,  null);
		table.addContainerProperty(WARTOSC_CAPTION, String.class,  null);
		table.setSizeFull();
		table.setPageLength(0);
	}
	
	public void setMainController(HasMetadatumDetails mainController) {
		controller.setMainController(mainController);
	}
	
	public void setDatum(MetadatumModel dana) {
		this.dana = dana;
	}
	
	public void displayTable() {
		table.removeAllItems();
		if (dana.getDescribingMetadata() != null) {
			for (MetadatumModel metadana : dana.getDescribingMetadata()) {
				table.addItem(new Object[] {
						metadana.getSimpleDatumModel().getName(), 
						metadana.getSimpleDatumModel().getStringValue()}, 
						metadana);
			}
		}
	}
	
	public void hideTable() {
		table.removeAllItems();
	}

}
