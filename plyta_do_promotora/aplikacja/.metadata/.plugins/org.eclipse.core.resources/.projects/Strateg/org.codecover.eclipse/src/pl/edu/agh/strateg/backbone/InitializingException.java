/**
	@author drew
 */

package pl.edu.agh.strateg.backbone;

public class InitializingException extends Exception {
	public InitializingException(String message) {
		super(message);
	}
}
