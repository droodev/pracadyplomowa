/**
 * @author drew
 */
package pl.edu.agh.strateg.model.metadata;

import java.util.Collection;

import pl.edu.agh.strateg.model.metadata.description.Descriptor;

import com.google.common.base.Optional;

public interface MetadataContainer {
	public static final MetadataContainer EMPTY = new EmptyMetadataContainer();

	public void addMetadatum(Metadatum metadatum);

	public <T> void removeMetadatum(Descriptor descriptor);

	public <T> Optional<Metadatum> getMetadatum(Descriptor descriptor);

	public void updateMetadatum(Metadatum alteredMetadatum);

	public int getSize();

	public Collection<Metadatum> getAll();

	public boolean contains(Descriptor descriptor);
}
