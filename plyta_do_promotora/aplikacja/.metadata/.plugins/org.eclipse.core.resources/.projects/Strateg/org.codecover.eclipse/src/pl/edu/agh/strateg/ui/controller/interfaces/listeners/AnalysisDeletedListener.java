package pl.edu.agh.strateg.ui.controller.interfaces.listeners;

public interface AnalysisDeletedListener extends DatabaseProblemListener{

	void analysisDeletedSuccessfully();
	void analysisDeletingFailed();
	
}
