/**
	@author drew
 */

package pl.edu.agh.strateg.backbone;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.apache.log4j.Logger;
import org.apache.shiro.guice.web.ShiroWebModule;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;

public class InitListener extends GuiceServletContextListener {

	private static final Logger LOG = Logger.getLogger(Thread.currentThread()
			.getStackTrace()[1].getClassName());

	private ServletContext context;
	private Injector rootInjector;

	private static Injector tempStaticRootInjector;

	@Override
	protected Injector getInjector() {
		rootInjector = Guice.createInjector(new MainModule(),
				new MainServerModule(), new ShiroModule(context),
				ShiroWebModule.guiceFilterModule());
		tempStaticRootInjector = rootInjector;
		return rootInjector;
	}

	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		LOG.debug(String.format("Context initialization started"));
		context = servletContextEvent.getServletContext();
		super.contextInitialized(servletContextEvent);
		rootInjector.getInstance(ServiceManager.class).initService();
		LOG.debug(String.format("Context initialization ended"));
	}

	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		LOG.debug(String.format("Context destroy started"));
		super.contextDestroyed(servletContextEvent);
		rootInjector.getInstance(ServiceManager.class).destroyService();
		LOG.debug(String.format("Context destroy ended"));
	}

	public static Injector getInjectorTemp() {
		return tempStaticRootInjector;
	}
}
