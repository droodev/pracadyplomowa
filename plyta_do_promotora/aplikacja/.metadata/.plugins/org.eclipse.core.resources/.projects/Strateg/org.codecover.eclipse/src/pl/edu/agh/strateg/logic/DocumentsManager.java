/**
	@author drew
 */

package pl.edu.agh.strateg.logic;

import java.util.UUID;

import pl.edu.agh.strateg.model.Document;

public interface DocumentsManager extends MetadatumQuerable<Document>,
		EntityManager<UUID, Document> {

}
