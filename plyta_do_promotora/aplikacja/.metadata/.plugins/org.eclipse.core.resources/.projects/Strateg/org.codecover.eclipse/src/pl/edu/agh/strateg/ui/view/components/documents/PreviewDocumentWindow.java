package pl.edu.agh.strateg.ui.view.components.documents;

import pl.edu.agh.strateg.security.Roles;
import pl.edu.agh.strateg.ui.model.documents.DocumentsDataModel;
import pl.edu.agh.strateg.ui.view.components.common.PreviewWindow;

import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.ui.BrowserFrame;

public class PreviewDocumentWindow extends PreviewWindow {
	
	private static final String WINDOW_CAPTION = "Podgląd dokumentu";
	
	private DocumentsDataModel daneDokumentu;
    private BrowserFrame pdfContents;
	
	public void changeDocumentsData(DocumentsDataModel daneDokumentu) {
		this.daneDokumentu = daneDokumentu;
		fillPdfContents();
		fillTable(daneDokumentu.getMetadata());
	}
	
	
	public PreviewDocumentWindow(boolean isReadOnly) {
		super(WINDOW_CAPTION, isReadOnly);
		super.setEditRole(Roles.EDIT_DOCUMENT);
		super.setDeleteRole(Roles.DELETE_DOCUMENT);
	}

	protected void initComponents() {
		
		super.initComponents();
		pdfContents = new BrowserFrame();
	    pdfContents.setSizeFull();
	    pdfContents.setHeight(13, Unit.CM);
		
	}

	private void fillPdfContents() {
		
		StreamSource s = new StreamSource() {

	        public java.io.InputStream getStream() {
	        	return daneDokumentu.getStream();
	        }
	    };
	    
	    StreamResource resource = new StreamResource(s, "document.pdf");
	    resource.setMIMEType("application/pdf");
        pdfContents.setSource(resource);

	}



	protected void initLayouts() {
		super.initLayouts();
		customLayout.addComponent(pdfContents);

	}
	

	
	public DocumentsDataModel getDocumentsData() {
		return daneDokumentu;
	}


}
