package pl.edu.agh.strateg.ui.controller.main;

import java.io.Serializable;
import java.util.List;

import pl.edu.agh.strateg.ui.Views;
import pl.edu.agh.strateg.ui.controller.component.analysis.EditAnalysisController;
import pl.edu.agh.strateg.ui.controller.component.common.EditController;
import pl.edu.agh.strateg.ui.controller.component.common.ChooseMetadataController;
import pl.edu.agh.strateg.ui.controller.component.common.ChosenMetadataController;
import pl.edu.agh.strateg.ui.controller.component.documents.PreviewController;
import pl.edu.agh.strateg.ui.controller.component.documents.SearchResultsController;
import pl.edu.agh.strateg.ui.controller.interfaces.HasEditWindow;
import pl.edu.agh.strateg.ui.controller.interfaces.HasPreviewWindow;
import pl.edu.agh.strateg.ui.controller.interfaces.HasConfirmWindow;
import pl.edu.agh.strateg.ui.controller.interfaces.HasChooseMetadata;
import pl.edu.agh.strateg.ui.controller.interfaces.HasChosenMetadata;
import pl.edu.agh.strateg.ui.controller.interfaces.HasSearchResults;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.AnalysisDeletedListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.AnalysisUpdatedListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.FoundAnalysisListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.GotAllMetadaneListener;
import pl.edu.agh.strateg.ui.model.analysis.AnalysisDataModel;
import pl.edu.agh.strateg.ui.model.common.DataModel;
import pl.edu.agh.strateg.ui.model.communication.GatewayImpl;
import pl.edu.agh.strateg.ui.model.metadata.MetadataGroup;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;
import pl.edu.agh.strateg.ui.view.components.common.EditWindow;
import pl.edu.agh.strateg.ui.view.components.common.ConfirmWindow;
import pl.edu.agh.strateg.ui.view.components.common.TitlePanel;
import pl.edu.agh.strateg.ui.view.views.AnalysesView;

import com.vaadin.navigator.Navigator;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;

public class AnalysesController implements ClickListener, HasChooseMetadata, HasChosenMetadata, Serializable, GotAllMetadaneListener, 
							HasSearchResults, HasConfirmWindow, FoundAnalysisListener, AnalysisUpdatedListener, 
								HasPreviewWindow, HasEditWindow, AnalysisDeletedListener {
	
	private Navigator navigator;
	private ChooseMetadataController chooseMetadataController;
	private ChosenMetadataController chosenMetadataController;
	private SearchResultsController searchResultsController;
	
	private static final String PYTANIE_CZY_WSZYSTKIE = "Czy na pewno chcesz wyświetlić wszystkie analizy?";
	private static final String PYTANIE_CZY_USUNAC = "Czy na pewno chcesz usunąć?";
	
	@SuppressWarnings("unused")
	private PreviewController previewController;
	private EditController editController;
	
	private AnalysesView analysesView;
	
	private AnalysisDataModel currentData;
	
	public AnalysesController(Navigator navigator) {
		this.navigator = navigator;
	}
	
	public void setAnalysesView(AnalysesView analizaView) {
		this.analysesView = analizaView;
	}
	
	@Override
	public void setChosenMetadataController(ChosenMetadataController wybraneMetadaneController) {
		this.chosenMetadataController = wybraneMetadaneController;
		
	}

	@Override
	public void setChooseMetadataController(
			ChooseMetadataController wybierzMetadaneController) {
		this.chooseMetadataController = wybierzMetadaneController;
		
	}
	
	public void setSearchResultsController(
			SearchResultsController wynikiWyszukiwaniaController) {
		this.searchResultsController = wynikiWyszukiwaniaController;
		
	}
	
	public void setPreviewController(PreviewController podgladController) {
		this.previewController = podgladController;
	}
	
	public void setEditController(EditController edytujController) {
		this.editController = edytujController;
		
	}
	

	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if (button.getCaption().equals(AnalysesView.STRONA_GLOWNA_BUTTON_CAPTION)) {
			mainPageButtonClicked();
		}
		else if (button.getCaption().equals(AnalysesView.NOWA_ANALIZA_BUTTON_CAPTION)) {
			newAnalysisButtonClicked();
		}
		else if (button.getCaption().equals(AnalysesView.SZUKAJ_BUTTON_CAPTION)) {
			searchButtonClicked();
		}
		
	}



	private void newAnalysisButtonClicked() {
		navigator.navigateTo(Views.NEW_ANALYSIS_VIEW);
		
	}

	private void mainPageButtonClicked() {
		navigator.navigateTo(Views.MAIN_VIEW);
		
	}

	private void searchButtonClicked() {
		if (chosenMetadataController.areValuesValid()) {
			if (chosenMetadataController.getAllValues().isEmpty()) {
				analysesView.displayConfirmWindow(PYTANIE_CZY_WSZYSTKIE);
			}
			else {
				sendFindQuery();
			}
		}
		else {
			analysesView.displayNotValidValuesWarning();
		}
		
	}
	
	private void sendFindQuery() {
		GatewayImpl.getInstance().findAnalyses(chosenMetadataController.getAllValues(), this);
	}

	
	public void fillChooseMetadataPanel() {
		GatewayImpl.getInstance().getAllDescriptors(this);
	}
	
	@Override
	public void chosenData(MetadatumModel metadana) {
		chosenMetadataController.datumChosen(metadana);
		chooseMetadataController.deleteDatum(metadana);
	}
	
	public void previewDatum(DataModel model) {
		this.currentData = (AnalysisDataModel) model;
		analysesView.displayPreviewWindow((AnalysisDataModel)model);
	}
	
	public void addEditWindow() {
		analysesView.displayEditWindow(currentData);
	}
	

	@Override
	public void gettingDescriptorsFailed() {
		System.out.println("Getting descriptors failed");
		
	}

	@Override
	public void descriptorsReceived(MetadataGroup grupaMetadanych) {
		chooseMetadataController.fillPanelWithData(grupaMetadanych);

		
	}
	
	@Override
	public void findingAnalysisFailed() {
		analysesView.findingAnalysisFailed();
		
	}

	@Override
	public void analysesReceived(List<DataModel> daneDokumentu) {
		searchResultsController.displayData(daneDokumentu);
		
	}
	
	@Override
	public void windowClose(CloseEvent e) {
		Window window = e.getWindow();
		if (window instanceof EditWindow) {
			if (!editController.isChangeConfirmed()) {
				analysesView.modificationCancelled();
			}
			else {
				AnalysisDataModel daneAnalizy = ((EditAnalysisController) editController).getAnalysisData();
				GatewayImpl.getInstance().updateAnalysis(daneAnalizy, this);
			}
		}
		else if (window instanceof ConfirmWindow) {
			ConfirmWindow pWindow = (ConfirmWindow) window;
			if (pWindow.isConfirmed()) {
				if (pWindow.getQuestion().equals(PYTANIE_CZY_WSZYSTKIE)) {
					sendFindQuery();
				}
				else if (pWindow.getQuestion().equals(PYTANIE_CZY_USUNAC)) {
					GatewayImpl.getInstance().deleteAnalysis(currentData, this);
				}
			}
		}
		
	}
	
	@Override
	public void analysisUpdatedSuccessfully() {
		analysesView.analysisUpdatedSuccessfully();
		sendFindQuery();
	}

	@Override
	public void analysisUpdatingFailed() {
		analysesView.analysisUpdatingFailed();
		sendFindQuery();
		
	}

	@Override
	public void datumValueChanged(MetadatumModel metadana) {
		
	}

	@Override
	public boolean canBeDeleted(MetadatumModel metadana) {
		return false;
	}

	@Override
	public void restoreDatum(MetadatumModel metadana) {
		
	}

	@Override
	public void chooseData(DataModel model) {
		
	}

	@Override
	public void databaseProblemOccured() {
		analysesView.databaseProblemOccured();
		
	}
	
	public void addNavigator(TitlePanel panel) {
		panel.setNavigator(navigator);
	}

	@Override
	public void deleteDatum() {
		analysesView.displayConfirmWindow(PYTANIE_CZY_USUNAC);
	}

	@Override
	public void analysisDeletedSuccessfully() {
		analysesView.analysisDeletedSuccessfully();
		sendFindQuery();
	}

	@Override
	public void analysisDeletingFailed() {
		analysesView.analysisDeletingFailed();
		sendFindQuery();
	}


}
