package pl.edu.agh.strateg.ui.controller.interfaces;

import pl.edu.agh.strateg.ui.controller.component.metadata.DescriptorDetailsController;

public interface HasMetadatumDetails {
	
	void setDescriptorDetailsController(DescriptorDetailsController controller);

}
