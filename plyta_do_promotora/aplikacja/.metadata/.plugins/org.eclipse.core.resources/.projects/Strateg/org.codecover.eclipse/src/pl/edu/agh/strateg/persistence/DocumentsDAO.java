/**
	@author drew
 */

package pl.edu.agh.strateg.persistence;

import java.util.UUID;

import pl.edu.agh.strateg.model.Document;

public interface DocumentsDAO extends GeneralDAO<Document, UUID> {

}