/**
	@author drew
 */

package pl.edu.agh.strateg.model.analysis;

public class ThreatAnalysisHeader extends ColumnAnalysisHeader {

	ThreatAnalysisHeader(String name, int orderingNumber) {
		super(name, orderingNumber);
	}

	@Override
	public boolean isPrimary() {
		return false;
	}

}
