/**
	@author drew
 */

package pl.edu.agh.strateg.persistence.querying;

import static pl.edu.agh.strateg.helpers.ExtendedPreconditions.*;

import java.util.ArrayList;
import java.util.List;

import pl.edu.agh.strateg.helpers.ExtendedPreconditions;
import pl.edu.agh.strateg.persistence.querying.constraint.EqualsConstraint;
import pl.edu.agh.strateg.persistence.querying.constraint.ExistConstraint;
import pl.edu.agh.strateg.persistence.querying.constraint.FieldConstraint;
import pl.edu.agh.strateg.persistence.querying.constraint.FieldConstraintVisitor;

public class MetadatumQuery {
	List<FieldConstraint> constraints;

	private MetadatumQuery(List<FieldConstraint> constraints) {
		ExtendedPreconditions.checkArgumentNotNull(constraints);
		this.constraints = constraints;
	}

	public static Builder getBuilder() {
		return new Builder();
	}

	public void accept(FieldConstraintVisitor visitor) {
		for (FieldConstraint constraint : constraints) {
			constraint.accept(visitor);
		}
	}

	public static class Builder {
		private final List<FieldConstraint> constraints = new ArrayList<FieldConstraint>();

		public MetadataCriterion metadatumName(String name) {
			checkArgumentNotNull(name);
			return new MetadataCriterion(name);
		}

		public MetadatumQuery build() {
			return new MetadatumQuery(constraints);
		}

		public class MetadataCriterion {
			private String name;

			private MetadataCriterion(String name) {
				this.name = name;
			}

			public Builder exists() {
				constraints.add(new ExistConstraint(name));
				return Builder.this;
			}

			public Builder equalTo(Object o) {
				checkArgumentNotNull(o);
				constraints.add(new EqualsConstraint(name, o));
				return Builder.this;
			}
		}
	}

}
