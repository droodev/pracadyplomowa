package pl.edu.agh.strateg.ui.controller.interfaces.listeners;

import pl.edu.agh.strateg.ui.model.common.DataModel;

public interface GotFileListener extends DatabaseProblemListener {
	
	void gettingFileFailed();
	void fileReceived(DataModel daneModel);

}
