package pl.edu.agh.strateg.ui.view.components.common;

import java.util.List;

import pl.edu.agh.strateg.security.Roles;
import pl.edu.agh.strateg.ui.controller.component.documents.PreviewController;
import pl.edu.agh.strateg.ui.controller.interfaces.HasPreviewWindow;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;
import pl.edu.agh.strateg.ui.view.components.common.access_controlled.AccessControlledButton;
import pl.edu.agh.strateg.utils.Icons;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public abstract class PreviewWindow extends Window{
	
	public static final String EDYTUJ_BUTTON_CAPTION = "Edytuj";
	public static final String USUN_BUTTON_CAPTION = "Usuń";
	private static final String NAZWA_CAPTION = "Nazwa";
	private static final String WARTOSC_CAPTION = "Wartość";
	
	private AccessControlledButton edytujButton;
	private AccessControlledButton usunButton;
	
	protected VerticalLayout customLayout;
	
	private Table table = new Table();
	
	private PreviewController controller;
	
	private boolean isReadOnly;

	public PreviewWindow(String windowCaption, boolean isReadOnly) {
		controller = new PreviewController(this);
		this.isReadOnly = isReadOnly;
		
		initComponents();
		initLayouts();
		
		setHeight(15, Unit.CM);
		setWidth(30, Unit.CM);
		setResizable(false);
		setCaption(windowCaption);
		
	}

	protected void initComponents() {
		
		edytujButton = new AccessControlledButton();
		usunButton = new AccessControlledButton();
		
		edytujButton.setCaption(EDYTUJ_BUTTON_CAPTION);
		usunButton.setCaption(USUN_BUTTON_CAPTION);
		
		edytujButton.setIcon(new ThemeResource(Icons.EDIT));
		usunButton.setIcon(new ThemeResource(Icons.DELETE));
		
		edytujButton.addClickListener(controller);
		usunButton.addClickListener(controller);
		
		table.addContainerProperty(NAZWA_CAPTION, String.class,  null);
		table.addContainerProperty(WARTOSC_CAPTION, Object.class,  null);
		table.setSizeFull();
		table.setPageLength(0);
		
	}

	protected void initLayouts() {
		VerticalLayout layout = new VerticalLayout();
		HorizontalLayout mainLayout = new HorizontalLayout();
		
		HorizontalLayout buttonLayout = new HorizontalLayout();
		VerticalLayout daneLayout = new VerticalLayout();
		
		daneLayout.setSizeFull();
		mainLayout.setSizeFull();
		
		buttonLayout.setSpacing(true);
		daneLayout.setSpacing(true);
		
		mainLayout.setMargin(true);
		mainLayout.setSpacing(true);
		
		buttonLayout.addComponent(edytujButton);
		buttonLayout.addComponent(usunButton);
		
		daneLayout.addComponent(buttonLayout);
		daneLayout.addComponent(table);
		
		customLayout = new VerticalLayout();
		
		mainLayout.addComponent(daneLayout);
		mainLayout.addComponent(customLayout);
		
		layout.addComponent(mainLayout);
		setContent(layout);
		
		if (isReadOnly) {
			edytujButton.setVisible(false);
			usunButton.setVisible(false);
		}

		
	}
	
	protected void setEditRole(String role) {
		edytujButton.setRole(Roles.EDIT_DOCUMENT);
		edytujButton.setEnabled(true);
	}
	
	protected void setDeleteRole(String role) {
		usunButton.setRole(Roles.DELETE_DOCUMENT);
		usunButton.setEnabled(true);
	}
	
	public void fillTable(List<MetadatumModel> metadane) {
		table.removeAllItems();
		try {
			for (MetadatumModel metadana : metadane) {
				table.addItem(new Object[] {
						metadana.getSimpleDatumModel().getName(), 
						metadana.getSimpleDatumModel().getStringValue()}, 
						metadana);
			}
				
		}
		catch(NullPointerException e) {
			return;
		}
	}
	
	public void setMainController(HasPreviewWindow mainController) {
		controller.setMainController(mainController);
	}

}
