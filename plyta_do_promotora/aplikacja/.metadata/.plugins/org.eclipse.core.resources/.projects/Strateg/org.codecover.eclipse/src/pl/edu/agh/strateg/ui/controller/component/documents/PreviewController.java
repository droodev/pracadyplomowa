package pl.edu.agh.strateg.ui.controller.component.documents;

import java.io.Serializable;

import pl.edu.agh.strateg.ui.controller.interfaces.HasEditWindow;
import pl.edu.agh.strateg.ui.controller.interfaces.HasPreviewWindow;
import pl.edu.agh.strateg.ui.view.components.common.PreviewWindow;
import pl.edu.agh.strateg.ui.view.components.documents.PreviewDocumentWindow;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public class PreviewController implements ClickListener, Serializable {
	
	private PreviewWindow previewWindow;
	private HasPreviewWindow mainController;
	
	public PreviewController(PreviewWindow podgladWindow) {
		this.previewWindow = podgladWindow;
	}

	public void setMainController(HasPreviewWindow mainController) {
		this.mainController = mainController;
		mainController.setPreviewController(this);
	}


	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if (button.getCaption().equals(PreviewDocumentWindow.EDYTUJ_BUTTON_CAPTION)) {
			editButtonClicked();
		}
		else if (button.getCaption().equals(PreviewDocumentWindow.USUN_BUTTON_CAPTION)) {
			deleteButtonClicked();
		}
		
		
	}


	private void editButtonClicked() {
		previewWindow.close();
		((HasEditWindow)mainController).addEditWindow();
	}

	private void deleteButtonClicked() {
		previewWindow.close();
		mainController.deleteDatum();
	}


}
