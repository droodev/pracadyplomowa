/**
	@author drew
 */

package pl.edu.agh.strateg.model.metadata.description;

public enum Domain {
	DOCUMENT, ANALYSIS, REPORT, DESCRIPTOR;
}
