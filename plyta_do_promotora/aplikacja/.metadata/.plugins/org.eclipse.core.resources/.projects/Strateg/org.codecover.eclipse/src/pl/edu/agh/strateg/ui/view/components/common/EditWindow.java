package pl.edu.agh.strateg.ui.view.components.common;

import pl.edu.agh.strateg.ui.controller.component.common.EditController;
import pl.edu.agh.strateg.ui.controller.interfaces.HasEditWindow;
import pl.edu.agh.strateg.utils.Icons;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public abstract class EditWindow extends Window{
	
	public static final String POTWIERDZ_BUTTON_CAPTION = "Potwierdź";
	public static final String ANULUJ_BUTTON_CAPTION = "Anuluj";
	
	private static final String NOT_VALID_VALUES_WARNING = "Wprowadzone wartości nie są poprawne";
	private static final String DATABASE_PROBLEM = "Problem przy zapisie do bazy";
	private static final String WYBIERZ_METADANE_CAPTION = "Wybierz metadane";
	private static final String WYBRANE_METADANE_CAPTION = "Wybrane metadane";
	
	protected EditController controller;
	
	private Button potwierdzButton;
	private Button anulujButton;
	
	private ChooseMetadataPanel wybierzMetadanePanel;
	private ChosenMetadataPanel wybraneMetadanePanel;
	
	protected HorizontalLayout customElementsTopLayout;
	protected HorizontalLayout customElementsBottomLayout;
	
	public EditWindow(String windowCaption) {
		
		setHeight(15, Unit.CM);
		setWidth(30, Unit.CM);
		setResizable(false);
		setCaption(windowCaption);
	}

	protected void init() {
		initComponents();
		initLayouts();
		setControllerToAllComponents();
		controller.fillWybierzMetadanePanel();
	}
	
	public void setMainController(HasEditWindow mainController) {
		addCloseListener(mainController);
		controller.setMainController(mainController);
	}
	
	public void displayNotValidValuesWarning() {
		Notification.show(NOT_VALID_VALUES_WARNING, Type.ERROR_MESSAGE);
		
	}
	
	protected void initComponents() {
		potwierdzButton = new Button(POTWIERDZ_BUTTON_CAPTION);
		anulujButton = new Button(ANULUJ_BUTTON_CAPTION);
		
		potwierdzButton.setIcon(new ThemeResource(Icons.CONFIRM));
		anulujButton.setIcon(new ThemeResource(Icons.CANCEL));
		
		wybierzMetadanePanel = new ChooseMetadataPanel(WYBIERZ_METADANE_CAPTION);
		wybraneMetadanePanel = new ChosenMetadataPanel(WYBRANE_METADANE_CAPTION, true);
		
	}
	
	protected void initLayouts() {
		customElementsTopLayout = new HorizontalLayout();
		customElementsBottomLayout = new HorizontalLayout();
		
		VerticalLayout layout = new VerticalLayout();
		
		VerticalLayout mainLayout = new VerticalLayout();
		
		HorizontalLayout upperLayout = new HorizontalLayout();
		VerticalLayout componentsLayout = new VerticalLayout();
		
		upperLayout.setSizeFull();
		componentsLayout.setSizeFull();
		mainLayout.setSizeFull();
		
		upperLayout.setSpacing(true);
		componentsLayout.setSpacing(true);
		
		mainLayout.setMargin(true);
		mainLayout.setSpacing(true);
		
		HorizontalLayout buttonLayout = new HorizontalLayout();
		buttonLayout.addComponent(potwierdzButton);
		buttonLayout.addComponent(anulujButton);
		
		upperLayout.addComponent(wybierzMetadanePanel);
		upperLayout.addComponent(buttonLayout);
		
		upperLayout.setExpandRatio(wybierzMetadanePanel, 0.75f);
		upperLayout.setExpandRatio(buttonLayout, 0.25f);
		
		upperLayout.setComponentAlignment(buttonLayout, Alignment.TOP_RIGHT);
		
		buttonLayout.setSpacing(true);

		componentsLayout.addComponent(customElementsTopLayout);
		componentsLayout.addComponent(wybraneMetadanePanel);
		componentsLayout.addComponent(customElementsBottomLayout);
		
		
		componentsLayout.setComponentAlignment(customElementsTopLayout, Alignment.TOP_LEFT);
		
		mainLayout.addComponent(upperLayout);
		mainLayout.addComponent(componentsLayout);
		
		layout.addComponent(mainLayout);
		setContent(layout);
		
		
	}
	
	
	protected void setControllerToAllComponents() {
		wybierzMetadanePanel.setMainController(controller);
		wybraneMetadanePanel.setMainController(controller);
		setListenerToButtons();
	}

	private void setListenerToButtons() {
		potwierdzButton.addClickListener(controller);
		anulujButton.addClickListener(controller);
		
	}
	
	public void databaseProblemOccured() {
		Notification.show(DATABASE_PROBLEM, Type.ERROR_MESSAGE);
		
	}

}
