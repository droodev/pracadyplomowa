package pl.edu.agh.strateg.ui.view.components.common.insert;

import java.util.Date;

import pl.edu.agh.strateg.ui.controller.component.common.InsertDatumValueController;
import pl.edu.agh.strateg.utils.CustomDateFormatter;

import com.vaadin.ui.AbstractField;
import com.vaadin.ui.DateField;

public class InsertDate implements InsertComponent {
	
	private static final String NIEPOPRAWNY_FORMAT_DATY_CAPTION = "Niepoprawny format daty";
	private static final String NIEPOPRAWNY_TYP_ERROR = "Niepoprawny typ";
	
	private DateField field;

	public InsertDate(InsertDatumValueController controller) {
		field =  new DateField();
		field.setDateFormat(CustomDateFormatter.getDateFormat());
		field.setParseErrorMessage(NIEPOPRAWNY_FORMAT_DATY_CAPTION);
		field.setValue(new Date());
		field.setRequired(true);
		field.setImmediate(true);
		field.addValueChangeListener(controller);
	}

	@Override
	public Object getValue() {
		return field.getValue();
	}

	@Override
	public AbstractField<?> getField() {
		return field;
	}

	@Override
	public boolean isValid() {
		return field.isValid();
	}

	@Override
	public void setValue(Object object) {
		if (!(object instanceof Date)) {
			throw new RuntimeException(NIEPOPRAWNY_TYP_ERROR);
		}
		field.setValue((Date) object);
		
	}

}
