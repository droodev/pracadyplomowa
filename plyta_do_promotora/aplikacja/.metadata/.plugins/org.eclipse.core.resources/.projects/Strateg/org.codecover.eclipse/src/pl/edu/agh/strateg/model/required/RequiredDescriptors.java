/**
	@author drew
 */

package pl.edu.agh.strateg.model.required;

import java.util.Iterator;
import java.util.LinkedList;

import pl.edu.agh.strateg.model.metadata.MapMetadataContainer;
import pl.edu.agh.strateg.model.metadata.SimpleMetadatum;
import pl.edu.agh.strateg.model.metadata.description.CollectionLimitedDescriptor;
import pl.edu.agh.strateg.model.metadata.description.Descriptor;
import pl.edu.agh.strateg.model.metadata.description.Domain;
import pl.edu.agh.strateg.model.metadata.description.Requirements;
import pl.edu.agh.strateg.model.metadata.description.SimpleDescriptor;

import com.google.common.collect.Lists;

public class RequiredDescriptors implements Iterable<Descriptor> {

	private static final String PERIOD_NAME = "Okres";
	private static final String DATE_NAME = "Data";
	private static final String GOAL_NAME = "Priorytet";

	public static final Descriptor DOCUMENT_NAME;
	public static final Descriptor CATEGORY;
	public static final Descriptor GOAL;
	public static final Descriptor DATE;

	public static final Descriptor PERIOD;

	static {
		DOCUMENT_NAME = SimpleDescriptor
				.getCreator()
				.withObligatories(
						Lists.newArrayList(Domain.DOCUMENT, Domain.REPORT,
								Domain.ANALYSIS)).createString("Nazwa danej");

		// Enums changed to strings;/ not beautiful, but needed to use with
		// morphia
		CATEGORY = CollectionLimitedDescriptor
				.createLimitedDescriptor(
						Lists.newArrayList(Category.toStringArray()),
						SimpleDescriptor
								.getCreator()
								.withObligatories(
										Lists.newArrayList(Domain.DESCRIPTOR))
								.withRequirements(
										Requirements
												.getBuilder()
												.addRequirement(
														Domain.DESCRIPTOR)
												.conditional(
														Lists.newArrayList(
																GOAL_NAME,
																DATE_NAME),
														Category.OBSERVABLE
																.toString())
												.addRequirement(
														Domain.DESCRIPTOR)
												.conditional(
														Lists.newArrayList(PERIOD_NAME),
														Category.GOAL
																.toString())
												.build())
								.createString("Kategoria"));

		DATE = SimpleDescriptor
				.getCreator()
				.withDescribingMetada(
						new MapMetadataContainer(Lists
								.newArrayList(new SimpleMetadatum(CATEGORY,
										Category.GENERAL.toString()))))
				.createDate(DATE_NAME);

		PERIOD = SimpleDescriptor
				.getCreator()
				.withDescribingMetada(
						new MapMetadataContainer(Lists
								.newArrayList(new SimpleMetadatum(CATEGORY,
										Category.GENERAL.toString()))))
				.createPeriod(PERIOD_NAME);

		GOAL = CollectionLimitedDescriptor.createLimitedDescriptor(
				java.util.Collections.EMPTY_LIST,
				SimpleDescriptor
						.getCreator()
						.withDescribingMetada(
								new MapMetadataContainer(Lists
										.newArrayList(new SimpleMetadatum(
												CATEGORY, Category.GENERAL
														.toString()))))
						.createString(GOAL_NAME));

	}

	public static final LinkedList<Descriptor> ORDERED_DESCRIPTORS_LIST = Lists
			.newLinkedList(Lists.newArrayList(DOCUMENT_NAME, CATEGORY, DATE,
					PERIOD, GOAL));

	public static Iterator<Descriptor> getIterator() {
		return new RequiredDescriptors().iterator();
	}

	@Override
	public Iterator<Descriptor> iterator() {
		return new Iterator<Descriptor>() {
			Iterator<Descriptor> iterator = ORDERED_DESCRIPTORS_LIST.iterator();

			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}

			@Override
			public Descriptor next() {
				return iterator.next();
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}

}
