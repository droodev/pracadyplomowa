package pl.edu.agh.strateg.ui.controller.interfaces;

import pl.edu.agh.strateg.ui.model.common.DataModel;

public interface HasChosenDocument {
	
	DataModel getChosenDocument();

}
