package pl.edu.agh.strateg.ui.controller.component.documents;

import java.io.Serializable;
import java.util.List;

import pl.edu.agh.strateg.ui.controller.interfaces.HasSearchResults;
import pl.edu.agh.strateg.ui.model.common.DataModel;
import pl.edu.agh.strateg.ui.view.components.common.SearchResultsPanel;
import pl.edu.agh.strateg.ui.view.views.AbstractDocumentsView;
import pl.edu.agh.strateg.ui.view.views.DocumentsView;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public class SearchResultsController implements ClickListener, Serializable {
	
	private SearchResultsPanel panel;
	private HasSearchResults mainController;
	
	public SearchResultsController(SearchResultsPanel panel) {
		this.panel = panel;
	}
	
	public void setMainController(HasSearchResults mainController) {
		this.mainController = mainController;
		mainController.setSearchResultsController(this);
	}

	
	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if (button.getCaption().equals(AbstractDocumentsView.PODGLAD_DOKUMENTU_BUTTON_CAPTION)) {
			DataModel model = (DataModel) button.getData();
			mainController.previewDatum(model);
		}
		else if (button.getCaption().equals(DocumentsView.WYBIERZ_BUTTON_CAPTION)) {
			DataModel model = (DataModel) button.getData();
			mainController.chooseData(model);
		}
		
	}
	
	public void displayData(List<DataModel> dane) {
		panel.displayData(dane);
	}
	
	public void reset() {
		panel.reset();
	}

}
