package pl.edu.agh.strateg.ui.view.components.analysis;

import pl.edu.agh.strateg.model.analysis.Analysis;
import pl.edu.agh.strateg.ui.controller.component.analysis.ReadOnlyAnalysisTableController;
import pl.edu.agh.strateg.ui.model.documents.DocumentsDataModel;
import pl.edu.agh.strateg.ui.view.components.documents.PreviewDocumentWindow;

import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

public class ReadOnlyAnalysisTable extends AbstractAnalysisTable{
	
	private static final String PREVIEW = "Zobacz załącznik";
	private static final String NIE_UDALO_SIE_POBRAC = "Nie udało się pobrać załącznika";
	
	private PreviewDocumentWindow podgladDokumentuWindow;
	
	public ReadOnlyAnalysisTable() {
		super();
		controller = new ReadOnlyAnalysisTableController(this);
		tableLayout.setWidth("600px");
		dodajCzynnikButton.setVisible(false);
	}
	
	@Override
	protected MenuBar createMenuBar(int col, int row) {
		
		Command wyswietlDokumentCommand = new Command() {
			
			@Override
			public void menuSelected(MenuItem selectedItem) {
				displayPreviewDocumentWindow(menuItems.get(selectedItem));
				
			}
		};
		MenuBar menu = new MenuBar();
		
		MenuItem item = menu.addItem("", null, null);
		MenuItem itemWithCommand = item.addItem(PREVIEW, null, wyswietlDokumentCommand);
		menuItems.put(itemWithCommand, new Wspolrzedne(col, row));
		menuBars.put(itemWithCommand, menu);
		
		menu.setWidth("10px");
		menu.setAutoOpen(true);
		
		return menu;
	}
	
	protected void displayPreviewDocumentWindow(Wspolrzedne wspolrzedne) {
		podgladDokumentuWindow = new PreviewDocumentWindow(true);
		((ReadOnlyAnalysisTableController)controller).findDocument(zalaczniki.get(wspolrzedne));
		getUI().addWindow(podgladDokumentuWindow);
		podgladDokumentuWindow.setModal(true);
		
	}

	@Override
	protected Component createField() {
		Label label = new Label();
		return label;
	}
	
	
	protected void setFieldValue(int i, int j, int wartosc) {
		HorizontalLayout layout = (HorizontalLayout) tableLayout.getComponent(i, j);
		Label label = (Label)layout.getComponent(0);
		label.setValue(String.valueOf(wartosc));
	}

	@Override
	protected String getFieldValue(int i, int j) {
		HorizontalLayout layout = (HorizontalLayout) tableLayout.getComponent(i, j);
		Label label = (Label)layout.getComponent(0);
		return label.getValue();
	}
	
	@Override
	public void fillTable(Analysis analysis) {
		super.fillTable(analysis);
		for (MenuItem menuItem : menuItems.keySet()) {
			Wspolrzedne wsp = menuItems.get(menuItem);
			if (zalaczniki.get(wsp) == null) {
				menuItem.getParent().setVisible(false);
			}
		}
	}
	
	public void gotData(DocumentsDataModel dane) {
		podgladDokumentuWindow.changeDocumentsData(dane);
	}
	
	public void displayError() {
		Notification.show(NIE_UDALO_SIE_POBRAC, Type.ERROR_MESSAGE);
	}
	
}
