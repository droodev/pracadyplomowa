package pl.edu.agh.strateg.ui.controller.component.documents;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import pl.edu.agh.strateg.ui.controller.component.common.EditController;
import pl.edu.agh.strateg.ui.model.common.DataTypes;
import pl.edu.agh.strateg.ui.model.documents.DocumentsDataModel;
import pl.edu.agh.strateg.ui.model.metadata.MetadataGroup;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;
import pl.edu.agh.strateg.ui.view.components.common.EditWindow;
import pl.edu.agh.strateg.ui.view.components.documents.EditDocumentWindow;

import com.vaadin.ui.Upload.Receiver;

public class EditDocumentController extends EditController implements Receiver{
	
	private DocumentsDataModel documentsDataModel;
	
	private File file;
	
	public EditDocumentController(EditWindow edytujWindow, DocumentsDataModel daneDokumentu) {
		super(edytujWindow, DataTypes.DOKUMENT);
		this.documentsDataModel = daneDokumentu;
	}
	
	@Override
	protected void confirmButtonClicked() {
		super.confirmButtonClicked();
		if (file != null) {
			try {
				documentsDataModel.setStream(new FileInputStream(file));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		documentsDataModel.setMetadata(chosenMetadataController.getAllValues());
		editWindow.close();
		
	}

	
	@Override
	public OutputStream receiveUpload(String filename, String mimeType) {
        FileOutputStream fos = null; 
        try {
            file = new File(filename);
            fos = new FileOutputStream(file);
        } catch (final java.io.FileNotFoundException e) {
        	((EditDocumentWindow)editWindow).displayUploadProbleWarning();
        	System.out.println(file.getAbsolutePath());
            return null;
        }
        ((EditDocumentWindow)editWindow).successfullyUploaded(filename);
        return fos;
	}
	
	
	public DocumentsDataModel getDocumentData() {
		return documentsDataModel;
	}


	@Override
	public void descriptorsReceived(MetadataGroup grupaMetadanych) {
		super.descriptorsReceived(grupaMetadanych);
		for (MetadatumModel m : documentsDataModel.getMetadata()) {
			chosenData(m);
			chosenMetadataController.setValue(m, m.getSimpleDatumModel().getValue());
		}
	}

}
