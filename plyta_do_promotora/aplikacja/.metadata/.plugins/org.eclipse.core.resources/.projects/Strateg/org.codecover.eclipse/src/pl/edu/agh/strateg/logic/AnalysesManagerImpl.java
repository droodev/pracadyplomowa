/**
	@author drew
 */

package pl.edu.agh.strateg.logic;

import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import pl.edu.agh.strateg.logic.async.OperationFlowTemplate;
import pl.edu.agh.strateg.logic.async.OperationFlowTemplate.Operation;
import pl.edu.agh.strateg.logic.async.OperationListener;
import pl.edu.agh.strateg.logic.verification.VerificationStatus;
import pl.edu.agh.strateg.logic.verification.Verifier;
import pl.edu.agh.strateg.model.analysis.Analysis;
import pl.edu.agh.strateg.persistence.AnalysesDAO;
import pl.edu.agh.strateg.persistence.PersistenceException;
import pl.edu.agh.strateg.persistence.querying.MetadatumQuery;

import com.google.common.base.Optional;

public class AnalysesManagerImpl implements AnalysesManager {

	private Verifier verifier;
	private AnalysesDAO analysesDAO;

	@Inject
	private AnalysesManagerImpl(Verifier verifier, AnalysesDAO analysesDAO) {
		this.verifier = verifier;
		this.analysesDAO = analysesDAO;
	}

	@Override
	public void addEntity(final Analysis entity,
			OperationListener<Boolean> listener) {
		new OperationFlowTemplate<Boolean>(listener, new Operation<Boolean>() {

			@Override
			public VerificationStatus validation() {
				return verifier.verifyAnalysis(entity,
						Optional.<Object> absent());
			}

			@Override
			public Boolean operate() throws PersistenceException {
				return analysesDAO.saveIfNotExists(entity);
			}

		}).execute();

	}

	@Override
	public void deleteEntity(final Analysis entity,
			OperationListener<Boolean> listener) {
		new OperationFlowTemplate<Boolean>(listener, new Operation<Boolean>() {

			@Override
			public VerificationStatus validation() {
				return verifier.verifyAnalysis(entity,
						Optional.<Object> absent());
			}

			@Override
			public Boolean operate() throws PersistenceException {
				return analysesDAO.deleteIfExists(entity);
			}

		}).execute();

	}

	@Override
	public void updateEntity(final Analysis entity,
			OperationListener<Boolean> listener) {
		new OperationFlowTemplate<Boolean>(listener, new Operation<Boolean>() {

			@Override
			public VerificationStatus validation() {
				return verifier.verifyAnalysis(entity,
						Optional.<Object> absent());
			}

			@Override
			public Boolean operate() throws PersistenceException {
				return analysesDAO.updateIfExists(entity);
			}

		}).execute();
	}

	@Override
	public void getEntity(final UUID id,
			OperationListener<Optional<Analysis>> listener) {
		new OperationFlowTemplate<Optional<Analysis>>(listener,
				new Operation<Optional<Analysis>>() {

					@Override
					public VerificationStatus validation() {
						return VerificationStatus.OK;
					}

					@Override
					public Optional<Analysis> operate()
							throws PersistenceException {
						return analysesDAO.get(id);
					}

				}).execute();

	}

	@Override
	public void getAllEntities(OperationListener<List<Analysis>> listener) {
		new OperationFlowTemplate<List<Analysis>>(listener,
				new Operation<List<Analysis>>() {

					@Override
					public VerificationStatus validation() {
						return VerificationStatus.OK;
					}

					@Override
					public List<Analysis> operate() throws PersistenceException {
						return analysesDAO.getAll();
					}

				}).execute();

	}

	@Override
	public void queryByDescriptors(final MetadatumQuery query,
			OperationListener<List<Analysis>> listener) {
		new OperationFlowTemplate<List<Analysis>>(listener,
				new Operation<List<Analysis>>() {

					@Override
					public VerificationStatus validation() {
						return VerificationStatus.OK;
					}

					@Override
					public List<Analysis> operate() throws PersistenceException {
						return analysesDAO.queryByMetadata(query);
					}

				}).execute();
	}
}
