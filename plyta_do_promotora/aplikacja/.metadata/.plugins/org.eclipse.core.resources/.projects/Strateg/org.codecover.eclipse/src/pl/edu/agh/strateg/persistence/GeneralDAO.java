/**
	@author drew
 */

package pl.edu.agh.strateg.persistence;

import java.util.List;

import pl.edu.agh.strateg.persistence.querying.MetadatumQuery;

import com.google.common.base.Optional;

//TODO What about some exceptions which may occur, connected with null pointers for example

public interface GeneralDAO<T, K> {

	public boolean deleteIfExists(T entity) throws PersistenceException;

	public boolean saveIfNotExists(T entity) throws PersistenceException;

	public boolean updateIfExists(T entity) throws PersistenceException;

	public Optional<T> get(K keyName) throws PersistenceException;

	public List<T> getAll() throws PersistenceException;

	public List<T> queryByMetadata(MetadatumQuery query)
			throws PersistenceException;

	public boolean exists(T entity) throws PersistenceException;
}
