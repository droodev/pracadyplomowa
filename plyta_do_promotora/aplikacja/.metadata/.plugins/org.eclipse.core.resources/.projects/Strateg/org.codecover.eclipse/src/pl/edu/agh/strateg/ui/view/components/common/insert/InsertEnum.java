package pl.edu.agh.strateg.ui.view.components.common.insert;

import java.util.List;

import pl.edu.agh.strateg.ui.controller.component.common.InsertDatumValueController;

import com.vaadin.ui.AbstractField;
import com.vaadin.ui.ComboBox;

public class InsertEnum implements InsertComponent {
	
	private static final String NIEPOPRAWNY_TYP_ERROR = "Niepoprawny typ";

	private ComboBox field;
	private List<Object> listaMozliwosci;

	public InsertEnum(List<Object> listaMozliwosci, InsertDatumValueController controller) {
		this.listaMozliwosci = listaMozliwosci;
		field = new ComboBox(null, listaMozliwosci);
		field.setImmediate(true);
		field.setRequired(true);
		field.setNullSelectionAllowed(false);
		field.setTextInputAllowed(false);
		if (!listaMozliwosci.isEmpty()) {
			field.setValue(listaMozliwosci.get(0));
		}
		field.addValueChangeListener(controller);
	}

	@Override
	public Object getValue() {
		return field.getValue();
	}

	@Override
	public AbstractField<?> getField() {
		return field;
	}

	@Override
	public boolean isValid() {
		return field.isValid();
	}
	
	@Override
	public void setValue(Object object) {
		if (!(listaMozliwosci.contains(object))) {
			throw new RuntimeException(NIEPOPRAWNY_TYP_ERROR);
		}
		field.setValue(object);
		
	}

}
