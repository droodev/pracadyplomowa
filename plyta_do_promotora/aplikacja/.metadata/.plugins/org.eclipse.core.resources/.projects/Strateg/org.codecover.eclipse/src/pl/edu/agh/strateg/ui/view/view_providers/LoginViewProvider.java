package pl.edu.agh.strateg.ui.view.view_providers;

import pl.edu.agh.strateg.ui.Views;
import pl.edu.agh.strateg.ui.controller.main.LoginController;
import pl.edu.agh.strateg.ui.view.views.LoginView;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewProvider;

public class LoginViewProvider implements ViewProvider{
	
	private LoginController loginController;
	
	private Navigator.ClassBasedViewProvider provider = 
			new Navigator.ClassBasedViewProvider(Views.LOGIN_VIEW, LoginView.class);
	
	public LoginViewProvider(LoginController loginController) {
		this.loginController = loginController;
	}
	
	@Override
	public String getViewName(String viewAndParameters) {
		return provider.getViewName(viewAndParameters);
	}
	
	@Override
	public View getView(String viewName) {
		LoginView view = (LoginView) provider.getView(viewName);
		view.setController(loginController);
		return view;
	}

}
