package pl.edu.agh.strateg.ui.controller.interfaces.listeners;

public interface FileCreatedListener extends DatabaseProblemListener {
	
	void fileCreatedSuccessfully();
	void fileCreationFailed();

}
