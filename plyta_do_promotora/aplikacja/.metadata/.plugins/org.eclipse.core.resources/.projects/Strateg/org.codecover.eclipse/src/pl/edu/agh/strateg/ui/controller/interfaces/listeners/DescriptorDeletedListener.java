package pl.edu.agh.strateg.ui.controller.interfaces.listeners;

public interface DescriptorDeletedListener extends DatabaseProblemListener{

	void descriptorDeletedSuccessfully();
	void descriptorDeletingFailed();
	
}
