package pl.edu.agh.strateg.ui.controller.component.common;

import java.io.Serializable;
import java.util.List;

import pl.edu.agh.strateg.ui.controller.interfaces.HasChosenMetadata;
import pl.edu.agh.strateg.ui.model.common.SimpleDatumModel;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;
import pl.edu.agh.strateg.ui.view.components.common.ChosenMetadataPanel;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public class ChosenMetadataController implements ClickListener, Serializable {
	
	private ChosenMetadataPanel panel;
	private HasChosenMetadata mainController;
	
	public ChosenMetadataController(ChosenMetadataPanel panel) {
		this.panel = panel;
	}
	
	public void datumChosen(MetadatumModel dana) {
		panel.addFilter(dana);
	}
	
	public void setMainController(HasChosenMetadata mainController) {
		this.mainController = mainController;
		mainController.setChosenMetadataController(this);
	}
	
	public List<MetadatumModel> getAllValues() {
		return panel.getAllValues();
	}
	
	public boolean areValuesValid() {
		return panel.areFieldsValid();
	}
	
	public boolean contains(String nazwaMetadanej) {
		return panel.contains(nazwaMetadanej);
	}
	
	public void setValue(MetadatumModel metadana, Object wartosc) {
		panel.setValue(metadana, wartosc);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		MetadatumModel metadana = (MetadatumModel) button.getData();
		if (mainController.canBeDeleted(metadana)) {
			panel.removeFilter(metadana);
			mainController.restoreDatum(metadana);
		}
		else {
			panel.cannotBeDeleted();
		}
		
	}
	
	public void datumValueChanged(SimpleDatumModel model) {
		MetadatumModel metadana = panel.getMetadatum(model.getName());
		metadana.getSimpleDatumModel().setValue(model.getValue());
		mainController.datumValueChanged(metadana);
	}

}
