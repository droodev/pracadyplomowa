/**
	@author drew
 */

package pl.edu.agh.strateg.persistence.mongo.proxy;

import java.util.HashMap;
import java.util.Map;

import pl.edu.agh.strateg.helpers.ExtendedPreconditions;
import pl.edu.agh.strateg.model.metadata.MapMetadataContainer;
import pl.edu.agh.strateg.model.metadata.MetadataContainer;
import pl.edu.agh.strateg.model.metadata.Metadatum;
import pl.edu.agh.strateg.persistence.mongo.helpers.Mappings;

import com.google.code.morphia.annotations.Embedded;

public class ContainerProxy {

	@Embedded(value = Mappings.Container.METADATA)
	private final Map<DescriptorProxy, MetadatumProxy> metadata = new HashMap<DescriptorProxy, MetadatumProxy>();

	private boolean isEmpty = false;

	private ContainerProxy() {
		// for DB MAPPING
	}

	private ContainerProxy(MetadataContainer container) {
		ExtendedPreconditions.checkArgumentNotNull(container);
		if (container.getSize() == 0) {
			isEmpty = true;
			return;
		}
		for (Metadatum m : container.getAll()) {
			MetadatumProxy rawMetadatum = MetadatumProxy.proxify(m);
			metadata.put(rawMetadatum.getDescriptor(), rawMetadatum);
		}
	}

	public MetadataContainer deproxify() {
		MapMetadataContainer deproxified = new MapMetadataContainer();
		if (isEmpty) {
			return MetadataContainer.EMPTY;
		}
		for (MetadatumProxy m : metadata.values()) {
			deproxified.addMetadatum(m.deproxify());
		}
		return deproxified;
	}

	public static ContainerProxy proxify(MetadataContainer container) {
		return new ContainerProxy(container);
	}
}
