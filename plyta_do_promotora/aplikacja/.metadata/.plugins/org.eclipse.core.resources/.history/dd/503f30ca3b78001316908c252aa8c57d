package pl.edu.agh.strateg.ui.view.components.analysis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import pl.edu.agh.strateg.model.analysis.Analysis;
import pl.edu.agh.strateg.model.analysis.SimpleAnalysis;
import pl.edu.agh.strateg.model.analysis.ColumnAnalysisHeader;
import pl.edu.agh.strateg.model.analysis.OpportunityAnalysisHeader;
import pl.edu.agh.strateg.model.analysis.RowAnalysisHeader;
import pl.edu.agh.strateg.model.analysis.SWOTField;
import pl.edu.agh.strateg.model.analysis.StrengthAnalysisHeader;
import pl.edu.agh.strateg.ui.controller.component.analysis.AnalysisTableController;
import pl.edu.agh.strateg.ui.controller.interfaces.HasAnalysisTable;
import pl.edu.agh.strateg.utils.Icons;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public abstract class AbstractAnalysisTable extends VerticalLayout {
	
	public static final String DODAJ_CZYNNIK_BUTTON_CAPTION = "Dodaj czynnik";
	
	private static final String DATABASE_PROBLEM = "Problem przy dost�pie do bazy";
	
	private VerticalLayout mainLayout;
	
	protected GridLayout tableLayout;
	protected Button dodajCzynnikButton;
	
	protected Map<Wspolrzedne, UUID> zalaczniki= new HashMap<Wspolrzedne, UUID>();
	protected Map<MenuItem, Wspolrzedne> menuItems = new HashMap<MenuItem, Wspolrzedne>();
	protected Map<MenuItem, MenuBar> menuBars = new HashMap<MenuItem, MenuBar>();
	protected MenuItem wybraneMenu;
	
	private int ileSzans = 0;
	private int ileZagrozen = 0;
	private int ileSil = 0;
	private int ileSlabosci = 0;
	
	protected AnalysisTableController controller;
	
	protected class Wspolrzedne {
		int x;
		int y;
		
		public Wspolrzedne(int x, int y) {
			this.x = x;
			this.y = y;
		}
		
		@Override
		public String toString() {
			return String.valueOf(x) + ", " + String.valueOf(y);
		}
	}
	
	public AbstractAnalysisTable() {
		initComponents();
		initLayouts();
	}


	private void initComponents() {
		dodajCzynnikButton = new Button(DODAJ_CZYNNIK_BUTTON_CAPTION);
		dodajCzynnikButton.setIcon(new ThemeResource(Icons.NEW));
	}


	private void initLayouts() {
		mainLayout = new VerticalLayout();
		mainLayout.setSpacing(true);
		
		tableLayout = new GridLayout();
		tableLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
		tableLayout.setSpacing(true);
		tableLayout.setHeight("380px");
		tableLayout.setWidth("900px");
		tableLayout.setMargin(true);
		
		mainLayout.addComponent(dodajCzynnikButton);
		mainLayout.setComponentAlignment(dodajCzynnikButton, Alignment.TOP_RIGHT);
		
		Panel panel = new Panel();
		panel.setContent(tableLayout);
		
		mainLayout.addComponent(panel);
		addComponent(mainLayout);
		
	}
	
	public void setMainController(HasAnalysisTable mainController) {
		controller.setMainController(mainController);
		dodajCzynnikButton.addClickListener(controller);
	}

	public void addOpportunity(String nazwa) {
		tableLayout.setRowExpandRatio(0, 1f);
		ileSzans++;
		Label szansaLabel = new Label(nazwa);
		szansaLabel.setStyleName("rotated");
		insertColumn(ileSzans);
		tableLayout.addComponent(szansaLabel, ileSzans, 0);
		for (int i = 1; i <= ileSil + ileSlabosci; ++i) {
			Component component = createField();
			if (i <= ileSil) {
				component.setStyleName("yellow");
			}
			else {
				component.setStyleName("green");
			}
			
			HorizontalLayout layout = new HorizontalLayout();
			layout.addComponent(component);
			layout.addComponent(createMenuBar(ileSzans, i));
			
			tableLayout.addComponent(layout, ileSzans, i);
		}
	}
	
	public void addThread(String nazwa) {
		tableLayout.setRowExpandRatio(0, 1f);
		ileZagrozen++;
		Label zagrozenieLabel = new Label(nazwa);
		zagrozenieLabel.setStyleName("rotated");
		tableLayout.setColumns(ileSzans+ileZagrozen+1);
		tableLayout.addComponent(zagrozenieLabel, ileSzans+ileZagrozen, 0);
		for (int i = 1; i <= ileSil + ileSlabosci; ++i) {
			Component component = createField();
			if (i <= ileSil) {
				component.setStyleName("pink");
			}
			else {
				component.setStyleName("blue");
			}
			
			HorizontalLayout layout = new HorizontalLayout();
			layout.addComponent(component);
			layout.addComponent(createMenuBar(ileSzans + ileZagrozen, i));
			
			tableLayout.addComponent(layout, ileSzans+ileZagrozen, i);
		}

	}
	
	public void addStrength(String nazwa) {
		tableLayout.setColumnExpandRatio(0, 1f);
		ileSil++;
		Label silaLabel = new Label(nazwa);

		tableLayout.insertRow(ileSil);
		tableLayout.addComponent(silaLabel, 0, ileSil);
		for (int i = 1; i <= ileSzans + ileZagrozen; ++i) {
			Component component = createField();
			if (i <= ileSzans) {
				component.setStyleName("yellow");
			}
			else {
				component.setStyleName("pink");
			}
			HorizontalLayout layout = new HorizontalLayout();
			layout.addComponent(component);
			layout.addComponent(createMenuBar(i, ileSil));
			
			tableLayout.addComponent(layout, i, ileSil);
		}
		
	}
	
	public void addWeakness(String nazwa) {
		tableLayout.setColumnExpandRatio(0, 1f);
		ileSlabosci++;
		Label slaboscLabel = new Label(nazwa);
		
		tableLayout.insertRow(ileSil+ileSlabosci);
		tableLayout.addComponent(slaboscLabel, 0, ileSil+ileSlabosci);
		for (int i = 1; i <= ileSzans + ileZagrozen; ++i) {
			Component component = createField();
			if (i <= ileSzans) {
				component.setStyleName("green");
			}
			else {
				component.setStyleName("blue");
			}
			
			HorizontalLayout layout = new HorizontalLayout();
			layout.addComponent(component);
			layout.addComponent(createMenuBar(i, ileSil + ileSlabosci));
			
			tableLayout.addComponent(layout, i, ileSil+ileSlabosci);
		}
	}
	
	protected abstract Component createField();
	
   private void insertColumn(int col) {
	   tableLayout.setColumns(ileSzans+ileZagrozen+1);
        for (int i = ileSzans+ileZagrozen-1; i >= col; --i) {
        	for (int j = ileSil+ileSlabosci; j >= 0; --j) {
        		Component component = tableLayout.getComponent(i, j);
        		tableLayout.removeComponent(i, j);
        		tableLayout.addComponent(component, i+1, j);
        	}
        }
    }


	public boolean areValuesValid() {
		for (int i = 1; i <= ileSzans+ileZagrozen; ++i) {
			for (int j = 1; j <= ileSil+ileSlabosci; ++j) {
				if (!controller.isValid(getFieldValue(i, j))) {
					return false;
				}
			}
		}
		return true;
	}
	
	protected abstract String getFieldValue(int i, int j);
	
	
	public Analysis getAnalysis() {

		Analysis analysis = SimpleAnalysis.createEmptyAnalysisWithNoMetadata();
		RowAnalysisHeader[] silyISlabosci = new RowAnalysisHeader[ileSil+ileSlabosci+1];
		ColumnAnalysisHeader[] szanseIZagrozenia = new ColumnAnalysisHeader[ileSzans+ileZagrozen+1];
		for (int i = 1; i <= ileSil; ++i) {
			silyISlabosci[i] = analysis.addStrength(((Label)tableLayout.getComponent(0, i)).getValue());
		}
		for (int i = ileSil + 1; i <= ileSil + ileSlabosci; ++i) {
			silyISlabosci[i] = analysis.addWeakness(((Label)tableLayout.getComponent(0, i)).getValue());
		}
		for (int i = 1; i <= ileSzans; ++i) {
			szanseIZagrozenia[i] = analysis.addOpportunity(((Label)tableLayout.getComponent(i, 0)).getValue());
		}
		for (int i = ileSzans + 1; i <= ileSzans + ileZagrozen; ++i) {
			szanseIZagrozenia[i] = analysis.addThreat(((Label)tableLayout.getComponent(i, 0)).getValue());
		}
		
		for (int i = 1; i <= ileSzans + ileZagrozen; ++i ) {
			for (int j = 1; j <= ileSil + ileSlabosci; ++j) {
				UUID dokumentUUID = null;
				for (Wspolrzedne wsp : zalaczniki.keySet()) {
					if (wsp.x == i && wsp.y == j) {
						dokumentUUID = zalaczniki.get(wsp);
						break;
					}
				}
				int wartosc = Integer.valueOf(getFieldValue(i, j));
				SWOTField field;
				if (dokumentUUID != null) {
					field = SWOTField.createSWOTField(wartosc, dokumentUUID);
				}
				else {
					field = SWOTField.createSWOTField(wartosc);
				}
				analysis.setCell(silyISlabosci[j], szanseIZagrozenia[i], field);
				
			}
		
		}
		return analysis;
	}



	public boolean isAnalysisEmpty() {
		if (ileSil == 0 || ileSlabosci == 0 || ileSzans == 0 || ileZagrozen == 0) {
			return true;
		}
		return false;
	}
	
	public void fillTable(Analysis analysis) {
		
		List<ColumnAnalysisHeader> kolumny = new ArrayList<ColumnAnalysisHeader>();
		kolumny.addAll(analysis.getColumns());
		
		List<RowAnalysisHeader> wiersze = new ArrayList<RowAnalysisHeader>();
		wiersze.addAll(analysis.getRows());
		
		for (ColumnAnalysisHeader column : kolumny) {
			if (column instanceof OpportunityAnalysisHeader) {
				addOpportunity(column.getName());
			}
			else {
				addThread(column.getName());
			}
		}
		
		for (RowAnalysisHeader row : wiersze) {
			if (row instanceof StrengthAnalysisHeader) {
				addStrength(row.getName());
			}
			else {
				addWeakness(row.getName());
			}
		}
		for (ColumnAnalysisHeader column : kolumny) {
			for (RowAnalysisHeader row : wiersze) {
				if (analysis.getCell(row, column).isPresent()) {
					int wiersz = wiersze.indexOf(row)+1;
					int kolumna = kolumny.indexOf(column)+1;
					SWOTField field = analysis.getCell(row, column).get();
					if (field.getAttachmentUUID() != null) {
						for (MenuItem menuItem : menuItems.keySet()) {
							if (menuItems.get(menuItem).x == kolumna && menuItems.get(menuItem).y == wiersz) {
								wybraneMenu = menuItem;
								break;
							}
						}
						addAttachment(field.getAttachmentUUID());
					}
					int wartosc = field.getWeight();
					setFieldValue(kolumna, wiersz, wartosc);
				}
			}
		}
		
	}
	
	protected abstract void setFieldValue(int i, int j, int wartosc);


	public void clear() {
		ileSzans = 0;
		ileZagrozen = 0;
		ileSil = 0;
		ileSlabosci = 0;
		
		tableLayout.removeAllComponents();
		zalaczniki.clear();
		menuItems.clear();
	}


	public void addAttachment(UUID uuid) {
		Wspolrzedne wspolrzedne = menuItems.get(wybraneMenu);
		menuBars.get(wybraneMenu).setStyleName("dark");
		zalaczniki.put(wspolrzedne, uuid);
	}

	protected abstract MenuBar createMenuBar(int col, int row);
	
	public void databaseProblemOccured() {
		Notification.show(DATABASE_PROBLEM, Type.ERROR_MESSAGE);
		
	}
	
}
