/**
	@author drew
 */

package pl.edu.agh.strateg.logic;

import static pl.edu.agh.strateg.helpers.ExtendedPreconditions.*;

import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import org.apache.log4j.Logger;

import pl.edu.agh.strateg.logic.async.OperationFlowTemplate;
import pl.edu.agh.strateg.logic.async.OperationFlowTemplate.Operation;
import pl.edu.agh.strateg.logic.async.OperationListener;
import pl.edu.agh.strateg.logic.verification.VerificationStatus;
import pl.edu.agh.strateg.logic.verification.Verifier;
import pl.edu.agh.strateg.model.Document;
import pl.edu.agh.strateg.persistence.DocumentsDAO;
import pl.edu.agh.strateg.persistence.PersistenceException;
import pl.edu.agh.strateg.persistence.querying.MetadatumQuery;

import com.google.common.base.Optional;

public class DocumentsManagerImpl implements EntityManager<UUID, Document>,
		DocumentsManager {

	private static Logger LOG = Logger.getLogger(Thread.currentThread()
			.getStackTrace()[1].getClassName());

	private final DocumentsDAO dataDAO;
	private final Verifier verifier;

	@Inject
	public DocumentsManagerImpl(Verifier verifier, DocumentsDAO dataDAO) {
		checkArgumentsNotNull(dataDAO, verifier);
		this.verifier = verifier;
		this.dataDAO = dataDAO;
	}

	@Override
	public void addEntity(final Document data,
			OperationListener<Boolean> listener) {
		LOG.debug(String.format("Adding data operation"));
		new OperationFlowTemplate<Boolean>(listener, new Operation<Boolean>() {

			@Override
			public VerificationStatus validation() {
				return verifier.verifyDocument(data, Optional.<Object> absent());
			}

			@Override
			public Boolean operate() throws PersistenceException {
				return dataDAO.saveIfNotExists(data);
			}

		}).execute();
	}

	@Override
	public void deleteEntity(final Document data,
			OperationListener<Boolean> listener) {
		new OperationFlowTemplate<Boolean>(listener, new Operation<Boolean>() {

			@Override
			public VerificationStatus validation() {
				return verifier.verifyDocument(data, Optional.<Object> absent());
			}

			@Override
			public Boolean operate() throws PersistenceException {
				return dataDAO.deleteIfExists(data);
			}

		}).execute();

	}

	@Override
	public void updateEntity(final Document data,
			OperationListener<Boolean> listener) {
		new OperationFlowTemplate<Boolean>(listener, new Operation<Boolean>() {

			@Override
			public VerificationStatus validation() {
				return verifier.verifyDocument(data, Optional.<Object> absent());
			}

			@Override
			public Boolean operate() throws PersistenceException {
				return dataDAO.updateIfExists(data);
			}

		}).execute();

	}

	@Override
	public void getEntity(final UUID id,
			OperationListener<Optional<Document>> listener) {
		new OperationFlowTemplate<Optional<Document>>(listener,
				new Operation<Optional<Document>>() {

					@Override
					public VerificationStatus validation() {
						return VerificationStatus.OK;
					}

					@Override
					public Optional<Document> operate()
							throws PersistenceException {
						return dataDAO.get(id);
					}

				}).execute();

	}

	@Override
	public void getAllEntities(OperationListener<List<Document>> listener) {
		new OperationFlowTemplate<List<Document>>(listener, new Operation<List<Document>>() {

			@Override
			public VerificationStatus validation() {
				return VerificationStatus.OK;
			}

			@Override
			public List<Document> operate() throws PersistenceException {
				return dataDAO.getAll();
			}

		}).execute();

	}

	@Override
	public void queryByDescriptors(final MetadatumQuery query,
			OperationListener<List<Document>> listener) {
		new OperationFlowTemplate<List<Document>>(listener, new Operation<List<Document>>() {

			@Override
			public VerificationStatus validation() {
				return VerificationStatus.OK;
			}

			@Override
			public List<Document> operate() throws PersistenceException {
				return dataDAO.queryByMetadata(query);
			}

		}).execute();

	}

}
