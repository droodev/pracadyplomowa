/**
	@author drew
 */

package pl.edu.agh.strateg.model.analysis;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.base.Optional;

public class SimpleAnalysisTest {

	private Analysis emptyAnalysis;
	private Analysis filledAnalysis;

	private RowAnalysisHeader strengthRow;
	private RowAnalysisHeader weaknessRow;

	private ColumnAnalysisHeader opportunityColumn;
	private ColumnAnalysisHeader threatColumn;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		emptyAnalysis = SimpleAnalysis.createEmptyAnalysisWithNoMetadata();

		filledAnalysis = SimpleAnalysis.createEmptyAnalysisWithNoMetadata();
		strengthRow = filledAnalysis.addStrength("Strength");
		weaknessRow = filledAnalysis.addWeakness("Weakness");
		opportunityColumn = filledAnalysis.addOpportunity("Opportunity");
		threatColumn = filledAnalysis.addThreat("Threat");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void addOneTest() {
		RowAnalysisHeader newStrength = emptyAnalysis.addStrength("A");
		assertThat(emptyAnalysis.getRows().size(), equalTo(1));
		assertThat(emptyAnalysis.getRows().iterator().next(),
				sameInstance(newStrength));
	}

	@Test
	public void orderingTest() {
		RowAnalysisHeader newStrength = filledAnalysis.addStrength("a");
		RowAnalysisHeader newWeakness = filledAnalysis.addWeakness("B");
		ColumnAnalysisHeader newOpportunity = filledAnalysis
				.addOpportunity("c");
		ColumnAnalysisHeader newThreat = filledAnalysis.addThreat("d");
		assertThat(filledAnalysis.getRows().size(), equalTo(4));
		assertThat(filledAnalysis.getColumns().size(), equalTo(4));

		Iterator<RowAnalysisHeader> rowsIterator = filledAnalysis.getRows()
				.iterator();
		assertThat(rowsIterator.next(), sameInstance(strengthRow));
		assertThat(rowsIterator.next(), sameInstance(newStrength));
		assertThat(rowsIterator.next(), sameInstance(weaknessRow));
		assertThat(rowsIterator.next(), sameInstance(newWeakness));

		Iterator<ColumnAnalysisHeader> columnsIterator = filledAnalysis
				.getColumns().iterator();
		assertThat(columnsIterator.next(), sameInstance(opportunityColumn));
		assertThat(columnsIterator.next(), sameInstance(newOpportunity));
		assertThat(columnsIterator.next(), sameInstance(threatColumn));
		assertThat(columnsIterator.next(), sameInstance(newThreat));
	}

	@Test
	public void getSetValueTest() {
		assertThat(filledAnalysis.getCell(strengthRow, threatColumn),
				equalTo(Optional.<SWOTField> absent()));
		SWOTField swotField = SWOTField.createSWOTField(990);
		filledAnalysis.setCell(strengthRow, threatColumn, swotField);
		assertThat(filledAnalysis.getCell(strengthRow, threatColumn).get(),
				equalTo(swotField));
	}

	@Test
	public void setCheckedValueTest() {
		SWOTField swotField = SWOTField.createSWOTField(990);
		filledAnalysis.setCell(strengthRow, threatColumn, swotField);
		assertThat(filledAnalysis.setCellChecked(strengthRow, threatColumn,
				swotField), equalTo(false));
		assertThat(filledAnalysis.setCellChecked(weaknessRow, threatColumn,
				swotField), equalTo(true));
	}

	@Test
	public void removeColumnTest() {
		filledAnalysis.removeColumn(opportunityColumn);
		assertThat(filledAnalysis.getColumns().size(), equalTo(1));
		assertThat(filledAnalysis.getRows().size(), equalTo(2));
	}

	@Test
	public void removeRowTest() {
		filledAnalysis.removeRow(weaknessRow);
		assertThat(filledAnalysis.getColumns().size(), equalTo(2));
		assertThat(filledAnalysis.getRows().size(), equalTo(1));
	}
}
