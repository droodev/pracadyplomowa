package pl.edu.agh.strateg.ui.controller.interfaces;

import pl.edu.agh.strateg.ui.controller.component.analysis.AnalysisTableController;
import pl.edu.agh.strateg.ui.controller.component.analysis.AddFactorWindowController;
import pl.edu.agh.strateg.ui.model.analysis.FactorTypes;

public interface HasAnalysisTable {
	
	void setAnalysisTableController(AnalysisTableController controller);
	void setAddFactorWindowController(AddFactorWindowController controller);
	void addFactor(String nazwa, FactorTypes typ);
	void addFactorButtonClicked();

}
