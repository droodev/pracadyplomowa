package pl.edu.agh.strateg.ui;

import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import pl.edu.agh.strateg.ui.controller.main.*;
import pl.edu.agh.strateg.ui.view.view_providers.*;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
@Theme("strateg")
public class StrategUI extends UI {

	private static final Logger LOG = Logger.getLogger(Thread.currentThread()
			.getStackTrace()[1].getClassName());

	private Navigator navigator = new Navigator(this, this);

	private LoginController loginController = new LoginController(navigator);
	private MainController glownyController = new MainController(navigator);
	private AnalysesController analizaController = new AnalysesController(
			navigator);
	private NewAnalysisController nowaAnalizaController = new NewAnalysisController(
			navigator);
	private DocumentsController dokumentyController = new DocumentsController(
			navigator);
	private NewDocumentController nowyDokumentController = new NewDocumentController(
			navigator);
	private DescriptorsController metadaneController = new DescriptorsController(
			navigator);
	private NewDescriptorController nowaMetadanaController = new NewDescriptorController(
			navigator);
	private ReportController raportController = new ReportController(navigator);
	private NewReportController nowyRaportController = new NewReportController(
			navigator);

	public StrategUI() {
		LOG.debug(String.format("Created"));
	}

	@Override
	protected void init(VaadinRequest request) {
		LOG.debug(String.format("Initializing"));
		// new Initializer().start();
		initNavigator();
	}

	private void initNavigator() {
		AnalysesViewProvider analizaViewProvider = new AnalysesViewProvider(
				analizaController);
		MainViewProvider glownyViewProvider = new MainViewProvider(
				glownyController);
		LoginViewProvider loginViewProvider = new LoginViewProvider(
				loginController);
		DocumentsViewProvider dokumentyViewProvider = new DocumentsViewProvider(
				dokumentyController);
		DescriptorsViewProvider metadaneViewProvider = new DescriptorsViewProvider(
				metadaneController);
		NewDescriptorViewProvider nowaMetadanaViewProvider = new NewDescriptorViewProvider(
				nowaMetadanaController);
		NewAnalysisViewProvider nowaAnalizaViewProvider = new NewAnalysisViewProvider(
				nowaAnalizaController);
		NewDocumentViewProvider nowyDokumentViewProvider = new NewDocumentViewProvider(
				nowyDokumentController);
		NewReportViewProvider nowyRaportViewProvider = new NewReportViewProvider(
				nowyRaportController);
		ReportsViewProvider raportViewProvider = new ReportsViewProvider(
				raportController);

		navigator.addProvider(analizaViewProvider);
		navigator.addProvider(glownyViewProvider);
		navigator.addProvider(loginViewProvider);
		navigator.addProvider(dokumentyViewProvider);
		navigator.addProvider(metadaneViewProvider);
		navigator.addProvider(nowaMetadanaViewProvider);
		navigator.addProvider(nowaAnalizaViewProvider);
		navigator.addProvider(nowyDokumentViewProvider);
		navigator.addProvider(nowyRaportViewProvider);
		navigator.addProvider(raportViewProvider);

		Subject currentUser = SecurityUtils.getSubject();

		LOG.debug(String.format("Current user is: %s",
				currentUser.getPrincipal()));

		final String viewName = currentUser.isAuthenticated() ? Views.MAIN_VIEW
				: Views.LOGIN_VIEW;

		navigator.navigateTo(viewName);
	}

}
