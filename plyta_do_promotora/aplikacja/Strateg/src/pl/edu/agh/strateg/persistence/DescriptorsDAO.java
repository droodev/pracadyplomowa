/**
	@author drew
 */

package pl.edu.agh.strateg.persistence;

import pl.edu.agh.strateg.model.metadata.description.Descriptor;

public interface DescriptorsDAO extends GeneralDAO<Descriptor, String> {

}
