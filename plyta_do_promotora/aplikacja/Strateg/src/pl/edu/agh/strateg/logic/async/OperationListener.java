/**
	@author drew
 */

package pl.edu.agh.strateg.logic.async;

import pl.edu.agh.strateg.logic.verification.VerificationStatus;
import pl.edu.agh.strateg.persistence.PersistenceException;

public interface OperationListener<R> {
	public void preOperation();

	public void postVerification(VerificationStatus status);

	/**
	 * Contract - return value is never NULL
	 */
	public void postOperation(R returnValue);

	public void databaseProblemOccured(PersistenceException e);
}
