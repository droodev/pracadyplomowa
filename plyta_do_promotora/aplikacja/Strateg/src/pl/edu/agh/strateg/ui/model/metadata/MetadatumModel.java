package pl.edu.agh.strateg.ui.model.metadata;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import pl.edu.agh.strateg.ui.model.common.SimpleDatumModel;
import pl.edu.agh.strateg.ui.model.common.DataTypes;

public class MetadatumModel implements Serializable{

	private SimpleDatumModel simpleDatumModel;
	private List<Object> possibleValues;
	private List<MetadatumModel> describingMetadata;
	private List<DataTypes> whereRequired;
	private HashMap<DataTypes, List<RequiredMetadatum>> requirements;
	
	public MetadatumModel(SimpleDatumModel nazwaIWartosc, 
			List<MetadatumModel> metadaneOpisujace, List<Object> listaMozliwosci,
			List<DataTypes> gdzieWymagana, HashMap<DataTypes, List<RequiredMetadatum>> wymagania) {
		this(nazwaIWartosc, metadaneOpisujace, listaMozliwosci);
		this.whereRequired = gdzieWymagana;
		this.requirements = wymagania;
		
	}
	

	public MetadatumModel(SimpleDatumModel nazwaIWartosc, 
			List<MetadatumModel> metadaneOpisujace, List<Object> listaMozliwosci) {
		this(nazwaIWartosc, metadaneOpisujace);
		this.possibleValues = listaMozliwosci;
	}
	
	public MetadatumModel(SimpleDatumModel nazwaIWartosc, List<MetadatumModel> metadaneOpisujace) {
		this.describingMetadata = metadaneOpisujace;
		this.simpleDatumModel = nazwaIWartosc;
	}
	
	public List<RequiredMetadatum> getRequirements(DataTypes typ) {
		if (requirements == null) {
			return null;
		}
		return requirements.get(typ);
	}

	public void setPossibleValues(List<Object> listaMozliwosci) {
		this.possibleValues = listaMozliwosci;
	}

	public void setWhereRequired(List<DataTypes> gdzieWymagana) {
		this.whereRequired = gdzieWymagana;
	}

	public SimpleDatumModel getSimpleDatumModel() {
		return simpleDatumModel;
	}

	public List<MetadatumModel> getDescribingMetadata() {
		return describingMetadata;
	}

	public void setDescribingMetadata(List<MetadatumModel> metadaneOpisujace) {
		this.describingMetadata = metadaneOpisujace;
	}

	public List<Object> getPossibleValues() {
		return possibleValues;
	}
	
	public boolean isRequired(DataTypes typ) {
		if (whereRequired == null || !whereRequired.contains(typ)) {
			return false;
		}
		return true;
	}
	
	public void setRequirements(HashMap<DataTypes, List<RequiredMetadatum>> wymagania) {
		this.requirements = wymagania;
	}


	public List<DataTypes> getWhereRequired() {
		return whereRequired;
	}


	public HashMap<DataTypes, List<RequiredMetadatum>> getRequirements() {
		return requirements;
	}
	

	
}
