package pl.edu.agh.strateg.ui.model.communication.comparators;

import java.util.Comparator;

import pl.edu.agh.strateg.model.analysis.Analysis;

public class AnalysisComparator implements Comparator<Analysis>{

	@Override
	public int compare(Analysis analysis1, Analysis analysis2) {
		return new DescribableComparator().compare(analysis1, analysis2);
	}

}
