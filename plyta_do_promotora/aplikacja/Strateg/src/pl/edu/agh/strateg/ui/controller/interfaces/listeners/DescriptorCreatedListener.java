package pl.edu.agh.strateg.ui.controller.interfaces.listeners;

public interface DescriptorCreatedListener extends DatabaseProblemListener {
	
	void descriptorCreatedSuccessfully();
	void descriptorCreationFailed();
	
}
