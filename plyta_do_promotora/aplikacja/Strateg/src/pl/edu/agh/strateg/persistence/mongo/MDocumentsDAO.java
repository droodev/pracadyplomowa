/**
	@author drew
 */

package pl.edu.agh.strateg.persistence.mongo;

import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import pl.edu.agh.strateg.model.Document;
import pl.edu.agh.strateg.persistence.DocumentsDAO;
import pl.edu.agh.strateg.persistence.PersistenceException;
import pl.edu.agh.strateg.persistence.mongo.proxy.DocumentProxy;
import pl.edu.agh.strateg.persistence.mongo.querying.DocumentQueryProducer;
import pl.edu.agh.strateg.persistence.querying.MetadatumQuery;

import com.google.code.morphia.dao.BasicDAO;
import com.google.code.morphia.query.QueryResults;

public class MDocumentsDAO extends
		MAdaptingDAOBase<UUID, Document, DocumentProxy> implements DocumentsDAO {

	@Inject
	public MDocumentsDAO(BasicDAO<DocumentProxy, UUID> forProxyDAO) {
		super(forProxyDAO);
	}

	@Override
	public boolean exists(Document modelEntity) throws PersistenceException {
		try {
			return forProxyDAO.get(modelEntity.getUUID()) != null;
		} catch (RuntimeException e) {
			throw new PersistenceException(e);
		}
	}

	@Override
	public List<Document> queryByMetadata(MetadatumQuery query)
			throws PersistenceException {
		try {
			DocumentQueryProducer queryBuilder = new DocumentQueryProducer(
					forProxyDAO.createQuery());
			query.accept(queryBuilder);
			QueryResults<DocumentProxy> queryResults = forProxyDAO
					.find(queryBuilder.getQuery());
			List<Document> foundList = transformQueryResult(queryResults);
			return foundList;
		} catch (RuntimeException e) {
			throw new PersistenceException(e);
		}
	}

	@Override
	public DocumentProxy getProxy(Document modelEntity) {
		return DocumentProxy.proxify(modelEntity);
	}

	@Override
	public Document deproxify(DocumentProxy proxyEntity) {
		return proxyEntity.deproxify();
	}

}
