/**
	@author drew
 */

package pl.edu.agh.strateg.backbone;

import javax.inject.Inject;
import javax.inject.Provider;

import org.apache.log4j.Logger;

import com.vaadin.server.UIClassSelectionEvent;
import com.vaadin.server.UICreateEvent;
import com.vaadin.server.UIProvider;
import com.vaadin.ui.UI;

public class StrategUIProvider extends UIProvider {

	private static final Logger LOG = Logger.getLogger(Thread.currentThread()
			.getStackTrace()[1].getClassName());

	private Provider<UI> providingUI;
	private Class<? extends UI> uiClass;

	@Inject
	private StrategUIProvider(Provider<UI> providingUI,
			Class<? extends UI> uiClass) {
		this.providingUI = providingUI;
		this.uiClass = uiClass;
	}

	@Override
	public Class<? extends UI> getUIClass(UIClassSelectionEvent event) {
		LOG.debug(String.format("Providing UI class"));
		return uiClass;
	}

	@Override
	public UI createInstance(UICreateEvent event) {
		LOG.debug("Providing UI instance");
		return providingUI.get();
	}

}
