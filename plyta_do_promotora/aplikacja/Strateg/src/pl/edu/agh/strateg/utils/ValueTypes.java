package pl.edu.agh.strateg.utils;

public enum ValueTypes {
	
	INTEGER("Liczba ca�kowita"),
	DOUBLE("Liczba rzeczywista"),
	STRING("Tekst"),
	DATE("Data"),
	ENUM("Wyliczeniowy"),
	PERIOD("Okres"),
	TAGGED("Znacznik");
	
	private String caption;
	
	private ValueTypes(String caption) {
		this.caption = caption;
	}
	
	public String getCaption() {
		return caption;
	}

}
