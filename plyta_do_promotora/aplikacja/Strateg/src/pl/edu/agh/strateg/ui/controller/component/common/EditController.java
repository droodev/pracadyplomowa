package pl.edu.agh.strateg.ui.controller.component.common;

import java.io.Serializable;

import pl.edu.agh.strateg.ui.controller.interfaces.HasEditWindow;
import pl.edu.agh.strateg.ui.controller.interfaces.HasChooseMetadata;
import pl.edu.agh.strateg.ui.controller.interfaces.HasChosenMetadata;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.GotAllMetadaneListener;
import pl.edu.agh.strateg.ui.model.common.DataTypes;
import pl.edu.agh.strateg.ui.model.communication.GatewayImpl;
import pl.edu.agh.strateg.ui.model.metadata.MetadataGroup;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;
import pl.edu.agh.strateg.ui.model.metadata.RequiredMetadatum;
import pl.edu.agh.strateg.ui.view.components.common.EditWindow;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public abstract class EditController implements HasChooseMetadata, HasChosenMetadata, 
GotAllMetadaneListener, ClickListener, Serializable{
	
	protected ChooseMetadataController chooseMetadataController;
	protected ChosenMetadataController chosenMetadataController;
	
	protected HasEditWindow mainController;

	protected EditWindow editWindow;
	
	private boolean changeConfirmed = false;
	protected DataTypes dataTypes;
	
	public EditController(EditWindow edytujWindow, DataTypes typDanych) {
		this.editWindow = edytujWindow;
		this.dataTypes = typDanych;
	}
	
	public void setChooseMetadataController(
			ChooseMetadataController wybierzMetadaneController) {
		this.chooseMetadataController = wybierzMetadaneController;
	}
	
	public void setChosenMetadataController(ChosenMetadataController wybraneMetadaneController) {
		this.chosenMetadataController = wybraneMetadaneController;
	}
	
	public void setMainController(HasEditWindow mainController) {
		this.mainController = mainController;
		mainController.setEditController(this);
	}

	
	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if (button.getCaption().equals(EditWindow.POTWIERDZ_BUTTON_CAPTION)) {
			confirmButtonClicked();
		}
		else if (button.getCaption().equals(EditWindow.ANULUJ_BUTTON_CAPTION)) {
			anulujButtonClicked();
		}
		
	}
	
	protected void confirmButtonClicked() {
		changeConfirmed = true;
		
	}

	private void anulujButtonClicked() {
		changeConfirmed = false;
		editWindow.close();
		
	}
	

	private void dodajWszystkieWymagane(MetadatumModel metadana) {
		if (metadana.getRequirements(dataTypes) == null) {
			return;
		}
		for (RequiredMetadatum wymaganaMetadana : metadana.getRequirements(dataTypes)) {
			if (!chosenMetadataController.contains(wymaganaMetadana.getDescriptorName())) {
				if (wymaganaMetadana.getValue() == null || 
						wymaganaMetadana.getValue().equals(metadana.getSimpleDatumModel().getValue())) {
					MetadatumModel wymagana = chooseMetadataController.deleteDatum(wymaganaMetadana.getDescriptorName());
					chosenMetadataController.datumChosen(wymagana);
				}
			}
		}
		
	}
	
	@Override
	public boolean canBeDeleted(MetadatumModel metadana) {
		if (metadana.isRequired(dataTypes)) {
			return false;
		}
		for (MetadatumModel m : chosenMetadataController.getAllValues()) {
			if (m.getRequirements(dataTypes) == null) {
				continue;
			}
			for (RequiredMetadatum wymaganaMetadana : m.getRequirements(dataTypes)) {
				if (wymaganaMetadana.getDescriptorName().equals(metadana.getSimpleDatumModel().getName())) {
					if (wymaganaMetadana.getValue() == null || 
						wymaganaMetadana.getValue().equals(m.getSimpleDatumModel().getValue())) {
						return false;
					}
				}
			}
		}
		return true;
	}
	
	@Override
	public void restoreDatum(MetadatumModel metadana) {
		chooseMetadataController.restoreDatum(metadana);
		
	}

	@Override
	public void chosenData(MetadatumModel metadana) {
		chosenMetadataController.datumChosen(metadana);
		chooseMetadataController.deleteDatum(metadana);

	}
	
	public void fillWybierzMetadanePanel() {
		GatewayImpl.getInstance().getAllDescriptors(this);
	}

	@Override
	public void gettingDescriptorsFailed() {
		System.out.println("Getting descriptors failed");
		
	}

	@Override
	public void descriptorsReceived(MetadataGroup grupaMetadanych) {
		chooseMetadataController.fillPanelWithData(grupaMetadanych);
		
	}
	
	public boolean isChangeConfirmed() {
		return changeConfirmed;
	}
	
	@Override
	public void datumValueChanged(MetadatumModel metadana) {
		dodajWszystkieWymagane(metadana);
	}
	
	@Override
	public void databaseProblemOccured() {
		editWindow.databaseProblemOccured();
		
	}

}
