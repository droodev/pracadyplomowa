package pl.edu.agh.strateg.ui.controller.main;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pl.edu.agh.strateg.ui.Views;
import pl.edu.agh.strateg.ui.controller.component.common.ChooseMetadataController;
import pl.edu.agh.strateg.ui.controller.component.common.ChosenMetadataController;
import pl.edu.agh.strateg.ui.controller.interfaces.HasChooseMetadata;
import pl.edu.agh.strateg.ui.controller.interfaces.HasChosenMetadata;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.FileCreatedListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.GotAllMetadaneListener;
import pl.edu.agh.strateg.ui.model.common.DataTypes;
import pl.edu.agh.strateg.ui.model.communication.GatewayImpl;
import pl.edu.agh.strateg.ui.model.metadata.MetadataGroup;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;
import pl.edu.agh.strateg.ui.model.metadata.RequiredMetadatum;
import pl.edu.agh.strateg.ui.view.components.common.TitlePanel;
import pl.edu.agh.strateg.ui.view.views.AbstractNewDocumentView;

import com.vaadin.navigator.Navigator;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public abstract class AbstractNewDocumentController implements HasChooseMetadata, HasChosenMetadata, ClickListener, 
													GotAllMetadaneListener, FileCreatedListener, Serializable{
	
	protected Navigator navigator;

	private ChooseMetadataController chooseMetadataController;
	private ChosenMetadataController chosenMetadataController;
	protected AbstractNewDocumentView newDocumentView;
	
	protected DataTypes dataTypes;
	
	public AbstractNewDocumentController(Navigator navigator) {
		this.navigator = navigator;
	}
	
	public void setNewDocumentView(AbstractNewDocumentView nowyDokumentView) {
		this.newDocumentView = nowyDokumentView;
	}
	
	public void setChooseMetadataController(ChooseMetadataController wybierzMetadaneController) {
		this.chooseMetadataController = wybierzMetadaneController;
	}
	
	public void setChosenMetadataController(ChosenMetadataController wybraneMetadaneController) {
		this.chosenMetadataController = wybraneMetadaneController;
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if (button.getCaption().equals(AbstractNewDocumentView.STRONA_GLOWNA_BUTTON_CAPTION)) {
			mainPageButtonClicked();
		}
		else if (button.getCaption().equals(newDocumentView.POTWIERDZ_BUTTON_CAPTION)) {
			confirmButtonClicked();
		}
		else if (button.getCaption().equals(newDocumentView.ANULUJ_BUTTON_CAPTION)) {
			cancelButtonClicked();
		}
		
	}
	
	public void mainPageButtonClicked() {
		navigator.navigateTo(Views.MAIN_VIEW);
	}
	
	public void confirmButtonClicked() {

		if (!chosenMetadataController.areValuesValid()) {
			newDocumentView.displayNotValidValuesWarning();
			return;
		}
		List<MetadatumModel> metadane = chosenMetadataController.getAllValues();
		addNewDocument(metadane);
		
	}
	
	protected abstract void addNewDocument(List<MetadatumModel> metadane);
	
	protected abstract void cancelButtonClicked();
	
	@Override
	public void chosenData(MetadatumModel metadana) {
		chosenMetadataController.datumChosen(metadana);
		chooseMetadataController.deleteDatum(metadana);

	}
	
	public void fillChooseMetadataPanel() {
		GatewayImpl.getInstance().getAllDescriptors(this);
	}

	@Override
	public void gettingDescriptorsFailed() {
		System.out.println("Getting descriptors failed");
		
	}

	@Override
	public void descriptorsReceived(MetadataGroup grupaMetadanych) {
		chooseMetadataController.fillPanelWithData(grupaMetadanych);
		List<MetadatumModel> wymaganeMetadane = new ArrayList<>();
		getRequiredMetadata(grupaMetadanych, wymaganeMetadane);
		for (MetadatumModel metadana : wymaganeMetadane) {
			chosenData(metadana);
		}
	}
	
	private void getRequiredMetadata(MetadataGroup grupaMetadanych, List<MetadatumModel> wymaganeMetadane) {
		if (grupaMetadanych.getData() != null) {
			for (MetadatumModel metadana : grupaMetadanych.getData()) {
				if (metadana.isRequired(dataTypes)) {
					wymaganeMetadane.add(metadana);
				}
			}
		}
		if (grupaMetadanych.getGroups() != null) {
			for (MetadataGroup grupa : grupaMetadanych.getGroups()) {
				getRequiredMetadata(grupa, wymaganeMetadane);
			}
		}
	}
	
	@Override
	public void fileCreatedSuccessfully() {
		newDocumentView.successfullyAdded();
		
	}

	@Override
	public void fileCreationFailed() {
		newDocumentView.addingFailed();
	}

	@Override
	public void datumValueChanged(MetadatumModel metadana) {
		addAllRequired(metadana);
	}
	
	private void addAllRequired(MetadatumModel metadana) {
		if (metadana.getRequirements(dataTypes) == null) {
			return;
		}
		for (RequiredMetadatum wymaganaMetadana : metadana.getRequirements(dataTypes)) {
			if (!chosenMetadataController.contains(wymaganaMetadana.getDescriptorName())) {
				if (wymaganaMetadana.getValue() == null || 
						wymaganaMetadana.getValue().equals(metadana.getSimpleDatumModel().getValue())) {
					MetadatumModel wymagana = chooseMetadataController.deleteDatum(wymaganaMetadana.getDescriptorName());
					chosenMetadataController.datumChosen(wymagana);
				}
			}
		}
		
	}
	
	@Override
	public boolean canBeDeleted(MetadatumModel metadana) {
		if (metadana.isRequired(dataTypes)) {
			return false;
		}
		for (MetadatumModel m : chosenMetadataController.getAllValues()) {
			if (m.getRequirements(dataTypes) == null) {
				continue;
			}
			for (RequiredMetadatum wymaganaMetadana : m.getRequirements(dataTypes)) {
				if (wymaganaMetadana.getDescriptorName().equals(metadana.getSimpleDatumModel().getName())) {
					if (wymaganaMetadana.getValue() == null || 
						wymaganaMetadana.getValue().equals(m.getSimpleDatumModel().getValue())) {
						return false;
					}
				}
			}
		}
		return true;
	}

	@Override
	public void restoreDatum(MetadatumModel metadana) {
		chooseMetadataController.restoreDatum(metadana);
	}
	
	public void addNavigator(TitlePanel panel) {
		panel.setNavigator(navigator);
	}

}
