/**
	@author drew
 */

package pl.edu.agh.strateg.backbone;

import javax.servlet.ServletContext;

import org.apache.shiro.guice.web.ShiroWebModule;
import org.apache.shiro.realm.SimpleAccountRealm;

import pl.edu.agh.strateg.security.Roles;

import com.google.inject.Provides;

public class ShiroModule extends ShiroWebModule {

	public ShiroModule(ServletContext servletContext) {
		super(servletContext);
	}

	@Override
	protected void configureShiroWeb() {
		bindRealm().to(SimpleAccountRealm.class);
	}

	@Provides
	public SimpleAccountRealm provideSimpleAccountRealm() {
		SimpleAccountRealm realm = new SimpleAccountRealm();
		realm.addAccount("kwp", "kwp", 
				Roles.ADD_DOCUMENT, Roles.EDIT_DOCUMENT, Roles.SEE_DESCRIPTORS, Roles.SEE_DOCUMENTS);
		realm.addAccount("kgp", "kgp", 
				Roles.ADD_ANALYSIS, Roles.ADD_DESCRIPTOR, Roles.ADD_DOCUMENT, Roles.CREATE_REPORT,
				Roles.EDIT_ANALYSIS, Roles.EDIT_DESCRIPTOR, Roles.EDIT_DOCUMENT, 
				Roles.DELETE_DESCRIPTOR, Roles.DELETE_ANALYSIS, Roles.DELETE_DOCUMENT, 
				Roles.SEE_ANALYSES, Roles.SEE_DESCRIPTORS, Roles.SEE_DOCUMENTS, Roles.SEE_REPORTS);
		return realm;
	}
}
