package pl.edu.agh.strateg.ui.view.views;

import pl.edu.agh.strateg.ui.controller.main.NewAnalysisController;
import pl.edu.agh.strateg.ui.view.components.analysis.AbstractAnalysisTable;
import pl.edu.agh.strateg.ui.view.components.analysis.AddFactorWindow;
import pl.edu.agh.strateg.ui.view.components.analysis.EditableAnalysisTable;
import pl.edu.agh.strateg.ui.view.components.common.TitlePanel;
import pl.edu.agh.strateg.ui.view.components.common.ChooseMetadataPanel;
import pl.edu.agh.strateg.ui.view.components.common.ChosenMetadataPanel;
import pl.edu.agh.strateg.utils.Icons;
import pl.edu.agh.strateg.utils.InputStreamList;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.VerticalLayout;

public class NewAnalysisView extends VerticalLayout implements View {
	
	public static final String STRONA_GLOWNA_BUTTON_CAPTION = "Strona g��wna";
	public static final String POTWIERDZ_BUTTON_CAPTION = "Dodaj analiz�";
	public static final String ANULUJ_BUTTON_CAPTION = "Powr�t do analiz";
	
	private static final String PAGE_CAPTION = "NOWA ANALIZA";
	private static final String WYBRANE_METADANE_CAPTION = "Wybrane metadane";
	private static final String DODANO_POPRAWNIE_CAPTION = "Dodano poprawnie";
	private static final String NIE_UDALO_SIE_DODAC_CAPTION = "Nie uda�o si� doda�";
	private static final String DATABASE_PROBLEM = "Problem przy dost�pie do bazy";
	private static final String NOT_VALID_VALUES_WARNING = "Wprowadzone warto�ci nie s� poprawne";
	private static final String EMPTY_VALUES_WARNING = "Tabela musi mie� ka�dy czynnik";
	
	private Button stronaGlownaButton;
	private Button potwierdzButton;
	private Button anulujButton;
	
	
	private TitlePanel titlePanel;
	
	private ChooseMetadataPanel wybierzMetadanePanel;
	private ChosenMetadataPanel wybraneMetadanePanel;
	private AddFactorWindow dodajCzynnikWindow;
	private AbstractAnalysisTable analizaTable;
	
	private NewAnalysisController controller;
	
	public NewAnalysisView() {
		initComponents();
		initLayouts();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		controller.fillChooseMetadataPanel();
		InputStreamList.getInstance().clear();
	}

	
	public void addedSuccessfully() {
		resetContent();
		Notification.show(DODANO_POPRAWNIE_CAPTION, Type.TRAY_NOTIFICATION);
	}
	
	public void addingFailed() {
		Notification.show(NIE_UDALO_SIE_DODAC_CAPTION, Type.ERROR_MESSAGE);
	}

	
	private void resetContent() {
		wybierzMetadanePanel.clear();
		clearWybraneMetadanePanel();
		controller.fillChooseMetadataPanel();
		resetAnalizaTable();
	}

	private void resetAnalizaTable() {
		analizaTable.clear();
	}

	private void clearWybraneMetadanePanel() {
		wybraneMetadanePanel.removeAllFilters();
	}
	
	public void setControllerToAllComponents(NewAnalysisController controller) {
		this.controller = controller;
		controller.setNewAnalysisView(this);
		wybierzMetadanePanel.setMainController(controller);
		wybraneMetadanePanel.setMainController(controller);
		dodajCzynnikWindow.setMainController(controller);
		analizaTable.setMainController(controller);
		setListenerToButtons();
		controller.addNavigator(titlePanel);
	}
	
	public void displayAddFactorWindow() {
		getUI().addWindow(dodajCzynnikWindow);
		dodajCzynnikWindow.setModal(true);
	}
	
	
	private void setListenerToButtons() {
		stronaGlownaButton.addClickListener(controller);
		potwierdzButton.addClickListener(controller);
		anulujButton.addClickListener(controller);
		
	}

	private void initComponents() {
		stronaGlownaButton = new Button(STRONA_GLOWNA_BUTTON_CAPTION);
		potwierdzButton = new Button(POTWIERDZ_BUTTON_CAPTION);
		anulujButton = new Button(ANULUJ_BUTTON_CAPTION);
		
		stronaGlownaButton.setIcon(new ThemeResource(Icons.MAIN));
		potwierdzButton.setIcon(new ThemeResource(Icons.CONFIRM));
		anulujButton.setIcon(new ThemeResource(Icons.CANCEL));
		
		wybierzMetadanePanel = new ChooseMetadataPanel();
		wybraneMetadanePanel = new ChosenMetadataPanel(WYBRANE_METADANE_CAPTION);
		analizaTable = new EditableAnalysisTable();
		dodajCzynnikWindow = new AddFactorWindow();
		
		titlePanel = new TitlePanel(PAGE_CAPTION, Icons.TITLE_NEW_ANALYSIS, true);
		
	}


	private void initLayouts() {
		VerticalLayout mainLayout = new VerticalLayout();
		HorizontalLayout upperLayout = new HorizontalLayout();
		HorizontalLayout bottomLayout = new HorizontalLayout();
		
		mainLayout.setSizeFull();
		mainLayout.setSpacing(true);
		mainLayout.setMargin(true);
		
		upperLayout.setSizeFull();
		upperLayout.setSpacing(true);
		
		bottomLayout.setSizeFull();
		bottomLayout.setSpacing(true);
		
		HorizontalLayout middleLayout = new HorizontalLayout();
		middleLayout.setSizeFull();
		middleLayout.setSpacing(true);
		
		
		HorizontalLayout buttonLayout = new HorizontalLayout();
		buttonLayout.addComponent(potwierdzButton);
		buttonLayout.addComponent(anulujButton);
		
		buttonLayout.setSpacing(true);
		
		upperLayout.addComponent(stronaGlownaButton);
		upperLayout.addComponent(wybierzMetadanePanel);
		upperLayout.addComponent(buttonLayout);
		
		upperLayout.setExpandRatio(stronaGlownaButton, 0.25f);
		upperLayout.setExpandRatio(wybierzMetadanePanel, 0.5f);
		upperLayout.setExpandRatio(buttonLayout, 0.25f);
		
		upperLayout.setComponentAlignment(stronaGlownaButton, Alignment.TOP_LEFT);
		upperLayout.setComponentAlignment(buttonLayout, Alignment.TOP_RIGHT);
		
		bottomLayout.addComponent(wybraneMetadanePanel);
		bottomLayout.addComponent(analizaTable);
		
		bottomLayout.setExpandRatio(wybraneMetadanePanel, 0.25f);
		bottomLayout.setExpandRatio(analizaTable, 0.75f);
		
		mainLayout.addComponent(titlePanel);
		mainLayout.addComponent(upperLayout);
		mainLayout.addComponent(middleLayout);
		mainLayout.addComponent(bottomLayout);
		addComponent(mainLayout);
		
	}

	public void displayNotValidValuesWarning() {
		Notification.show(NOT_VALID_VALUES_WARNING, Type.ERROR_MESSAGE);
	}

	public void emptyAnalysis() {
		Notification.show(EMPTY_VALUES_WARNING, Type.ERROR_MESSAGE);
	}
	
	public void databaseProblemOccured() {
		Notification.show(DATABASE_PROBLEM, Type.ERROR_MESSAGE);
	}


}
