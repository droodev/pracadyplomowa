package pl.edu.agh.strateg.ui.view.components.common.insert;


import pl.edu.agh.strateg.ui.controller.component.common.InsertDatumValueController;

import com.vaadin.ui.AbstractField;
import com.vaadin.ui.TextField;

public class InsertString implements InsertComponent{
	
	private TextField field;
	
	public InsertString(InsertDatumValueController controller) {
		field = new TextField();
		field.setImmediate(true);
		field.setRequired(true);
		field.addValueChangeListener(controller);
	}

	@Override
	public Object getValue() {
		return field.getValue();
	}

	@Override
	public AbstractField<?> getField() {
		return field;
	}
	
	@Override
	public boolean isValid() {
		return field.isValid();
	}
	
	@Override
	public void setValue(Object object) {
		field.setValue(String.valueOf(object));
	}

}
