/**
	@author drew
 */

package pl.edu.agh.strateg.logic.verification;

public abstract class VerificationStatus {

	public static final VerificationStatus OK = new VerificationStatus() {

		@Override
		public String getDescription() {
			return "OK";
		}
	};

	public abstract String getDescription();

	public boolean isOK() {
		return this.equals(OK);
	}
}
