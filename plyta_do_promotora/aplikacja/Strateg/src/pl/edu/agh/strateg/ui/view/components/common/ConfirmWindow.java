package pl.edu.agh.strateg.ui.view.components.common;

import pl.edu.agh.strateg.ui.controller.interfaces.HasConfirmWindow;
import pl.edu.agh.strateg.utils.Icons;

import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class ConfirmWindow extends Window{ 
	
	private static final String POTWIERDZ_CAPTION = "Potwierd�";
	private static final String ANULUJ_CAPTION = "Anuluj";
	private String pytanie;
	
	private Button potwierdzButton;
	private Button anulujButton;
	private Label label;
	
	private boolean confirmed = false;
	
	public ConfirmWindow(HasConfirmWindow mainController, String pytanie) {
		
		this.pytanie = pytanie;
		potwierdzButton = new Button(POTWIERDZ_CAPTION);
		potwierdzButton.setIcon(new ThemeResource(Icons.CONFIRM));
		
		anulujButton = new Button(ANULUJ_CAPTION);
		anulujButton.setIcon(new ThemeResource(Icons.CANCEL));
		
		addListenersToButtons();
		
		label = new Label("<b>" + pytanie + "</b>", ContentMode.HTML);
		
		HorizontalLayout buttonLayout = new HorizontalLayout();
		buttonLayout.setSpacing(true);
		buttonLayout.setSizeFull();
		
		VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.setSpacing(true);
		mainLayout.setMargin(true);
		
		buttonLayout.addComponent(potwierdzButton);
		buttonLayout.addComponent(anulujButton);
		
		buttonLayout.setComponentAlignment(potwierdzButton, Alignment.BOTTOM_LEFT);
		buttonLayout.setComponentAlignment(anulujButton, Alignment.BOTTOM_RIGHT);
		
		mainLayout.addComponent(label);
		mainLayout.addComponent(buttonLayout);
		
		setContent(mainLayout);
		setHeight(4, Unit.CM);
		setWidth(10, Unit.CM);
		setResizable(false);
		
		addCloseListener(mainController);
		
	}
	
	private void addListenersToButtons() {
		potwierdzButton.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				confirmed = true;
				close();
			}
		});
		anulujButton.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				confirmed = false;
				close();
			}
		});
		
	}

	public boolean isConfirmed() {
		return confirmed;
	}
	
	public String getQuestion() {
		return pytanie;
	}

}
