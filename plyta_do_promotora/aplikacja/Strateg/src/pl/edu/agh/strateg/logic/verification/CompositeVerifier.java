/**
	@author drew
 */

package pl.edu.agh.strateg.logic.verification;

import javax.inject.Inject;

import pl.edu.agh.strateg.model.Document;
import pl.edu.agh.strateg.model.analysis.Analysis;
import pl.edu.agh.strateg.model.metadata.MetadataContainer;
import pl.edu.agh.strateg.model.metadata.Metadatum;
import pl.edu.agh.strateg.model.metadata.description.Descriptor;

import com.google.common.base.Optional;

public class CompositeVerifier implements Verifier {

	private VerifierPart<Document> dataVerfier;
	private VerifierPart<Descriptor> descriptorVerifier;
	private VerifierPart<Metadatum> metadatumVerifier;
	private VerifierPart<Analysis> analysisVerifier;
	private VerifierPart<MetadataContainer> containerVerifier;

	@Inject
	public CompositeVerifier(VerifierPart<Document> dataVerfier,
			VerifierPart<Descriptor> descriptorVerifier,
			VerifierPart<Metadatum> metadatumVerifier,
			VerifierPart<Analysis> analysisVerifier,
			VerifierPart<MetadataContainer> containerVerifier) {
		this.dataVerfier = dataVerfier;
		this.descriptorVerifier = descriptorVerifier;
		this.metadatumVerifier = metadatumVerifier;
		this.analysisVerifier = analysisVerifier;
		this.containerVerifier = containerVerifier;
	}

	@Override
	public VerificationStatus verifyMetadatum(Metadatum metadatum,
			Optional<Object> parent) {
		return metadatumVerifier.verify(metadatum, parent, this);
	}

	@Override
	public VerificationStatus verifyDocument(Document data,
			Optional<Object> parent) {
		return dataVerfier.verify(data, parent, this);
	}

	@Override
	public VerificationStatus verifyDescriptor(Descriptor descriptor,
			Optional<Object> parent) {
		return descriptorVerifier.verify(descriptor, parent, this);
	}

	@Override
	public VerificationStatus verifyAnalysis(Analysis analysis,
			Optional<Object> parent) {
		return analysisVerifier.verify(analysis, parent, this);
	}

	@Override
	public VerificationStatus verifyContainter(MetadataContainer container,
			Optional<Object> parent) {
		return containerVerifier.verify(container, parent, this);
	};

}
