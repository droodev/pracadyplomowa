package pl.edu.agh.strateg.ui.controller.main;

import java.io.Serializable;

import pl.edu.agh.strateg.ui.Views;
import pl.edu.agh.strateg.ui.view.components.common.TitlePanel;

import com.vaadin.navigator.Navigator;

public class MainController implements Serializable{
	
	private Navigator navigator;
	
	public MainController(Navigator navigator) {
		this.navigator = navigator;
	}
	
	public void documentsButtonClicked() {
		navigator.navigateTo(Views.DOCUMENTS_VIEW);
		
	}
	
	public void analysesButtonClicked() {
		navigator.navigateTo(Views.ANALYSES_VIEW);
		
	}
	
	public void descriptorsButtonClicked() {
		navigator.navigateTo(Views.DESCRIPTORS_VIEW);
		
	}
	
	public void reportsButtonClicked() {
		navigator.navigateTo(Views.REPORTS_VIEW);
		
	}
	
	public void addNavigator(TitlePanel panel) {
		panel.setNavigator(navigator);
	}



}
