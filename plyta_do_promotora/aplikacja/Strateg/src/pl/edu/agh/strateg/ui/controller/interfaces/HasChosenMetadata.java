package pl.edu.agh.strateg.ui.controller.interfaces;

import pl.edu.agh.strateg.ui.controller.component.common.ChosenMetadataController;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;

public interface HasChosenMetadata {
	
	void setChosenMetadataController(ChosenMetadataController wybraneMetadaneController);
	void datumValueChanged(MetadatumModel metadana);
	boolean canBeDeleted(MetadatumModel metadana);
	void restoreDatum(MetadatumModel metadana);

}
