package pl.edu.agh.strateg.ui.controller.interfaces.listeners;

public interface FileUpdatedListener extends DatabaseProblemListener {
	
	void fileUpdatedSuccessfully();
	void fileUpdatingFailed();

}
