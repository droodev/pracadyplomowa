package pl.edu.agh.strateg.ui.model.communication.comparators;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;

import pl.edu.agh.strateg.model.Describable;
import pl.edu.agh.strateg.model.metadata.Metadatum;

public class DescribableComparator implements Comparator<Describable>{

	@Override
	public int compare(Describable o1, Describable o2) {
		Collection<Metadatum> metadata1 = o1.getDescribingMetadata().getAll();
		Collection<Metadatum> metadata2 = o2.getDescribingMetadata().getAll();
		
		Date date1 = null;
		Date date2 = null;
		String name1 = null;
		String name2 = null;
		
		for (Metadatum metadatum : metadata1) {
			if (metadatum.getDescription().getName().equals("Data")) {
				date1 = (Date) metadatum.getValue();
				break;
			}
			else if (metadatum.getDescription().getName().equals("Nazwa danej")) {
				name1 = (String) metadatum.getValue();
				break;
			}
		}
		
		for (Metadatum metadatum : metadata2) {
			if (metadatum.getDescription().getName().equals("Data")) {
				date2 = (Date) metadatum.getValue();
				break;
			}
			else if (metadatum.getDescription().getName().equals("Nazwa danej")) {
				name2 = (String) metadatum.getValue();
				break;
			}
		}
		
		if (date1 != null && date2 != null) {
			if (date1.equals(date2)) {
				return name1.compareTo(name2);
			}
			return date1.before(date2) ? 1 : -1;
		}
		else if (date1 != null) {
			return -1;
		}
		else if (date2 != null) {
			return 1;
		}
		else {
			return name1.compareTo(name2);
		}
	}

}
