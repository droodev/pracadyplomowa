/**
	@author drew
 */

package pl.edu.agh.strateg.persistence.mongo;

import static com.google.common.base.Preconditions.*;
import static pl.edu.agh.strateg.helpers.ExtendedPreconditions.*;

import javax.inject.Inject;

import org.apache.log4j.Logger;

import pl.edu.agh.strateg.helpers.annotations.ServiceShutdown;
import pl.edu.agh.strateg.helpers.annotations.ServiceStart;

import com.google.code.morphia.Datastore;
import com.google.code.morphia.Morphia;
import com.mongodb.MongoClient;

public class DatastoreManager {
	private static final Logger LOG = Logger.getLogger(Thread.currentThread()
			.getStackTrace()[1].getClassName());

	// TODO What? I can't change it without recompiling? Are you crazy?
	private static final String DATABASE_NAME = "TempBase";

	private final MongoClient client;
	private final Morphia morphia;
	private Datastore datastore;

	@Inject
	public DatastoreManager(MongoClient client, Morphia morphia) {
		checkArgumentsNotNull(client, morphia);
		this.client = client;
		this.morphia = morphia;
	}

	@ServiceStart
	public void start() {
		LOG.debug(String.format("Starting MongoDB datastore"));
		datastore = morphia.createDatastore(client, DATABASE_NAME);
	}

	public Datastore getDatastore() {
		checkState(datastore != null);
		return datastore;
	}

	@ServiceShutdown
	public void shutdown() {
		LOG.debug(String.format("Shutting down MongoDB datastore"));
		client.close();
	}

}
