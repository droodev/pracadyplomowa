package pl.edu.agh.strateg.ui.view.components.analysis;

import pl.edu.agh.strateg.ui.controller.component.analysis.AddFactorWindowController;
import pl.edu.agh.strateg.ui.controller.interfaces.HasAnalysisTable;
import pl.edu.agh.strateg.ui.model.analysis.FactorTypes;
import pl.edu.agh.strateg.utils.Icons;

import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class AddFactorWindow extends Window {
	
	private static final String POTWIERDZ_BUTTON_CAPTION = "Potwierd�";
	private static final String NAZWA_CAPTION = "Nazwa";
	private static final String TYP_CAPTION = "Typ";
	private static final String WINDOW_CAPTION = "Dodaj czynnik";
	
	private static final String EMPTY_NAME_WARNING = "Nazwa nie mo�e by� pusta";
	
	private Label wpiszNazweLabel;
	private Label wybierzTypLabel;
	private ComboBox typyCombobox;
	private TextField textField;
	private Button button;
	
	private AddFactorWindowController controller;
	
	public AddFactorWindow() {
		
		controller = new AddFactorWindowController(this);
		
		setHeight(5, Unit.CM);
		setWidth(10, Unit.CM);
		setClosable(false);
		setResizable(false);
		setCaption(WINDOW_CAPTION);
		
		initComponents();
		initLayouts();
	}
	
	public void setMainController(HasAnalysisTable analizaController) {
		controller.setMainController(analizaController);
	}

	private void initComponents() {
		wpiszNazweLabel = new Label("<b>" + NAZWA_CAPTION + "</b>", ContentMode.HTML);
		wybierzTypLabel = new Label("<b>" + TYP_CAPTION + "</b>", ContentMode.HTML);
		
		textField = new TextField();
		textField.setSizeFull();
		
		button = new Button(POTWIERDZ_BUTTON_CAPTION);
		button.setIcon(new ThemeResource(Icons.CONFIRM));
		button.addClickListener(controller);
		
		initCombobox();
	}

	private void initCombobox() {
		typyCombobox = new ComboBox();
		typyCombobox.addItem(FactorTypes.SILA);
		typyCombobox.addItem(FactorTypes.SLABOSC);
		typyCombobox.addItem(FactorTypes.SZANSA);
		typyCombobox.addItem(FactorTypes.ZAGROZENIE);
		
		typyCombobox.setItemCaption(FactorTypes.SILA, FactorTypes.SILA.getCaption());
		typyCombobox.setItemCaption(FactorTypes.SLABOSC, FactorTypes.SLABOSC.getCaption());
		typyCombobox.setItemCaption(FactorTypes.SZANSA, FactorTypes.SZANSA.getCaption());
		typyCombobox.setItemCaption(FactorTypes.ZAGROZENIE, FactorTypes.ZAGROZENIE.getCaption());
		
		typyCombobox.setValue(FactorTypes.SILA);
		typyCombobox.setNullSelectionAllowed(false);
		typyCombobox.setTextInputAllowed(false);
	}
	
	private void initLayouts() {
		VerticalLayout layout = new VerticalLayout();
		layout.setMargin(true);
		layout.setSpacing(true);
		
		HorizontalLayout upperLayout = new HorizontalLayout();
		upperLayout.setSpacing(true);
		upperLayout.setSizeFull();
		
		VerticalLayout leftLayout = new VerticalLayout();
		VerticalLayout rightLayout = new VerticalLayout();
		
		rightLayout.addComponent(wpiszNazweLabel);
		rightLayout.addComponent(textField);
		
		leftLayout.addComponent(wybierzTypLabel);
		leftLayout.addComponent(typyCombobox);
		
		
		upperLayout.addComponent(leftLayout);
		upperLayout.addComponent(rightLayout);
		
		layout.addComponent(upperLayout);
		layout.addComponent(new VerticalLayout());
		layout.addComponent(button);
		layout.setComponentAlignment(button, Alignment.BOTTOM_RIGHT);
		
		setContent(layout);
		
	}
	
	public String getName() {
		return textField.getValue();
	}
	
	public FactorTypes getType() {
		return (FactorTypes) typyCombobox.getValue();
	}
	
	public void reset() {
		textField.setValue("");
		typyCombobox.setValue(FactorTypes.SILA);
	}

	public void emptyName() {
		Notification.show(EMPTY_NAME_WARNING, Type.ERROR_MESSAGE);
		
	}

}
