package pl.edu.agh.strateg.ui.controller.component.documents;

import java.io.Serializable;
import java.util.List;

import pl.edu.agh.strateg.ui.controller.component.common.ChooseMetadataController;
import pl.edu.agh.strateg.ui.controller.component.common.ChosenMetadataController;
import pl.edu.agh.strateg.ui.controller.interfaces.HasConfirmWindow;
import pl.edu.agh.strateg.ui.controller.interfaces.HasChooseMetadata;
import pl.edu.agh.strateg.ui.controller.interfaces.HasChosenMetadata;
import pl.edu.agh.strateg.ui.controller.interfaces.HasChosenDocument;
import pl.edu.agh.strateg.ui.controller.interfaces.HasSearchResults;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.FoundFilesListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.GotAllMetadaneListener;
import pl.edu.agh.strateg.ui.model.common.DataModel;
import pl.edu.agh.strateg.ui.model.communication.GatewayImpl;
import pl.edu.agh.strateg.ui.model.documents.DocumentsDataModel;
import pl.edu.agh.strateg.ui.model.metadata.MetadataGroup;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;
import pl.edu.agh.strateg.ui.view.components.common.ConfirmWindow;
import pl.edu.agh.strateg.ui.view.components.documents.SearchDocumentsLayout;
import pl.edu.agh.strateg.ui.view.views.AbstractDocumentsView;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

public class SearchDocumentsController implements HasChooseMetadata, HasChosenMetadata, HasSearchResults, CloseListener,
					HasConfirmWindow, HasChosenDocument, GotAllMetadaneListener, FoundFilesListener, ClickListener, Serializable{
	
	private ChooseMetadataController chooseMetadataController;
	protected ChosenMetadataController chosenMetadataController;
	private SearchResultsController searchResultsController;
	
	private DataModel chosenData;
	private boolean isChosen = false;
	
	SearchDocumentsLayout searchDocumentsLayout;
	
	public void setChosenMetadataController(ChosenMetadataController wybraneMetadaneController) {
		this.chosenMetadataController = wybraneMetadaneController;
	}

	public void setSearchResultsController(
			SearchResultsController wynikiWyszukiwaniaController) {
		this.searchResultsController = wynikiWyszukiwaniaController;
		
	}
	
	public void setChooseMetadataController(ChooseMetadataController wybierzMetadaneController) {
		this.chooseMetadataController = wybierzMetadaneController;
	}
	
	public void setSearchDocumentsLayout (SearchDocumentsLayout wyszukajDokumentyLayout) {
		this.searchDocumentsLayout = wyszukajDokumentyLayout;
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if (button.getCaption().equals(AbstractDocumentsView.SZUKAJ_BUTTON_CAPTION)) {
			searchButtonClicked();
		}
	}
	

	protected void searchButtonClicked() {
		if (chosenMetadataController.areValuesValid()) {
			if (chosenMetadataController.getAllValues().isEmpty()) {
				searchDocumentsLayout.displayConfirmWindow();
			}
			else {
				sendFindQuery();
			}
		}
		else {
			searchDocumentsLayout.displayNotValidValuesWarning();
		}
		
	}
	
	public void chooseData(DataModel daneModel) {
		this.chosenData = daneModel;
		this.isChosen = true;
		searchDocumentsLayout.documentChosen(daneModel);
	}
	
	public void fillChooseMetadataPanel() {
		GatewayImpl.getInstance().getAllDescriptors(this);
	}

	@Override
	public void chosenData(MetadatumModel metadana) {
		chosenMetadataController.datumChosen(metadana);
		chooseMetadataController.deleteDatum(metadana);
	}
	
	@Override
	public void gettingDescriptorsFailed() {
		System.out.println("Getting descriptors failed");
		
	}

	@Override
	public void descriptorsReceived(MetadataGroup grupaMetadanych) {
		chooseMetadataController.fillPanelWithData(grupaMetadanych);
		
	}

	@Override
	public void findingFilesFailed() {
		searchDocumentsLayout.findingFilesFailed();
		
	}
	
	@Override
	public void filesReceived(List<DataModel> daneDokumentu) {
		searchResultsController.displayData(daneDokumentu);
		
	}
	
	@Override
	public void datumValueChanged(MetadatumModel metadana) {

	}

	@Override
	public boolean canBeDeleted(MetadatumModel metadana) {
		return false;
	}

	@Override
	public void restoreDatum(MetadatumModel metadana) {
		
	}

	protected void sendFindQuery() {
		GatewayImpl.getInstance().findFiles(chosenMetadataController.getAllValues(), this);
	}

	@Override
	public void windowClose(CloseEvent e) {
		Window window = e.getWindow();
		if (window instanceof ConfirmWindow) {
			if (((ConfirmWindow)window).isConfirmed()) {
				sendFindQuery();
			}
		}
	}

	@Override
	public void previewDatum(DataModel model) {
		
	}

	@Override
	public DocumentsDataModel getChosenDocument() {
		if (isChosen) {
			return (DocumentsDataModel) chosenData;
		}
		else {
			return null;
		}
	}

	@Override
	public void databaseProblemOccured() {
		searchDocumentsLayout.databaseProblemOccured();
		
	}

}
