package pl.edu.agh.strateg.ui.view.components.common.access_controlled;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import com.vaadin.ui.Button;

public class AccessControlledButton extends Button{
	private String role;
	
	public AccessControlledButton() {
		
	}
	
	public AccessControlledButton(String role) {
		this.role = role;
	}
	
	public void setRole(String role) {
		this.role = role;
	}
	
	@Override
	public void setEnabled(boolean isEnabled) {
		Subject currentUser = SecurityUtils.getSubject();
		if (currentUser.hasRole(role)) {
			super.setEnabled(isEnabled);
		}
		else {
			super.setEnabled(false);
		}
	}
	
	@Override
	public boolean isEnabled() {
		Subject currentUser = SecurityUtils.getSubject();
		if (currentUser.hasRole(role)) {
			return super.isEnabled();
		}
		else {
			return false;
		}
	}

}
