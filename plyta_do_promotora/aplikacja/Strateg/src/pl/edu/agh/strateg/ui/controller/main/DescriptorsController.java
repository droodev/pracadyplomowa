package pl.edu.agh.strateg.ui.controller.main;

import java.io.Serializable;

import pl.edu.agh.strateg.ui.Views;
import pl.edu.agh.strateg.ui.controller.component.common.EditController;
import pl.edu.agh.strateg.ui.controller.component.common.ChooseMetadataController;
import pl.edu.agh.strateg.ui.controller.component.metadata.EditDescriptorController;
import pl.edu.agh.strateg.ui.controller.component.metadata.DescriptorDetailsController;
import pl.edu.agh.strateg.ui.controller.interfaces.HasEditWindow;
import pl.edu.agh.strateg.ui.controller.interfaces.HasConfirmWindow;
import pl.edu.agh.strateg.ui.controller.interfaces.HasMetadatumDetails;
import pl.edu.agh.strateg.ui.controller.interfaces.HasChooseMetadata;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.GotAllMetadaneListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.DescriptorDeletedListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.DescriptorUpdatedListener;
import pl.edu.agh.strateg.ui.model.communication.GatewayImpl;
import pl.edu.agh.strateg.ui.model.metadata.MetadataGroup;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;
import pl.edu.agh.strateg.ui.view.components.common.ConfirmWindow;
import pl.edu.agh.strateg.ui.view.components.common.TitlePanel;
import pl.edu.agh.strateg.ui.view.components.metadata.EditDescriptorWindow;
import pl.edu.agh.strateg.ui.view.views.DescriptorsView;

import com.vaadin.navigator.Navigator;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

public class DescriptorsController implements HasChooseMetadata, HasEditWindow, HasMetadatumDetails, ClickListener, GotAllMetadaneListener, 
						Serializable, CloseListener, DescriptorUpdatedListener, DescriptorDeletedListener, HasConfirmWindow {
	
	private Navigator navigator;
	
	private DescriptorDetailsController descriptorDetailsController;
	private ChooseMetadataController chooseMetadataController;
	
	private DescriptorsView descriptorsView;
	
	private MetadatumModel chosenDescriptor;

	private EditController editController;
	
	public DescriptorsController(Navigator navigator) {
		this.navigator = navigator;
	}
	
	public void setDescriptorsView(DescriptorsView metadaneView) {
		this.descriptorsView = metadaneView;
	}


	public void setDescriptorDetailsController(DescriptorDetailsController szczegolyMetadanejController) {
		this.descriptorDetailsController = szczegolyMetadanejController;
	}
	

	public void setChooseMetadataController(ChooseMetadataController wybierzMetadaneController) {
		this.chooseMetadataController = wybierzMetadaneController;
	}
	
	public void setEditController(EditController edytujController) {
		this.editController = edytujController;
		
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if (button.getCaption().equals(DescriptorsView.STRONA_GLOWNA_BUTTON_CAPTION)) {
			mainPageButtonClicked();
		}
		else if (button.getCaption().equals(DescriptorsView.NOWA_METADANA_BUTTON_CAPTION)) {
			newDescriptorButtonClicked();
		}
		else if (button.getCaption().equals(DescriptorsView.EDYTUJ_BUTTON_CAPTION)) {
			editButtonClicked();
		}
		else if (button.getCaption().equals(DescriptorsView.USUN_BUTTON_CAPTION)) {
			deleteButtonClicked();
		}
		
	}
	
	public void mainPageButtonClicked() {
		navigator.navigateTo(Views.MAIN_VIEW);
	}
	
	public void newDescriptorButtonClicked() {
		navigator.navigateTo(Views.NEW_DESCRIPTOR_VIEW);
	}
	
	public void editButtonClicked() {
		descriptorsView.displayEditWindow(chosenDescriptor);
	}
	
	public void deleteButtonClicked() {
		descriptorsView.displayConfirmWindow();
	}
	
	
	public void fillChooseFiltersPanel() {
		GatewayImpl.getInstance().getAllDescriptors(this);
	}


	@Override
	public void chosenData(MetadatumModel metadana) {
		this.chosenDescriptor = metadana;
		descriptorsView.enableButtons();
		descriptorDetailsController.displayMetadatum(metadana);
		descriptorsView.fillName(metadana.getSimpleDatumModel().getName());
	}


	@Override
	public void gettingDescriptorsFailed() {
		System.out.println("Getting descriptors failed");
		
	}


	@Override
	public void descriptorsReceived(MetadataGroup grupaMetadanych) {
		chooseMetadataController.fillPanelWithData(grupaMetadanych);
	}
	
	public void addEditWindow() {
		descriptorsView.displayEditWindow(chosenDescriptor);
	}
	
	@Override
	public void windowClose(CloseEvent e) {
		Window window = e.getWindow();
		if (window instanceof EditDescriptorWindow) {
			if (!editController.isChangeConfirmed()) {
				descriptorsView.modificationCancelled();
			}
			else {
				GatewayImpl.getInstance().updateDescriptor(((EditDescriptorController)editController).getMetadatum(), this);
			}
		}
		
		else if (window instanceof ConfirmWindow) {
			if (((ConfirmWindow) window).isConfirmed()) {
				GatewayImpl.getInstance().deleteDescriptor(chosenDescriptor, this);
			}
		}
		
	}
	
	@Override
	public void descriptorUpdatedSuccessfully() {
		descriptorsView.descriptorUpdatedSuccessfully();
		fillChooseFiltersPanel();
		chosenData(chosenDescriptor);
	}

	@Override
	public void descriptorUpdatingFailed() {
		descriptorsView.descriptorUpdatingFailed();
		
	}

	@Override
	public void databaseProblemOccured() {
		descriptorsView.databaseProblemOccured();
		
	}
	
	public void addNavigator(TitlePanel panel) {
		panel.setNavigator(navigator);
	}

	@Override
	public void descriptorDeletedSuccessfully() {
		descriptorsView.descriptorDeletedSuccessfully();
		fillChooseFiltersPanel();
		this.chosenDescriptor = null;
		descriptorDetailsController.clear();
		descriptorsView.reset();
	}

	@Override
	public void descriptorDeletingFailed() {
		descriptorsView.descriptorDeletingFailed();
		
	}


}
