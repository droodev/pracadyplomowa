package pl.edu.agh.strateg.ui.controller.component.common;

import java.io.Serializable;

import pl.edu.agh.strateg.ui.model.common.SimpleDatumModel;
import pl.edu.agh.strateg.ui.view.components.common.insert.InsertDatumValuePanel;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;

public class InsertDatumValueController implements ValueChangeListener, Serializable{
	
	private InsertDatumValuePanel panel;
	private ChosenMetadataController chosenMetadataController;
	
	public InsertDatumValueController(InsertDatumValuePanel panel, ChosenMetadataController wybraneMetadaneController) {
		this.panel = panel;
		this.chosenMetadataController = wybraneMetadaneController;
	}
	
	@Override
	public void valueChange(ValueChangeEvent event) {
		try {
			SimpleDatumModel model = panel.getValue();
			model.setValue(event.getProperty().getValue());
			chosenMetadataController.datumValueChanged(model);
		}
		catch (Exception e) {
			// wprowadzona wartosc nie jest poprawna wiec ja mozna olac
		}

		
	}

}
