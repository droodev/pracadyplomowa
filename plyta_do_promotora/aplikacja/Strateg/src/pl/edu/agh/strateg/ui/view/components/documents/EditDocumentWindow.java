package pl.edu.agh.strateg.ui.view.components.documents;

import pl.edu.agh.strateg.ui.controller.component.documents.EditDocumentController;
import pl.edu.agh.strateg.ui.model.documents.DocumentsDataModel;
import pl.edu.agh.strateg.ui.view.components.common.EditWindow;
import pl.edu.agh.strateg.utils.Icons;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.Receiver;

public class EditDocumentWindow extends EditWindow {
	
	private static final String WINDOW_CAPTION = "Edytuj dokument";
	private static final String ZALADOWANO_CAPTION = "Za�adowano: ";
	private static final String UPLOAD_ERROR = "Nie uda�o si� otworzy� pliku";
	private static final String FILE_NOT_UPLOADED_ERROR = "Nie dodano za��cznika";

	private static final String UPLOAD_CAPTION = "Edytuj za��cznik";
	private static final String WYBIERZ_CAPTION = "Wybierz";

	
	private Upload upload;
	

	public EditDocumentWindow(DocumentsDataModel daneDokumentu) {
		super(WINDOW_CAPTION);
		controller = new EditDocumentController(this, daneDokumentu);
		super.init();
	}
	

	
	public void displayFileNotUploadedWarning() {
		Notification.show(FILE_NOT_UPLOADED_ERROR, Type.ERROR_MESSAGE);
	}

	
	public void displayUploadProbleWarning() {
		Notification.show(UPLOAD_ERROR, Type.ERROR_MESSAGE);
	}

	
	public void successfullyUploaded(String fileName) {
		Notification.show(ZALADOWANO_CAPTION + fileName, Type.TRAY_NOTIFICATION);
	}
	

	@Override
	protected void initComponents() {
		super.initComponents();
		upload = new Upload();
		upload.setButtonCaption(WYBIERZ_CAPTION);
		upload.setCaption(UPLOAD_CAPTION);
		upload.setIcon(new ThemeResource(Icons.ATTACH));
		upload.setImmediate(true);
		
	}
	
	@Override
	protected void initLayouts() {
		super.initLayouts();
		customElementsTopLayout.addComponent(upload);
	}


	protected void setControllerToAllComponents() {
		super.setControllerToAllComponents();
		upload.setReceiver((Receiver) controller);
	}


}
