/**
	@author drew
 */

package pl.edu.agh.strateg.model.metadata;

import static pl.edu.agh.strateg.helpers.ExtendedPreconditions.*;

import java.util.Date;

import pl.edu.agh.strateg.helpers.DateHelper;
import pl.edu.agh.strateg.model.metadata.description.Descriptor;

public class SimpleMetadatum implements Metadatum {

	protected Descriptor descriptor;

	protected Object value;

	public SimpleMetadatum(Descriptor descriptor, Object value) {
		checkArgumentsNotNull(descriptor, value);
		this.descriptor = descriptor;
		if (descriptor.getValueType().equals(Date.class)) {
			this.value = DateHelper.normalizeDate((Date) value);
			return;
		}
		this.value = value;
	}

	public SimpleMetadatum() {

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((descriptor == null) ? 0 : descriptor.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!Metadatum.class.isAssignableFrom(obj.getClass())) {
			return false;
		}
		Metadatum other = (Metadatum) obj;
		if (descriptor == null) {
			if (other.getDescription() != null)
				return false;
		} else if (!descriptor.equals(other.getDescription()))
			return false;
		if (value == null) {
			if (other.getValue() != null)
				return false;
		} else if (!value.equals(other.getValue()))
			return false;
		return true;
	}

	@Override
	public Descriptor getDescription() {
		return descriptor;
	}

	@Override
	public Object getValue() {
		return value;
	}

	@Override
	public void setValue(Object newValue) {
		checkArgumentNotNull(newValue);
		value = newValue;

	}

}
