package pl.edu.agh.strateg.ui.model.documents;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import pl.edu.agh.strateg.ui.model.common.DataModel;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;
import pl.edu.agh.strateg.utils.ResetOnCloseInputStream;

public class DocumentsDataModel extends DataModel implements Serializable{
	
	private ResetOnCloseInputStream stream;
	private UUID uuid;
	
	public DocumentsDataModel() {
		
	}
	
	public DocumentsDataModel(List<MetadatumModel> metadane) {
		super(metadane);
	}
	
	public DocumentsDataModel(List<MetadatumModel> metadane, InputStream stream) {
		super(metadane);
		this.stream = new ResetOnCloseInputStream(new BufferedInputStream(stream));
	}


	public ResetOnCloseInputStream getStream() {
		return stream;
	}

	public void setStream(InputStream stream) {
		this.stream = new ResetOnCloseInputStream(new BufferedInputStream(stream));
	}
	
	public UUID getUuid() {
		return uuid;
	}


	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

}
