package pl.edu.agh.strateg.ui.view.components.common.insert;

import java.text.ParseException;

import com.vaadin.ui.Component;
import com.vaadin.ui.VerticalLayout;

public class InsertTagged implements InsertComponent{

	@Override
	public Object getValue() throws ParseException {
		return null;
	}

	@Override
	public void setValue(Object object) {
		
	}

	@Override
	public Component getField() {
		return new VerticalLayout();
	}

	@Override
	public boolean isValid() {
		return true;
	}

}
