package pl.edu.agh.strateg.ui.view.components.common.insert;

import java.util.Date;
import java.util.List;

import pl.edu.agh.strateg.model.metadata.description.Period;
import pl.edu.agh.strateg.ui.controller.component.common.InsertDatumValueController;


public class ComponentsFactory {
	
	private InsertDatumValueController controller;
	
	public ComponentsFactory(InsertDatumValueController controller) {
		this.controller = controller;
	}
	
	
	public InsertComponent getComponent(Class<?> klasa, List<Object> listaMozliwosci) {
		
		if (listaMozliwosci != null) {
			return new InsertEnum(listaMozliwosci, controller);
		}
		else if (klasa.equals(Date.class)) {
			return new InsertDate(controller);
		}
		else if (klasa.equals(Double.class)){
			return new InsertDouble(controller);
		}
		else if (klasa.equals(Integer.class)){
			return new InsertInteger(controller);
		}
		else if (klasa.equals(Period.class)) {
			return new InsertPeriod();
		}
		else if (klasa.equals(Void.class)) {
			return new InsertTagged();
		}
		else {
			return new InsertString(controller);
		}
		
		
	}

}
