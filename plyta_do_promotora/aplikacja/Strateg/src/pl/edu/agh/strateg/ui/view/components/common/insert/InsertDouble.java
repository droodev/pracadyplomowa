package pl.edu.agh.strateg.ui.view.components.common.insert;

import java.text.ParseException;

import pl.edu.agh.strateg.ui.controller.component.common.InsertDatumValueController;
import pl.edu.agh.strateg.utils.CustomDoubleFormatter;
import pl.edu.agh.strateg.utils.DoubleValidator;

import com.vaadin.ui.AbstractField;
import com.vaadin.ui.TextField;

public class InsertDouble implements InsertComponent {
	
	private static final String NIEPOPRAWNY_TYP_ERROR = "Niepoprawny typ";

	private TextField field;
	
	public InsertDouble(InsertDatumValueController controller) {
		field = new TextField();
		field.addValidator(new DoubleValidator());
		field.setRequired(true);
		field.setImmediate(true);
		field.addValueChangeListener(controller);
	}

	@Override
	public Object getValue() throws ParseException {
		return CustomDoubleFormatter.parse(field.getValue());

	}

	@Override
	public AbstractField<?> getField() {
		return field;
	}

	@Override
	public boolean isValid() {
		return field.isValid();
	}
	
	@Override
	public void setValue(Object object) {
		if (!(object instanceof Double)) {
			throw new RuntimeException(NIEPOPRAWNY_TYP_ERROR);
		}
		field.setValue(CustomDoubleFormatter.format((double) object));
		
	}

}
