/**
	@author drew
 */

package pl.edu.agh.strateg.logic;

import pl.edu.agh.strateg.model.metadata.description.Descriptor;

public interface DescriptorsManager extends MetadatumQuerable<Descriptor>,
		EntityManager<String, Descriptor> {

}
