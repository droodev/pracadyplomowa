package pl.edu.agh.strateg.ui.controller.interfaces.listeners;

public interface AnalysisUpdatedListener extends DatabaseProblemListener {
	
	void analysisUpdatedSuccessfully();
	void analysisUpdatingFailed();

}
