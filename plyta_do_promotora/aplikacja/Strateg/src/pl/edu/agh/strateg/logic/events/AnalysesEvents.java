/**
	@author drew
 */

package pl.edu.agh.strateg.logic.events;

import java.util.List;
import java.util.UUID;

import pl.edu.agh.strateg.logic.async.OperationListener;
import pl.edu.agh.strateg.model.analysis.Analysis;
import pl.edu.agh.strateg.model.analysis.SimpleAnalysis;
import pl.edu.agh.strateg.persistence.querying.MetadatumQuery;

import com.google.common.base.Optional;

public class AnalysesEvents {

	public static class SaveIfNotExistsAnalysisEvent extends
			OperationEventBase<SimpleAnalysis, Boolean> {
		public SaveIfNotExistsAnalysisEvent(SimpleAnalysis analysis,
				OperationListener<Boolean> listener) {
			super(Optional.<SimpleAnalysis> of(analysis), listener);
		}
	}

	public static class DeleteIfExistsAnalysisEvent extends
			OperationEventBase<SimpleAnalysis, Boolean> {
		public DeleteIfExistsAnalysisEvent(SimpleAnalysis analysis,
				OperationListener<Boolean> listener) {
			super(Optional.<SimpleAnalysis> of(analysis), listener);
		}
	}

	public static class UpdateIfExistsAnalysisEvent extends
			OperationEventBase<SimpleAnalysis, Boolean> {
		public UpdateIfExistsAnalysisEvent(SimpleAnalysis analysis,
				OperationListener<Boolean> listener) {
			super(Optional.<SimpleAnalysis> of(analysis), listener);
		}
	}

	public static class GetAnalysisEvent extends
			OperationEventBase<UUID, Optional<Analysis>> {
		public GetAnalysisEvent(UUID uuid,
				OperationListener<Optional<Analysis>> listener) {
			super(Optional.<UUID> of(uuid), listener);
		}
	}

	public static class GetAllAnalysesEvent extends
			OperationEventBase<Object, List<Analysis>> {
		public GetAllAnalysesEvent(OperationListener<List<Analysis>> listener) {
			super(Optional.absent(), listener);
		}
	}

	public static class QueryAnalysesByMetadataEvent extends
			OperationEventBase<MetadatumQuery, List<Analysis>> {
		public QueryAnalysesByMetadataEvent(Optional<MetadatumQuery> data,
				OperationListener<List<Analysis>> listener) {
			super(data, listener);
		}

	}
}
