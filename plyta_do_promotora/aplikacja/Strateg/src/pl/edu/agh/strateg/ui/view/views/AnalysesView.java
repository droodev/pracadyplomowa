package pl.edu.agh.strateg.ui.view.views;

import pl.edu.agh.strateg.ui.controller.main.AnalysesController;
import pl.edu.agh.strateg.ui.model.analysis.AnalysisDataModel;
import pl.edu.agh.strateg.ui.view.components.analysis.EditAnalysisWindow;
import pl.edu.agh.strateg.ui.view.components.analysis.PreviewAnalysisWindow;
import pl.edu.agh.strateg.ui.view.components.common.ConfirmWindow;
import pl.edu.agh.strateg.ui.view.components.common.TitlePanel;
import pl.edu.agh.strateg.ui.view.components.common.ChooseMetadataPanel;
import pl.edu.agh.strateg.ui.view.components.common.ChosenMetadataPanel;
import pl.edu.agh.strateg.ui.view.components.common.SearchResultsPanel;
import pl.edu.agh.strateg.utils.Icons;
import pl.edu.agh.strateg.utils.InputStreamList;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.VerticalLayout;

public class AnalysesView extends VerticalLayout implements View {
	
	public static final String STRONA_GLOWNA_BUTTON_CAPTION = "Strona g��wna";
	public static final String NOWA_ANALIZA_BUTTON_CAPTION = "Nowa analiza";
	public static final String SZUKAJ_BUTTON_CAPTION = "Szukaj";
	
	private static final String PAGE_CAPTION = "ANALIZA SWOT";
	private static final String WYBIERZ_FILTR_CAPTION = "Wybierz filtr";
	
	private static final String NIE_UDALO_SIE_POBRAC_PLIKOW = "Nie uda�o si� pobra� analiz";
	private static final String NOT_VALID_VALUES_WARNING = "Wprowadzone warto�ci nie s� poprawne";
	private static final String NIE_UDALO_SIE_ZMODYFIKOWAC = "Nie uda�o si� zmodyfikowa�";
	private static final String NIE_MOZNA_USUNAC = "Nie mo�na usun��";
	private static final String DATABASE_PROBLEM = "Problem przy dost�pie do bazy";
	private static final String ZMODYFIKOWANO_POPRAWNIE = "Zmodyfikowano poprawnie";
	private static final String USUNIETO = "Usuni�to";
	
	private static final String TABLE_COLUMN_NAME = "Analizy";
	
	private Button stronaGlownaButton;
	private Button nowaAnalizaButton;
	private Button szukajButton;
	
	private TitlePanel titlePanel;
	
	private ChooseMetadataPanel wybierzMetadanePanel;
	private ChosenMetadataPanel wybraneMetadanePanel;
	private SearchResultsPanel wynikiWyszukiwaniaPanel;
	
	private AnalysesController controller;
	
	public AnalysesView() {
		initComponents();
		initLayouts();
	}
	
	public void setControllerToAllComponents(AnalysesController controller) {
		this.controller = controller;
		controller.setAnalysesView(this);
		wybierzMetadanePanel.setMainController(controller);
		wybraneMetadanePanel.setMainController(controller);
		wynikiWyszukiwaniaPanel.setMainController(controller);
		setListenerToButtons();
		controller.addNavigator(titlePanel);
	}
	
	public void displayPreviewWindow(AnalysisDataModel model) {
		PreviewAnalysisWindow podgladAnalizyWindow = new PreviewAnalysisWindow();
		podgladAnalizyWindow.setMainController(controller);
		podgladAnalizyWindow.changeAnalysisData(model);
		getUI().addWindow(podgladAnalizyWindow);
		podgladAnalizyWindow.setModal(true);
	}
	
	public void displayEditWindow(AnalysisDataModel daneAnalizy) {
		EditAnalysisWindow edytujAnalizeWindow = new EditAnalysisWindow(daneAnalizy);
		edytujAnalizeWindow.setMainController(controller);
		getUI().addWindow(edytujAnalizeWindow);
		edytujAnalizeWindow.setModal(true);
	}
	
	public void displayConfirmWindow(String pytanie) {
		ConfirmWindow potwierdzWindow = new ConfirmWindow(controller, pytanie);
		getUI().addWindow(potwierdzWindow);
		potwierdzWindow.setModal(true);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		InputStreamList.getInstance().clear();
		fillPanel();
	}
	
	private void fillPanel() {
		controller.fillChooseMetadataPanel();
		InputStreamList.getInstance().clear();
	}
	
	private void initComponents() {
		stronaGlownaButton = new Button(STRONA_GLOWNA_BUTTON_CAPTION);
		nowaAnalizaButton = new Button(NOWA_ANALIZA_BUTTON_CAPTION);
		szukajButton = new Button(SZUKAJ_BUTTON_CAPTION);
		
		stronaGlownaButton.setIcon(new ThemeResource(Icons.MAIN));
		szukajButton.setIcon(new ThemeResource(Icons.FIND));
		nowaAnalizaButton.setIcon(new ThemeResource(Icons.NEW));
		
		wybierzMetadanePanel = new ChooseMetadataPanel(WYBIERZ_FILTR_CAPTION);
		wybraneMetadanePanel = new ChosenMetadataPanel();
		wynikiWyszukiwaniaPanel = new SearchResultsPanel(TABLE_COLUMN_NAME);
		
		titlePanel = new TitlePanel(PAGE_CAPTION, Icons.TITLE_ANALYSES, true);
		
	}

	private void initLayouts() {
		VerticalLayout mainLayout = new VerticalLayout();
		HorizontalLayout upperLayout = new HorizontalLayout();
		HorizontalLayout bottomLayout = new HorizontalLayout();
		HorizontalLayout buttonLayout = new HorizontalLayout();
		
		mainLayout.setSizeFull();
		mainLayout.setSpacing(true);
		mainLayout.setMargin(true);
		
		upperLayout.setSizeFull();
		upperLayout.setSpacing(true);
		
		bottomLayout.setSizeFull();
		bottomLayout.setSpacing(true);
		
		buttonLayout.setSizeFull();
		buttonLayout.setSpacing(true);
		
		buttonLayout.addComponent(stronaGlownaButton);
		
		upperLayout.addComponent(buttonLayout);
		upperLayout.addComponent(wybierzMetadanePanel);
		upperLayout.addComponent(nowaAnalizaButton);
		
		upperLayout.setComponentAlignment(buttonLayout, Alignment.TOP_LEFT);
		upperLayout.setComponentAlignment(nowaAnalizaButton, Alignment.TOP_RIGHT);
		
		upperLayout.setExpandRatio(buttonLayout, 0.25f);
		upperLayout.setExpandRatio(wybierzMetadanePanel, 0.5f);
		upperLayout.setExpandRatio(nowaAnalizaButton, 0.25f);
		
		VerticalLayout wybraneLayout = new VerticalLayout();
		wybraneLayout.setSizeFull();
		wybraneLayout.setSpacing(true);
		
		wybraneLayout.addComponent(wybraneMetadanePanel);
		wybraneLayout.addComponent(szukajButton);
		
		wybraneLayout.setComponentAlignment(szukajButton, Alignment.BOTTOM_RIGHT);
		
		bottomLayout.addComponent(wybraneLayout);
		bottomLayout.addComponent(wynikiWyszukiwaniaPanel);
		
		bottomLayout.setExpandRatio(wybraneLayout, 0.25f);
		bottomLayout.setExpandRatio(wynikiWyszukiwaniaPanel, 0.75f);
		
		mainLayout.addComponent(titlePanel);
		mainLayout.addComponent(upperLayout);
		mainLayout.addComponent(bottomLayout);
		addComponent(mainLayout);
		
	}

	private void setListenerToButtons() {

		stronaGlownaButton.addClickListener(controller);
		nowaAnalizaButton.addClickListener(controller);
		szukajButton.addClickListener(controller);
		
	}
	
	public void findingAnalysisFailed() {
		Notification.show(NIE_UDALO_SIE_POBRAC_PLIKOW, Type.ERROR_MESSAGE);
		
	}
	
	public void modificationCancelled() {
		
		
	}

	public void analysisUpdatedSuccessfully() {
		Notification.show(ZMODYFIKOWANO_POPRAWNIE, Type.TRAY_NOTIFICATION);
		
	}

	public void analysisUpdatingFailed() {
		Notification.show(NIE_UDALO_SIE_ZMODYFIKOWAC, Type.ERROR_MESSAGE);
		
	}
	
	public void analysisDeletedSuccessfully() {
		Notification.show(USUNIETO, Type.TRAY_NOTIFICATION);
		
	}

	public void analysisDeletingFailed() {
		Notification.show(NIE_MOZNA_USUNAC, Type.ERROR_MESSAGE);
		
	}

	public void displayNotValidValuesWarning() {
		Notification.show(NOT_VALID_VALUES_WARNING, Type.ERROR_MESSAGE);
		
	}
	
	public void databaseProblemOccured() {
		Notification.show(DATABASE_PROBLEM, Type.ERROR_MESSAGE);
	}


}
