package pl.edu.agh.strateg.ui.view.components.documents;

import pl.edu.agh.strateg.ui.controller.component.documents.SearchDocumentsController;
import pl.edu.agh.strateg.ui.controller.interfaces.HasChosenDocument;
import pl.edu.agh.strateg.ui.model.documents.DocumentsDataModel;
import pl.edu.agh.strateg.ui.view.views.DocumentsView;

import com.vaadin.ui.Window;

public class ChooseDocumentWindow extends Window implements HasChosenDocument{
	
	private static final String WINDOW_CAPTION = "Wybierz dokument";
	
	private SearchDocumentsLayout wyszukajDokumentyLayout;
	SearchDocumentsController controller;
	
	public ChooseDocumentWindow() {
		wyszukajDokumentyLayout = new SearchDocumentsLayout(DocumentsView.WYBIERZ_BUTTON_CAPTION);
		controller = new SearchDocumentsController();
		wyszukajDokumentyLayout.setControllerToAllComponents(controller);
		wyszukajDokumentyLayout.fillPanel();
		setContent(wyszukajDokumentyLayout);
		
		setHeight(15, Unit.CM);
		setWidth(30, Unit.CM);
		setResizable(false);
		setCaption(WINDOW_CAPTION);
	}

	@Override
	public DocumentsDataModel getChosenDocument() {
		return controller.getChosenDocument();
	}

}
