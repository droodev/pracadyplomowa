package pl.edu.agh.strateg.ui.controller.main;

import pl.edu.agh.strateg.ui.Views;
import pl.edu.agh.strateg.ui.model.communication.GatewayImpl;
import pl.edu.agh.strateg.ui.view.components.common.ConfirmWindow;

import com.vaadin.navigator.Navigator;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;

public class ReportController extends AbstractDocumentsController {
	
	public ReportController(Navigator navigator) {
		super(navigator);
	}
	
	protected void newDocumentButtonClicked() {
		navigator.navigateTo(Views.NEW_REPORT_VIEW);
	}
	

	protected void sendFindQuery() {
		GatewayImpl.getInstance().findReports(chosenMetadataController.getAllValues(), this);
	}

	@Override
	public void windowClose(CloseEvent e) {
		Window window = e.getWindow();
		if (window instanceof ConfirmWindow) {
			if (((ConfirmWindow)window).isConfirmed()) {
				sendFindQuery();
			}
		}
		
	}

	@Override
	public void deleteDatum() {
		throw new RuntimeException("Report cannot be deleted");
	}


}
