package pl.edu.agh.strateg.ui;

public class Views {
	public static final String LOGIN_VIEW = "logowanie";
	public static final String MAIN_VIEW = "start";
	public static final String DOCUMENTS_VIEW = "dokumenty";
	public static final String NEW_DOCUMENT_VIEW = "nowy_dokument";
	public static final String ANALYSES_VIEW = "analiza";
	public static final String NEW_ANALYSIS_VIEW = "nowa_analiza";
	public static final String REPORTS_VIEW = "raport";
	public static final String NEW_REPORT_VIEW = "nowy_raport";
	public static final String DESCRIPTORS_VIEW = "metadane";
	public static final String NEW_DESCRIPTOR_VIEW = "nowa_metadana";
}
