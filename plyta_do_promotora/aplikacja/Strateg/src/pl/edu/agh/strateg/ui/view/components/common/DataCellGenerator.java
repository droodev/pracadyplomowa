package pl.edu.agh.strateg.ui.view.components.common;

import java.util.List;

import pl.edu.agh.strateg.ui.controller.component.documents.SearchResultsController;
import pl.edu.agh.strateg.ui.model.common.DataModel;
import pl.edu.agh.strateg.ui.view.views.DocumentsView;

import com.vaadin.ui.Table;
import com.vaadin.ui.Table.ColumnGenerator;

public class DataCellGenerator implements ColumnGenerator {
	
	private List<DataModel> wszystkieDane;
	private SearchResultsController controller;
	private String buttonCaption;
	
	public DataCellGenerator(List<DataModel> dane, SearchResultsController controller,
			String buttonCaption) {
		this.buttonCaption = buttonCaption;
		this.wszystkieDane = dane;
		this.controller = controller;
	}

	@Override
	public Object generateCell(Table source, Object itemId, Object columnId) {
		DataModel model = wszystkieDane.get((Integer) itemId);
		if (buttonCaption.equals(DocumentsView.PODGLAD_DOKUMENTU_BUTTON_CAPTION)) {
			return new TableCellPanelWithPreview(model, controller);
		}
		else {
			return new TableCellPanelWithConfirm(model, controller);
		}
		
	}



}
