package pl.edu.agh.strateg.ui.model.communication;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.google.common.base.Optional;

import pl.edu.agh.strateg.model.Document;
import pl.edu.agh.strateg.model.SimpleDocument;
import pl.edu.agh.strateg.model.analysis.Analysis;
import pl.edu.agh.strateg.model.analysis.SimpleAnalysis;
import pl.edu.agh.strateg.model.metadata.MapMetadataContainer;
import pl.edu.agh.strateg.model.metadata.MetadataContainer;
import pl.edu.agh.strateg.model.metadata.Metadatum;
import pl.edu.agh.strateg.model.metadata.SimpleMetadatum;
import pl.edu.agh.strateg.model.metadata.description.CollectionLimitedDescriptor;
import pl.edu.agh.strateg.model.metadata.description.Descriptor;
import pl.edu.agh.strateg.model.metadata.description.Domain;
import pl.edu.agh.strateg.model.metadata.description.Period;
import pl.edu.agh.strateg.model.metadata.description.Requirement;
import pl.edu.agh.strateg.model.metadata.description.SimpleDescriptor;
import pl.edu.agh.strateg.model.required.Category;
import pl.edu.agh.strateg.persistence.querying.MetadatumQuery;
import pl.edu.agh.strateg.persistence.querying.MetadatumQuery.Builder;
import pl.edu.agh.strateg.ui.model.analysis.AnalysisDataModel;
import pl.edu.agh.strateg.ui.model.common.DataModel;
import pl.edu.agh.strateg.ui.model.common.SimpleDatumModel;
import pl.edu.agh.strateg.ui.model.common.DataTypes;
import pl.edu.agh.strateg.ui.model.communication.comparators.AnalysisComparator;
import pl.edu.agh.strateg.ui.model.communication.comparators.DescriptorsComparator;
import pl.edu.agh.strateg.ui.model.communication.comparators.DocumentsComparator;
import pl.edu.agh.strateg.ui.model.communication.comparators.MetadataComparator;
import pl.edu.agh.strateg.ui.model.documents.DocumentsDataModel;
import pl.edu.agh.strateg.ui.model.metadata.MetadataGroup;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;
import pl.edu.agh.strateg.ui.model.metadata.RequiredMetadatum;
import pl.edu.agh.strateg.utils.CustomDateFormatter;

public class Adapter implements Serializable {

	public Descriptor metadatumModelToDescriptor(MetadatumModel metadana) {

		Descriptor mainDescriptor = metadatumModelToSimpleDescriptor(metadana);

		Collection<Metadatum> metadatumList = null;
		List<MetadatumModel> metadaneOpisujace = metadana.getDescribingMetadata();
		if (metadaneOpisujace != null && !metadaneOpisujace.isEmpty()) {
			metadatumList = new ArrayList<>();
			for (MetadatumModel m : metadaneOpisujace) {
				Descriptor desc = createDescriptor(m.getSimpleDatumModel()
						.getName(), m.getSimpleDatumModel().getValueType());
				Metadatum metadatum = createMetadatum(desc, m
						.getSimpleDatumModel().getValue());
				metadatumList.add(metadatum);
			}
			MetadataContainer container = new MapMetadataContainer(
					metadatumList);
			mainDescriptor.setDescribingMetadata(container);
		}

		if (metadana.getPossibleValues() != null) {
			Collection<Object> mozliwosci = metadana.getPossibleValues();
			Descriptor toreturn = CollectionLimitedDescriptor
					.createLimitedDescriptor(mozliwosci,
							(Descriptor) mainDescriptor);
			return toreturn;
		}

		return mainDescriptor;
	}

	public Descriptor metadatumModelToSimpleDescriptor(MetadatumModel metadana) {
		Class<?> typ = metadana.getSimpleDatumModel().getValueType();
		String nazwa = metadana.getSimpleDatumModel().getName();
		return createDescriptor(nazwa, typ);
	}

	public MetadataGroup createMetadataGroup(List<Descriptor> descriptors) {
		
		Collections.sort(descriptors, new DescriptorsComparator());
		
		MetadataGroup grupa = new MetadataGroup("Wszystkie");

		MetadataGroup ogolne = new MetadataGroup("Og�lne");
		MetadataGroup mierniki = new MetadataGroup("Mierniki");
		MetadataGroup inne = new MetadataGroup("Inne");
		List<MetadataGroup> grupy = new ArrayList<MetadataGroup>();
		grupy.add(ogolne);
		grupy.add(mierniki);
		grupy.add(inne);
		grupa.setGroups(grupy);
		List<MetadatumModel> ogolneMetadane = new ArrayList<MetadatumModel>();
		List<MetadatumModel> miernikiMetadane = new ArrayList<MetadatumModel>();
		List<MetadatumModel> inneMetadane = new ArrayList<MetadatumModel>();
		
		Map<String, MetadatumModel> parsedMetadane = new HashMap<>();
		for (Descriptor descriptor : descriptors) {
			MetadatumModel metadana = descriptorToMetadatumModel(descriptor, parsedMetadane);
			if (metadana.getDescribingMetadata() != null) {
				for (MetadatumModel descMetadana : metadana.getDescribingMetadata()) {
					if (descMetadana.getSimpleDatumModel().getName()
							.equals("Kategoria")) {
						if (descMetadana.getSimpleDatumModel().getValue()
								.equals(Category.GENERAL.toString())) {
							ogolneMetadane.add(metadana);
						} else if (descMetadana.getSimpleDatumModel().getValue()
								.equals(Category.GOAL.toString())) {
							// do not add
						} else if (descMetadana.getSimpleDatumModel().getValue()
								.equals(Category.OBSERVABLE.toString())) {
							miernikiMetadane.add(metadana);
						} else {
							inneMetadane.add(metadana);
						}
						break;
					}
				}
			} else {
				inneMetadane.add(metadana);
			}

		}
		ogolne.setData(ogolneMetadane);
		mierniki.setData(miernikiMetadane);
		inne.setData(inneMetadane);

		return grupa;
	}

	public MetadatumModel descriptorToMetadatumModel(Descriptor descriptor, Map<String, MetadatumModel> alreadyParsed) {
		SimpleDatumModel model = new SimpleDatumModel(
				descriptor.getValueType());
		model.setName(descriptor.getName());
		List<MetadatumModel> metadaneOpisujace = null;
		MetadataContainer container = descriptor.getDescribingMetadata();
		List<Metadatum> metadata = new ArrayList<>();
		metadata.addAll(container.getAll());
		Collections.sort(metadata, new MetadataComparator());
		if (metadata.size() != 0) {
			metadaneOpisujace = new ArrayList<MetadatumModel>();
			for (Metadatum met : metadata) {
				String name = met.getDescription().getName();
				if (alreadyParsed.containsKey(name)) {
					MetadatumModel m = alreadyParsed.get(name);
					MetadatumModel newMetadana = new MetadatumModel(
						new SimpleDatumModel(
							m.getSimpleDatumModel().getName(), m.getSimpleDatumModel().getValue(), m.getSimpleDatumModel().getValueType()), 
						m.getDescribingMetadata(), 
						m.getPossibleValues(), 
						m.getWhereRequired(), 
						m.getRequirements());
					newMetadana.getSimpleDatumModel().setValue(met.getValue());
					metadaneOpisujace.add(newMetadana);
				}
				else {
					MetadatumModel m = descriptorToMetadatumModel(met.getDescription(), alreadyParsed);
					alreadyParsed.put(name, m);
					m.getSimpleDatumModel().setValue(met.getValue());
					metadaneOpisujace.add(m);
				}
			}
		}
		List<Object> mozliweWartosci = null;
		if (descriptor.isLimited()) {
			mozliweWartosci = new ArrayList<>(descriptor.toLimited()
					.getRelevantValues());
		}
		MetadatumModel metadana = new MetadatumModel(model, metadaneOpisujace,
				mozliweWartosci);
		List<DataTypes> typy = new ArrayList<>();
		if (descriptor.isObligatoryIn(Domain.ANALYSIS)) {
			typy.add(DataTypes.ANALIZA);
		}
		if (descriptor.isObligatoryIn(Domain.DESCRIPTOR)) {
			typy.add(DataTypes.METADANA);
		}
		if (descriptor.isObligatoryIn(Domain.DOCUMENT)) {
			typy.add(DataTypes.DOKUMENT);
		}
		if (descriptor.isObligatoryIn(Domain.REPORT)) {
			typy.add(DataTypes.RAPORT);
		}

		metadana.setWhereRequired(typy);

		HashMap<DataTypes, List<RequiredMetadatum>> wymagania = new HashMap<>();

		if (descriptor.getRequirements().getAllRequirements(Domain.ANALYSIS)
				.isPresent()) {
			List<RequiredMetadatum> wymagane = new ArrayList<>();
			for (Requirement r : descriptor.getRequirements()
					.getAllRequirements(Domain.ANALYSIS).get()) {
				wymagane.add(new RequiredMetadatum(r.getOnValue().get(), r
						.getMetadataName()));
			}
			wymagania.put(DataTypes.ANALIZA, wymagane);
		}
		if (descriptor.getRequirements().getAllRequirements(Domain.DOCUMENT)
				.isPresent()) {
			List<RequiredMetadatum> wymagane = new ArrayList<>();
			for (Requirement r : descriptor.getRequirements()
					.getAllRequirements(Domain.DOCUMENT).get()) {
				wymagane.add(new RequiredMetadatum(r.getOnValue().get(), r
						.getMetadataName()));
			}
			wymagania.put(DataTypes.DOKUMENT, wymagane);
		}
		if (descriptor.getRequirements().getAllRequirements(Domain.DESCRIPTOR)
				.isPresent()) {
			List<RequiredMetadatum> wymagane = new ArrayList<>();
			for (Requirement r : descriptor.getRequirements()
					.getAllRequirements(Domain.DESCRIPTOR).get()) {
				wymagane.add(new RequiredMetadatum(r.getOnValue().get(), r
						.getMetadataName()));
			}
			wymagania.put(DataTypes.METADANA, wymagane);
		}
		if (descriptor.getRequirements().getAllRequirements(Domain.REPORT)
				.isPresent()) {
			List<RequiredMetadatum> wymagane = new ArrayList<>();
			for (Requirement r : descriptor.getRequirements()
					.getAllRequirements(Domain.REPORT).get()) {
				wymagane.add(new RequiredMetadatum(r.getOnValue().get(), r
						.getMetadataName()));
			}
			wymagania.put(DataTypes.RAPORT, wymagane);
		}

		metadana.setRequirements(wymagania);
		return metadana;
	}

	public MetadatumModel metadatumToMetadatumModel(Metadatum metadatum, Map<String, MetadatumModel> alreadyParsed) {
		Descriptor descriptor = metadatum.getDescription();
		MetadatumModel metadana = descriptorToMetadatumModel(descriptor, alreadyParsed);
		Object wartosc = metadatum.getDescription().getValueType().equals(Void.class) ?
				null :
					metadatum.getValue();
		metadana.getSimpleDatumModel().setValue(wartosc);
		return metadana;
	}

	
	private MetadataContainer listMetadatumModelToMetadataContainer(
			List<MetadatumModel> metadane) {
		MetadataContainer container = new MapMetadataContainer();
		for (MetadatumModel metadana : metadane) {
			container.addMetadatum(
					createMetadatum(metadatumModelToSimpleDescriptor(metadana), 
					metadana.getSimpleDatumModel().getValue())
			);
		}
		return container;
	}
	
	private List<MetadatumModel> metadataContainerToListMetadatumModel(MetadataContainer container) {
		List<Metadatum> metadata = new ArrayList<>();
		metadata.addAll(container.getAll());
		Collections.sort(metadata, new MetadataComparator());
		List<MetadatumModel> metadane = new ArrayList<>();
		Map<String, MetadatumModel> parsedMetadane = new HashMap<>();
		for (Metadatum metadatum : metadata) {
			metadane.add(metadatumToMetadatumModel(metadatum, parsedMetadane));
		}
		return metadane;
	}

	public Document createDocument(File file, List<MetadatumModel> metadane) {
		Document document = new SimpleDocument();
		try {
			InputStream inputStream = new FileInputStream(file);
			document.setData(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
		List<Metadatum> metadata = new ArrayList<>();
		for (MetadatumModel metadana : metadane) {
			Descriptor descriptor = metadatumModelToSimpleDescriptor(metadana);
			Metadatum metadatum = createMetadatum(descriptor, metadana
					.getSimpleDatumModel().getValue());
			metadata.add(metadatum);
		}
		MapMetadataContainer container = new MapMetadataContainer(metadata);
		document.setDescribingMetadata(container);
		return document;
	}

	public Document documentsDataModelToData(DocumentsDataModel daneDokumentuModel) {

		SimpleDocument document = SimpleDocument
				.createWithUUID(daneDokumentuModel.getUuid());
		document.setData(daneDokumentuModel.getStream().getStream());
		document.setDescribingMetadata(listMetadatumModelToMetadataContainer(daneDokumentuModel.getMetadata()));
		return document;
	}

	private Descriptor createDescriptor(String nazwa, Class<?> typ) {
		if (typ.equals(String.class)) {
			return SimpleDescriptor.getCreator().createString(nazwa);
		} else if (typ.equals(Integer.class)) {
			return SimpleDescriptor.getCreator().createInteger(nazwa);
		} else if (typ.equals(Double.class)) {
			return SimpleDescriptor.getCreator().createDouble(nazwa);
		} else if (typ.equals(Date.class)) {
			return SimpleDescriptor.getCreator().createDate(nazwa);
		} else if (typ.equals(Period.class)) {
			return SimpleDescriptor.getCreator().createPeriod(nazwa);
		} else if (typ.equals(Void.class)) {
			return SimpleDescriptor.getCreator().createTagging(nazwa);
		} else {
			return SimpleDescriptor.getCreator().create(nazwa, typ.getClass());
		}
	}

	private Metadatum createMetadatum(Descriptor desc, Object wartosc) {
		if (desc.getValueType().equals(String.class)) {
			return new SimpleMetadatum((Descriptor) desc, (String) wartosc);
		} else if (desc.getValueType().equals(Integer.class)) {
			return new SimpleMetadatum((Descriptor) desc, (Integer) wartosc);
		} else if (desc.getValueType().equals(Double.class)) {
			return new SimpleMetadatum((Descriptor) desc, (Double) wartosc);
		} else if (desc.getValueType().equals(Date.class)) {
			return new SimpleMetadatum((Descriptor) desc, (Date) wartosc);
		} else if (desc.getValueType().equals(Period.class)) {
			return new SimpleMetadatum((Descriptor) desc, (Period) wartosc);
		} else if (desc.getValueType().equals(Void.class)) {
			return new SimpleMetadatum((Descriptor) desc, Optional.absent());
		} else {
			return new SimpleMetadatum((Descriptor) desc, (Object) wartosc);
		}
	}

	public MetadatumQuery createMetadatumQuery(List<MetadatumModel> metadane) {
		Builder builder = MetadatumQuery.getBuilder();
		for (MetadatumModel m : metadane) {
			Object wartosc = m.getSimpleDatumModel().getValue() == null ?
					Optional.absent() :
						m.getSimpleDatumModel().getValue();
			builder.metadatumName(m.getSimpleDatumModel().getName()).equalTo(wartosc);
		}
		return builder.build();
	}

	public List<DataModel> dataListToDocumentsDataModelList(List<Document> dataList) {
		List<DataModel> daneDokumentuModels = new ArrayList<>();
		if (dataList != null) {
			Collections.sort(dataList, new DocumentsComparator());
			for (Document data : dataList) {
				daneDokumentuModels.add(dataToDocumentsDataModel(data));
			}
		}
		return daneDokumentuModels;
		
	}

	public DocumentsDataModel dataToDocumentsDataModel(Document data) {

		DocumentsDataModel model = new DocumentsDataModel();
		model.setUuid(data.getUUID());
		model.setStream(data.getData());
		model.setMetadata(metadataContainerToListMetadatumModel(data.getDescribingMetadata()));
		return model;
	}
	
	public SimpleAnalysis createAnalysis(AnalysisDataModel analiza) {
		Analysis analysis = analiza.getAnalysis();
		analysis.setDescribingMetadata(listMetadatumModelToMetadataContainer(analiza.getMetadata()));
		return (SimpleAnalysis) analysis;
	}

	public List<DataModel> analysisListToAnalysisDataModelList(List<Analysis> analysisList) {
		Collections.sort(analysisList, new AnalysisComparator());
		List<DataModel> daneAnalizyList = new ArrayList<>();
		for (Analysis analysis: analysisList) {
			AnalysisDataModel model = new AnalysisDataModel();
			model.setAnalysis(analysis);
			model.setMetadata(metadataContainerToListMetadatumModel(analysis.getDescribingMetadata()));
			daneAnalizyList.add(model);
		}
		return daneAnalizyList;
	}
	
	public DataModel createDummyReport() {
		DocumentsDataModel model = new DocumentsDataModel();
		List<MetadatumModel> metadane = new ArrayList<>();
		try {
			MetadatumModel okres = new MetadatumModel(
					new SimpleDatumModel(
							"Okres", 
							Period.create(CustomDateFormatter.parse("01.01.2009"), CustomDateFormatter.parse("31.12.2009")), 
							Period.class), 
					null);
			MetadatumModel data = new MetadatumModel(
					new SimpleDatumModel(
							"Data", 
							CustomDateFormatter.parse("10.03.2010"), 
							Date.class), 
					null);
			metadane.add(okres);
			metadane.add(data);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		InputStream stream = null;
		System.out.println(System.getProperty("user.dir"));
		try {
			stream = new FileInputStream(new File("C:\\Users\\as\\Desktop\\pracaInzynierska\\Projekt\\pracadyplomowa\\aplikacja\\Strateg\\resources\\Sprawozdanie.pdf"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		model.setMetadata(metadane);
		model.setStream(stream);
		model.setUuid(UUID.randomUUID());
		return model;
	}


}
