package pl.edu.agh.strateg.ui.view.view_providers;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import pl.edu.agh.strateg.security.Roles;
import pl.edu.agh.strateg.ui.Views;
import pl.edu.agh.strateg.ui.controller.main.NewReportController;
import pl.edu.agh.strateg.ui.view.views.AccessDeniedView;
import pl.edu.agh.strateg.ui.view.views.NewReportView;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewProvider;

public class NewReportViewProvider implements ViewProvider {
	
	private NewReportController raportController;
	
	private Navigator.ClassBasedViewProvider provider = 
			new Navigator.ClassBasedViewProvider(Views.NEW_REPORT_VIEW, NewReportView.class);
	
	private Navigator.ClassBasedViewProvider noPermissionProvider = 
			new Navigator.ClassBasedViewProvider(Views.NEW_REPORT_VIEW, AccessDeniedView.class);
	
	public NewReportViewProvider(NewReportController raportController) {
		this.raportController = raportController;
	}
	
	@Override
	public String getViewName(String viewAndParameters) {
		Subject currentUser = SecurityUtils.getSubject();
		if (currentUser.isAuthenticated() && currentUser.hasRole(Roles.CREATE_REPORT)) {
			return provider.getViewName(viewAndParameters);
		}
		else {
			return noPermissionProvider.getViewName(viewAndParameters);
		}
	}
	@Override
	public View getView(String viewName) {
		Subject currentUser = SecurityUtils.getSubject();
		if (currentUser.isAuthenticated() && currentUser.hasRole(Roles.CREATE_REPORT)) {
			NewReportView view = (NewReportView) provider.getView(viewName);
			view.setControllerToAllComponents(raportController);
			return view;
		}
		else {
			AccessDeniedView view = (AccessDeniedView) noPermissionProvider.getView(viewName);
			return view;
		}
	}

}
