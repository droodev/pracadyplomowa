/**
	@author drew
 */

package pl.edu.agh.strateg.persistence.mongo.proxy;

import pl.edu.agh.strateg.model.metadata.Metadatum;
import pl.edu.agh.strateg.persistence.mongo.helpers.Mappings;
import pl.edu.agh.strateg.persistence.mongo.helpers.MorphiaRequirement;

import com.google.code.morphia.annotations.Property;

public class PropertyMetadatumProxy extends MetadatumProxy {

	protected PropertyMetadatumProxy(Metadatum metadatum) {
		super(metadatum);
		rawValue = metadatum.getValue();
	}

	@MorphiaRequirement
	private PropertyMetadatumProxy() {
	}

	@Property(value = Mappings.Metadata.VALUE)
	private Object rawValue;

	@Override
	public Object getValue() {
		return rawValue;
	}

	// @PostLoad
	// public void init() {
	// value = rawValue;
	// }

}
