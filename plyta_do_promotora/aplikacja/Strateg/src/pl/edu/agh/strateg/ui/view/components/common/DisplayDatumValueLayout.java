package pl.edu.agh.strateg.ui.view.components.common;

import pl.edu.agh.strateg.ui.model.common.SimpleDatumModel;

import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class DisplayDatumValueLayout extends VerticalLayout { 
	
	private Label nazwaLabel = new Label();
	private Label wartoscLabel = new Label();
	
	private SimpleDatumModel model;
	
	public DisplayDatumValueLayout(SimpleDatumModel model) {
		this.model = model;
		init();
	}
	 
	public void refresh() {
		nazwaLabel.setValue("<b>" + model.getName() + "</b>");
		wartoscLabel.setValue(model.getStringValue());
	}
	
	public SimpleDatumModel getValue() {
		return model; 
	}

	private void init() {
		HorizontalLayout mainLayout = new HorizontalLayout();
		mainLayout.setSizeFull();
		mainLayout.setSpacing(true);
		mainLayout.setMargin(new MarginInfo(false, true, false, true));
		nazwaLabel.setContentMode(ContentMode.HTML);
		refresh();
		
		mainLayout.addComponent(nazwaLabel);
		mainLayout.addComponent(wartoscLabel);
		
		addComponent(mainLayout);
		setSizeFull();
	}

}
