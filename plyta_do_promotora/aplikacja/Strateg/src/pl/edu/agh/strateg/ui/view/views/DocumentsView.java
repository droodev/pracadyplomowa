package pl.edu.agh.strateg.ui.view.views;

import pl.edu.agh.strateg.ui.controller.interfaces.HasEditWindow;
import pl.edu.agh.strateg.ui.model.documents.DocumentsDataModel;
import pl.edu.agh.strateg.ui.view.components.common.ConfirmWindow;
import pl.edu.agh.strateg.ui.view.components.documents.EditDocumentWindow;
import pl.edu.agh.strateg.utils.Icons;

import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

public class DocumentsView extends AbstractDocumentsView {
	
	public static final String CZY_USUNAC = "Czy na pewno chcesz usun�� dokument?";
	
	private static final String NIE_UDALO_SIE_ZMODYFIKOWAC = "Nie uda�o si� zmodyfikowa�";
	private static final String ZMODYFIKOWANO_POPRAWNIE = "Zmodyfikowano poprawnie";
	private static final String NIE_MOZNA_USUNAC = "Nie mo�na usun��";
	private static final String USUNIETO_POPRAWNIE = "Usuni�to";
	
	public DocumentsView() {
		super();
		areDataReadOnly = false;
	}
	
	
	protected void setCaptions() {
		NOWY_DOKUMENT_BUTTON_CAPTION = "Nowy dokument";
		PAGE_CAPTION = "DOKUMENTY";
		ICON = Icons.TITLE_DOCUMENTS;
	}
	
	public void displayEditWindow(DocumentsDataModel daneDokumentu) {
		EditDocumentWindow edytujDokumentWindow = new EditDocumentWindow(daneDokumentu);
		edytujDokumentWindow.setMainController((HasEditWindow) controller);
		getUI().addWindow(edytujDokumentWindow);
		edytujDokumentWindow.setModal(true);
	}
	
	public void displayConfirmDeletingWindow() {
		ConfirmWindow potwierdzWindow = new ConfirmWindow(dokumentyController, CZY_USUNAC);
		getUI().addWindow(potwierdzWindow);
		potwierdzWindow.setModal(true);
	}
	
	
	public void modificationCancelled() {
		
		
	}

	public void fileUpdatedSuccessfully() {
		Notification.show(ZMODYFIKOWANO_POPRAWNIE, Type.TRAY_NOTIFICATION);
		
	}

	public void fileUpdatingFailed() {
		Notification.show(NIE_UDALO_SIE_ZMODYFIKOWAC, Type.ERROR_MESSAGE);
		
	}
	
	public void fileDeletedSuccessfully() {
		Notification.show(USUNIETO_POPRAWNIE, Type.TRAY_NOTIFICATION);
		
	}

	public void fileDeletingFailed() {
		Notification.show(NIE_MOZNA_USUNAC, Type.ERROR_MESSAGE);
		
	}

	
}
