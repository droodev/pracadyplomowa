/**
	@author drew
 */

package pl.edu.agh.strateg.helpers;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

public class ByteOperationsHelper {

	public static ByteArrayOutputStream copyToByteArray(InputStream dataStream) {
		ByteArrayOutputStream data = new ByteArrayOutputStream();
		BufferedInputStream bufferedInputStream = new BufferedInputStream(
				dataStream);
		try {
			while (bufferedInputStream.available() > 0) {
				data.write(bufferedInputStream.read());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return data;
	}

	public static void zeroByteBufer(ByteBuffer data) {
		java.util.Arrays.fill(data.array(), (byte) 0);
		data.clear();
	}

}
