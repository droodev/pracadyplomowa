/**
	@author drew
 */

package pl.edu.agh.strateg.helpers;

import java.util.ArrayList;

import pl.edu.agh.strateg.backbone.DatabaseInitializer;

public class ExceptionsHelper {

	private ExceptionsHelper() {
		// TODO Auto-generated constructor stub
	}

	public static void printErrorsArray(final ArrayList<Throwable> errors) {
		StringBuilder builder = new StringBuilder();
		int i = 1;
		builder.append("Error(s) during intialization!\n");
		for (Throwable e : errors) {
			builder.append(String.format("\tError %d: %s\n", i, e.getMessage()));
			++i;
		}
		DatabaseInitializer.LOG.error(builder.toString());
	}

}
