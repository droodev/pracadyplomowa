/**
	@author drew
 */

package pl.edu.agh.strateg.logic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;

import pl.edu.agh.strateg.helpers.ExtendedPreconditions;
import pl.edu.agh.strateg.logic.async.OperationFlowTemplate;
import pl.edu.agh.strateg.logic.async.OperationFlowTemplate.Operation;
import pl.edu.agh.strateg.logic.async.OperationListener;
import pl.edu.agh.strateg.logic.verification.VerificationStatus;
import pl.edu.agh.strateg.logic.verification.Verifier;
import pl.edu.agh.strateg.model.metadata.Metadatum;
import pl.edu.agh.strateg.model.metadata.description.CollectionLimitedDescriptor;
import pl.edu.agh.strateg.model.metadata.description.Descriptor;
import pl.edu.agh.strateg.model.metadata.description.LimitedDescriptor;
import pl.edu.agh.strateg.model.metadata.description.SimpleDescriptor;
import pl.edu.agh.strateg.model.required.Category;
import pl.edu.agh.strateg.model.required.RequiredDescriptors;
import pl.edu.agh.strateg.persistence.AnalysesDAO;
import pl.edu.agh.strateg.persistence.DescriptorsDAO;
import pl.edu.agh.strateg.persistence.DocumentsDAO;
import pl.edu.agh.strateg.persistence.PersistenceException;
import pl.edu.agh.strateg.persistence.querying.MetadatumQuery;

import com.google.common.base.Optional;

public class DescriptorsManagerImpl implements
		EntityManager<String, Descriptor>, DescriptorsManager {
	private static final Logger LOG = Logger.getLogger(Thread.currentThread()
			.getStackTrace()[1].getClassName());
	private DescriptorsDAO descriptorsDAO;
	private AnalysesDAO analysesDAO;
	private DocumentsDAO documentsDAO;
	private Verifier verifier;

	@Inject
	public DescriptorsManagerImpl(DescriptorsDAO descriptorsDAO,
			AnalysesDAO analysesDAO, DocumentsDAO documentsDAO,
			Verifier verifier) {
		ExtendedPreconditions.checkArgumentsNotNull(descriptorsDAO,
				analysesDAO, documentsDAO, verifier);
		this.descriptorsDAO = descriptorsDAO;
		this.analysesDAO = analysesDAO;
		this.documentsDAO = documentsDAO;
		this.verifier = verifier;
	}

	@Override
	public void addEntity(final Descriptor descriptor,
			OperationListener<Boolean> listener) {
		new OperationFlowTemplate<Boolean>(listener, new Operation<Boolean>() {

			@Override
			public VerificationStatus validation() {
				return verifier.verifyDescriptor(descriptor,
						Optional.<Object> absent());
			}

			@Override
			public Boolean operate() throws PersistenceException {
				Optional<String> savedDescriptorNameOpt;
				if (!descriptorsDAO.saveIfNotExists(descriptor)) {
					savedDescriptorNameOpt = Optional.absent();
					return false;
				} else {
					savedDescriptorNameOpt = Optional.of(descriptor.getName());
				}
				if (goalDescriptorLimitsNeedsUpdate(descriptor,
						savedDescriptorNameOpt)) {
					updateGoalDescriptorLimits(savedDescriptorNameOpt.get());
				}
				return true;

			}

			private boolean goalDescriptorLimitsNeedsUpdate(
					final Descriptor descriptor,
					Optional<String> savedDescriptorNameOpt) {
				if (!savedDescriptorNameOpt.isPresent()) {
					return false;
				}

				Optional<Metadatum> categoryMetadatumOpt = descriptor
						.getDescribingMetadata()
						.getMetadatum(
								SimpleDescriptor.getCreator().createString(
										RequiredDescriptors.CATEGORY.getName()));

				if (!addedNewGoal(categoryMetadatumOpt)) {
					return false;
				}
				return true;
			}

			private void updateGoalDescriptorLimits(
					String savedNewGoalCategoryDescriptorName)
					throws PersistenceException {
				Optional<Descriptor> goalGot = descriptorsDAO
						.get(RequiredDescriptors.GOAL.getName());
				if (!goalGot.isPresent()) {
					LOG.error(String
							.format("Goal descriptor is absent. This situation shuldns't appear!"));
					return;
				}

				Descriptor gotGoalDescriptor = (Descriptor) goalGot.get();

				CollectionLimitedDescriptor updatedDescriptor = createDescriptorWithExtendedBoundingList(
						savedNewGoalCategoryDescriptorName, gotGoalDescriptor);
				descriptorsDAO.updateIfExists(updatedDescriptor);
			}

			private ArrayList<Object> createExtendedBoundingList(
					String extensionValue, Descriptor descriptorToExtend) {
				ArrayList<Object> arrayList = new ArrayList<>();
				if (!descriptorToExtend.isLimited()) {
					LOG.warn(String
							.format("Desciptor \"%s\" should be probably limited."
									+ " Although it's not, everything will work, but probably"
									+ "something should be in the other way",
									descriptorToExtend.getName()));
					return arrayList;
				}
				LimitedDescriptor limitedVersion = descriptorToExtend
						.toLimited();
				Collection<?> relevantValues = limitedVersion
						.getRelevantValues();
				arrayList.addAll(relevantValues);

				arrayList.add(extensionValue);
				return arrayList;
			}

			private CollectionLimitedDescriptor createDescriptorWithExtendedBoundingList(
					String boundingToAdd, Descriptor baseDescriptor) {
				ArrayList<Object> arrayList = createExtendedBoundingList(
						boundingToAdd, baseDescriptor);
				return CollectionLimitedDescriptor.createLimitedDescriptor(
						arrayList, baseDescriptor);
			}

			private boolean addedNewGoal(Optional<Metadatum> categoryName) {
				return categoryName.isPresent()
						&& categoryName.get().getValue()
								.equals(Category.GOAL.toString());
			}

		}).execute();

	}

	@Override
	public void deleteEntity(final Descriptor descriptor,
			OperationListener<Boolean> listener) {
		new OperationFlowTemplate<>(listener, new Operation<Boolean>() {

			@Override
			public VerificationStatus validation() {
				return verifier.verifyDescriptor(descriptor,
						Optional.<Object> absent());
			}

			@Override
			public Boolean operate() throws PersistenceException {
				MetadatumQuery query = MetadatumQuery.getBuilder()
						.metadatumName(descriptor.getName()).exists().build();
				int existingRefs = 0;
				existingRefs += analysesDAO.queryByMetadata(query).size();
				existingRefs += documentsDAO.queryByMetadata(query).size();
				existingRefs += descriptorsDAO.queryByMetadata(query).size();
				if (existingRefs != 0) {
					return false;
				} else {
					return descriptorsDAO.deleteIfExists(descriptor);
				}
			}

		}).execute();

	}

	@Override
	public void updateEntity(final Descriptor descriptor,
			OperationListener<Boolean> listener) {
		new OperationFlowTemplate<>(listener, new Operation<Boolean>() {

			@Override
			public VerificationStatus validation() {
				return verifier.verifyDescriptor(descriptor,
						Optional.<Object> absent());
			}

			@Override
			public Boolean operate() throws PersistenceException {
				return descriptorsDAO.updateIfExists(descriptor);
			}

		}).execute();

	}

	@Override
	public void getEntity(final String name,
			OperationListener<Optional<Descriptor>> listener) {
		new OperationFlowTemplate<>(listener,
				new Operation<Optional<Descriptor>>() {

					@Override
					public VerificationStatus validation() {
						return VerificationStatus.OK;
					}

					@Override
					public Optional<Descriptor> operate()
							throws PersistenceException {
						return descriptorsDAO.get(name);
					}

				}).execute();

	}

	@Override
	public void getAllEntities(OperationListener<List<Descriptor>> listener) {
		new OperationFlowTemplate<>(listener,
				new Operation<List<Descriptor>>() {

					@Override
					public VerificationStatus validation() {
						return VerificationStatus.OK;
					}

					@Override
					public List<Descriptor> operate()
							throws PersistenceException {
						return descriptorsDAO.getAll();
					}
				}).execute();

	}

	@Override
	public void queryByDescriptors(final MetadatumQuery query,
			OperationListener<List<Descriptor>> listener) {
		new OperationFlowTemplate<>(listener,
				new Operation<List<Descriptor>>() {

					@Override
					public VerificationStatus validation() {
						return VerificationStatus.OK;
					}

					@Override
					public List<Descriptor> operate()
							throws PersistenceException {
						return descriptorsDAO.queryByMetadata(query);
					}
				}).execute();
	}

}
