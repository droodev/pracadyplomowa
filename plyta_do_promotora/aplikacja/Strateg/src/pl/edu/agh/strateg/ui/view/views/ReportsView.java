package pl.edu.agh.strateg.ui.view.views;

import pl.edu.agh.strateg.ui.model.documents.DocumentsDataModel;
import pl.edu.agh.strateg.utils.Icons;


public class ReportsView extends AbstractDocumentsView {
	
	public ReportsView() {
		super();
		areDataReadOnly = false;
	}
	
	protected void setCaptions() {
		PYTANIE_DO_POTWIERDZENIA = "Czy na pewno chcesz wy�wietli� wszystkie raporty?";
		NOWY_DOKUMENT_BUTTON_CAPTION = "Nowy raport";
		PAGE_CAPTION = "RAPORTY";
		TABLE_COLUMN_NAME = "Raporty";
		NIE_UDALO_SIE_POBRAC_PLIKOW = "Nie uda�o si� pobra� raport�w";
		ICON = Icons.TITLE_REPORTS;
	}
	
	@Override
	public void displayPreviewWindow(DocumentsDataModel model) {
		super.displayPreviewWindow(model);
		podgladDokumentuWindow.disableButtons();
	}

}
