package pl.edu.agh.strateg.ui.view.views;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pl.edu.agh.strateg.model.metadata.description.Period;
import pl.edu.agh.strateg.ui.controller.main.NewDescriptorController;
import pl.edu.agh.strateg.ui.view.components.common.TitlePanel;
import pl.edu.agh.strateg.ui.view.components.common.ChooseMetadataPanel;
import pl.edu.agh.strateg.ui.view.components.common.ChosenMetadataPanel;
import pl.edu.agh.strateg.utils.Icons;
import pl.edu.agh.strateg.utils.InputStreamList;
import pl.edu.agh.strateg.utils.ValueTypes;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class NewDescriptorView extends VerticalLayout implements View {
	
	public static final String STRONA_GLOWNA_BUTTON_CAPTION = "Strona g��wna";
	public static final String POTWIERDZ_BUTTON_CAPTION = "Dodaj metadan�";
	public static final String ANULUJ_BUTTON_CAPTION = "Powr�t do metadanych";
	
	private static final String PAGE_CAPTION = "NOWA METADANA";
	private static final String DODAJ_METADANE_OPISUJACE_CAPTION = "Dodaj metadane opisuj�ce";
	private static final String WYBRANE_METADANE_OPISUJACE_CAPTION = "Wybrane metadane";
	private static final String NAZWA_LABEL_CAPTION = "Nazwa";
	private static final String TYP_LABEL_CAPTION = "Typ";
	private static final String MOZLIWE_WARTOSCI_CAPTION = "Mo�liwe warto�ci";
	private static final String DODAJ_MOZLIWE_WARTOSCI_CAPTION = "Dodaj mo�liwe warto�ci";
	private static final String EMPTY_NAME_WARNING = "Nazwa nie mo�e by� pusta";
	private static final String NOT_VALID_VALUES_WARNING = "Wprowadzone warto�ci nie s� poprawne";
	private static final String DODANO_POPRAWNIE_CAPTION = "Dodano poprawnie";
	private static final String NIE_UDALO_SIE_DODAC_CAPTION = "Nie uda�o si� doda�";
	private static final String DATABASE_PROBLEM = "Problem przy dost�pie do bazy";
	private static final String PUSTA_WARTOSC_WARNING = "Wpisz warto��";
	private static final String DODAJ_BUTTON_CAPTION = "Dodaj";
	
	private Button stronaGlownaButton;
	private Button potwierdzButton;
	private Button anulujButton;
	
	private ChooseMetadataPanel wybierzMetadanePanel;
	private ChosenMetadataPanel wybraneMetadanePanel;
	private TitlePanel titlePanel;
	
	private Label nazwaLabel;
	private TextField nazwaTextField;
	
	private Label typLabel;
	private ComboBox typComboBox;
	
	private Table mozliweWartosciTable;
	private TextField wartoscTextField;
	private Button dodajButton;
	private Panel wpiszListeMozliwosciPanel;
	
	private NewDescriptorController controller;
	
	public NewDescriptorView() {
		initComponents();
		initLayouts();
	}

	private void initComponents() {
		titlePanel = new TitlePanel(PAGE_CAPTION, Icons.TITLE_NEW_DESCRIPTOR, true);
		initButtons();
		initPanels();
		initTextFields();
		initLabels();
		initCombobox();
		initTable();
	}

	private void initButtons() {
		stronaGlownaButton = new Button(STRONA_GLOWNA_BUTTON_CAPTION);
		potwierdzButton = new Button(POTWIERDZ_BUTTON_CAPTION);
		anulujButton = new Button(ANULUJ_BUTTON_CAPTION);
		dodajButton = new Button(DODAJ_BUTTON_CAPTION);
		
		stronaGlownaButton.setIcon(new ThemeResource(Icons.MAIN));
		potwierdzButton.setIcon(new ThemeResource(Icons.CONFIRM));
		anulujButton.setIcon(new ThemeResource(Icons.CANCEL));
		dodajButton.setIcon(new ThemeResource(Icons.NEW));
		
		stronaGlownaButton.setImmediate(true);
		potwierdzButton.setImmediate(true);
		anulujButton.setImmediate(true);
		dodajButton.setImmediate(true);
	}

	private void initTextFields() {
		nazwaTextField = new TextField();
		nazwaTextField.setValue("");
		wartoscTextField = new TextField();
		wartoscTextField.setValue("");
	}

	private void initTable() {
		mozliweWartosciTable = new Table();
		mozliweWartosciTable.addContainerProperty(MOZLIWE_WARTOSCI_CAPTION, String.class, null);
		mozliweWartosciTable.setPageLength(0);
	}
	

	private void initLabels() {
		nazwaLabel = new Label("<b>" + NAZWA_LABEL_CAPTION + "</b>");
		nazwaLabel.setContentMode(ContentMode.HTML);
		
		typLabel = new Label("<b>" + TYP_LABEL_CAPTION + "</b>");
		typLabel.setContentMode(ContentMode.HTML);
	}

	private void initPanels() {
		wybierzMetadanePanel = new ChooseMetadataPanel(DODAJ_METADANE_OPISUJACE_CAPTION);
		wybraneMetadanePanel = new ChosenMetadataPanel(WYBRANE_METADANE_OPISUJACE_CAPTION, true);
	}

	private void initCombobox() {
		typComboBox = new ComboBox();
		typComboBox.addItem(ValueTypes.INTEGER);
		typComboBox.addItem(ValueTypes.DOUBLE);
		typComboBox.addItem(ValueTypes.DATE);
		typComboBox.addItem(ValueTypes.PERIOD);
		typComboBox.addItem(ValueTypes.STRING);
		typComboBox.addItem(ValueTypes.TAGGED);
		typComboBox.addItem(ValueTypes.ENUM);
		
		typComboBox.setItemCaption(ValueTypes.INTEGER, ValueTypes.INTEGER.getCaption());
		typComboBox.setItemCaption(ValueTypes.DOUBLE, ValueTypes.DOUBLE.getCaption());
		typComboBox.setItemCaption(ValueTypes.DATE, ValueTypes.DATE.getCaption());
		typComboBox.setItemCaption(ValueTypes.PERIOD, ValueTypes.PERIOD.getCaption());
		typComboBox.setItemCaption(ValueTypes.STRING, ValueTypes.STRING.getCaption());
		typComboBox.setItemCaption(ValueTypes.TAGGED, ValueTypes.TAGGED.getCaption());
		typComboBox.setItemCaption(ValueTypes.ENUM, ValueTypes.ENUM.getCaption());
		
		typComboBox.setNullSelectionAllowed(false);
		typComboBox.setValue(ValueTypes.INTEGER);
		typComboBox.setImmediate(true);
		typComboBox.setTextInputAllowed(false);
		
		typComboBox.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (typComboBox.getValue().equals(ValueTypes.ENUM)) {
					pokazWpiszListeMozliwosciLayout();
				}
				else {
					ukryjWpiszListeMozliwosciLayout();
				}
			}
		});
	}
	

	private void pokazWpiszListeMozliwosciLayout() {
		wpiszListeMozliwosciPanel.setVisible(true);
		mozliweWartosciTable.removeAllItems();
		
		wartoscTextField.setValue("");
		
		dodajButton.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				if (wartoscTextField.getValue().isEmpty()) {
					Notification.show(PUSTA_WARTOSC_WARNING, Type.WARNING_MESSAGE);
				}
				else {
					mozliweWartosciTable.addItem(
							new Object[] {wartoscTextField.getValue()}, 
							wartoscTextField.getValue());
					wartoscTextField.setValue("");
				}
			}
		});
		
	}
	
	private void ukryjWpiszListeMozliwosciLayout() {
		wpiszListeMozliwosciPanel.setVisible(false);
	}

	private void initLayouts() {
		VerticalLayout mainLayout = new VerticalLayout();
		
		mainLayout.setSpacing(true);
		
		HorizontalLayout upperLayout = new HorizontalLayout();
		VerticalLayout bottomLayout = new VerticalLayout();
		
		HorizontalLayout nazwaLayout = new HorizontalLayout();
		HorizontalLayout typLayout = new HorizontalLayout();
		wpiszListeMozliwosciPanel = new Panel();
		HorizontalLayout wpiszListeMozliwosciLayout = new HorizontalLayout();
		
		wpiszListeMozliwosciLayout.setSpacing(true);
		wpiszListeMozliwosciLayout.setMargin(true);
		wpiszListeMozliwosciLayout.addComponent(mozliweWartosciTable);
		wpiszListeMozliwosciLayout.addComponent(wartoscTextField);
		wpiszListeMozliwosciLayout.addComponent(dodajButton);
		
		wpiszListeMozliwosciPanel.setContent(wpiszListeMozliwosciLayout);
		wpiszListeMozliwosciPanel.setVisible(false);
		wpiszListeMozliwosciPanel.setCaption(DODAJ_MOZLIWE_WARTOSCI_CAPTION);
		
		upperLayout.setSizeFull();
		bottomLayout.setSizeFull();
		mainLayout.setSizeFull();
		
		upperLayout.setSpacing(true);
		bottomLayout.setSpacing(true);
		
		mainLayout.setMargin(true);
		
		HorizontalLayout buttonLayout = new HorizontalLayout();
		buttonLayout.addComponent(potwierdzButton);
		buttonLayout.addComponent(anulujButton);
		
		upperLayout.addComponent(stronaGlownaButton);
		upperLayout.addComponent(wybierzMetadanePanel);
		upperLayout.addComponent(buttonLayout);
		
		upperLayout.setExpandRatio(stronaGlownaButton, 0.25f);
		upperLayout.setExpandRatio(wybierzMetadanePanel, 0.5f);
		upperLayout.setExpandRatio(buttonLayout, 0.25f);
		
		upperLayout.setComponentAlignment(stronaGlownaButton, Alignment.TOP_LEFT);
		upperLayout.setComponentAlignment(buttonLayout, Alignment.TOP_RIGHT);
		
		buttonLayout.setSpacing(true);
		nazwaLayout.setSpacing(true);
		typLayout.setSpacing(true);
		
		nazwaLayout.addComponent(nazwaLabel);
		nazwaLayout.addComponent(nazwaTextField);
		
		typLayout.addComponent(typLabel);
		typLayout.addComponent(typComboBox);
		
		nazwaLayout.setComponentAlignment(nazwaLabel, Alignment.TOP_LEFT);
		nazwaLayout.setComponentAlignment(nazwaTextField, Alignment.TOP_LEFT);
		
		bottomLayout.addComponent(nazwaLayout);
		bottomLayout.addComponent(typLayout);
		bottomLayout.addComponent(wpiszListeMozliwosciPanel);
		bottomLayout.addComponent(wybraneMetadanePanel);
		
		mainLayout.addComponent(titlePanel);
		mainLayout.addComponent(upperLayout);
		mainLayout.addComponent(bottomLayout);
		
		addComponent(mainLayout);
		
	}
	
	public Class<?> getType() {
		ValueTypes typ = (ValueTypes) typComboBox.getValue();
		if (typ.equals(ValueTypes.DATE)) {
			return Date.class;
		}
		else if (typ.equals(ValueTypes.PERIOD)) {
			return Period.class;
		}
		else if (typ.equals(ValueTypes.DOUBLE)) {
			return Double.class;
		}
		else if (typ.equals(ValueTypes.INTEGER)) {
			return Integer.class;
		}
		else if (typ.equals(ValueTypes.TAGGED)) {
			return Void.class;
		}
		else {
			return String.class;
		}
	}
	
	public boolean isEnum() {
		return typComboBox.getValue().equals(ValueTypes.ENUM);
	}
	
	public List<Object> getPossibleValues() {
		List<Object> wartosci = new ArrayList<Object>();
		wartosci.addAll( mozliweWartosciTable.getItemIds());
		return wartosci;
	}
	

	@Override
	public void enter(ViewChangeEvent event) {
		fillPanel();
		InputStreamList.getInstance().clear();
	}
	
	public String getNewName() {
		return nazwaTextField.getValue();
	}
	
	public void displayEmptyNameWarning() {
		Notification.show(EMPTY_NAME_WARNING, Type.ERROR_MESSAGE);
	}
	
	public void displayNotValidValuesWarning() {
		Notification.show(NOT_VALID_VALUES_WARNING, Type.ERROR_MESSAGE);
	}
	
	public void successfullyAdded() {
		resetContent();
		Notification.show(DODANO_POPRAWNIE_CAPTION, Type.TRAY_NOTIFICATION);
		
	}

	private void resetContent() {
		wybierzMetadanePanel.clear();
		clearWybraneMetadanePanel();
		controller.fillChooseMetadataPanel();
		nazwaTextField.setValue("");
		typComboBox.setValue(ValueTypes.INTEGER);
	}
	
	public void addingFailed() {
		resetContent();
		Notification.show(NIE_UDALO_SIE_DODAC_CAPTION, Type.ERROR_MESSAGE);
	}

	
	private void clearWybraneMetadanePanel() {
		wybraneMetadanePanel.removeAllFilters();
	}
	
	public void setControllerToAllComponents(
			NewDescriptorController nowaMetadanaController) {
		this.controller = nowaMetadanaController;
		nowaMetadanaController.setNewDescriptorView(this);
		wybierzMetadanePanel.setMainController(controller);
		wybraneMetadanePanel.setMainController(controller);
		setListenerToButtons();
		controller.addNavigator(titlePanel);
	}
	


	private void setListenerToButtons() {
		stronaGlownaButton.addClickListener(controller);
		potwierdzButton.addClickListener(controller);
		anulujButton.addClickListener(controller);
	}
	
	private void fillPanel() {
		controller.fillChooseMetadataPanel();
		
	}
	
	public void databaseProblemOccured() {
		resetContent();
		Notification.show(DATABASE_PROBLEM, Type.ERROR_MESSAGE);
	}
	

}
