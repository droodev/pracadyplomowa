package pl.edu.agh.strateg.ui.controller.main;

import pl.edu.agh.strateg.ui.Views;
import pl.edu.agh.strateg.ui.controller.component.documents.PreviewController;
import pl.edu.agh.strateg.ui.controller.component.documents.SearchDocumentsController;
import pl.edu.agh.strateg.ui.controller.interfaces.HasPreviewWindow;
import pl.edu.agh.strateg.ui.model.common.DataModel;
import pl.edu.agh.strateg.ui.model.documents.DocumentsDataModel;
import pl.edu.agh.strateg.ui.view.components.common.TitlePanel;
import pl.edu.agh.strateg.ui.view.views.AbstractDocumentsView;

import com.vaadin.navigator.Navigator;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;

public abstract class AbstractDocumentsController extends SearchDocumentsController implements HasPreviewWindow{
	
	protected Navigator navigator;
	
	@SuppressWarnings("unused")
	private PreviewController previewController;
	
	protected AbstractDocumentsView documentsView;
	protected DocumentsDataModel currentData;
	
	public AbstractDocumentsController(Navigator navigator) {
		this.navigator = navigator;
	}
	
	public void setDocumentsView(AbstractDocumentsView dokumentyView) {
		this.documentsView = dokumentyView;
	}
	
	
	public void setPreviewController(PreviewController podgladController) {
		this.previewController = podgladController;
	}
	

	
	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if (button.getCaption().equals(AbstractDocumentsView.STRONA_GLOWNA_BUTTON_CAPTION)) {
			mainPageButtonClicked();
		}
		else if (button.getCaption().equals(documentsView.NOWY_DOKUMENT_BUTTON_CAPTION)) {
			newDocumentButtonClicked();
		}
		else if (button.getCaption().equals(AbstractDocumentsView.SZUKAJ_BUTTON_CAPTION)) {
			super.searchButtonClicked();
		}
		
	}

	private void mainPageButtonClicked() {
		navigator.navigateTo(Views.MAIN_VIEW);
		
	}

	protected abstract void newDocumentButtonClicked();
	
	
	public void previewDatum(DataModel model) {
		this.currentData = (DocumentsDataModel) model;
		documentsView.displayPreviewWindow((DocumentsDataModel)model);
	}
	
	public void addNavigator(TitlePanel panel) {
		panel.setNavigator(navigator);
	}

}
