package pl.edu.agh.strateg.ui.model.communication.comparators;

import java.util.Comparator;

import pl.edu.agh.strateg.model.metadata.Metadatum;

public class MetadataComparator implements Comparator<Metadatum>{



	@Override
	public int compare(Metadatum metadatum1, Metadatum metadatum2) {
		return new DescriptorsComparator().compare(metadatum1.getDescription(), metadatum2.getDescription());
	}

}
