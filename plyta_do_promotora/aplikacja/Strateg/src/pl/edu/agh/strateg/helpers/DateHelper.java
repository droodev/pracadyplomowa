/**
	@author drew
 */

package pl.edu.agh.strateg.helpers;

import java.util.Calendar;
import java.util.Date;

public class DateHelper {

	private DateHelper() {
		// TODO Auto-generated constructor stub
	}

	public static Date normalizeDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

}
