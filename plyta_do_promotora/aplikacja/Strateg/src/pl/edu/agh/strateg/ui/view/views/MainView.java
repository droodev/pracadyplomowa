package pl.edu.agh.strateg.ui.view.views;

import pl.edu.agh.strateg.security.Roles;
import pl.edu.agh.strateg.ui.controller.main.MainController;
import pl.edu.agh.strateg.ui.view.components.common.TitlePanel;
import pl.edu.agh.strateg.ui.view.components.common.access_controlled.AccessControlledNativeButton;
import pl.edu.agh.strateg.utils.Icons;
import pl.edu.agh.strateg.utils.InputStreamList;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;

public class MainView extends VerticalLayout implements View {
	
	private static final String DOKUMENTY_CAPTION = "Dokumenty";
	private static final String ANALIZA_CAPTION = "Analiza SWOT";
	private static final String RAPORT_CAPTION = "Raporty";
	private static final String METADANE_CAPTION = "Metadane";
	
	private static final String PAGE_CAPTION = "STRONA G��WNA";
	
	private AccessControlledNativeButton dokumentyButton;
	private AccessControlledNativeButton analizaButton;
	private AccessControlledNativeButton raportButton;
	private AccessControlledNativeButton metadaneButton;
	
	private TitlePanel titlePanel;
	
	private MainController controller;
	
	public MainView() {
		titlePanel = new TitlePanel(PAGE_CAPTION, Icons.TITLE_MAIN, true);
		initButtons();
		addListeners();
		initLayouts();
	}
	
	public void setControllerToAllComponents(MainController controller) {
		this.controller = controller;
		controller.addNavigator(titlePanel);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		InputStreamList.getInstance().clear();
	}
	
	private void initButtons() {
		dokumentyButton = new AccessControlledNativeButton(Roles.SEE_DOCUMENTS);
		analizaButton = new AccessControlledNativeButton(Roles.SEE_ANALYSES);
		raportButton = new AccessControlledNativeButton(Roles.SEE_REPORTS);
		metadaneButton = new AccessControlledNativeButton(Roles.SEE_DESCRIPTORS);
		
		dokumentyButton.setCaption(DOKUMENTY_CAPTION);
		analizaButton.setCaption(ANALIZA_CAPTION);
		raportButton.setCaption(RAPORT_CAPTION);
		metadaneButton.setCaption(METADANE_CAPTION);
		
		dokumentyButton.setImmediate(true);
		analizaButton.setImmediate(true);
		raportButton.setImmediate(true);
		metadaneButton.setImmediate(true);
		
		dokumentyButton.setEnabled(true);
		analizaButton.setEnabled(true);
		raportButton.setEnabled(true);
		metadaneButton.setEnabled(true);
		
		dokumentyButton.setIcon(new ThemeResource(Icons.DOCUMENTS));
		analizaButton.setIcon(new ThemeResource(Icons.ANALYSES));
		raportButton.setIcon(new ThemeResource(Icons.REPORTS));
		metadaneButton.setIcon(new ThemeResource(Icons.DESCRIPTORS));
		
		dokumentyButton.addStyleName("tile");
		analizaButton.addStyleName("tile");
		raportButton.addStyleName("tile");
		metadaneButton.addStyleName("tile");
	}

	private void addListeners() {
		dokumentyButton.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				controller.documentsButtonClicked();
				
			}
		});
		
		analizaButton.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				controller.analysesButtonClicked();
				
			}
		});
		
		metadaneButton.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				controller.descriptorsButtonClicked();
				
			}
		});
		
		raportButton.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				controller.reportsButtonClicked();
				
			}
			
		});
		
	}
	


	private void initLayouts() {
		
		VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.setMargin(true);
		
		HorizontalLayout layout = new HorizontalLayout();
		layout.setSpacing(true);
		layout.setSizeFull();
		
		VerticalLayout leftLayout = new VerticalLayout();
		VerticalLayout rightLayout = new VerticalLayout();
		
		leftLayout.setSizeFull();
		rightLayout.setSizeFull();
		leftLayout.setMargin(true);
		rightLayout.setMargin(true);
		
		leftLayout.setSpacing(true);
		rightLayout.setSpacing(true);
		
		leftLayout.addComponent(dokumentyButton);
		leftLayout.addComponent(analizaButton);
		
		rightLayout.addComponent(metadaneButton);
		rightLayout.addComponent(raportButton);
		
		layout.addComponent(leftLayout);
		layout.addComponent(rightLayout);
		
		mainLayout.addComponent(titlePanel);
		mainLayout.addComponent(layout);
		
		addComponent(mainLayout);
		
		
	}

}
