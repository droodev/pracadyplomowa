/**
	@author drew
 */

package pl.edu.agh.strateg.logic.events;

import static pl.edu.agh.strateg.helpers.ExtendedPreconditions.*;
import pl.edu.agh.strateg.logic.async.OperationListener;

import com.google.common.base.Optional;

public abstract class OperationEventBase<D, R> implements
		OperationEvent<D, R> {
	private Optional<D> data;
	private OperationListener<R> listener;

	protected OperationEventBase(Optional<D> data,
			OperationListener<R> listener) {
		checkArgumentsNotNull(data, listener);
		this.data = data;
		this.listener = listener;
	}

	@Override
	public OperationListener<R> getListener() {
		return listener;
	}

	@Override
	public Optional<D> getOperationData() {
		return data;
	}

}
