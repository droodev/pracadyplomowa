/**
	@author drew
 */

package pl.edu.agh.strateg.logic.events;

import java.util.List;

import pl.edu.agh.strateg.logic.async.OperationListener;
import pl.edu.agh.strateg.model.metadata.description.Descriptor;
import pl.edu.agh.strateg.persistence.querying.MetadatumQuery;

import com.google.common.base.Optional;

public class DescriptorEvents {
	public static class SaveIfNotExistDescriptorEvent extends
			OperationEventBase<Descriptor, Boolean> {
		@SuppressWarnings("unchecked")
		public SaveIfNotExistDescriptorEvent(Descriptor data,
				OperationListener<Boolean> listener) {
			super((Optional<Descriptor>) (Object) Optional.of(data), listener);
		}
	}

	public static class UpdateIfExistsDescriptorEvent extends
			OperationEventBase<Descriptor, Boolean> {
		@SuppressWarnings("unchecked")
		public UpdateIfExistsDescriptorEvent(Descriptor data,
				OperationListener<Boolean> listener) {
			super((Optional<Descriptor>) (Object) Optional.of(data), listener);
		}
	}

	public static class DeleteIfExistsDescriptorEvent extends
			OperationEventBase<Descriptor, Boolean> {
		@SuppressWarnings("unchecked")
		public DeleteIfExistsDescriptorEvent(Descriptor data,
				OperationListener<Boolean> listener) {
			super((Optional<Descriptor>) (Object) Optional.of(data), listener);
		}
	}

	public static class GetDescriptorEvent extends
			OperationEventBase<String, Optional<Descriptor>> {
		public GetDescriptorEvent(String data,
				OperationListener<Optional<Descriptor>> listener) {
			super(Optional.of(data), listener);
		}
	}

	public static class GetAllDescriptorsEvent extends
			OperationEventBase<Object, List<Descriptor>> {
		public GetAllDescriptorsEvent(
				OperationListener<List<Descriptor>> listener) {
			super(Optional.absent(), listener);
		}
	}

	public static class QueryDescriptorsByMetadataEvent extends
			OperationEventBase<MetadatumQuery, List<Descriptor>> {

		public QueryDescriptorsByMetadataEvent(Optional<MetadatumQuery> data,
				OperationListener<List<Descriptor>> listener) {
			super(data, listener);
		}

	}

}
