package pl.edu.agh.strateg.ui.view.view_providers;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import pl.edu.agh.strateg.ui.Views;
import pl.edu.agh.strateg.ui.controller.main.NewDocumentController;
import pl.edu.agh.strateg.ui.view.views.AccessDeniedView;
import pl.edu.agh.strateg.ui.view.views.NewDocumentView;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewProvider;

public class NewDocumentViewProvider implements ViewProvider {
	
	private NewDocumentController dokumentyController;
	
	private Navigator.ClassBasedViewProvider provider = 
			new Navigator.ClassBasedViewProvider(Views.NEW_DOCUMENT_VIEW, NewDocumentView.class);
	
	private Navigator.ClassBasedViewProvider noPermissionProvider = 
			new Navigator.ClassBasedViewProvider(Views.NEW_DOCUMENT_VIEW, AccessDeniedView.class);
	
	public NewDocumentViewProvider(NewDocumentController dokumentyController) {
		this.dokumentyController = dokumentyController;
	}
	
	@Override
	public String getViewName(String viewAndParameters) {
		Subject currentUser = SecurityUtils.getSubject();
		if (currentUser.isAuthenticated()) {
			return provider.getViewName(viewAndParameters);
		}
		else {
			return noPermissionProvider.getViewName(viewAndParameters);
		}
	}
	@Override
	public View getView(String viewName) {
		
		Subject currentUser = SecurityUtils.getSubject();
		if (currentUser.isAuthenticated()) {
			NewDocumentView view = (NewDocumentView) provider.getView(viewName);
			view.setControllerToAllComponents(dokumentyController);
			return view;
		}
		else {
			AccessDeniedView view = (AccessDeniedView) noPermissionProvider.getView(viewName);
			return view;
		}
	}

}
