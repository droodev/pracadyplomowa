package pl.edu.agh.strateg.ui.controller.interfaces.listeners;

import java.util.List;

import pl.edu.agh.strateg.ui.model.common.DataModel;

public interface FoundAnalysisListener extends DatabaseProblemListener {
	
	void findingAnalysisFailed();
	void analysesReceived(List<DataModel> daneModel);

}
