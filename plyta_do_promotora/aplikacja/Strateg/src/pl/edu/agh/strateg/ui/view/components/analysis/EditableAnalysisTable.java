package pl.edu.agh.strateg.ui.view.components.analysis;

import java.util.UUID;

import pl.edu.agh.strateg.ui.controller.component.analysis.AnalysisTableController;
import pl.edu.agh.strateg.ui.view.components.documents.ChooseDocumentWindow;

import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;

public class EditableAnalysisTable extends AbstractAnalysisTable{
	
	private static final String ADD = "Dodaj za��cznik";
	private static final String DODANO_ZALACZNIK = "Dodano za��cznik";
	
	private ChooseDocumentWindow wybierzDokumentWindow;
	
	public EditableAnalysisTable() {
		super();
		controller = new AnalysisTableController(this);
	}

	
	@Override
	protected Component createField() {
		TextField textField = new TextField();
		textField.setValue("0");
		textField.addValidator(controller);
		textField.setImmediate(true);
		return textField;
	}
	
	@Override
	protected MenuBar createMenuBar(int col, int row) {
		
		Command dodajDokumentCommand = new Command() {
			
			@Override
			public void menuSelected(MenuItem selectedItem) {
				wybraneMenu = selectedItem;
				displayChooseDocumentWindow();
				
			}
		};
		MenuBar menu = new MenuBar();
		
		MenuItem item = menu.addItem("", null, null);
		MenuItem itemWithCommand = item.addItem(ADD, null, dodajDokumentCommand);
		menuItems.put(itemWithCommand, new Wspolrzedne(col, row));
		menuBars.put(itemWithCommand, menu);
		
		menu.setWidth("10px");
		menu.setAutoOpen(true);
		
		return menu;
	}
	
	protected void displayChooseDocumentWindow() {
		wybierzDokumentWindow = new ChooseDocumentWindow();
		getUI().addWindow(wybierzDokumentWindow);
		wybierzDokumentWindow.addCloseListener(controller);
		wybierzDokumentWindow.setModal(true);
		
	}

	protected String getFieldValue(int i, int j) {
		HorizontalLayout layout = (HorizontalLayout) tableLayout.getComponent(i, j);
		TextField textField = (TextField)layout.getComponent(0);
		return textField.getValue();
	}
	
	protected void setFieldValue(int i, int j, int wartosc) {
		HorizontalLayout layout = (HorizontalLayout) tableLayout.getComponent(i, j);
		TextField textField = (TextField)layout.getComponent(0);
		textField.setValue(String.valueOf(wartosc));
	}

	@Override
	public void addAttachment(UUID uuid) {
		super.addAttachment(uuid);
		Notification.show(DODANO_ZALACZNIK, Type.TRAY_NOTIFICATION);
	}

}
