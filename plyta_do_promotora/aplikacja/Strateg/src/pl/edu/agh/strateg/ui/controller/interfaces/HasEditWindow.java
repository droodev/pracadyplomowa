package pl.edu.agh.strateg.ui.controller.interfaces;

import com.vaadin.ui.Window.CloseListener;

import pl.edu.agh.strateg.ui.controller.component.common.EditController;

public interface HasEditWindow extends CloseListener{
	
	void setEditController(EditController edytujController);
	void addEditWindow();
}
