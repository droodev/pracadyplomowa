package pl.edu.agh.strateg.ui.controller.main;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;

import pl.edu.agh.strateg.ui.Views;
import pl.edu.agh.strateg.ui.model.common.DataTypes;
import pl.edu.agh.strateg.ui.model.communication.GatewayImpl;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;
import pl.edu.agh.strateg.ui.view.views.NewDocumentView;

import com.vaadin.navigator.Navigator;
import com.vaadin.ui.Upload.Receiver;

public class NewDocumentController extends AbstractNewDocumentController implements Receiver {
	
	
	private File file;
	
	public NewDocumentController(Navigator navigator) {
		super(navigator);
		dataTypes = DataTypes.DOKUMENT;
	}
	
	
	public void confirmButtonClicked() {
		if (file == null) {
			((NewDocumentView)newDocumentView).wyswietlFileNotUploadedWarning();
			return;
		}
		super.confirmButtonClicked();
		
	}
	
	
	protected void addNewDocument(List<MetadatumModel> metadane) {
		GatewayImpl.getInstance().addNewFile(file, metadane, this);
		
	}

	protected void cancelButtonClicked() {
		navigator.navigateTo(Views.DOCUMENTS_VIEW);
	}
	
	
	@Override
	public OutputStream receiveUpload(String filename, String mimeType) {
        FileOutputStream fos = null; 
        try {
            file = new File(filename);
            fos = new FileOutputStream(file);
        } catch (final java.io.FileNotFoundException e) {
        	((NewDocumentView)newDocumentView).wyswietlUploadProblemWarning();
        	System.out.println(file.getAbsolutePath());
            return null;
        }
        ((NewDocumentView)newDocumentView).zaladowanoPoprawnie(filename);
        return fos;
	}


	@Override
	public void databaseProblemOccured() {
		newDocumentView.databaseProblemOccured();
		
	}


}
