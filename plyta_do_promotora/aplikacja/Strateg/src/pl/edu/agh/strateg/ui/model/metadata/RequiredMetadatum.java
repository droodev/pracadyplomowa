package pl.edu.agh.strateg.ui.model.metadata;

import java.io.Serializable;

public class RequiredMetadatum implements Serializable{
	
	private Object value;
	private String descriptorName;
	
	public RequiredMetadatum(Object value, String nazwa) {
		this.value = value;
		this.descriptorName = nazwa;
	}

	public Object getValue() {
		return value;
	}

	public String getDescriptorName() {
		return descriptorName;
	}
	
}
