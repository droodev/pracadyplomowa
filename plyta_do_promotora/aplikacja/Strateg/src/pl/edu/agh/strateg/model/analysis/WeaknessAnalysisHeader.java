/**
	@author drew
 */

package pl.edu.agh.strateg.model.analysis;

public class WeaknessAnalysisHeader extends RowAnalysisHeader {

	WeaknessAnalysisHeader(String name, int orderingNumber) {
		super(name, orderingNumber);
	}

	@Override
	public boolean isPrimary() {
		return false;
	}

}
