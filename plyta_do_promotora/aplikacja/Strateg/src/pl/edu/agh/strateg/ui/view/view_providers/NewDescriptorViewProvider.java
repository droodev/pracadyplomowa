package pl.edu.agh.strateg.ui.view.view_providers;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import pl.edu.agh.strateg.security.Roles;
import pl.edu.agh.strateg.ui.Views;
import pl.edu.agh.strateg.ui.controller.main.NewDescriptorController;
import pl.edu.agh.strateg.ui.view.views.AccessDeniedView;
import pl.edu.agh.strateg.ui.view.views.NewDescriptorView;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewProvider;

public class NewDescriptorViewProvider implements ViewProvider {
	
	private NewDescriptorController nowaMetadanaController;
	
	private Navigator.ClassBasedViewProvider provider = 
			new Navigator.ClassBasedViewProvider(Views.NEW_DESCRIPTOR_VIEW, NewDescriptorView.class);
	
	private Navigator.ClassBasedViewProvider noPermissionProvider = 
			new Navigator.ClassBasedViewProvider(Views.NEW_DESCRIPTOR_VIEW, AccessDeniedView.class);
	
	public NewDescriptorViewProvider(NewDescriptorController nowaMetadanaController) {
		this.nowaMetadanaController = nowaMetadanaController;
	}

	@Override
	public String getViewName(String viewAndParameters) {
		Subject currentUser = SecurityUtils.getSubject();
		if (currentUser.isAuthenticated() && currentUser.hasRole(Roles.ADD_DESCRIPTOR)) {
			return provider.getViewName(viewAndParameters);
		}
		else {
			return noPermissionProvider.getViewName(viewAndParameters);
		}
	}

	@Override
	public View getView(String viewName) {
		Subject currentUser = SecurityUtils.getSubject();
		if (currentUser.isAuthenticated() && currentUser.hasRole(Roles.ADD_DESCRIPTOR)) {
			NewDescriptorView nowaMetadanaView = (NewDescriptorView) provider.getView(viewName);
			nowaMetadanaView.setControllerToAllComponents(nowaMetadanaController);
			return nowaMetadanaView;
		}
		else {
			AccessDeniedView view = (AccessDeniedView) noPermissionProvider.getView(viewName);
			return view;
		}
	}

}
