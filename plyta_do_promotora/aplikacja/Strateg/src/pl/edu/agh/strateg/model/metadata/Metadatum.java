/**
 * @author drew
 */
package pl.edu.agh.strateg.model.metadata;

import pl.edu.agh.strateg.model.metadata.description.Descriptor;

public interface Metadatum {
	public Descriptor getDescription();

	public Object getValue();

	public void setValue(Object newValue);

}
