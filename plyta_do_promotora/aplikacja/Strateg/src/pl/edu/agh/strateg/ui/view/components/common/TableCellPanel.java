package pl.edu.agh.strateg.ui.view.components.common;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import pl.edu.agh.strateg.ui.controller.component.documents.SearchResultsController;
import pl.edu.agh.strateg.ui.model.common.DataModel;
import pl.edu.agh.strateg.ui.model.common.SimpleDatumModel;
import pl.edu.agh.strateg.ui.model.documents.DocumentsDataModel;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;

import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.BrowserFrame;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public abstract class TableCellPanel extends Panel{
	
	private VerticalLayout podgladLayout = new VerticalLayout();
	private HorizontalLayout dataLayout = new  HorizontalLayout();
	private VerticalLayout buttonLayout = new VerticalLayout();
	private HorizontalLayout mainLayout = new HorizontalLayout();
	
	protected SearchResultsController controller;
	protected DataModel model;

	public TableCellPanel(DataModel model, SearchResultsController controller) {
		
		this.model = model;
		this.controller = controller;
		setSizeFull();
		
		List<List<SimpleDatumModel>> columns = new ArrayList<List<SimpleDatumModel>>();
		List<SimpleDatumModel> wartosci1 = new ArrayList<SimpleDatumModel>();
		List<SimpleDatumModel> wartosci2 = new ArrayList<SimpleDatumModel>();
		int i = 0;
		for (MetadatumModel m : model.getMetadata()) {
			if (i < 10) {
				wartosci1.add(m.getSimpleDatumModel());
			}
			else {
				wartosci2.add(m.getSimpleDatumModel());
			}
			++i;
		}
		columns.add(wartosci1);
		if (i > 10) {
			columns.add(wartosci2);
		}
		
		setContent(mainLayout);
		mainLayout.setSizeFull();
		mainLayout.setSpacing(true);
		mainLayout.setMargin(true);
		
		if (model instanceof DocumentsDataModel) {
			addPodglad(((DocumentsDataModel)model).getStream());
			addData(columns);
			addButton();
			
			mainLayout.setExpandRatio(podgladLayout, 0.25f);
			mainLayout.setExpandRatio(dataLayout, 0.5f);
			mainLayout.setExpandRatio(buttonLayout, 0.25f);
		}
		else {
			addData(columns);
			addButton();
			
			mainLayout.setExpandRatio(dataLayout, 0.75f);
			mainLayout.setExpandRatio(buttonLayout, 0.25f);
		}

		
	}

	private void addPodglad(final InputStream inputStream) {
		if (inputStream != null) {
			BrowserFrame pdfContents = new BrowserFrame();
		    pdfContents.setSizeFull();
			StreamSource s = new StreamSource() {
		        public java.io.InputStream getStream() {
		        	return inputStream;
		        }
		    };
		    StreamResource resource = new StreamResource(s, "document.pdf");
		    resource.setMIMEType("application/pdf");
	        pdfContents.setSource(resource);
	        podgladLayout.addComponent(pdfContents);
			
		}
		mainLayout.addComponent(podgladLayout);
	}
	

	private void addData(List<List<SimpleDatumModel>> columns) {
		dataLayout.setSizeFull();
		mainLayout.addComponent(dataLayout);
		for (List<SimpleDatumModel> column : columns) {
			VerticalLayout newLayout = new VerticalLayout();
			for (SimpleDatumModel model : column) {
				DisplayDatumValueLayout layout = new DisplayDatumValueLayout(model);
				newLayout.addComponent(layout);
			}
			dataLayout.addComponent(newLayout);
			newLayout.setSizeFull();
		}
	}
	
	private void addButton() {
		
		Button button = createButton();
		buttonLayout.addComponent(button);
		buttonLayout.setComponentAlignment(button, Alignment.MIDDLE_RIGHT);
		
		mainLayout.addComponent(buttonLayout);
		mainLayout.setComponentAlignment(buttonLayout, Alignment.MIDDLE_RIGHT);

	}

	protected abstract Button createButton();


}
