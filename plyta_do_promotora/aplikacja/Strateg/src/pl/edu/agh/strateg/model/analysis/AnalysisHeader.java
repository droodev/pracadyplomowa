/**
	@author drew
 */

package pl.edu.agh.strateg.model.analysis;

public interface AnalysisHeader {
	public String getName();
}
