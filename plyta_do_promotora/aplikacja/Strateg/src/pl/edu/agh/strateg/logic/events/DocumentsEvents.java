/**
	@author drew
 */

package pl.edu.agh.strateg.logic.events;

import java.util.List;
import java.util.UUID;

import pl.edu.agh.strateg.logic.async.OperationListener;
import pl.edu.agh.strateg.model.Document;
import pl.edu.agh.strateg.persistence.querying.MetadatumQuery;

import com.google.common.base.Optional;

public class DocumentsEvents {
	public static class SaveIfNotExistsDocumentEvent extends
			OperationEventBase<Document, Boolean> {
		public SaveIfNotExistsDocumentEvent(Document data, OperationListener<Boolean> listener) {
			super(Optional.of(data), listener);
		}

	}

	public static class UpdateIfExistsDocumentEvent extends
			OperationEventBase<Document, Boolean> {
		public UpdateIfExistsDocumentEvent(Document data,
				OperationListener<Boolean> listener) {
			super(Optional.of(data), listener);
		}

	}

	public static class DeleteIfExistsDocumentEvent extends
			OperationEventBase<Document, Boolean> {
		public DeleteIfExistsDocumentEvent(Document document,
				OperationListener<Boolean> listener) {
			super(Optional.of(document), listener);
		}

	}

	public static class GetDocumentEvent extends
			OperationEventBase<UUID, Optional<Document>> {
		public GetDocumentEvent(UUID data,
				OperationListener<Optional<Document>> listener) {
			super(Optional.of(data), listener);
		}

	}

	public static class GetAllDocumentsEvent extends
			OperationEventBase<Object, List<Document>> {
		public GetAllDocumentsEvent(OperationListener<List<Document>> listener) {
			super(Optional.absent(), listener);
		}
	}

	public static class QueryDocumentsByMetadataEvent extends
			OperationEventBase<MetadatumQuery, List<Document>> {

		public QueryDocumentsByMetadataEvent(Optional<MetadatumQuery> data,
				OperationListener<List<Document>> listener) {
			super(data, listener);
		}

	}

}
