package com.example.songbook;

import java.util.Iterator;

import com.mongodb.BasicDBList;
import com.mongodb.DBObject;
import com.vaadin.data.util.ObjectProperty;

import songbook.vaadin.model.SongItem;
import songbook.vaadin.model.SongPart;
import songbook.vaadin.model.SongQualifiedPart;

public class DBConverter {

	
	private static final String TITLE_FIELD = "title";
	private static final String CHORUSES_FIELD = "choruses";
	private static final String VERSES_FIELD = "verses";
	private static final String BRIDGES_FIELD = "bridges";

	public static SongItem convertToItem(DBObject songObject){
		if(!songObject.containsField(TITLE_FIELD)){
			throw new IllegalArgumentException();
		}
		if(!songObject.containsField(CHORUSES_FIELD)){
			throw new IllegalArgumentException();
		}
		if(!songObject.containsField(VERSES_FIELD)){
			throw new IllegalArgumentException();
		}
		if(!songObject.containsField(BRIDGES_FIELD)){
			throw new IllegalArgumentException();
		}
		SongItem toReturn = new SongItem();
		
		toReturn.addItemProperty(new SongQualifiedPart(0, SongPart.TITLE), wrapToProperty(songObject.get(TITLE_FIELD)));
		
		BasicDBList versesList = (BasicDBList) songObject.get(VERSES_FIELD);
		BasicDBList chorusList = (BasicDBList) songObject.get(CHORUSES_FIELD);
		BasicDBList bridgesList = (BasicDBList) songObject.get(BRIDGES_FIELD);
		
		addList(SongPart.VERSE, versesList, toReturn);
		addList(SongPart.CHORUS, chorusList, toReturn);
		addList(SongPart.BRIDGE, bridgesList, toReturn);
		
		
		return toReturn;

	}

	private static ObjectProperty<String> wrapToProperty(Object object) {
		return new ObjectProperty<String>((String) object);
	}
	
	private static void printList(String title, BasicDBList list){
		System.out.println(title);
		for(Iterator<Object> it = list.iterator(); it.hasNext();){
			System.out.println(it.next());
		}
	}
	
	private static void addList(SongPart songPart, BasicDBList list, SongItem item){
		SongQualifiedPart part = new SongQualifiedPart(0, songPart);
		for(Iterator<Object> it = list.iterator(); it.hasNext();){
			item.addItemProperty(part, wrapToProperty(it.next()));
			part = part.getSuccessor();
		}
	}
}
