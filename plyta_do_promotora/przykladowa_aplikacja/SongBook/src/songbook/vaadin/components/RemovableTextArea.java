package songbook.vaadin.components;

import songbook.vaadin.components.inner.SongWriterController;
import songbook.vaadin.model.SongQualifiedPart;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;

public class RemovableTextArea extends CustomField<String>{
	
	private SongQualifiedPart qualifiedName;

	public void setQualifiedName(SongQualifiedPart qualifiedName) {
		this.qualifiedName = qualifiedName;
		setCaption(qualifiedName.toString());
	}

	public SongQualifiedPart getQualifiedName() {
		return qualifiedName;
	}

	private VerticalLayout mainLayout;
	private Button removeButton;
	private TextArea textArea;
	
	
	
	public RemovableTextArea(SongWriterController songWriterController,SongQualifiedPart qualifiedName) {
		super();
		this.qualifiedName = qualifiedName;
		initilializeComponents();

		setUpMainLayout();
		
		setUpRemoveButton(songWriterController, this);
		
		composeComponent();
	}

	private void setUpRemoveButton(final SongWriterController songWriterController,
			final RemovableTextArea qualifiedNameProvider) {
		removeButton.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				songWriterController.fieldRemoved(qualifiedNameProvider.getQualifiedName());
			}
		});
	}

	private void composeComponent() {
		mainLayout.addComponent(textArea);
		mainLayout.addComponent(removeButton);
	}

	private void initilializeComponents() {
		mainLayout = new VerticalLayout();
		removeButton = new Button("Usu�");
		textArea = new TextArea();
	}

	private void setUpMainLayout() {
		mainLayout.setSpacing(true);
		setCaption(qualifiedName.toString());
	}

	@Override
	protected Component initContent() {
		return mainLayout;
	}

	@Override
	public Class<? extends String> getType() {
		return String.class;
	}
	
	
	
}
