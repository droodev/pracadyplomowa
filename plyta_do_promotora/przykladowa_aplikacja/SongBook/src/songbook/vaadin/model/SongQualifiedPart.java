package songbook.vaadin.model;

import java.io.Serializable;

public class SongQualifiedPart implements Serializable{
	int ordinalNumber;
	SongPart part;
	
	public SongQualifiedPart(int ordinalNumber, SongPart part) {
		this.ordinalNumber = ordinalNumber;
		this.part = part;
	}
	public int getOrdinalNumber() {
		return ordinalNumber;
	}
	public SongPart getPart() {
		return part;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ordinalNumber;
		result = prime * result + ((part == null) ? 0 : part.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SongQualifiedPart other = (SongQualifiedPart) obj;
		if (ordinalNumber != other.ordinalNumber)
			return false;
		if (part != other.part)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return part + " " + (ordinalNumber + 1);
	}
	
	public SongQualifiedPart getPredecessor(){
		if(ordinalNumber == 0){
			throw new IndexOutOfBoundsException("There is no predecessor of this qualified name");
		}
		return new SongQualifiedPart(ordinalNumber-1, part);
	}
	
	public SongQualifiedPart getSuccessor(){
		return new SongQualifiedPart(ordinalNumber+1, part);
	}
	
}
