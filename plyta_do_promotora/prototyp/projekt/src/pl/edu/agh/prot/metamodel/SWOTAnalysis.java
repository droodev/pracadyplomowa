package pl.edu.agh.prot.metamodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SWOTAnalysis implements Serializable{
	private String name;
	private List<String> rowsInternal;
	private List<String> rowsExternal;
	private List<String> columnsHelpful;
	private List<String> columnsHarmful;
	private int[][] values;
	
	public SWOTAnalysis(String name, List<String> rowsInternal, List<String> rowsExternal,
			List<String> columnsHelpful, List<String> columnsHarmful){
		this.name = name;
		this.rowsInternal = rowsInternal==null ? new ArrayList<String>(): rowsInternal;
		this.rowsExternal = rowsExternal==null ? new ArrayList<String>(): rowsExternal;
		this.columnsHelpful = columnsHelpful==null ? new ArrayList<String>(): columnsHelpful;
		this.columnsHarmful = columnsHarmful==null ? new ArrayList<String>(): columnsHarmful;
		
		values = new int[this.rowsExternal.size() + this.rowsInternal.size()][this.columnsHarmful.size() + this.columnsHelpful.size()];
	}

	public List<String> getRowsInternal() {
		return rowsInternal;
	}

	public List<String> getRowsExternal() {
		return rowsExternal;
	}

	public List<String> getColumnsHelpful() {
		return columnsHelpful;
	}

	public List<String> getColumnsHarmful() {
		return columnsHarmful;
	}

	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		return getName();
	}
	
	public void setAt(int row, int col, int val){
		values[row][col]=val;
	}
	public int getAt(int row, int col){
		return values[row][col];
	}
	
}
