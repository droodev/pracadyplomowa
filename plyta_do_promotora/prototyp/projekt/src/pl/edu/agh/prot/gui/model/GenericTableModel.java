package pl.edu.agh.prot.gui.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import pl.edu.agh.prot.document.DocumentContainer;
import pl.edu.agh.prot.metamodel.Category;
import pl.edu.agh.prot.metamodel.MetaComponent;
import pl.edu.agh.prot.metamodel.MetaType;

public class GenericTableModel extends AbstractTableModel implements DocumentsSelectionListener{

	
	private String[] headers;
	private List<MetaComponent> dataList = new ArrayList<MetaComponent>();
	private DocumentContainer container;
	private Category category;

	public GenericTableModel(String[] headers, DocumentContainer container,
			Category type) {
		
		super();
		this.headers = headers;
		this.container = container;
		this.category = type;
	}

	
	@Override
	public String getColumnName(int column) {
		return headers[column];
	}
	
	@Override
	public int getColumnCount() {
		return headers.length;
	}

	@Override
	public int getRowCount() {
		return dataList.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if(columnIndex==0){
			return dataList.get(rowIndex).getName();
		}
		return dataList.get(rowIndex).getValue();
	}
	
	
	@Override
	public void selectedDocumentChanged(int index){
		if(index<0){
			return;
		}
		dataList = container.getList().get(index).getByCategory(category);
		fireTableDataChanged();
	}
}
