package pl.edu.agh.prot.gui.model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class DocumentsTableListener implements ListSelectionListener {

	List<DocumentsSelectionListener> listenersList = new ArrayList<DocumentsSelectionListener>();
	
	@Override
	public void valueChanged(ListSelectionEvent arg0) {
		if(!arg0.getValueIsAdjusting()){
			for(DocumentsSelectionListener listener : listenersList){
				ListSelectionModel list = (ListSelectionModel) arg0.getSource();
				listener.selectedDocumentChanged(list.getMaxSelectionIndex());
			}
		}

	}
	
	public void addListener(DocumentsSelectionListener listener){
		listenersList.add(listener);
	}

}
