package pl.edu.agh.prot.document;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import pl.edu.agh.prot.metamodel.Category;
import pl.edu.agh.prot.metamodel.HashMetaComponentContainer;
import pl.edu.agh.prot.metamodel.MetaComponent;
import pl.edu.agh.prot.metamodel.MetaComponentContainer;
import pl.edu.agh.prot.metamodel.MetaComponentImplementation;
import pl.edu.agh.prot.metamodel.MetaType;
import pl.edu.agh.prot.metamodel.PoliceHeadquater;
import pl.edu.agh.prot.metamodel.RequiredMetaName;

public class ListDocument implements Document {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MetaComponentContainer components;

	// This constructor should be done better
	public ListDocument(String name) {
		components = new HashMetaComponentContainer();
		components.add(new MetaComponentImplementation(MetaType.STRING,
				RequiredMetaName.NAME.toString(), name, Category.GENERAL));
		components.add(new MetaComponentImplementation(MetaType.STRING,
				RequiredMetaName.HEADQ.toString(), null, Category.GENERAL));
		components.add(new MetaComponentImplementation(MetaType.STRING,
				RequiredMetaName.CR_DATE.toString(), null, Category.GENERAL));
		components.add(new MetaComponentImplementation(MetaType.STRING,
				RequiredMetaName.AUTHOR.toString(), null, Category.GENERAL));
	}

	@Override
	public Document copy() {
		Document toReturn = new ListDocument("NOT_INTERESTING");
		toReturn.addAll(components.toComponentList());
		return toReturn;
	}
	
	@Override
	public MetaComponent getMeta(String name) {
		return components.get(name);
	}

	@Override
	public String getName() {
		return (String)getGeneric(RequiredMetaName.NAME);
	}

	@Override
	public String getAuthor() {
		return (String)getGeneric(RequiredMetaName.AUTHOR);
	}

	@Override
	public PoliceHeadquater getHeadquater() {
		return (PoliceHeadquater)getGeneric(RequiredMetaName.HEADQ);
	}

	@Override
	public Date getCreationDate() {
		return (Date) getGeneric(RequiredMetaName.CR_DATE);
	}

	@Override
	public Date getPeriodStart() {
		return (Date) getGeneric(RequiredMetaName.ST_PERIOD);
	}

	@Override
	public Date getPeriodEnd() {
		return (Date) getGeneric(RequiredMetaName.END_PERIOD);
	}

	@Override
	public boolean renameMeta(String name, String newName) {
		return components.rename(name, newName);
	}
	
	@Override
	public void setName(String newName) {
		components.get(RequiredMetaName.NAME.toString()).changeValue(MetaType.STRING, newName);	
	}

	@Override
	public void setAuthor(String author) {
		getMeta(RequiredMetaName.AUTHOR.toString()).changeValue(
				MetaType.STRING, author);
	}

	@Override
	public void setHeadquater(PoliceHeadquater headquater) {
		getMeta(RequiredMetaName.HEADQ.toString()).changeValue(MetaType.STRING,
				headquater);

	}

	@Override
	public void setCreationDate(Date date) {
		getMeta(RequiredMetaName.CR_DATE.toString()).changeValue(MetaType.DATE,
				date);

	}

	@Override
	public void setPeriodStart(Date date) {
		getMeta(RequiredMetaName.ST_PERIOD.toString()).changeValue(
				MetaType.DATE, date);

	}

	@Override
	public void setPeriodEnd(Date date) {
		getMeta(RequiredMetaName.END_PERIOD.toString()).changeValue(
				MetaType.DATE, date);

	}

	@Override
	public List<MetaComponent> getByCategory(Category category) {
		ArrayList<MetaComponent> toReturn = new ArrayList<MetaComponent>();
		for (MetaComponent comp : components.toComponentList()) {
			if (comp.getCategory().equals(category)) {
				toReturn.add(comp);
			}
		}
		return toReturn;
	}

	@Override
	public boolean addMeta(String name, Object value, MetaType metaType,
			Category category) {
		return components.add(new MetaComponentImplementation(metaType, name,
				value, category));
	}

	@Override
	public boolean removeMeta(String name) {
		return components.remove(name);
	}

	@Override
	public boolean changeMetaObject(String name, Object newObject,
			MetaType metaType) {
		MetaComponent meta = getMeta(name);
		return meta.changeValue(metaType, newObject);
	}

	@Override
	public boolean changeMetaCategory(String name, Category category) {
		MetaComponent comp = getMeta(name);
		return comp.changeCategory(category);
	}

	@Override
	public boolean changeMetaName(String name, String newName) {
		return components.rename(name, newName);
	}
	
	private Object getGeneric(RequiredMetaName metaEnum){
		MetaComponent comp = getMeta(metaEnum.toString());
		if (comp != null) {
			return comp.getValue();
		}
		return null;
	}

	@Override
	public void addAll(Collection<? extends MetaComponent> arg0) {
		components = new HashMetaComponentContainer();
		components.addAll(arg0);
	}
	
	@Override
	public String toString() {
		return getName();
	}

}
