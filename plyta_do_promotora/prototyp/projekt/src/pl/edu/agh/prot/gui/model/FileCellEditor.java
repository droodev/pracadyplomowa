package pl.edu.agh.prot.gui.model;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

public class FileCellEditor extends AbstractCellEditor implements TableCellEditor{

	private JFileChooser chooser = new JFileChooser();
	private JButton button = new JButton("Edycja");
	private File file;
	public FileCellEditor() {
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				chooser.showOpenDialog(null);
				if(chooser.getSelectedFile()!=null){
					file = chooser.getSelectedFile();
				}
				fireEditingStopped();
			}
		});
	}
	
	@Override
	public boolean isCellEditable(EventObject e) {
		if(e==null || !(e instanceof MouseEvent) || ((MouseEvent)e).getClickCount()>=2){
			return true;
		}
		return false;
	}
	
	
	@Override
	public Object getCellEditorValue() {
		return file;
	}



	@Override
	public Component getTableCellEditorComponent(JTable table, Object value,
			boolean isSelected, int row, int column) {
		try {
			file = (File) value;
		} catch (ClassCastException e) {
			file = new File("C:");
		}
		return button;
	}

}
