package pl.edu.agh.prot.gui.model;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

import pl.edu.agh.prot.metamodel.SWOTAnalysis;

public class SWOTTableModel extends AbstractTableModel{
	private SWOTAnalysis analysis;

	public SWOTTableModel(SWOTAnalysis analysis) {
		this.analysis = analysis;
	}

	@Override
	public int getColumnCount() {
		return analysis.getColumnsHarmful().size() + analysis.getColumnsHelpful().size();
	}

	@Override
	public int getRowCount() {
		return analysis.getRowsExternal().size() + analysis.getRowsInternal().size();
	}

	@Override
	public boolean isCellEditable(int arg0, int arg1) {
		return true;
	}
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		try{
		analysis.setAt(rowIndex, columnIndex, Integer.parseInt((String)aValue));
		}catch(NumberFormatException e){
			//TODO Pewnie jakiś dialog;)
		}
	}
	
	@Override
	public String getColumnName(int column) {
		int positiveSize = analysis.getColumnsHelpful().size();
		if(column <positiveSize){
			return analysis.getColumnsHelpful().get(column);
		}
		return analysis.getColumnsHarmful().get(column-positiveSize);
	}
	
	@Override
	public Object getValueAt(int row, int column) {
		return analysis.getAt(row, column);
	}
	
	
	
}
