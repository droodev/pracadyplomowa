package pl.edu.agh.prot.gui.model.SWOTModels;

import java.util.List;

import javax.swing.DefaultListModel;

public class SWOTAddListModel extends DefaultListModel<String>{
	private List<String> concreteList;

	public SWOTAddListModel(List<String> concreteList) {
		this.concreteList = concreteList;
	}
	
	@Override
	public String getElementAt(int index) {
		return concreteList.get(index);
	}
	
	@Override
	public int getSize() {
		return concreteList.size();
	}
	
	public void add(String s){
		concreteList.add(s);
		fireIntervalAdded(this, getSize(), getSize());
	}
	
	public void remove(int[] elements){
		for(int i= elements.length-1; i>=0; --i){
			concreteList.remove(elements[i]);
		}
		fireIntervalRemoved(this, 0, getSize()+elements.length);
	}

	public List<String> getConcreteList() {
		return concreteList;
	}
	
	
}
