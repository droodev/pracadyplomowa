package pl.edu.agh.prot.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import pl.edu.agh.prot.gui.model.DocumentsSelectionListener;

public class GIFPanel extends JPanel implements DocumentsSelectionListener{

	private BufferedImage image;
	private int randomNumber = -1;
	private final static Random RAND = new Random();

	public GIFPanel() {
		try {
			image = ImageIO.read(new File("data/1.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// setMaximumSize(new Dimension(300, 300));
		setBorder(BorderFactory.createLineBorder(Color.BLACK));
		setVisible(true);

	}

	@Override
	    protected void paintComponent(Graphics g) {
		 	super.paintComponent(g);
		 	if(randomNumber!=-1){
		 		g.drawImage(image, 0, 0, getHeight(), getWidth(), null); // see javadoc for more info on the parameters
		 	}
	    }

	@Override
	public void selectedDocumentChanged(int index) {
		try {
			switch (RAND.nextInt(3)) {
			case 0:

				image = ImageIO.read(new File("data/1.png"));

				break;
			case 1:
				image = ImageIO.read(new File("data/2.png"));
				break;
			case 2:
				image = ImageIO.read(new File("data/3.png"));
				break;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		randomNumber=1;
		repaint();
		
	}
}
