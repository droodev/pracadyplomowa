package pl.edu.agh.prot.metamodel;

import java.io.File;

public class MetaComponentImplementation implements MetaComponent {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected MetaType type;
	protected String name;
	protected Object value;
	protected Category category;
	
	
	public MetaComponentImplementation(MetaType metaType, String name, Object value,
			Category category) {
		this(metaType, name, category);
		this.value = value;
	}
	
	public MetaComponentImplementation(MetaType metaType) {
		this.type = metaType;
	}
	
	public MetaComponentImplementation(MetaType metaType, String name, Category category) {
		this(metaType);
		this.name = name;
		this.category = category;
	}
	
	public MetaComponent rename(String newName) {
		return new MetaComponentImplementation(type, newName, value, category);
	}
	
	@Override
	public MetaType getType() {
		return type;
	}
	

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Object getValue() {
		return value;
	}

	@Override
	public Category getCategory() {
		return category;
	}

	// risky -> if the arguments are not correct
	@Override
	public boolean changeValue(MetaType type, Object value) {
		this.type = type;
		this.value = value;
		return true;
	}
	
	// risky -> if the argument is not correct
	@Override
	public boolean changeCategory(Category category) {
		this.category = category;
		return true;
	}
	
	@Override
	public String toString() {
		return (String)value;
	}

	

}
