package pl.edu.agh.prot.metamodel;

public enum Category {
GENERAL, MEASURE, ATTACHMENT, OTHER;
}
