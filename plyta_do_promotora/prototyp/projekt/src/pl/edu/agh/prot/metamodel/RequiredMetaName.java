package pl.edu.agh.prot.metamodel;

public enum RequiredMetaName {
	NAME("Nazwa"), AUTHOR("Autor"), HEADQ("Jednostka"), CR_DATE(
			"Data utowrzenia"), ST_PERIOD("Za okres: początek"), END_PERIOD(
			"Za okres: koniec");

	private String name;

	private RequiredMetaName(String name) {
		this.name = name;
	}

	public String toString() {
		return name;
	}

}
