package pl.edu.agh.prot.metamodel;

public enum MetaType {
INT, STRING, FLOAT, DATE, FILE;
}
