package pl.edu.agh.prot.gui;

import javax.swing.JPanel;
import java.awt.GridLayout;
import javax.swing.JButton;

import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JDialog;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Enumeration;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JLabel;

import pl.edu.agh.prot.document.DocumentContainer;
import pl.edu.agh.prot.gui.model.ColorTableRenderer;
import pl.edu.agh.prot.gui.model.ColumnHeaderRenderer;
import pl.edu.agh.prot.gui.model.RowHeader;
import pl.edu.agh.prot.gui.model.SWOTTableModel;
import pl.edu.agh.prot.gui.model.SWOTModels.SWOTListModel;
import pl.edu.agh.prot.metamodel.SWOTAnalysis;

public class SWOTPanel extends JPanel {

	private DocumentContainer documentContainer;
	private SWOTListModel listModel;
	private JList<SWOTAnalysis> list;
	private JScrollPane rightPanel;
	/**
	 * Create the panel.
	 */
	public SWOTPanel(DocumentContainer documentContainer) {
		
		this.documentContainer = documentContainer;
		
		listModel = new SWOTListModel(documentContainer.getAnalysis());
		
		JButton addButton = new JButton("Dodaj");
		addButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				addButtonAction(arg0);
				
			}
		});
		
		JButton delButton = new JButton("Usuń");
		delButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				delButtonAction(arg0);
				
			}
		});
		
		rightPanel = new JScrollPane();
		
		list = new JList<SWOTAnalysis>();
		list.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				selectionHandler(arg0);
				
			}
		});
		
		list.setModel(listModel);
		
		JLabel lblTabeleSwot = new JLabel("Tabele SWOT");
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(delButton)
						.addComponent(addButton)
						.addComponent(list, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblTabeleSwot))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(rightPanel, GroupLayout.DEFAULT_SIZE, 307, Short.MAX_VALUE)
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(rightPanel, GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(addButton)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(delButton)
							.addGap(1)
							.addComponent(lblTabeleSwot)
							.addGap(3)
							.addComponent(list, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
					.addContainerGap())
		);
		setLayout(groupLayout);

	}

	protected void selectionHandler(ListSelectionEvent arg0) {
		if(list.getSelectionModel().getValueIsAdjusting()){
			return;
		}
		if(list.getSelectedIndices().length!=1){
			return;
		}
		SWOTAnalysis selected = list.getSelectedValue();
		//rightPanel = new JScrollPane(new JTable(new SWOTTableModel(selected)));
		//rightPanel = new JScrollPane(/**/);
		JTable table = new JTable(new SWOTTableModel(selected));
		table.setSelectionForeground(Color.BLACK);
		rightPanel.setViewportView(table);
		table.setShowGrid(true);
		rightPanel.setRowHeaderView(new RowHeader(table, selected));
		Enumeration<TableColumn> columns = table.getColumnModel().getColumns();
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		table.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent arg0) {
				pressedCell(arg0);
				JTable table = (JTable)arg0.getSource();
				int row = table.rowAtPoint(arg0.getPoint());
				int col = table.columnAtPoint(arg0.getPoint());
				if(col ==-1 || row == -1){
					return;
				}
				if(arg0.getButton() == MouseEvent.BUTTON3){
					new AddPopupMenu().show(table, arg0.getPoint().x, arg0.getPoint().y);
				}
			}
			
			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		while (columns.hasMoreElements()) {
			TableColumn col;
		   col= columns.nextElement();
		        col.setHeaderRenderer(new ColumnHeaderRenderer());
		        System.out.println("Coords: " + selected.getRowsInternal().size() + "...." + selected.getColumnsHelpful().size());
		        col.setCellRenderer(new ColorTableRenderer(selected.getRowsInternal().size(),selected.getColumnsHelpful().size()));
		        col.setPreferredWidth(50);
		}
		//rightPanel.setViewportView();
		rightPanel.revalidate();
		rightPanel.repaint();
		revalidate();
		repaint();
	}

	protected void pressedCell(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	protected void delButtonAction(ActionEvent arg0) {
		if (list.getSelectedIndex()==-1){
			return;
		}
		listModel.remove(list.getSelectedIndices());
		
	}

	protected void addButtonAction(ActionEvent arg0) {
		//listModel.add(new SWOTAnalysis("adc", null, null, null, null));
		SWOTAddPanel panel = new SWOTAddPanel();
		new JOptionPane().showMessageDialog(this, panel);
		SWOTAnalysis got = panel.getCreatedAnalysis();
		if(got==null){
			JOptionPane.showMessageDialog(this,
				    "Pole nazwy pozostało puste");
		}else{
			listModel.add(got);
		}
	}
	
}

class AddPopupMenu extends JPopupMenu{
	
	JMenuItem addAttach = new JMenuItem("Dodaj odnośnik");
	JMenuItem delAttach = new JMenuItem("Usuń odnośnik");
	JMenuItem openAttach = new JMenuItem("Otwórz odnośnik");
	
	public AddPopupMenu() {
		add(addAttach);
		add(delAttach);
		add(openAttach);
	}
}
