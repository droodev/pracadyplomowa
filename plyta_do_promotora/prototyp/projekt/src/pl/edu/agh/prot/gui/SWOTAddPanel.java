package pl.edu.agh.prot.gui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JList;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractListModel;

import pl.edu.agh.prot.gui.model.SWOTModels.SWOTAddActivities;
import pl.edu.agh.prot.gui.model.SWOTModels.SWOTAddListModel;
import pl.edu.agh.prot.metamodel.SWOTAnalysis;

public class SWOTAddPanel extends JPanel{
	private JTextField nameTextField;
	private JTextField SWOTTextField;
	private SWOTAddListModel internalListModel  = new SWOTAddListModel(new ArrayList<String>());
	private SWOTAddListModel externalListModel  = new SWOTAddListModel(new ArrayList<String>());
	private SWOTAddListModel positiveListModel  = new SWOTAddListModel(new ArrayList<String>());
	private SWOTAddListModel negativeListModel  = new SWOTAddListModel(new ArrayList<String>());

	/**
	 * Create the panel.
	 */
	public SWOTAddPanel() {
		JLabel lblNewLabel = new JLabel("Nazwa: ");
		
		nameTextField = new JTextField();
		nameTextField.setColumns(10);
		
		SWOTTextField = new JTextField();
		SWOTTextField.setColumns(10);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(SWOTAddActivities.values()));
		comboBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				addComboAction(arg0);
			}
		});
		
		JPanel panel = new JPanel();
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(lblNewLabel, Alignment.LEADING)
						.addComponent(comboBox, Alignment.LEADING, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
						.addComponent(SWOTTextField)
						.addComponent(nameTextField, GroupLayout.DEFAULT_SIZE, 371, Short.MAX_VALUE))
					.addGap(101))
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 558, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel)
						.addComponent(nameTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(SWOTTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 292, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(16, Short.MAX_VALUE))
		);
		
		JLabel lblNegatywne = new JLabel("Zagrożenia");
		
		JLabel lblWewntrzne = new JLabel("Siły");
		
		JLabel lblZewntrzne = new JLabel("Słabości");
		
		JList positiveList = new JList();
		positiveList.setModel(positiveListModel);	
		
		JList negativeList = new JList();
		negativeList.setModel(negativeListModel);	
		
		JList externalList = new JList();
		externalList.setModel(externalListModel);	

		
		JLabel lblNewLabel_1 = new JLabel("Szanse");
		
		JList internalList = new JList();
		internalList.setModel(internalListModel);	
		
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblNewLabel_1)
							.addGap(84))
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(positiveList, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)))
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(negativeList, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNegatywne))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblWewntrzne)
						.addComponent(internalList, GroupLayout.PREFERRED_SIZE, 131, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblZewntrzne, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)
						.addComponent(externalList, GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE))
					.addGap(2))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(6)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_1)
						.addComponent(lblNegatywne)
						.addComponent(lblWewntrzne)
						.addComponent(lblZewntrzne, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(negativeList, GroupLayout.DEFAULT_SIZE, 255, Short.MAX_VALUE)
						.addComponent(positiveList, GroupLayout.DEFAULT_SIZE, 255, Short.MAX_VALUE)
						.addComponent(internalList, GroupLayout.PREFERRED_SIZE, 266, GroupLayout.PREFERRED_SIZE)
						.addComponent(externalList, GroupLayout.DEFAULT_SIZE, 255, Short.MAX_VALUE))
					.addContainerGap())
		);
		panel.setLayout(gl_panel);
		setLayout(groupLayout);

	}
	
	protected void addComboAction(ActionEvent event) {
    	JComboBox combo= (JComboBox)event.getSource();
    	String text = SWOTTextField.getText();
    	if(text.equals("")){
    		return;
    	}
    	SWOTTextField.setText("");
    	SWOTAddListModel temp;
    	switch(combo.getSelectedIndex()){
    	case 1:
    		temp=positiveListModel;
    		break;
    	case 2:
    		temp=negativeListModel;
    		break;
    	case 3:
    		temp=internalListModel;
    		break;
    	case 4:
    		temp=externalListModel;
    		break;
    	default:
    		temp=null;
    	}
    	if(temp!=null){
    		temp.add(text);
    	}
    	combo.setSelectedIndex(0);
	}
	
	public SWOTAnalysis getCreatedAnalysis(){
		if (nameTextField.getText().equals("")){
			return null;
		}
		return new SWOTAnalysis(nameTextField.getText(), internalListModel.getConcreteList(), externalListModel.getConcreteList(), positiveListModel.getConcreteList(), negativeListModel.getConcreteList());
	}

}
