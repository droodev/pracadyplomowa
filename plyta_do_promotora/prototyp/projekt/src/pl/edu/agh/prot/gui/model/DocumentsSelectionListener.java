package pl.edu.agh.prot.gui.model;

public interface DocumentsSelectionListener {
	public void selectedDocumentChanged(int index);
}
