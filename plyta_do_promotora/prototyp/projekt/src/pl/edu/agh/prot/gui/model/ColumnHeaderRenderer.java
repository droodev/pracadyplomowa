package pl.edu.agh.prot.gui.model;

import java.awt.Color;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import pl.edu.agh.prot.gui.VerticalLabel;

public class ColumnHeaderRenderer extends DefaultTableCellRenderer{
	
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		// TODO Auto-generated method stub
		JLabel label = new JLabel(value.toString());
		label.setUI(new VerticalLabel(false));
		label.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		return label;
	}

}
