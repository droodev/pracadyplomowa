package pl.edu.agh.prot.gui.model;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class ColorTableRenderer extends DefaultTableCellRenderer {
	
	private int lastRow;
	private int lastColumn;
	
	public ColorTableRenderer(int lastRow, int lastColumn) {
		this.lastRow = lastRow;
		this.lastColumn = lastColumn;
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		// TODO Auto-generated method stub
		Component component = (Component)super.getTableCellRendererComponent(table, value, isSelected, hasFocus,
				row, column);
		Color col;
		if(row<lastRow && column<lastColumn){
			col = Color.YELLOW;
		}else if(row<lastRow && column>=lastColumn){
			col = Color.GREEN;
		}else if(row>=lastRow && column<lastColumn){
			col = Color.CYAN;
		}else{
			col = Color.BLUE;
		}
		col = col.brighter();
		int rgb = col.getRGB();
		int red = (rgb >> 16) & 0xFF;
		int green = (rgb >> 8) & 0xFF;
		int blue = rgb & 0xFF;
		col = new Color(red, green, blue, 0x33);
		component.setBackground(col);
		return component;
	}
}
