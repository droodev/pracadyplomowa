package pl.edu.agh.prot.document;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import pl.edu.agh.prot.metamodel.Category;
import pl.edu.agh.prot.metamodel.MetaComponent;
import pl.edu.agh.prot.metamodel.SWOTAnalysis;

//To extract interface
public class DocumentsManager {
	private String dirName;
	private List<Document> documents;
	private List<SWOTAnalysis> swots;

	public DocumentsManager(String dirName) {
		this.dirName = dirName;
	}

	public List<Document> getDocuments() {
		if (documents != null) {
			return documents;
		}
		documents = new ArrayList<Document>();
		readDocumentsFromFile(EXT, documents);
		return documents;
	}
	
	public List<SWOTAnalysis> getSwots() {
		if (swots != null) {
			return swots;
		}
		swots = new ArrayList<SWOTAnalysis>();
		readDocumentsFromFile(EXT2, swots);
		return swots;
	}

	// pfe - prototype file extension
	static final String EXT = ".pfe";
	static final String EXT2 = ".pfs";

	public <T> void persistDocuments(List<T> list, String ext) {
		File tempFile;
		File rootDir = new File(dirName);
		if (!rootDir.exists()) {
			if (rootDir.mkdir()) {
				System.out.println("Dir not created");
			}
		}
		for (File file : rootDir.listFiles()) {
			if(file.getName().endsWith(ext)){
				file.delete();
			}
		}
		for (T doc : list) {
			System.out.println("To save:" + doc);
			tempFile = new File(dirName +"/" + doc +"." + ext);
			FileOutputStream fileOut = null;
			ObjectOutputStream out = null;
			try {
				//tempFile.createNewFile();
				fileOut = new FileOutputStream(tempFile);
				out = new ObjectOutputStream(fileOut);
				out.writeObject(doc);
				System.out.println("Doc saved: " + tempFile.getPath());
				System.out.println("Exist? :" + tempFile.exists());
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally{
				if (out!=null){
					try {
						out.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	private <T> void readDocumentsFromFile(String ext, List<T> list) {
		FileInputStream fileStream = null;
		File rootDir = new File(dirName);
		if (!rootDir.exists()) {
			return;
		}
		ObjectInputStream objIn = null;
		for (File objectFile : rootDir.listFiles()) {
			if (objectFile.isFile() && objectFile.toString().endsWith(ext)) {
				try {
					fileStream = new FileInputStream(objectFile);
					objIn = new ObjectInputStream(fileStream);
					@SuppressWarnings("unchecked")
					T doc = (T)objIn.readObject();
					list.add(doc);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				} finally {
					if (fileStream != null) {
						try {
							fileStream.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					if (objIn != null) {
						try {
							objIn.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}

			}
		}
	}


}
