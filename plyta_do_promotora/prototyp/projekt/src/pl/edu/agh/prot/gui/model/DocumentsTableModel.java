package pl.edu.agh.prot.gui.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.swing.table.AbstractTableModel;

import pl.edu.agh.prot.document.Document;
import pl.edu.agh.prot.document.DocumentContainer;

public class DocumentsTableModel extends AbstractTableModel{

	private DocumentContainer container;
	private static String[] headers= {"Nazwa", "Jednostka", "Data"};
	
	public DocumentsTableModel(DocumentContainer container) {
		this.container = container;
	}

	@Override
	public int getColumnCount() {
		return 3;
	}

	@Override
	public String getColumnName(int column) {
		return headers[column];
	}
	
	@Override
	public int getRowCount() {
		return container.getList().size();
	}

/*	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	} */
	
	
	@Override
	public Object getValueAt(int row, int col) {
		Document doc= container.getList().get(row);
		switch(col){
		case 0:
			return doc.getName();
		case 1:
			return doc.getHeadquater();
		case 2:
			DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
			return formatter.format(doc.getCreationDate());
		}
		return null;
	}
	
	public void dataChanged(){
		fireTableDataChanged();
	}
}
