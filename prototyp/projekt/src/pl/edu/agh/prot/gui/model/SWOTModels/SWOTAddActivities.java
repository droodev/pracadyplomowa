package pl.edu.agh.prot.gui.model.SWOTModels;

public enum SWOTAddActivities {
	ADD("Dodaj"), HELP("Szanse"), HARM("Zagrożenia"), WEWN("Siły"), ZEWN("Słabości");
	
	private String desc;

	private SWOTAddActivities(String desc) {
		this.desc = desc;
	}
	
	@Override
	public String toString() {
		return desc;
	}
}
