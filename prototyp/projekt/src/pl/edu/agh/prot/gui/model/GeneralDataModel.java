package pl.edu.agh.prot.gui.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.text.DateFormatter;

import pl.edu.agh.prot.document.DocumentContainer;

public class GeneralDataModel implements DocumentsSelectionListener{
	
	private DocumentContainer container;
	private String author = "";
	private Date date;
	private String kwp = "";
	private JLabel authorLabel;
	private JLabel kwpLabel;
	private JLabel dateLabel;
	
	
	public GeneralDataModel(DocumentContainer container, JLabel authorLabel,
			JLabel kwpLabel, JLabel dateLabel) {
		super();
		this.container = container;
		this.authorLabel = authorLabel;
		this.kwpLabel = kwpLabel;
		this.dateLabel = dateLabel;
	}

	public String getAuthor() {
		return author;
	}

	public Date getDate() {
		return date;
	}

	public String getKwp() {
		return kwp;
	}


	public void selectedDocumentChanged(int index) {
		if(index==-1){
			return;
		}
		author = container.getList().get(index).getAuthor();
		date = container.getList().get(index).getCreationDate();
		kwp = container.getList().get(index).getHeadquater().toString();
		authorLabel.setText(author);
		kwpLabel.setText(kwp);
		SimpleDateFormat formatter =new SimpleDateFormat("dd.MM.yyyy");
		dateLabel.setText(formatter.format(date));
	}

}
