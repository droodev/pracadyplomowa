package pl.edu.agh.prot.gui.model.SWOTModels;

import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractListModel;

import pl.edu.agh.prot.metamodel.SWOTAnalysis;

public class SWOTListModel extends AbstractListModel<SWOTAnalysis>{
	private List<SWOTAnalysis> analysis = new ArrayList<SWOTAnalysis>();

	public SWOTListModel(List<SWOTAnalysis> analysis) {
		super();
		this.analysis = analysis;
	}

	@Override
	public SWOTAnalysis getElementAt(int arg0) {
		return analysis.get(arg0);
	}

	@Override
	public int getSize() {
		return analysis.size();
	}
	
	public void remove (int[] elements){
		
		for(int i= elements.length-1; i>=0; --i){
			analysis.remove(elements[i]);
		}
		fireIntervalRemoved(this, 0, analysis.size()+elements.length);
	}
	
	public void add (SWOTAnalysis elem){
		analysis.add(elem);
		fireContentsChanged(this, analysis.size()-1, analysis.size());
	}
	
}
