package pl.edu.agh.prot.gui.model.editingModels;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import pl.edu.agh.prot.document.Document;
import pl.edu.agh.prot.metamodel.Category;
import pl.edu.agh.prot.metamodel.MetaComponent;
import pl.edu.agh.prot.metamodel.MetaComponentImplementation;
import pl.edu.agh.prot.metamodel.MetaType;

public class GenericEditingModel extends AbstractTableModel {

	protected Document doc;
	protected List<MetaComponent> list;
	protected Category category;
	private String[] headers;

	public GenericEditingModel(Document doc, Category category, String[] headers) {
		this.doc = doc;
		this.category = category;
		this.headers = headers;
		updateList();
	}

	@Override
	public String getColumnName(int column) {
		return headers[column];
	}
	
	@Override
	public int getColumnCount() {
		return headers.length;
	}

	@Override
	public int getRowCount() {
		return list.size();
	}

	@Override
	public boolean isCellEditable(int arg0, int arg1) {
		return true;
	}

	@Override
	public Object getValueAt(int row, int column) {
		if (column == 0) {
			return list.get(row).getName();
		}
		return list.get(row).getValue();
	}
	
	@Override
	public void setValueAt(Object value, int row, int column) {
		if(column==0){
			editMetaName((String)getValueAt(row, column), (String)value);
		}
		if(column==1){
			editMetaValue((String)getValueAt(row, 0), value);
		}
	}


	protected void addNewMeta(String name, String value, Category category) {

		MetaType metaType = resolveType(value);
		Object realValue = castValue(value, metaType);
		// list.add(new MetaComponentImplementation(metaType, name, value,
		// category));
		doc.addMeta(name, realValue, metaType, category);
		updateList();
		fireTableDataChanged();
	}

	protected void editMetaName(String oldName, String newName) {
		// System.out.println("Old: " + oldName + " new: " + newName);
		doc.renameMeta(oldName, newName);
		updateList();
		fireTableDataChanged();
	}

	protected void editMetaValue(String name, Object value) {
		MetaType metaType = resolveType(value);
		Object realValue = castValue(value, metaType);
		doc.changeMetaObject(name, realValue, metaType);
		updateList();
		fireTableDataChanged();
	}
	
	public void addMeta(String name, String value){
		addNewMeta(name, value, category);
	}

	private MetaType resolveType(Object value) {
		// DODAĆ DATĘ!
		MetaType toReturn = MetaType.STRING;
		try {
			Integer.parseInt((String)value);
			toReturn = MetaType.INT;
		} catch (NumberFormatException | ClassCastException e) {
		}

		if (toReturn == MetaType.STRING) {

			try {
				Float.parseFloat((String)value);
				toReturn = MetaType.FLOAT;
			} catch (NumberFormatException | ClassCastException e) {
			}
		}
		
		if (toReturn == MetaType.STRING && value instanceof File) {
			toReturn = MetaType.FILE;
		}
		return toReturn;
	}

	private Object castValue(Object value, MetaType metaType) {
		Object toReturn;
		switch (metaType) {
		case INT:
			toReturn = Integer.parseInt((String)value);
			break;
		case FLOAT:
			toReturn = Float.parseFloat((String)value);
			break;
		default:
			toReturn = value;
		}
		return toReturn;
	}

	public void removeMeta(int index) {
		doc.removeMeta(list.get(index).getName());
		updateList();
		fireTableDataChanged();
	}

	public Document getDoc() {
		return doc;
	}

	private void updateList() {
		list = doc.getByCategory(category);
	}

}
