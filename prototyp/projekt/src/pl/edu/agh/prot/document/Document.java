package pl.edu.agh.prot.document;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import pl.edu.agh.prot.metamodel.*;

public interface Document extends Serializable{
	MetaComponent getMeta(String name);
	/*
	 * General, required meta data  
	 */
	String getName();
	String getAuthor();
	PoliceHeadquater getHeadquater();
	Date getCreationDate();
	Date getPeriodStart();
	Date getPeriodEnd();
	
	boolean renameMeta(String name, String newName);
	void setName(String newName);
	void setAuthor(String author);
	void setHeadquater(PoliceHeadquater headquater);
	void setCreationDate(Date date);
	void setPeriodStart(Date date);
	void setPeriodEnd(Date date);
	Document copy();
	/*End of general*/
	
	List<MetaComponent> getByCategory(Category category);
	boolean addMeta(String name, Object Value,MetaType metaType, Category category);
	boolean removeMeta(String name);
	boolean changeMetaObject(String name, Object newObject, MetaType metaType);
	boolean changeMetaCategory(String name, Category category);
	boolean changeMetaName(String name, String newName);
	void addAll(Collection<? extends MetaComponent> arg0);
}
