package pl.edu.agh.prot.document;

import java.util.ArrayList;
import java.util.List;

import pl.edu.agh.prot.metamodel.SWOTAnalysis;

public class DocumentContainer {
	
	//Temporal solution
			public static final String DOC_PATH = "data";
	
	private List<Document> container;
	private List<SWOTAnalysis> swots;
	
	DocumentsManager loader = new DocumentsManager(DOC_PATH);
	//Temporal solution
	public DocumentContainer(){
		container = loader.getDocuments();
		swots = loader.getSwots();
		if(container==null){
			container = new ArrayList<Document>();
		}
	}
	
	public List<Document> getList(){
		return container;
	}
	
	public List<SWOTAnalysis> getAnalysis(){
		return swots;
	}
	
	//to repair- not the same
	public boolean add(Document doc){
		container.add(doc);
		return true;
	}
	
	public boolean remove(Document doc){
		return container.remove(doc);
	}
	
	public void reset(){
		container = new ArrayList<Document>();
	}
	
	public void endWork(){
		loader.persistDocuments(container, "pfe");
		System.out.println("Persist Swots");
		loader.persistDocuments(swots, "pfs");
	}
}
