package pl.edu.agh.prot.console;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pl.edu.agh.prot.document.Document;
import pl.edu.agh.prot.document.DocumentContainer;
import pl.edu.agh.prot.document.ListDocument;
import pl.edu.agh.prot.gui.Projekt;
import pl.edu.agh.prot.metamodel.Category;
import pl.edu.agh.prot.metamodel.MetaComponent;
import pl.edu.agh.prot.metamodel.MetaType;
import pl.edu.agh.prot.metamodel.PoliceHeadquater;
import pl.edu.agh.prot.metamodel.SWOTAnalysis;

public class Test {

	 
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		final DocumentContainer container = new DocumentContainer();
		for(Document document: container.getList()){
			System.out.println("-----------------------------");
			System.out.println("Nazwa: " + document.getName());
			//System.out.println("Autor: " + document.getAuthor());
			//System.out.println("Komenda: " + document.getHeadquater());
			//System.out.println("Data: " + document.getCreationDate());
			/*
			for(MetaComponent comp: document.getByCategory(Category.MEASURE)){
				System.out.println("Miernik: " + comp.getValue().getClass());
			}*/
			//document.addMeta("Att", "/plik.moj", MetaType.STRING, Category.ATTACHMENT);
		}
		/*
		for(SWOTAnalysis a: container.getAnalysis()){
			System.out.println("Analysis: " + a);
		}
		
		List<String> test = new ArrayList<String>();
		test.add("a");
		test.add("b");
		
		List<String> test2 = new ArrayList<String>();
		test2.add("c");
		test2.add("d");
		
		
		container.getAnalysis().add(new SWOTAnalysis("TestowaAnaliza", test, test2, test2, test));
		*/
		/*
		Document doc1 = container.getList().get(0);
		doc1.addMeta("Att2", "/plik.moj", MetaType.STRING, Category.ATTACHMENT);
		
		Document doc2 = container.getList().get(1);
		doc2.addMeta("Współczynnik", 12.34, MetaType.FLOAT, Category.MEASURE);*/
		
		
		
		/*Document doc1 = new ListDocument("Zabójstwa");
		doc1.setAuthor("Jan Kowalski");
		doc1.setCreationDate(new Date());
		doc1.setHeadquater(PoliceHeadquater.KWP_OL);
		Document doc2 = new ListDocument("Kradziejstwa");
		doc2.setAuthor("Zbigniew Kradziłap");
		doc2.setCreationDate(new Date());
		doc2.setHeadquater(PoliceHeadquater.KWP_BG);
		container.add(doc1);
		container.add(doc2);*/
		//Document doc = container.getList().get(0);
		//doc.setAuthor("Ziomek");
		//container.add(new ListDocument("Drugaśny!"));
		//System.out.println("Added documents");
		/*for(Document document: container.getList()){
			System.out.println(document);
		}*/
		
		try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Projekt.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Projekt.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Projekt.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Projekt.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
		
		java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
            	
                new Projekt(container).setVisible(true);
            }
           
        });	
		
	}

}
