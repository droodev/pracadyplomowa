package pl.edu.agh.prot.metamodel;

public enum PoliceHeadquater {
	KGP("KGP"), KWP_WR("KWP Wrocław"), KWP_BG("KWP Bydgoszcz"), KWP_LU("KWP Lublin"),
	KWP_GO("KWP Gorzów"), KWP_LO("KWP Lódź"), KWP_KR("KWP Kraków"), KWP_RA("KWP Radom"),
	KWP_OP("KWP Opole"), KWP_RZ("KWP Rzeszów"), KWP_BK("KWP Białystok"), KWP_GD("KWP Gdańsk"),
	KWP_KA("KWP Katowice"), KWP_KI("KWP Kielce"), KWP_OL("KWP Olsztyn"), KWP_PO("KWP Poznań"),
	KWP_SZ("KWP Szczecin"), KWP_WA("Stołeczna KWP");
	
	private String name;
	
	private PoliceHeadquater(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return name;
	}
}
