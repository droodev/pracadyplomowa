package pl.edu.agh.prot.metamodel;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public interface MetaComponentContainer extends Serializable{
	public boolean add(MetaComponent arg0);
	public boolean addAll(Collection<? extends MetaComponent> arg0);
	public boolean contains(String name);
	public boolean isEmpty();
	public boolean remove(String name);
	public boolean removeAll(Collection<String> arg0);
	public boolean rename(String oldName, String newName);
	public MetaComponent getAt(int index);
	public MetaComponent get(String name);
	public List<MetaComponent> toComponentList();
	public int size();
}
