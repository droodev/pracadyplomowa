package pl.edu.agh.prot.metamodel;

import java.io.Serializable;

/*
 * CONTRACT:
 * Name - IMMUTABLE!!! = rename method as copy constructor
 * Value & type - changing together
 * getType - null == no data
 */

public interface MetaComponent extends Serializable{
	String getName();
	MetaComponent rename(String newName);
	
	Object getValue();
	MetaType getType();
	boolean changeValue(MetaType type, Object value);
	
	Category getCategory();
	boolean changeCategory(Category category);
}
