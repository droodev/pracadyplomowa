package pl.edu.agh.prot.metamodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

public class HashMetaComponentContainer implements MetaComponentContainer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TreeSet<MetaComponent> sortedSet;
	private Comparator<MetaComponent> comparator = new SetComparator();
	
	public HashMetaComponentContainer() {
		sortedSet = new TreeSet<MetaComponent>(comparator);
	}

	@Override
	public boolean add(MetaComponent arg0) {
		return sortedSet.add(arg0);
	}

	@Override
	public boolean addAll(Collection<? extends MetaComponent> arg0) {
		for (MetaComponent component : arg0) {
			if (sortedSet.contains(component)) {
				return false;
			}
		}
		for (MetaComponent component : arg0) {
			sortedSet.add(component);
		}
		return true;
	}

	@Override
	public boolean contains(String name) {
		for (MetaComponent mc : sortedSet) {
			if (mc.getName().equals(name)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isEmpty() {
		return sortedSet.isEmpty();
	}

	@Override
	public boolean remove(String name) {
		for (MetaComponent mc : sortedSet) {
			if (mc.getName().equals(name)) {
				return sortedSet.remove(mc);
			}
		}
		return false;
	}

	@Override
	public boolean removeAll(Collection<String> arg0) {
		for (String name : arg0) {
			if (!contains(name)) {
				return false;
			}
		}
		for (String name : arg0) {
			remove(name);
		}
		return true;
	}

	@Override
	public boolean rename(String oldName, String newName) {
		if (!contains(oldName) || contains(newName)) {
			return false;
		}
		for (MetaComponent mc : sortedSet) {
			if (mc.getName().equals(oldName)) {
				MetaComponent newComponent = mc.rename(newName);
				remove(oldName);
				add(newComponent);
				return true;
			}
		}
		return false;
	}

	@Override
	public MetaComponent getAt(int index) {
		return toComponentList().get(index);
	}

	@Override
	public MetaComponent get(String name) {
		for (MetaComponent component : toComponentList()) {
			if (component.getName().equals(name)) {
				return component;
			}
		}
		return null;
	}

	@Override
	public List<MetaComponent> toComponentList() {
		return new ArrayList<MetaComponent>(sortedSet);
	}

	@Override
	public int size() {
		return sortedSet.size();
	}

}
	
class SetComparator implements Serializable, Comparator<MetaComponent> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public int compare(MetaComponent o1, MetaComponent o2) {
		return o1.getName().compareTo(o2.getName());
	}

};

