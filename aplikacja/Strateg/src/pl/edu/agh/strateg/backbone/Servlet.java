/**
	@author drew
 */

package pl.edu.agh.strateg.backbone;

import javax.inject.Inject;
import javax.servlet.ServletException;

import org.apache.log4j.Logger;

import com.vaadin.server.ServiceException;
import com.vaadin.server.SessionInitEvent;
import com.vaadin.server.SessionInitListener;
import com.vaadin.server.UIProvider;
import com.vaadin.server.VaadinServlet;

public class Servlet extends VaadinServlet {
	private static final Logger LOG = Logger.getLogger(Thread.currentThread()
			.getStackTrace()[1].getClassName());

	private UIProvider uiProvider;

	@Inject
	private Servlet(UIProvider uiProvider) {
		this.uiProvider = uiProvider;
	}

	@Override
	protected void servletInitialized() throws ServletException {
		LOG.debug(String.format("Initializing servlet"));
		super.servletInitialized();
		getService().addSessionInitListener(new SessionInitListener() {

			@Override
			public void sessionInit(SessionInitEvent event)
					throws ServiceException {
				event.getSession().addUIProvider(uiProvider);
			}
		});
	}

}
