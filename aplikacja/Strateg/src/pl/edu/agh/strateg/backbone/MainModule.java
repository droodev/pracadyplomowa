/**
	@author drew
 */

package pl.edu.agh.strateg.backbone;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import pl.edu.agh.strateg.communication.LogicBus;
import pl.edu.agh.strateg.logic.AnalysesManager;
import pl.edu.agh.strateg.logic.AnalysesManagerImpl;
import pl.edu.agh.strateg.logic.BackendProvider;
import pl.edu.agh.strateg.logic.DescriptorsManager;
import pl.edu.agh.strateg.logic.DescriptorsManagerImpl;
import pl.edu.agh.strateg.logic.DocumentsManager;
import pl.edu.agh.strateg.logic.DocumentsManagerImpl;
import pl.edu.agh.strateg.logic.verification.AnalysisVerifier;
import pl.edu.agh.strateg.logic.verification.CompositeVerifier;
import pl.edu.agh.strateg.logic.verification.ContainerVerifier;
import pl.edu.agh.strateg.logic.verification.DescriptorVerifier;
import pl.edu.agh.strateg.logic.verification.DocumentVerifier;
import pl.edu.agh.strateg.logic.verification.MetadatumVerifier;
import pl.edu.agh.strateg.logic.verification.Verifier;
import pl.edu.agh.strateg.logic.verification.VerifierPart;
import pl.edu.agh.strateg.model.Document;
import pl.edu.agh.strateg.model.analysis.Analysis;
import pl.edu.agh.strateg.model.metadata.MetadataContainer;
import pl.edu.agh.strateg.model.metadata.Metadatum;
import pl.edu.agh.strateg.model.metadata.description.Descriptor;
import pl.edu.agh.strateg.persistence.AnalysesDAO;
import pl.edu.agh.strateg.persistence.DescriptorsDAO;
import pl.edu.agh.strateg.persistence.DocumentsDAO;
import pl.edu.agh.strateg.persistence.mongo.DatastoreManager;
import pl.edu.agh.strateg.persistence.mongo.MAnalysesDAO;
import pl.edu.agh.strateg.persistence.mongo.MDescriptorsDAO;
import pl.edu.agh.strateg.persistence.mongo.MDocumentsDAO;
import pl.edu.agh.strateg.persistence.mongo.proxy.AnalysisProxy;
import pl.edu.agh.strateg.persistence.mongo.proxy.DescriptorProxy;
import pl.edu.agh.strateg.persistence.mongo.proxy.DocumentProxy;
import pl.edu.agh.strateg.ui.model.communication.GatewayImpl;

import com.google.code.morphia.Datastore;
import com.google.code.morphia.Morphia;
import com.google.code.morphia.dao.BasicDAO;
import com.google.inject.AbstractModule;
import com.google.inject.Provider;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.TypeLiteral;
import com.mongodb.MongoClient;

public class MainModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(DocumentsManager.class).to(DocumentsManagerImpl.class).in(
				Singleton.class);
		bind(DescriptorsManager.class).to(DescriptorsManagerImpl.class).in(
				Singleton.class);
		bind(AnalysesManager.class).to(AnalysesManagerImpl.class).in(
				Singleton.class);

		bind(Verifier.class).to(CompositeVerifier.class).in(Singleton.class);

		bind(new TypeLiteral<VerifierPart<Document>>() {
		}).to(DocumentVerifier.class);
		bind(new TypeLiteral<VerifierPart<Descriptor>>() {
		}).to(DescriptorVerifier.class);
		bind(new TypeLiteral<VerifierPart<Metadatum>>() {
		}).to(MetadatumVerifier.class);
		bind(new TypeLiteral<VerifierPart<Analysis>>() {
		}).to(AnalysisVerifier.class);
		bind(new TypeLiteral<VerifierPart<MetadataContainer>>() {
		}).to(ContainerVerifier.class);

		bind(new TypeLiteral<List<Descriptor>>() {
		}).toInstance(new ArrayList<Descriptor>());

		bind(DocumentsDAO.class).to(MDocumentsDAO.class).in(Singleton.class);
		bind(DescriptorsDAO.class).to(MDescriptorsDAO.class)
				.in(Singleton.class);
		bind(AnalysesDAO.class).to(MAnalysesDAO.class).in(Singleton.class);

		bind(BackendProvider.class).in(Singleton.class);
		bind(LogicBus.class).in(Singleton.class);
		bind(DatabaseInitializer.class).in(Singleton.class);

		bind(new TypeLiteral<BasicDAO<DescriptorProxy, String>>() {
		}).toProvider(DescriptorBasicDAOProvider.class).in(Singleton.class);

		bind(new TypeLiteral<BasicDAO<DocumentProxy, UUID>>() {
		}).toProvider(DocumentBasicDAOProvider.class).in(Singleton.class);

		bind(new TypeLiteral<BasicDAO<AnalysisProxy, UUID>>() {
		}).toProvider(AnalysisBasicDAOProvider.class).in(Singleton.class);

		bind(GatewayImpl.class).in(Singleton.class);

		bind(Morphia.class).in(Singleton.class);
		bind(MongoClient.class).in(Singleton.class);
		bind(DatastoreManager.class).asEagerSingleton();

		bind(ServiceManager.class).in(Singleton.class);
	}

	private static class DescriptorBasicDAOProvider implements
			Provider<BasicDAO<DescriptorProxy, String>> {

		private Datastore ds;

		@Inject
		private DescriptorBasicDAOProvider(Datastore ds) {
			this.ds = ds;
		}

		@Override
		public BasicDAO<DescriptorProxy, String> get() {
			return new BasicDAO<>(DescriptorProxy.class, ds);
		}

	}

	private static class AnalysisBasicDAOProvider implements
			Provider<BasicDAO<AnalysisProxy, UUID>> {

		private Datastore ds;

		@Inject
		private AnalysisBasicDAOProvider(Datastore ds) {
			this.ds = ds;
		}

		@Override
		public BasicDAO<AnalysisProxy, UUID> get() {
			return new BasicDAO<>(AnalysisProxy.class, ds);
		}

	}

	private static class DocumentBasicDAOProvider implements
			Provider<BasicDAO<DocumentProxy, UUID>> {

		private Datastore ds;

		@Inject
		private DocumentBasicDAOProvider(Datastore ds) {
			this.ds = ds;
		}

		@Override
		public BasicDAO<DocumentProxy, UUID> get() {
			return new BasicDAO<>(DocumentProxy.class, ds);
		}

	}

	@Provides
	@Singleton
	@Inject
	public Datastore provideMongoDatastore(DatastoreManager datastoreManager) {
		return datastoreManager.getDatastore();
	}
}