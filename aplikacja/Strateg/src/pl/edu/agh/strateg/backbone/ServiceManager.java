/**
	@author drew
 */

package pl.edu.agh.strateg.backbone;

import javax.inject.Inject;

import org.apache.log4j.Logger;

import pl.edu.agh.strateg.helpers.ReflectionHelper;
import pl.edu.agh.strateg.helpers.ReflectionOperationException;
import pl.edu.agh.strateg.helpers.annotations.ServiceShutdown;
import pl.edu.agh.strateg.helpers.annotations.ServiceStart;
import pl.edu.agh.strateg.logic.BackendProvider;
import pl.edu.agh.strateg.persistence.mongo.DatastoreManager;

import com.google.inject.Provider;

public class ServiceManager {

	private static final Logger LOG = Logger.getLogger(Thread.currentThread()
			.getStackTrace()[1].getClassName());

	private DatastoreManager datastoreManager;
	private Provider<BackendProvider> backendProviderProvider;
	private Provider<DatabaseInitializer> databaseInitializerProvider;

	@Inject
	private ServiceManager(DatastoreManager datastoreManager,
			Provider<BackendProvider> backendProviderProvider,
			Provider<DatabaseInitializer> databaseInitializerProvider) {
		this.datastoreManager = datastoreManager;
		this.backendProviderProvider = backendProviderProvider;
		this.databaseInitializerProvider = databaseInitializerProvider;
	}

	public void initService() {
		try {
			ReflectionHelper.callNoParamAnnotatedMethod(datastoreManager,
					ServiceStart.class);
			ReflectionHelper.callNoParamAnnotatedMethod(
					backendProviderProvider.get(), ServiceStart.class);
			databaseInitializerProvider.get().initDatabase();
		} catch (ReflectionOperationException e) {
			LOG.fatal(String.format("Cannot invoke method annotated with %s",
					ServiceStart.class));
			throw new RuntimeException(e);
		} catch (InitializingException e) {
			LOG.error("Initialization problem occured. Booting stopped");
			throw new RuntimeException(e);
		}
	}

	public void destroyService() {
		try {
			ReflectionHelper.callNoParamAnnotatedMethod(
					backendProviderProvider.get(), ServiceShutdown.class);
			ReflectionHelper.callNoParamAnnotatedMethod(datastoreManager,
					ServiceShutdown.class);
		} catch (ReflectionOperationException e) {
			LOG.fatal(String.format("Cannot invoke method annotated with %s",
					ServiceShutdown.class));
			throw new RuntimeException(e);
		}
	}

}
