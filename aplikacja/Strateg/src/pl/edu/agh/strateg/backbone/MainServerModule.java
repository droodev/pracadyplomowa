/**
	@author drew
 */

package pl.edu.agh.strateg.backbone;

import pl.edu.agh.strateg.ui.StrategUI;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Singleton;
import com.google.inject.TypeLiteral;
import com.google.inject.servlet.ServletModule;
import com.vaadin.server.UIProvider;
import com.vaadin.ui.UI;

public class MainServerModule extends ServletModule {

	@Override
	protected void configureServlets() {
		super.configureServlets();

		bindServlet(Servlet.class);

		bindProvider(StrategUIProvider.class);
		bindUI(StrategUI.class);

	}

	private void bindUI(Class<StrategUI> uiClass) {
		bind(UI.class).to(uiClass);
		bind(new TypeLiteral<Class<? extends UI>>() {
		}).toInstance(StrategUI.class);
	}

	private void bindProvider(Class<StrategUIProvider> providerClass) {
		bind(UIProvider.class).to(providerClass);
	}

	private void bindServlet(Class<Servlet> servletClass) {
		bind(servletClass).in(Singleton.class);

		serve("/*")
				.with(servletClass,
						ImmutableMap
								.<String, String> builder()
								.put("widgetset",
										"pl.edu.agh.strateg.ui.widgetset.StrategWidgetset")
								.put("productionMode", "false").build());
	}

}
