/**
	@author drew
 */

package pl.edu.agh.strateg.persistence.mongo;

import pl.edu.agh.strateg.persistence.GeneralDAO;

/**
 * @author drew
 * 
 * @param <KEY>
 *            Key type
 * @param <VALUEM>
 *            Model value type
 * @param <VALUEP>
 *            Proxy value type
 */
public interface MAdaptingDAO<KEY, VALUEM, VALUEP> extends
		GeneralDAO<VALUEM, KEY> {

}
