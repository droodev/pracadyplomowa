/**
	@author drew
 */

package pl.edu.agh.strateg.persistence.mongo.querying;

import pl.edu.agh.strateg.helpers.ExtendedPreconditions;
import pl.edu.agh.strateg.persistence.mongo.helpers.Mappings;
import pl.edu.agh.strateg.persistence.mongo.proxy.AnalysisProxy;
import pl.edu.agh.strateg.persistence.querying.constraint.EqualsConstraint;
import pl.edu.agh.strateg.persistence.querying.constraint.ExistConstraint;
import pl.edu.agh.strateg.persistence.querying.constraint.FieldConstraintVisitor;

import com.google.code.morphia.query.Query;

public class AnalysesQueryProducer implements FieldConstraintVisitor {

	private Query<AnalysisProxy> analysisQuery;

	public AnalysesQueryProducer(Query<AnalysisProxy> analysisQuery) {
		ExtendedPreconditions.checkArgumentNotNull(analysisQuery);
		this.analysisQuery = analysisQuery.disableValidation();
	}

	public Query<AnalysisProxy> getQuery() {
		return analysisQuery;
	}

	@Override
	public void visitExists(ExistConstraint constraint) {
		analysisQuery.field(buildQueriedFieldLocation(constraint.getName()))
				.exists();

	}

	@Override
	public void visitEquals(EqualsConstraint constraint) {
		analysisQuery.field(buildQueriedFieldLocation(constraint.getName()))
				.equal(constraint.getReferential());

	}

	private String buildQueriedFieldLocation(String name) {
		return String.format("%s.%s.%s.%s",
				Mappings.Analyses.DESCRIBING_METADATA,
				Mappings.Container.METADATA, name, Mappings.Metadata.VALUE);

	}

}
