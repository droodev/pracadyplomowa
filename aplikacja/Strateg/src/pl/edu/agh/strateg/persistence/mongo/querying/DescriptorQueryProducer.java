/**
	@author drew
 */

package pl.edu.agh.strateg.persistence.mongo.querying;

import pl.edu.agh.strateg.helpers.ExtendedPreconditions;
import pl.edu.agh.strateg.persistence.mongo.helpers.Mappings;
import pl.edu.agh.strateg.persistence.mongo.proxy.DescriptorProxy;
import pl.edu.agh.strateg.persistence.querying.constraint.EqualsConstraint;
import pl.edu.agh.strateg.persistence.querying.constraint.ExistConstraint;
import pl.edu.agh.strateg.persistence.querying.constraint.FieldConstraintVisitor;

import com.google.code.morphia.query.Query;

public class DescriptorQueryProducer implements FieldConstraintVisitor {

	private Query<DescriptorProxy> descriptorQuery;

	public DescriptorQueryProducer(Query<DescriptorProxy> descriptorQuery) {
		ExtendedPreconditions.checkArgumentNotNull(descriptorQuery);
		this.descriptorQuery = descriptorQuery.disableValidation();
	}

	public Query<DescriptorProxy> getQuery() {
		return descriptorQuery;
	}

	@Override
	public void visitExists(ExistConstraint constraint) {
		String name = constraint.getName();
		descriptorQuery.field(buildQueriedFieldLocation(name)).exists();

	}

	@Override
	public void visitEquals(EqualsConstraint constraint) {
		String name = constraint.getName();
		descriptorQuery.field(buildQueriedFieldLocation(name)).equal(
				constraint.getReferential());
	}

	private String buildQueriedFieldLocation(String name) {
		return String.format("%s.%s.%s.%s",
				Mappings.Descriptors.DESCRIBING_METADATA,
				Mappings.Container.METADATA, name, Mappings.Metadata.VALUE);
	}

}
