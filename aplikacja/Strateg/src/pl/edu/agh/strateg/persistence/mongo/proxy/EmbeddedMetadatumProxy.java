/**
	@author drew
 */

package pl.edu.agh.strateg.persistence.mongo.proxy;

import pl.edu.agh.strateg.model.metadata.Metadatum;
import pl.edu.agh.strateg.persistence.mongo.helpers.Mappings;
import pl.edu.agh.strateg.persistence.mongo.helpers.MorphiaRequirement;

import com.google.code.morphia.annotations.Embedded;

public class EmbeddedMetadatumProxy extends MetadatumProxy {

	protected EmbeddedMetadatumProxy(Metadatum metadatum) {
		super(metadatum);
		rawValue = metadatum.getValue();
	}

	@MorphiaRequirement
	private EmbeddedMetadatumProxy() {
	}

	@Embedded(value = Mappings.Metadata.VALUE)
	private Object rawValue;

	@Override
	public Object getValue() {
		return rawValue;
	}

}
