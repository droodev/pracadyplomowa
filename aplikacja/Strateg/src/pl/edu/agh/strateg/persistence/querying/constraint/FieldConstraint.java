/**
	@author drew
 */

package pl.edu.agh.strateg.persistence.querying.constraint;

import static pl.edu.agh.strateg.helpers.ExtendedPreconditions.*;

public abstract class FieldConstraint {
	private String name;

	public FieldConstraint(String name) {
		checkArgumentNotNull(name);
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public abstract void accept(FieldConstraintVisitor visitor);
}
