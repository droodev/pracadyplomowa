/**
	@author drew
 */

package pl.edu.agh.strateg.persistence.mongo.proxy;

import java.io.ByteArrayOutputStream;
import java.util.UUID;

import org.apache.log4j.Logger;

import pl.edu.agh.strateg.helpers.ByteOperationsHelper;
import pl.edu.agh.strateg.helpers.ReflectionHelper;
import pl.edu.agh.strateg.helpers.ReflectionOperationException;
import pl.edu.agh.strateg.model.Document;
import pl.edu.agh.strateg.model.SimpleDocument;
import pl.edu.agh.strateg.persistence.mongo.helpers.Mappings;

import com.google.code.morphia.annotations.Embedded;
import com.google.code.morphia.annotations.Id;
import com.google.code.morphia.annotations.Property;

public final class DocumentProxy {

	private static Logger LOG = Logger.getLogger(Thread.currentThread()
			.getStackTrace()[1].getClassName());

	@Embedded(value = Mappings.Data.DATA)
	private byte[] data;

	@Embedded(value = Mappings.Data.CONTAINER)
	private ContainerProxy container;

	@Property(value = Mappings.Data.ID)
	@Id
	private UUID uuid = UUID.randomUUID();

	private DocumentProxy() {
	}

	private DocumentProxy(byte[] data, ContainerProxy container, UUID uuid) {

		this.data = data;
		this.container = container;
		this.uuid = uuid;
	}

	public static DocumentProxy proxify(Document data) {
		LOG.debug(String.format("Creating proxy"));
		ByteArrayOutputStream dataBuffer = ByteOperationsHelper
				.copyToByteArray(data.getData());
		return new DocumentProxy(dataBuffer.toByteArray(),
				ContainerProxy.proxify(data.getDescribingMetadata()),
				data.getUUID());
	}

	public Document deproxify() {
		Document doc = new SimpleDocument();
		doc.setData(data);
		doc.setDescribingMetadata(container.deproxify());
		try {
			ReflectionHelper.injectIntoDeclaredField("uuid", doc, uuid);
		} catch (ReflectionOperationException e) {
			LOG.error(String
					.format("Cannot deproxify read file with exception %s"), e);
		}
		return doc;
	}

}
