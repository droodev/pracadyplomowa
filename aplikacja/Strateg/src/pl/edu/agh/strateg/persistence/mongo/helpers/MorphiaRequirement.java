/**
	@author drew
 */

package pl.edu.agh.strateg.persistence.mongo.helpers;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
public @interface MorphiaRequirement {

}
