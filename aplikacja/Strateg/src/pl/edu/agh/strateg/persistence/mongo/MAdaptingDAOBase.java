/**
	@author drew
 */

package pl.edu.agh.strateg.persistence.mongo;

import java.util.ArrayList;
import java.util.List;

import pl.edu.agh.strateg.helpers.ExtendedPreconditions;
import pl.edu.agh.strateg.persistence.PersistenceException;

import com.google.code.morphia.dao.BasicDAO;
import com.google.code.morphia.query.QueryResults;
import com.google.common.base.Optional;

public abstract class MAdaptingDAOBase<K, VM, VP> implements
		MAdaptingDAO<K, VM, VP> {

	protected BasicDAO<VP, K> forProxyDAO;

	/**
	 * Translator is generally not necessary here. However, is needed by purpose
	 * - not to hide dependencies, to clearly show responsibility and
	 * collaborators and to provide comfortable type-checking
	 * 
	 * @param forProxyDAO
	 * @param translator
	 *            to be used with this class
	 */
	public MAdaptingDAOBase(BasicDAO<VP, K> forProxyDAO) {
		ExtendedPreconditions.checkArgumentsNotNull(forProxyDAO);
		this.forProxyDAO = forProxyDAO;
	}

	public abstract VP getProxy(VM modelEntity);

	public abstract VM deproxify(VP proxyEntity);

	@Override
	public boolean saveIfNotExists(VM modelEntity) throws PersistenceException {
		try {
			VP proxyEntity = getProxy(modelEntity);
			if (exists(modelEntity)) {
				return false;
			}
			forProxyDAO.save(proxyEntity);
			return true;
		} catch (RuntimeException e) {
			throw new PersistenceException(e);
		}

	}

	@Override
	public boolean deleteIfExists(VM modelEntity) throws PersistenceException {
		try {
			VP proxyEntity = getProxy(modelEntity);
			if (!exists(modelEntity)) {
				return false;
			}
			forProxyDAO.delete(proxyEntity);
			return true;
		} catch (RuntimeException e) {
			throw new PersistenceException(e);
		}
	}

	@Override
	public Optional<VM> get(K key) throws PersistenceException {
		try {
			VP proxyEntity = forProxyDAO.get(key);
			if (proxyEntity == null) {
				return Optional.<VM> absent();
			}
			return Optional.of(deproxify(proxyEntity));
		} catch (RuntimeException e) {
			throw new PersistenceException(e);
		}

	}

	@Override
	public boolean updateIfExists(VM modelEntity) throws PersistenceException {
		try {
			if (!exists(modelEntity)) {
				return false;
			}
			VP proxyEntity = getProxy(modelEntity);
			forProxyDAO.save(proxyEntity);
			return true;
		} catch (RuntimeException e) {
			throw new PersistenceException(e);
		}
	}

	@Override
	public List<VM> getAll() throws PersistenceException {
		try {
			QueryResults<VP> queryResults = forProxyDAO.find();
			return transformQueryResult(queryResults);
		} catch (RuntimeException e) {
			throw new PersistenceException(e);
		}
	}

	protected List<VM> transformQueryResult(QueryResults<VP> foundQueryResults) {
		ArrayList<VM> modelEntityList = new ArrayList<VM>();
		for (VP proxyEntity : foundQueryResults.asList()) {
			modelEntityList.add(deproxify(proxyEntity));
		}
		return modelEntityList;
	}

}
