/**
	@author drew
 */

package pl.edu.agh.strateg.persistence.mongo.proxy;

import java.util.ArrayList;
import java.util.Collection;

import pl.edu.agh.strateg.model.metadata.description.CollectionLimitedDescriptor;
import pl.edu.agh.strateg.model.metadata.description.Descriptor;
import pl.edu.agh.strateg.model.metadata.description.Domain;
import pl.edu.agh.strateg.model.metadata.description.Requirements;
import pl.edu.agh.strateg.model.metadata.description.SimpleDescriptor;
import pl.edu.agh.strateg.persistence.mongo.helpers.Mappings;

import com.google.code.morphia.annotations.Embedded;
import com.google.code.morphia.annotations.Id;
import com.google.code.morphia.annotations.Property;
import com.google.gwt.thirdparty.guava.common.collect.Lists;

/**
 * 
 * A'la typedef - to avoid parameterizing;)
 * 
 * @author drew
 * 
 */
public final class DescriptorProxy {

	// TODO It's ugly hack to remembering subclasses of typed descriptor(as
	// limited)
	// it COULD be changed
	@Embedded(value = Mappings.Descriptors.BOUNDING_COLLECTION)
	private ArrayList<Object> boundingCollection = new ArrayList<>();

	@Property
	private boolean isLimited = false;

	@Embedded(value = Mappings.Descriptors.VALUE_TYPE)
	protected Class<?> valueType;

	@Embedded(value = Mappings.Descriptors.DESCRIBING_METADATA)
	protected ContainerProxy describingMetadata;

	@Embedded(concreteClass = ArrayList.class, value = Mappings.Descriptors.OBLIGATORIES)
	protected Collection<Domain> obligatories;

	@Embedded(value = Mappings.Descriptors.REQUIREMENTS)
	protected Requirements requirements;

	@Id
	protected String name;

	private DescriptorProxy(Descriptor descriptor) {
		this.valueType = descriptor.getValueType();
		this.describingMetadata = ContainerProxy.proxify(descriptor
				.getDescribingMetadata());
		this.name = descriptor.getName();
		this.obligatories = descriptor.getObligatories();

		this.requirements = descriptor.getRequirements();

		if (descriptor.isLimited()) {
			isLimited = true;
			boundingCollection = new ArrayList<>(descriptor.toLimited()
					.getRelevantValues());
		}
	}

	private DescriptorProxy() {

	}

	public Descriptor deproxify() {
		Descriptor typedDescriptor = SimpleDescriptor.getCreator()
				.withDescribingMetada(describingMetadata.deproxify())
				.withObligatories(getObligatories())
				.withRequirements(getRequirements()).create(name, valueType);
		if (isLimited) {
			typedDescriptor = CollectionLimitedDescriptor
					.createLimitedDescriptor(boundingCollection,
							typedDescriptor);
		}
		return (Descriptor) typedDescriptor;
	}

	public static DescriptorProxy proxify(Descriptor descriptor) {
		return new DescriptorProxy(descriptor);
	}

	private Collection<Domain> getObligatories() {
		if (obligatories == null) {
			return Lists.newArrayList();
		}
		return obligatories;
	}

	private Requirements getRequirements() {
		if (requirements == null) {
			return Requirements.NONE;
		}
		return requirements;
	}

	/**
	 * Used by morphia as key, when this entity is used as map key
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return name;
	}

}
