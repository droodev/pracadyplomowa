/**
	@author drew
 */

package pl.edu.agh.strateg.persistence.mongo.querying;

import org.apache.log4j.Logger;

import pl.edu.agh.strateg.helpers.ExtendedPreconditions;
import pl.edu.agh.strateg.persistence.mongo.helpers.Mappings;
import pl.edu.agh.strateg.persistence.mongo.proxy.DocumentProxy;
import pl.edu.agh.strateg.persistence.querying.constraint.EqualsConstraint;
import pl.edu.agh.strateg.persistence.querying.constraint.ExistConstraint;
import pl.edu.agh.strateg.persistence.querying.constraint.FieldConstraintVisitor;

import com.google.code.morphia.query.Query;

public class DocumentQueryProducer implements FieldConstraintVisitor {

	private static final Logger LOG = Logger.getLogger(Thread.currentThread()
			.getStackTrace()[1].getClassName());

	private Query<DocumentProxy> buildingQuery;

	public DocumentQueryProducer(Query<DocumentProxy> buildingQuery) {
		ExtendedPreconditions.checkArgumentNotNull(buildingQuery);
		this.buildingQuery = buildingQuery.disableValidation();
	}

	public Query<DocumentProxy> getQuery() {
		return buildingQuery;
	}

	@Override
	public void visitExists(ExistConstraint constraint) {
		buildingQuery.field(buildQueriedFieldLocation(constraint.getName()))
				.exists();
	}

	@Override
	public void visitEquals(EqualsConstraint constraint) {
		buildingQuery.field(buildQueriedFieldLocation(constraint.getName()))
				.equal(constraint.getReferential());
	}

	private String buildQueriedFieldLocation(String descriptorName) {
		String fieldLocation = String.format("%s.%s.%s.%s",
				Mappings.Data.CONTAINER, Mappings.Container.METADATA,
				descriptorName, Mappings.Metadata.VALUE);
		LOG.debug(String.format("Descriptor name field location: %s",
				fieldLocation));
		return fieldLocation;
	}
}
