/**
	@author drew
 */

package pl.edu.agh.strateg.persistence.querying.constraint;

public class ExistConstraint extends FieldConstraint {

	public ExistConstraint(String name) {
		super(name);
	}

	@Override
	public void accept(FieldConstraintVisitor visitor) {
		visitor.visitExists(this);
	}

}
