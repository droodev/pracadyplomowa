/**
	@author drew
 */

package pl.edu.agh.strateg.persistence.querying.constraint;

import java.util.Date;

import pl.edu.agh.strateg.helpers.DateHelper;
import pl.edu.agh.strateg.helpers.ExtendedPreconditions;

public class EqualsConstraint extends FieldConstraint {

	private Object referential;

	public EqualsConstraint(String name, Object referential) {
		super(name);
		ExtendedPreconditions.checkArgumentNotNull(referential);
		if (referential.getClass().equals(Date.class)) {
			this.referential = DateHelper.normalizeDate((Date) referential);
			return;
		}
		this.referential = referential;
	}

	public Object getReferential() {
		return referential;
	}

	@Override
	public void accept(FieldConstraintVisitor visitor) {
		visitor.visitEquals(this);
	}

}
