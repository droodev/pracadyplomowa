/**
	@author drew
 */

package pl.edu.agh.strateg.persistence.querying.constraint;

public interface FieldConstraintVisitor {

	public void visitExists(ExistConstraint constraint);

	public void visitEquals(EqualsConstraint constraint);
}
