/**
	@author drew
 */

package pl.edu.agh.strateg.persistence.mongo;

import java.util.List;

import javax.inject.Inject;

import pl.edu.agh.strateg.model.metadata.description.Descriptor;
import pl.edu.agh.strateg.persistence.DescriptorsDAO;
import pl.edu.agh.strateg.persistence.PersistenceException;
import pl.edu.agh.strateg.persistence.mongo.proxy.DescriptorProxy;
import pl.edu.agh.strateg.persistence.mongo.querying.DescriptorQueryProducer;
import pl.edu.agh.strateg.persistence.querying.MetadatumQuery;

import com.google.code.morphia.dao.BasicDAO;
import com.google.code.morphia.query.QueryResults;

public class MDescriptorsDAO extends
		MAdaptingDAOBase<String, Descriptor, DescriptorProxy> implements
		DescriptorsDAO {

	@Inject
	public MDescriptorsDAO(BasicDAO<DescriptorProxy, String> forProxyDAO) {
		super(forProxyDAO);
	}

	@Override
	public boolean exists(Descriptor modelEntity) throws PersistenceException {
		try {
			DescriptorProxy foundDescritpor = forProxyDAO.get(modelEntity
					.getName());
			return foundDescritpor != null;
		} catch (Throwable e) {
			throw new PersistenceException(e);
		}
	}

	@Override
	public List<Descriptor> queryByMetadata(MetadatumQuery query)
			throws PersistenceException {
		try {
			DescriptorQueryProducer queryBuilder = new DescriptorQueryProducer(
					forProxyDAO.createQuery());
			query.accept(queryBuilder);
			QueryResults<DescriptorProxy> foundQueryResults = forProxyDAO
					.find(queryBuilder.getQuery());
			return transformQueryResult(foundQueryResults);
		} catch (RuntimeException e) {
			throw new PersistenceException(e);
		}
	}

	@Override
	public DescriptorProxy getProxy(Descriptor modelEntity) {
		return DescriptorProxy.proxify(modelEntity);
	}

	@Override
	public Descriptor deproxify(DescriptorProxy proxyEntity) {
		return proxyEntity.deproxify();
	}
}
