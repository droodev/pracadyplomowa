/**
	@author drew
 */

package pl.edu.agh.strateg.persistence.mongo.proxy;

import java.util.Date;
import java.util.HashSet;

import pl.edu.agh.strateg.model.metadata.Metadatum;
import pl.edu.agh.strateg.model.metadata.SimpleMetadatum;
import pl.edu.agh.strateg.persistence.mongo.helpers.MorphiaRequirement;

import com.google.code.morphia.annotations.Reference;
import com.google.gwt.thirdparty.guava.common.collect.Sets;

/**
 * This is internal and not generic because it is needless and uncomfortable.
 * The reason is simple. We use generics to preserve type-checking. However,
 * here the problem is, that after reading from database there is no information
 * about type(at compile time) so making this class typed is done by providing
 * needed type e.g. {@code <T> Metadatum<T> getMetadatum(Class<T>)} For this
 * type of operation it's needles to have generics. One could use just method to
 * get value which will look {@code <T> T getValue(Class<T>)} This approach
 * simplifies the architecture greatly too. There is no need to use
 * wildcards(<?>) in collections. Moreover, using parameterized classes in
 * database is very complicated too. Decision is clear - remove generics, use
 * runtime checking(still it must be done with generics approach so generics
 * looks like overkill cause they do what they are designed to)
 * 
 * @author drew
 * 
 */
public abstract class MetadatumProxy {

	@Reference
	protected DescriptorProxy descriptor;

	protected MetadatumProxy(Metadatum metadatum) {
		descriptor = DescriptorProxy.proxify(metadatum.getDescription());
	}

	public DescriptorProxy getDescriptor() {
		return descriptor;
	}

	@MorphiaRequirement
	protected MetadatumProxy() {
	}

	public abstract Object getValue();

	public Metadatum deproxify() {
		return new SimpleMetadatum(getDescriptor().deproxify(), getValue());
	}

	public static MetadatumProxy proxify(Metadatum metadatum) {
		Class<?> valType = metadatum.getDescription().getValueType();
		Class<?>[] tab = { String.class, Integer.class, Date.class,
				Double.class, Enum.class };
		HashSet<Class<?>> mustBePropertyAnnotatedTypeSet = Sets.newHashSet(tab);

		for (Class<?> clazz : mustBePropertyAnnotatedTypeSet) {
			if (clazz.isAssignableFrom(valType)) {
				return new PropertyMetadatumProxy(metadatum);
			}
		}
		return new EmbeddedMetadatumProxy(metadatum);
	}
}
