/**
	@author drew
 */

package pl.edu.agh.strateg.helpers;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.apache.log4j.Logger;

import com.google.common.base.Optional;

public class ReflectionHelper {

	private static Logger LOG = Logger.getLogger(Thread.currentThread()
			.getStackTrace()[1].getClassName());

	private ReflectionHelper() {

	}

	public static void injectIntoDeclaredField(String fieldName, Object target,
			Object value) throws ReflectionOperationException {
		LOG.debug(String.format(
				"Injecting  int field %s, value %s[object: %s]", fieldName,
				value, target));
		Field fieldToChange = null;
		Optional<Boolean> startingAccesibility = Optional.absent();
		try {
			fieldToChange = target.getClass().getDeclaredField("uuid");
			startingAccesibility = Optional.of(fieldToChange.isAccessible());
			fieldToChange.setAccessible(true);
			fieldToChange.set(target, value);
		} catch (Exception e) {
			throw new ReflectionOperationException(e);
		} finally {
			if (fieldToChange != null && startingAccesibility.isPresent()) {
				fieldToChange.setAccessible(startingAccesibility.get());
			}
		}
	}

	public static void callNoParamAnnotatedMethod(Object o,
			Class<? extends Annotation> annotation)
			throws ReflectionOperationException {
		Class<? extends Object> clazz = o.getClass();
		for (Method method : clazz.getDeclaredMethods()) {
			if (method.isAnnotationPresent(annotation)) {
				try {
					method.invoke(o);
				} catch (IllegalAccessException | IllegalArgumentException
						| InvocationTargetException e) {
					throw new ReflectionOperationException(e);
				}
			}
		}
	}
}
