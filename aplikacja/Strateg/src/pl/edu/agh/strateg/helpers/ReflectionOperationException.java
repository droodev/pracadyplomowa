/**
	@author drew
 */

package pl.edu.agh.strateg.helpers;

public class ReflectionOperationException extends Exception {

	public ReflectionOperationException(String cause) {
		super(cause);
	}

	public ReflectionOperationException(Exception cause) {
		super(cause);
	}

}
