/**
	@author drew
 */

package pl.edu.agh.strateg.communication;

import org.apache.log4j.Logger;

import com.google.common.eventbus.DeadEvent;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

public class LogicBus extends EventBus {
	private static Logger LOG = Logger.getLogger(Thread.currentThread()
			.getStackTrace()[1].getClassName());

	// private static LogicBus instance;
	// private static final int MAX_TASKS = 20;

	public LogicBus() {

		// Till now it's not needed to be asynchronous!
		// super("LogicBus", new ThreadPoolExecutor(2, MAX_TASKS, 300L,
		// TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(MAX_TASKS)));

		super();
		register(this);
	}

	@Subscribe
	public void deadEvent(DeadEvent e) {
		LOG.warn(String.format("Event: %s from %s was dispatched nowhere",
				e.getEvent(), e.getSource()));
	}

}
