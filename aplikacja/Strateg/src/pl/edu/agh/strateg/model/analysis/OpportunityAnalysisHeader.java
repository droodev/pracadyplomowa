/**
	@author drew
 */

package pl.edu.agh.strateg.model.analysis;

public class OpportunityAnalysisHeader extends ColumnAnalysisHeader {

	OpportunityAnalysisHeader(String name, int orderingNumber) {
		super(name, orderingNumber);
	}

	@Override
	public boolean isPrimary() {
		return true;
	}

}
