/**
 * @author drew
 */
package pl.edu.agh.strateg.model;

import pl.edu.agh.strateg.model.metadata.MetadataContainer;

public interface Describable {
	public MetadataContainer getDescribingMetadata();

	public void setDescribingMetadata(MetadataContainer newMetadata);
}
