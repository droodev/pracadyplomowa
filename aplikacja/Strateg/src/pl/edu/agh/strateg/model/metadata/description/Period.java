/**
	@author drew
 */

package pl.edu.agh.strateg.model.metadata.description;

import java.util.Date;

import pl.edu.agh.strateg.helpers.DateHelper;
import pl.edu.agh.strateg.helpers.ExtendedPreconditions;

import com.google.code.morphia.annotations.Property;
import com.google.common.base.Preconditions;

public class Period {
	@Property
	private Date start;
	@Property
	private Date end;

	private Period() {
		// FOR DB
	}

	private Period(Date start, Date end) {
		ExtendedPreconditions.checkArgumentsNotNull(start, end);
		Preconditions.checkState(start.before(end));
		this.start = DateHelper.normalizeDate(start);
		this.end = DateHelper.normalizeDate(end);
	}

	public Date getStart() {
		return start;
	}

	public Date getEnd() {
		return end;
	}

	public static Period create(Date start, Date end) {
		return new Period(start, end);
	}

}
