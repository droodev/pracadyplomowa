/**
	@author drew
 */

package pl.edu.agh.strateg.model.analysis;

public final class StrengthAnalysisHeader extends RowAnalysisHeader {

	StrengthAnalysisHeader(String name, int orderingNumber) {
		super(name, orderingNumber);
	}

	@Override
	public boolean isPrimary() {
		return true;
	}
}
