/**
	@author drew
 */

package pl.edu.agh.strateg.model.metadata.description;

import java.util.Collection;

import pl.edu.agh.strateg.model.metadata.MetadataContainer;

public class TaggingDescriptor extends SimpleDescriptor {

	TaggingDescriptor(String name, Class<?> valueType,
			MetadataContainer describingMetadata,
			Collection<Domain> obligatories, Requirements requirements) {
		super(name, valueType, describingMetadata, obligatories, requirements);
	}

	@Override
	public boolean isTagging() {
		return true;
	}

}
