/**
	@author drew
 */

package pl.edu.agh.strateg.model.analysis;

import pl.edu.agh.strateg.helpers.ExtendedPreconditions;

public abstract class AnalysisHeaderBase implements
		Comparable<AnalysisHeaderBase>, AnalysisHeader {
	private String name;
	private int orderingNumber;

	AnalysisHeaderBase(String name, int orderingNumber) {
		ExtendedPreconditions.checkArgumentNotNull(name);
		this.orderingNumber = orderingNumber;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	int getOrderingNumber() {
		return orderingNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	// TODO think about it's correctness
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!AnalysisHeader.class.isAssignableFrom(obj.getClass()))
			return false;
		AnalysisHeader other = (AnalysisHeader) obj;
		if (name == null) {
			if (other.getName() != null)
				return false;
		} else if (!name.equals(other.getName()))
			return false;
		return true;
	}

	@Override
	public int compareTo(AnalysisHeaderBase o) {
		ExtendedPreconditions.checkArgumentNotNull(o);
		if (this.equals(o)) {
			return 0;
		}
		if (o.isPrimary() ^ isPrimary()) {
			if (o.isPrimary()) {
				return 1;
			} else {
				return -1;
			}
		}
		if (getOrderingNumber() < o.getOrderingNumber()) {
			return -1;
		} else if (getOrderingNumber() > o.getOrderingNumber()) {
			return 1;
		} else {
			throw new RuntimeException(
					"Sorry, but this is stringly connected to domian problem class - this situation just can't occur");
		}
	}

	public abstract boolean isPrimary();
}
