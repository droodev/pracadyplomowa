/**
 * @author drew
 */
package pl.edu.agh.strateg.model.metadata;

import java.util.ArrayList;
import java.util.Collection;

import pl.edu.agh.strateg.model.metadata.description.Descriptor;

import com.google.common.base.Optional;

public final class EmptyMetadataContainer implements MetadataContainer {

	/**
	 * To be package-scoped
	 */
	EmptyMetadataContainer() {
	}

	@Override
	public void addMetadatum(Metadatum metadatum) {
		throw new UnsupportedOperationException("It's empty container");
	}

	@Override
	public <T> void removeMetadatum(Descriptor descriptor) {
		throw new UnsupportedOperationException("It's empty container");
	}

	@Override
	public <T> Optional<Metadatum> getMetadatum(Descriptor descriptor) {
		return Optional.absent();
	}

	@Override
	public void updateMetadatum(Metadatum alteredMetadatum) {
		throw new UnsupportedOperationException("It's empty container");
	}

	@Override
	public boolean contains(Descriptor descriptor) {
		return false;
	}

	@Override
	public int getSize() {
		return 0;
	}

	@Override
	public Collection<Metadatum> getAll() {
		return new ArrayList<>();
	}
}
