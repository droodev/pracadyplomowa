/**
	@author drew
 */

package pl.edu.agh.strateg.model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.UUID;

import org.apache.log4j.Logger;

import pl.edu.agh.strateg.helpers.ByteOperationsHelper;
import pl.edu.agh.strateg.helpers.ExtendedPreconditions;
import pl.edu.agh.strateg.model.metadata.MapMetadataContainer;
import pl.edu.agh.strateg.model.metadata.MetadataContainer;

import com.google.common.base.Preconditions;

/**
 * Always rewinded to 0 position! - it's helper while comparing, and let;s user
 * always have rewinded buffer
 * 
 * @author drew
 * 
 */
public class SimpleDocument implements Document {

	private static Logger LOG = Logger.getLogger(Thread.currentThread()
			.getStackTrace()[1].getClassName());

	// TODO must be lazy loading, flyweight, etc.
	private ByteArrayOutputStream data = new ByteArrayOutputStream();

	private MetadataContainer metadata;

	private UUID uuid = UUID.randomUUID();

	public SimpleDocument() {
		metadata = new MapMetadataContainer();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimpleDocument other = (SimpleDocument) obj;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		if (data == null) {
			if (other.data != null) {
				return false;
			}
		} else if (!Arrays.equals(data.toByteArray(), other.data.toByteArray())) {
			return false;
		}
		return true;
	}

	@Override
	public MetadataContainer getDescribingMetadata() {
		return metadata;
	}

	@Override
	public InputStream getData() {
		LOG.debug(String.format("Getting data..."));
		return new ByteArrayInputStream(data.toByteArray());
	}

	@Override
	public void setData(InputStream dataStream) {
		LOG.debug(String.format("Setting data..."));
		data = ByteOperationsHelper.copyToByteArray(dataStream);
	}

	@Override
	public void setData(byte[] newData) {
		LOG.debug(String.format("Setting data by array"));
		data = new ByteArrayOutputStream();
		data.write(newData, 0, newData.length);
	}

	@Override
	public void setDescribingMetadata(MetadataContainer newMetadata) {
		Preconditions.checkNotNull(newMetadata);
		metadata = newMetadata;
	}

	@Override
	public UUID getUUID() {
		return uuid;
	}

	private void setUUID(UUID uuid) {
		ExtendedPreconditions.checkArgumentNotNull(uuid);
		this.uuid = uuid;
	}

	public static SimpleDocument createWithUUID(UUID uuid) {
		SimpleDocument newDocument = new SimpleDocument();
		newDocument.setUUID(uuid);
		return newDocument;
	}
}
