/**
 * @author drew
 */
package pl.edu.agh.strateg.model;

import java.io.InputStream;
import java.util.UUID;

/**
 * Add abstract with concrete equals
 * 
 */
public interface Document extends Describable {
	public InputStream getData();

	public void setData(InputStream dataStream);

	/**
	 * In model it's a bit useless, but it is needed in proxyfying
	 */
	public void setData(byte[] newData);

	public UUID getUUID();

}
