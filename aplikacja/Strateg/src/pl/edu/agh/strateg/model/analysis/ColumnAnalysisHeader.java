/**
	@author drew
 */

package pl.edu.agh.strateg.model.analysis;

public abstract class ColumnAnalysisHeader extends
		AnalysisHeaderBase {

	ColumnAnalysisHeader(String name, int orderingNumber) {
		super(name, orderingNumber);
	}

}
