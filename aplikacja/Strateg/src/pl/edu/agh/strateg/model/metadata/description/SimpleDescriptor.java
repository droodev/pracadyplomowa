/**
	@author drew
 */

package pl.edu.agh.strateg.model.metadata.description;

import static pl.edu.agh.strateg.helpers.ExtendedPreconditions.*;

import java.util.Collection;
import java.util.Collections;

import pl.edu.agh.strateg.model.metadata.MetadataContainer;

public class SimpleDescriptor implements Descriptor {

	public static Creator getCreator() {
		return new Descriptor.Creator();
	}

	protected Class<?> valueType;
	protected MetadataContainer describingMetadata;
	protected Collection<Domain> obligatories;
	protected Requirements requirements;
	protected String name;

	SimpleDescriptor(String name, Class<?> valueType,
			MetadataContainer describingMetadata,
			Collection<Domain> obligatories, Requirements requirements) {
		checkArgumentsNotNull(name, valueType, describingMetadata,
				obligatories, requirements);
		this.name = name;
		this.valueType = valueType;
		this.describingMetadata = describingMetadata;
		this.obligatories = obligatories;
		this.requirements = requirements;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public boolean isLimited() {
		return false;
	}

	@Override
	public LimitedDescriptor toLimited() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Important: equals only at name. It's important, because it must be
	 * thought out. Maybe it's better to check equality by name and type. It
	 * would be super, if there could be connected only one type with one name!
	 * Maybe there is need to add to logic this feature, that there cannot be
	 * two descriptions with the same name and other types. So what to do with
	 * equals? Think bout it; Watch out - assuming that getting metadatum checks
	 * equality on name only! THIS COULD BE CHANGED
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!Descriptor.class.isAssignableFrom(obj.getClass()))
			return false;
		Descriptor other = (Descriptor) obj;
		if (name == null) {
			if (other.getName() != null)
				return false;
		} else if (!name.equals(other.getName()))
			return false;
		return true;
	}

	@Override
	public MetadataContainer getDescribingMetadata() {
		return describingMetadata;
	}

	@Override
	public void setDescribingMetadata(MetadataContainer newMetadata) {
		checkArgumentNotNull(newMetadata);
		describingMetadata = newMetadata;
	}

	@Override
	public Class<?> getValueType() {
		return valueType;
	}

	@Override
	public boolean isObligatoryIn(Domain domain) {
		return obligatories.contains(domain);
	}

	@Override
	public Collection<Domain> getObligatories() {
		return Collections.unmodifiableCollection(obligatories);
	}

	@Override
	public Requirements getRequirements() {
		return requirements;
	}

	@Override
	public boolean isTagging() {
		return false;
	}

}
