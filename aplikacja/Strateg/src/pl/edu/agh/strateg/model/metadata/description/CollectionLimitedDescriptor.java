/**
 * @author drew
 */
package pl.edu.agh.strateg.model.metadata.description;

import java.util.Collection;
import java.util.Collections;

import pl.edu.agh.strateg.helpers.ExtendedPreconditions;

public final class CollectionLimitedDescriptor extends SimpleDescriptor
		implements LimitedDescriptor {

	private final Collection<?> boundingCollection;

	private CollectionLimitedDescriptor(Collection<?> boundingCollection,
			Descriptor toLimitMetadatum) {
		super(toLimitMetadatum.getName(), toLimitMetadatum.getValueType(),
				toLimitMetadatum.getDescribingMetadata(), toLimitMetadatum
						.getObligatories(), toLimitMetadatum.getRequirements());
		ExtendedPreconditions.checkArgumentNotNull(boundingCollection);
		this.boundingCollection = boundingCollection;
	}

	@Override
	public Collection<?> getRelevantValues() {
		return Collections.unmodifiableCollection(boundingCollection);
	}

	@Override
	public boolean isLimited() {
		return true;
	}

	@Override
	public LimitedDescriptor toLimited() {
		return this;
	}

	public static CollectionLimitedDescriptor createLimitedDescriptor(
			Collection<?> boundingCollection, Descriptor toLimitDescriptor) {
		return new CollectionLimitedDescriptor(boundingCollection,
				toLimitDescriptor);
	}

}
