/**
	@author drew
 */

package pl.edu.agh.strateg.model.metadata.description;

import pl.edu.agh.strateg.helpers.ExtendedPreconditions;

import com.google.code.morphia.annotations.Embedded;
import com.google.code.morphia.annotations.Property;
import com.google.common.base.Optional;

public class Requirement {
	@Embedded
	private String metadataName;

	@Property
	private Object onValue;

	public String getMetadataName() {
		return metadataName;
	}

	private Requirement() {
		// FOR DB MAPPING
	}

	public Optional<Object> getOnValue() {
		return Optional.fromNullable(onValue);
	}

	private Requirement(String metadataName) {
		this(metadataName, null);
	}

	private Requirement(String metadataName, Object onValue) {
		ExtendedPreconditions.checkArgumentNotNull(metadataName);
		this.metadataName = metadataName;
		this.onValue = onValue;
	}

	public static Requirement createConditional(String metadataName,
			Object onValue) {
		return new Requirement(metadataName, onValue);
	}

	public static Requirement createUnconditional(String metadataName) {
		return new Requirement(metadataName);
	}
}
