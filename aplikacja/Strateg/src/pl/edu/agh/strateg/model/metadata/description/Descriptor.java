/**
 * @author drew
 */
package pl.edu.agh.strateg.model.metadata.description;

import static pl.edu.agh.strateg.helpers.ExtendedPreconditions.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import pl.edu.agh.strateg.model.Describable;
import pl.edu.agh.strateg.model.metadata.MetadataContainer;

public interface Descriptor extends Describable {
	public String getName();

	public Class<?> getValueType();

	public boolean isLimited();

	public LimitedDescriptor toLimited();

	public boolean isObligatoryIn(Domain domain);

	public Collection<Domain> getObligatories();

	public Requirements getRequirements();

	public boolean isTagging();

	public static class Creator {

		private MetadataContainer describingMetadata = MetadataContainer.EMPTY;
		private Collection<Domain> obligatories = new ArrayList<Domain>();
		private Requirements requirements = Requirements.NONE;

		public Creator withDescribingMetada(MetadataContainer container) {
			checkArgumentNotNull(container);
			this.describingMetadata = container;
			return this;
		}

		public Creator withObligatories(Collection<Domain> domains) {
			checkArgumentNotNull(domains);
			obligatories = new ArrayList<>(domains);
			return this;
		}

		public Creator withRequirements(Requirements requirements) {
			checkArgumentNotNull(requirements);
			this.requirements = requirements;
			return this;
		}

		public Descriptor create(String name, Class<?> type) {
			checkArgumentsNotNull(type, name);

			SimpleDescriptor createdDescriptor = new SimpleDescriptor(name,
					type, describingMetadata, obligatories, requirements);

			// // small patch to deprecated use of CREATOR
			// describingMetadata = MetadataContainer.EMPTY;
			// obligatories = new ArrayList<Domain>();
			// requirements = Requirements.NONE;

			return createdDescriptor;
		}

		public Descriptor createTagging(String name) {
			return new TaggingDescriptor(name, Void.class, describingMetadata,
					obligatories, requirements);
		}

		public Descriptor createUntyped(String name) {
			return new SimpleDescriptor(name, Object.class, describingMetadata,
					obligatories, requirements);
		}

		public Descriptor createString(String name) {
			return create(name, String.class);
		}

		public Descriptor createInteger(String name) {
			return create(name, Integer.class);
		}

		public Descriptor createDouble(String name) {
			return create(name, Double.class);
		}

		public Descriptor createDate(String name) {
			return create(name, Date.class);
		}

		public Descriptor createPeriod(String name) {
			return create(name, Period.class);
		}

	}

}