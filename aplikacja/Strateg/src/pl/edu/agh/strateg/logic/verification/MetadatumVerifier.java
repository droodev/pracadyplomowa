/**
	@author drew
 */

package pl.edu.agh.strateg.logic.verification;

import pl.edu.agh.strateg.model.metadata.Metadatum;

import com.google.common.base.Optional;

public class MetadatumVerifier implements VerifierPart<Metadatum> {

	public VerificationStatus verify(Metadatum metadatum,
			Optional<Object> parent, Verifier verifier) {

		Class<?> declaredValueType = metadatum.getDescription().getValueType();
		Class<?> valueType = metadatum.getValue().getClass();
		if (!declaredValueType.isAssignableFrom(valueType)) {
			return new NotVerified(
					"Declared and actual values of metadatum do not match");
		}
		VerificationStatus descriptorVerificationStatus = verifier
				.verifyDescriptor(metadatum.getDescription(),
						Optional.<Object> of(metadatum));

		return descriptorVerificationStatus;
	}
}
