/**
	@author drew
 */

package pl.edu.agh.strateg.logic.verification;

import static pl.edu.agh.strateg.helpers.ExtendedPreconditions.*;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import pl.edu.agh.strateg.model.Document;
import pl.edu.agh.strateg.model.metadata.MetadataContainer;
import pl.edu.agh.strateg.model.metadata.description.Descriptor;
import pl.edu.agh.strateg.model.metadata.description.SimpleDescriptor;

import com.google.common.base.Optional;

public class DocumentVerifier implements VerifierPart<Document> {

	private List<Descriptor> requiredDescriptors;

	@Inject
	public DocumentVerifier(List<Descriptor> requiredDescriptors) {
		checkArgumentNotNull(requiredDescriptors);
		this.requiredDescriptors = requiredDescriptors;
	}

	public DocumentVerifier(String[] requiredDescriptorsNames) {
		this(DocumentVerifier.createDescriptorList(requiredDescriptorsNames));
	}

	public VerificationStatus verify(Document document,
			Optional<Object> parent, Verifier verifier) {
		MetadataContainer describingMetadata = document.getDescribingMetadata();
		for (Descriptor required : requiredDescriptors) {
			if (!describingMetadata.contains(required)) {
				return new NotVerified("Not all required metadata");
			}
		}
		VerificationStatus verificationStatus = verifier.verifyContainter(
				document.getDescribingMetadata(),
				Optional.<Object> of(document));
		return verificationStatus;
	}

	private static List<Descriptor> createDescriptorList(
			String[] requiredDescriptorNames) {
		checkArgumentNotNull(requiredDescriptorNames);
		ArrayList<Descriptor> newRequiredDescriptors = new ArrayList<Descriptor>();
		for (String descName : requiredDescriptorNames) {
			// FIXME: think about comparing descriptors, making it by
			// Object.class
			// because only name counts is stupid
			newRequiredDescriptors.add(SimpleDescriptor.getCreator()
					.createUntyped(descName));
		}
		return newRequiredDescriptors;
	}
}
