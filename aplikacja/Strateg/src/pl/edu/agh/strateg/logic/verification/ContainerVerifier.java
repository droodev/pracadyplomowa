/**
	@author drew
 */

package pl.edu.agh.strateg.logic.verification;

import java.util.List;

import pl.edu.agh.strateg.model.Document;
import pl.edu.agh.strateg.model.analysis.Analysis;
import pl.edu.agh.strateg.model.metadata.MetadataContainer;
import pl.edu.agh.strateg.model.metadata.Metadatum;
import pl.edu.agh.strateg.model.metadata.description.Descriptor;
import pl.edu.agh.strateg.model.metadata.description.Domain;
import pl.edu.agh.strateg.model.metadata.description.Requirement;
import pl.edu.agh.strateg.model.metadata.description.SimpleDescriptor;

import com.google.common.base.Optional;
import com.google.gwt.thirdparty.guava.common.base.Preconditions;

public class ContainerVerifier implements VerifierPart<MetadataContainer> {

	@Override
	public VerificationStatus verify(MetadataContainer entity,
			Optional<Object> parent, Verifier verifier) {

		Preconditions.checkState(parent.isPresent());
		Domain domain = inferDomain(parent.get());
		VerificationStatus verifyMetadatumStatus;
		for (Metadatum m : entity.getAll()) {
			verifyMetadatumStatus = verifier.verifyMetadatum(m,
					Optional.<Object> of(entity));
			if (!verifyMetadatumStatus.isOK()) {
				return verifyMetadatumStatus;
			}
		}
		for (Metadatum m : entity.getAll()) {
			Optional<List<Requirement>> allRequirements = m.getDescription()
					.getRequirements().getAllRequirements(domain);
			if (!allRequirements.isPresent()) {
				continue;
			}
			for (Requirement r : allRequirements.get()) {
				Optional<Object> onValue = r.getOnValue();
				if (onValue.isPresent()) {
					if (m.getValue().equals(onValue.get())) {
						if (!entity.contains(SimpleDescriptor.getCreator()
								.create(r.getMetadataName(), Object.class))) {
							return new NotVerified("Requirements not exhausted");
						}
					}
				} else {
					if (!entity.contains(SimpleDescriptor.getCreator().create(
							r.getMetadataName(), Object.class))) {
						return new NotVerified("Requirements not exhausted");
					}
				}
			}
		}
		return VerificationStatus.OK;
	}

	private Domain inferDomain(Object parent) {
		if (Document.class.isAssignableFrom(parent.getClass())) {
			return Domain.DOCUMENT;
		} else if (Analysis.class.isAssignableFrom(parent.getClass())) {
			return Domain.ANALYSIS;
		} else if (Descriptor.class.isAssignableFrom(parent.getClass())) {
			return Domain.DESCRIPTOR;
		} else {
			throw new RuntimeException("Bad parent visited!");
		}
	}

}
