/**
	@author drew
 */

package pl.edu.agh.strateg.logic.async;

import static pl.edu.agh.strateg.helpers.ExtendedPreconditions.*;

import org.apache.log4j.Logger;

import pl.edu.agh.strateg.logic.verification.VerificationStatus;
import pl.edu.agh.strateg.persistence.PersistenceException;

public class OperationFlowTemplate<T> {

	private static Logger LOG = Logger.getLogger(Thread.currentThread()
			.getStackTrace()[1].getClassName());

	private Operation<T> operation;
	private OperationListener<T> listener;

	public OperationFlowTemplate(OperationListener<T> listener,
			Operation<T> operation) {
		checkArgumentsNotNull(operation, listener);
		this.operation = operation;
		this.listener = listener;
	}

	public void execute() {
		listener.preOperation();
		VerificationStatus validationStatus = operation.validation();
		listener.postVerification(validationStatus);
		if (validationStatus.equals(VerificationStatus.OK)) {
			try {
				listener.postOperation(operation.operate());
			} catch (PersistenceException e) {
				LOG.warn(String.format("Persistence exception caused by: %s", e
						.getCause().getMessage()));
				listener.databaseProblemOccured(e);
			}
		}
	}

	public static abstract class Operation<T> {
		public abstract VerificationStatus validation();

		public abstract T operate() throws PersistenceException;
	}

}
