/**
	@author drew
 */

package pl.edu.agh.strateg.logic.verification;

import pl.edu.agh.strateg.model.analysis.Analysis;

import com.google.common.base.Optional;

public class AnalysisVerifier implements VerifierPart<Analysis> {

	@Override
	public VerificationStatus verify(Analysis entity, Optional<Object> parent,
			Verifier verifier) {
		VerificationStatus verificationStatus = verifier.verifyContainter(
				entity.getDescribingMetadata(), Optional.<Object> of(entity));
		return verificationStatus;
	}

}
