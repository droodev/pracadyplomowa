/**
	@author drew
 */

package pl.edu.agh.strateg.logic.verification;

import pl.edu.agh.strateg.model.Document;
import pl.edu.agh.strateg.model.analysis.Analysis;
import pl.edu.agh.strateg.model.metadata.MetadataContainer;
import pl.edu.agh.strateg.model.metadata.Metadatum;
import pl.edu.agh.strateg.model.metadata.description.Descriptor;

import com.google.common.base.Optional;

public interface Verifier {

	public VerificationStatus verifyMetadatum(Metadatum metadatum,
			Optional<Object> parent);

	public VerificationStatus verifyDocument(Document data,
			Optional<Object> parent);

	public VerificationStatus verifyDescriptor(Descriptor descriptor,
			Optional<Object> parent);

	public VerificationStatus verifyAnalysis(Analysis analysis,
			Optional<Object> parent);

	public VerificationStatus verifyContainter(MetadataContainer container,
			Optional<Object> parent);

}
