/**
	@author drew
 */

package pl.edu.agh.strateg.logic.events;

import pl.edu.agh.strateg.logic.async.OperationListener;

import com.google.common.base.Optional;

public interface OperationEvent<D, R> {
	public OperationListener<R> getListener();

	public Optional<D> getOperationData();
}
