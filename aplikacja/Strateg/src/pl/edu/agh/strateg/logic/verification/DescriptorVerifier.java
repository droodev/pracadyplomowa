/**
	@author drew
 */

package pl.edu.agh.strateg.logic.verification;

import javax.inject.Inject;

import org.apache.log4j.Logger;

import pl.edu.agh.strateg.model.metadata.description.Descriptor;
import pl.edu.agh.strateg.persistence.PersistenceException;
import pl.edu.agh.strateg.persistence.mongo.MDescriptorsDAO;

import com.google.common.base.Optional;

public class DescriptorVerifier implements VerifierPart<Descriptor> {
	private static final Logger LOG = Logger.getLogger(Thread.currentThread()
			.getStackTrace()[1].getClassName());

	private MDescriptorsDAO descriptorsDAO;

	@Inject
	public DescriptorVerifier(MDescriptorsDAO descriptorsDAO) {
		this.descriptorsDAO = descriptorsDAO;
	}

	public VerificationStatus verify(Descriptor descriptor,
			Optional<Object> parent, Verifier verifier) {
		if (parent.isPresent()) {
			try {
				if (!descriptorsDAO.exists(descriptor)) {
					return new NotVerified(
							"Descriptor doesn't exist in database - unconsistency error");
				}
			} catch (PersistenceException e) {
				LOG.warn(String.format(e.getMessage()));
				return new NotVerified("Verifier cannot connect to database");
			}
		}
		VerificationStatus verificationStatus = verifier.verifyContainter(
				descriptor.getDescribingMetadata(),
				Optional.<Object> of(descriptor));
		return verificationStatus;
	}

}
