/**
	@author drew
 */

package pl.edu.agh.strateg.logic;

import java.util.UUID;

import pl.edu.agh.strateg.model.analysis.Analysis;

public interface AnalysesManager extends EntityManager<UUID, Analysis>,
		MetadatumQuerable<Analysis> {

}
