package pl.edu.agh.strateg.utils;

public class Icons {
	
	public static final String DOCUMENTS = "Icons/Documents.png";
	public static final String ANALYSES = "Icons/Analysis.png";
	public static final String DESCRIPTORS = "Icons/Metadata.png";
	public static final String REPORTS = "Icons/Report.png";
	public static final String MAIN = "Icons/Home.png";
	public static final String NEW_DOCUMENT = "Icons/Document-Add.png";
	public static final String LOGIN = "Icons/Login.png";
	public static final String LOGOUT = "Icons/Logout.png";
	public static final String NEW = "Icons/Add.png";
	public static final String FIND = "Icons/Search.png";
	public static final String CANCEL = "Icons/Cancel.png";
	public static final String CONFIRM = "Icons/Confirm.png";
	public static final String DELETE = "Icons/Delete.png";
	public static final String EDIT = "Icons/Edit.png";
	public static final String RETURN = "Icons/Back.png";
	public static final String ATTACH = "Icons/Attachment.png";
	public static final String PREVIEW = "Icons/Preview.png";
	public static final String TITLE_LOGIN = "Icons/TitleLogin.png";
	public static final String TITLE_NO_PERMISSION = "Icons/TitleLock.png";
	public static final String TITLE_MAIN = "Icons/TitleHome.png";
	public static final String TITLE_DOCUMENTS = "Icons/TitleDocuments.png";
	public static final String TITLE_NEW_DOCUMENT = "Icons/TitleNewDocument.png";
	public static final String TITLE_DESCRIPTORS = "Icons/TitleMetadata.png";
	public static final String TITLE_NEW_DESCRIPTOR = "Icons/TitleNewMetadatum.png";
	public static final String TITLE_ANALYSES = "Icons/TitleAnalysis.png";
	public static final String TITLE_NEW_ANALYSIS = "Icons/TitleNewAnalysis.png";
	public static final String TITLE_REPORTS = "Icons/TitleReport.png";
	public static final String TITLE_NEW_REPORT = "Icons/TitleNewReport.png";
}
