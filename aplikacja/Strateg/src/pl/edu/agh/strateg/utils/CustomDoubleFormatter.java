package pl.edu.agh.strateg.utils;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class CustomDoubleFormatter {
	
	private static NumberFormat format = NumberFormat.getInstance(new Locale("pl", "PL"));
	
	public static String format(double d) {
		return format.format(d);
	}
	
	public static double parse(String s) throws ParseException {
		Number number = format.parse(s);
	    return number.doubleValue();
	}

}
