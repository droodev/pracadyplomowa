package pl.edu.agh.strateg.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

public class ResetOnCloseInputStream extends InputStream implements Serializable {

    private final InputStream decorated;

    public ResetOnCloseInputStream(InputStream anInputStream) {
        if (!anInputStream.markSupported()) {
            throw new IllegalArgumentException("marking not supported");
        }

        anInputStream.mark( 1 << 24);
        decorated = anInputStream;
        InputStreamList.getInstance().addStream(this);
    }

    @Override
    public void close() throws IOException {
        decorated.reset();
    }

    @Override
    public int read() throws IOException {
        return decorated.read();
    }
    
    public void reallyClose() throws IOException {
    	decorated.close();
    }
    
    public InputStream getStream() {
    	return decorated;
    }
    
}