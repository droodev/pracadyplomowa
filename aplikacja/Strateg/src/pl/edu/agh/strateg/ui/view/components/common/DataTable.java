package pl.edu.agh.strateg.ui.view.components.common;

import java.util.List;

import pl.edu.agh.strateg.ui.controller.component.documents.SearchResultsController;
import pl.edu.agh.strateg.ui.model.common.DataModel;

import com.vaadin.ui.Table;

public class DataTable extends Table{
	
	private String columnName;
	private String buttonCaption;
	
	private List<DataModel> dane;
	private SearchResultsController controller;
	
	public DataTable(List<DataModel> dane, SearchResultsController controller, 
					String columnName, String buttonCaption) {
		this.buttonCaption = buttonCaption;
		this.dane = dane;
		this.controller = controller;
		this.columnName = columnName;
		init();
	}

	private void init() {
		setSizeFull();
		setPageLength(0);
		DataCellGenerator cellGenerator = new DataCellGenerator(dane, controller, buttonCaption);
		addGeneratedColumn(columnName, cellGenerator);
		for (int i = 0; i < dane.size(); ++i) {
			addItem(i);
		}
	}

}
