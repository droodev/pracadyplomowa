package pl.edu.agh.strateg.ui.controller.interfaces.listeners;

public interface DatabaseProblemListener {
	
	void databaseProblemOccured();

}
