package pl.edu.agh.strateg.ui.controller.interfaces.listeners;

public interface DescriptorUpdatedListener extends DatabaseProblemListener {
	
	void descriptorUpdatedSuccessfully();
	void descriptorUpdatingFailed();

}
