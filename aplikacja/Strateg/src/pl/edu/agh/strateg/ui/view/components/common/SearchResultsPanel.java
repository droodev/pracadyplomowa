package pl.edu.agh.strateg.ui.view.components.common;

import java.util.List;

import pl.edu.agh.strateg.ui.controller.component.documents.SearchResultsController;
import pl.edu.agh.strateg.ui.controller.interfaces.HasSearchResults;
import pl.edu.agh.strateg.ui.model.common.DataModel;
import pl.edu.agh.strateg.ui.view.views.AbstractDocumentsView;

import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public class SearchResultsPanel extends Panel {
	
	private static final String PANEL_CAPTION = "Wyniki wyszukiwania";
	
	private SearchResultsController controller;
	private DataTable table; 
	private VerticalLayout mainLayout = new VerticalLayout();
	
	private String tableColumnName;
	private String buttonCaption;
	
	public SearchResultsPanel(String tableColumnName, String buttonCaption) {
		this(tableColumnName);
		this.buttonCaption = buttonCaption;
	}
	
	public SearchResultsPanel(String tableColumnName) {
		this.buttonCaption = AbstractDocumentsView.PODGLAD_DOKUMENTU_BUTTON_CAPTION;
		this.tableColumnName = tableColumnName;
		setCaption(PANEL_CAPTION);
		setSizeFull();
		
		setContent(mainLayout);
		
		controller = new SearchResultsController(this);
	}

	
	public void setMainController(HasSearchResults mainController) {
		controller.setMainController(mainController);
	}
	
	public void displayData(List<DataModel> dane) {
		reset();
		table = new DataTable(dane, controller, tableColumnName, buttonCaption);
		mainLayout.addComponent(table);
	}
	
	public void reset() {
		if (table != null)  {
			mainLayout.removeComponent(table);
		}
	}

}
