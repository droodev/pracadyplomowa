package pl.edu.agh.strateg.ui.model.communication.comparators;

import java.util.Comparator;

import pl.edu.agh.strateg.model.Document;

public class DocumentsComparator implements Comparator<Document>{

	@Override
	public int compare(Document document1, Document document2) {
		return new DescribableComparator().compare(document1, document2);
	}



}
