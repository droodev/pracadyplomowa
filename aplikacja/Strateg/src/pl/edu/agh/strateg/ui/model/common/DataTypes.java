package pl.edu.agh.strateg.ui.model.common;

public enum DataTypes {
	
	DOKUMENT, 
	ANALIZA, 
	RAPORT, 
	METADANA;

}
