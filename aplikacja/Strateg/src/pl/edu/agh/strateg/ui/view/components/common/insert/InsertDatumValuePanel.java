package pl.edu.agh.strateg.ui.view.components.common.insert;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import pl.edu.agh.strateg.model.metadata.description.Period;
import pl.edu.agh.strateg.ui.controller.component.common.InsertDatumValueController;
import pl.edu.agh.strateg.ui.controller.component.common.ChosenMetadataController;
import pl.edu.agh.strateg.ui.model.common.SimpleDatumModel;
import pl.edu.agh.strateg.utils.ValueTypes;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;

public class InsertDatumValuePanel extends Panel {
	
	private Label nazwaITypLabel = new Label();
	private InsertComponent component;
	
	private SimpleDatumModel model;
	private List<Object> listaMozliwosci = null;
	
	private InsertDatumValueController insertWartoscDanejController;
	
	public InsertDatumValuePanel(SimpleDatumModel model, List<Object> listaMozliwosci, 
												ChosenMetadataController wybraneMetadaneController) {
		this.model = model;
		this.listaMozliwosci = listaMozliwosci;
		this.insertWartoscDanejController = new InsertDatumValueController(this, wybraneMetadaneController);
		init();
	}
	
	public InsertDatumValuePanel(SimpleDatumModel model, ChosenMetadataController wybraneMetadaneController) {
		this.model = model;
		this.insertWartoscDanejController = new InsertDatumValueController(this, wybraneMetadaneController);
		init();
	}
	
	private void init() {
		
		HorizontalLayout mainLayout = new HorizontalLayout();
		mainLayout.setSizeFull();
		mainLayout.setMargin(true);
		mainLayout.setSpacing(true);
		
		ValueTypes typ;
		if (listaMozliwosci != null) {
			typ = ValueTypes.ENUM;
		}
		else if (model.getValueType().equals(Integer.class)) {
			typ = ValueTypes.INTEGER;
		}
		else if (model.getValueType().equals(Double.class)) {
			typ = ValueTypes.DOUBLE;
		} 
		else if (model.getValueType().equals(Date.class)) {
			typ = ValueTypes.DATE;
		}
		else if (model.getValueType().equals(Period.class)) {
			typ = ValueTypes.PERIOD;
		}
		else if (model.getValueType().equals(Void.class)) {
			typ = ValueTypes.TAGGED;
		}
		else {
			typ = ValueTypes.STRING;
		}
		
		nazwaITypLabel.setContentMode(ContentMode.HTML);
		nazwaITypLabel.setValue("<b>" + model.getName() + "</b> " + "<i>(" + typ.getCaption() + ")</i>");
		
		Class<?> klasa = model.getValueType();
		component = new ComponentsFactory(insertWartoscDanejController).getComponent(klasa, listaMozliwosci);
		
		mainLayout.addComponent(nazwaITypLabel);
		mainLayout.addComponent(component.getField());
		
		this.setContent(mainLayout);
	}
	
	public SimpleDatumModel getValue() {
		try {
			model.setValue(component.getValue());
			return model; 
		} 
		catch (NumberFormatException e1) {
			return null;
		}
		catch (ParseException e2) {
			return null;
		}
		catch (IllegalStateException e3) {
			return null;
		}
		catch (IllegalArgumentException e4) {
			return null;
		}
		
	}
	
	public void setValue(Object object) {
		component.setValue(object);
		model.setValue(object);
	}
	
	public boolean isValueValid() {
		return component.isValid();
	}

}
