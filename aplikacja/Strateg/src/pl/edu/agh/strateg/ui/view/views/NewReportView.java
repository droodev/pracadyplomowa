package pl.edu.agh.strateg.ui.view.views;

import pl.edu.agh.strateg.utils.Icons;

public class NewReportView extends AbstractNewDocumentView {

	@Override
	protected void setCaptions() {
		POTWIERDZ_BUTTON_CAPTION = "Stw�rz raport";
		ANULUJ_BUTTON_CAPTION = "Powr�t do raport�w";
		PAGE_CAPTION = "NOWY RAPORT";
		NIE_UDALO_SIE_DODAC_CAPTION = "Stworzenie raportu nie powiod�o si�";
		ICON = Icons.TITLE_NEW_REPORT;
	}
	
	public NewReportView() {
		super();
		potwierdzButton.setEnabled(false);
	}
	


}
