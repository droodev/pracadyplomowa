package pl.edu.agh.strateg.ui.controller.component.analysis;

import pl.edu.agh.strateg.model.analysis.Analysis;
import pl.edu.agh.strateg.model.analysis.SimpleAnalysis;
import pl.edu.agh.strateg.ui.controller.component.common.EditController;
import pl.edu.agh.strateg.ui.controller.interfaces.HasAnalysisTable;
import pl.edu.agh.strateg.ui.model.analysis.AnalysisDataModel;
import pl.edu.agh.strateg.ui.model.analysis.FactorTypes;
import pl.edu.agh.strateg.ui.model.common.DataTypes;
import pl.edu.agh.strateg.ui.model.metadata.MetadataGroup;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;
import pl.edu.agh.strateg.ui.view.components.analysis.EditAnalysisWindow;
import pl.edu.agh.strateg.ui.view.components.common.EditWindow;

public class EditAnalysisController extends EditController implements HasAnalysisTable{
	
	private AnalysisDataModel analysisData;
	private AnalysisTableController analysisTableController;
	@SuppressWarnings("unused")
	private AddFactorWindowController addFactorWindowController;
	
	public EditAnalysisController(EditWindow edytujWindow, AnalysisDataModel daneAnalizy) {
		super(edytujWindow, DataTypes.ANALIZA);
		this.analysisData = daneAnalizy;
	}
	
	public AnalysisDataModel getAnalysisData() {
		return analysisData;
	}

	@Override
	public void setAnalysisTableController(AnalysisTableController controller) {
		this.analysisTableController = controller;
		
	}

	@Override
	public void setAddFactorWindowController(AddFactorWindowController controller) {
		this.addFactorWindowController = controller;
		
	}

	@Override
	public void addFactor(String nazwa, FactorTypes typ) {
		if (typ == FactorTypes.SILA) {
			analysisTableController.addStrength(nazwa);
		}
		else if (typ == FactorTypes.SLABOSC) {
			analysisTableController.addWeakness(nazwa);
		}
		else if (typ == FactorTypes.SZANSA) {
			analysisTableController.addOpportunity(nazwa);
		}
		else if (typ == FactorTypes.ZAGROZENIE) {
			analysisTableController.addThread(nazwa);
		}
		
	}

	@Override
	public void addFactorButtonClicked() {
		((EditAnalysisWindow) editWindow).displayAddFactorWindow();
		
	}
	
	@Override
	public void descriptorsReceived(MetadataGroup grupaMetadanych) {
		super.descriptorsReceived(grupaMetadanych);
		for (MetadatumModel m : analysisData.getMetadata()) {
			chosenData(m);
			chosenMetadataController.setValue(m, m.getSimpleDatumModel().getValue());
		}
	}
	
	@Override
	protected void confirmButtonClicked() {
		super.confirmButtonClicked();
		analysisData.setMetadata(chosenMetadataController.getAllValues());
		Analysis przedZmianaAnaliza = analysisData.getAnalysis();
		Analysis zmienionaAnaliza = analysisTableController.getAnalysis();
		((SimpleAnalysis)zmienionaAnaliza).setUUID(przedZmianaAnaliza.getUUID());
		analysisData.setAnalysis(zmienionaAnaliza);
		editWindow.close();
		
	}
	
}
