package pl.edu.agh.strateg.ui.model.communication;

import java.io.File;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import pl.edu.agh.strateg.ui.controller.interfaces.listeners.AnalysisCreatedListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.AnalysisDeletedListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.AnalysisUpdatedListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.FileCreatedListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.FileDeletedListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.FileUpdatedListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.FoundAnalysisListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.FoundFilesListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.GotAllMetadaneListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.GotFileListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.DescriptorCreatedListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.DescriptorDeletedListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.DescriptorUpdatedListener;
import pl.edu.agh.strateg.ui.model.analysis.AnalysisDataModel;
import pl.edu.agh.strateg.ui.model.documents.DocumentsDataModel;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;

public interface Gateway extends Serializable {
	
	void getAllDescriptors(GotAllMetadaneListener listener);
	void saveDescriptor(MetadatumModel metadana, DescriptorCreatedListener listener);
	void updateDescriptor(MetadatumModel metadana, DescriptorUpdatedListener listener);
	void deleteDescriptor(MetadatumModel metadana, DescriptorDeletedListener listener);
	
	void addNewFile(File file, List<MetadatumModel> metadane, FileCreatedListener listener);
	void updateFile(DocumentsDataModel daneDokumentuModel, FileUpdatedListener listener);
	void deleteFile(DocumentsDataModel daneDokumentuModel, FileDeletedListener listener);
	void findFiles(List<MetadatumModel> metadane, FoundFilesListener listener);
	void findFile(UUID uuid, GotFileListener listener);
	
	void addNewReport(File file, List<MetadatumModel> metadane, FileCreatedListener listener);
	void findReports(List<MetadatumModel> metadane, FoundFilesListener listener);
	
	void addNewAnalysis(AnalysisDataModel analiza, AnalysisCreatedListener listener);
	void updateAnalysis(AnalysisDataModel analiza, AnalysisUpdatedListener listener);
	void deleteAnalysis(AnalysisDataModel analiza, AnalysisDeletedListener listener);
	void findAnalyses(List<MetadatumModel> metadane, FoundAnalysisListener listener);

}
