package pl.edu.agh.strateg.ui.model.common;

import java.io.Serializable;
import java.util.Date;

import pl.edu.agh.strateg.model.metadata.description.Period;
import pl.edu.agh.strateg.utils.CustomDateFormatter;
import pl.edu.agh.strateg.utils.CustomDoubleFormatter;

public class SimpleDatumModel implements Serializable{
	
	private String name;
	private Object value;
	private Class<?> valueType;
	
	public SimpleDatumModel(Class<?> klasa) {
		this.valueType = klasa;
	}
	
	public SimpleDatumModel(String nazwa, Class<?> klasa) {
		this.name = nazwa;
		this.valueType = klasa;
	}
	
	public SimpleDatumModel(String nazwa, Object wartosc, Class<?> klasa) {
		
		if (wartosc != null && !wartosc.getClass().equals(klasa)) {
			throw new RuntimeException("Niezgodnosc typow!!!");
		}
		this.value = wartosc;
		this.name = nazwa;
		this.valueType = klasa;
	}
	
	public String getName() {
		return name;
	}
	
	public Object getValue() {
		return value;
	}
	
	public Class<?> getValueType() {
		return valueType;
	}
	
	public String getStringValue() {
		if (value == null) {
			return "";
		}
		if (value instanceof Date) {
			return CustomDateFormatter.format((Date) value);
		}
		if (value instanceof Period) {
			return CustomDateFormatter.format(((Period) value).getStart()) + " - " + 
										CustomDateFormatter.format(((Period) value).getEnd());
		}
		if (value instanceof Double) {
			return CustomDoubleFormatter.format((Double) value);
		}
		return value.toString();
	}

	public void setName(String nazwa) {
		this.name = nazwa;
	}

	public void setValue(Object wartosc) {
		if (wartosc != null && !wartosc.getClass().equals(this.valueType)) {
			throw new RuntimeException("Niezgodnosc typow!!!" + this.valueType + "#" + wartosc.getClass());
		}
		else {
			this.value = wartosc;
		}
	}

}
