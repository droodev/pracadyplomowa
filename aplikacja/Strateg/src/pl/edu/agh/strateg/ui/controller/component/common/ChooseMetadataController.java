package pl.edu.agh.strateg.ui.controller.component.common;

import java.io.Serializable;

import pl.edu.agh.strateg.ui.controller.interfaces.HasChooseMetadata;
import pl.edu.agh.strateg.ui.model.metadata.MetadataGroup;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;
import pl.edu.agh.strateg.ui.view.components.common.ChooseMetadataPanel;

import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;

public class ChooseMetadataController implements ItemClickListener, Serializable {
	
	private ChooseMetadataPanel panel;
	private HasChooseMetadata mainController;
	
	public ChooseMetadataController(ChooseMetadataPanel panel) {
		this.panel = panel;
	}

	
	public void setMainController(HasChooseMetadata mainController) {
		this.mainController = mainController;
		mainController.setChooseMetadataController(this);
	}

	@Override
	public void itemClick(ItemClickEvent event) {
		Object object = event.getItemId();
		if (object instanceof String) {
			MetadatumModel dana = panel.getMetadana((String) object);
			mainController.chosenData(dana);
		}
		
	}
	
	public MetadatumModel deleteDatum(MetadatumModel dana) {
		panel.deleteDatum(dana);
		return dana;
	}
	
	public MetadatumModel deleteDatum(String nazwa) {
		MetadatumModel metadana = panel.getMetadana(nazwa);
		panel.deleteDatum(metadana);
		return metadana;
	}
	
	public void restoreDatum(MetadatumModel dana) {
		panel.restoreDatum(dana);
	}
	
	public void fillPanelWithData(MetadataGroup dane) {
		panel.fillWithData(dane);
	}
	

}
