package pl.edu.agh.strateg.ui.controller.component.analysis;

import java.util.UUID;

import pl.edu.agh.strateg.ui.controller.interfaces.listeners.GotFileListener;
import pl.edu.agh.strateg.ui.model.common.DataModel;
import pl.edu.agh.strateg.ui.model.communication.GatewayImpl;
import pl.edu.agh.strateg.ui.model.documents.DocumentsDataModel;
import pl.edu.agh.strateg.ui.view.components.analysis.AbstractAnalysisTable;
import pl.edu.agh.strateg.ui.view.components.analysis.ReadOnlyAnalysisTable;

public class ReadOnlyAnalysisTableController extends AnalysisTableController implements GotFileListener{

	public ReadOnlyAnalysisTableController(AbstractAnalysisTable analizaTable) {
		super(analizaTable);
	}
	
	public void findDocument(UUID uuid) {
		GatewayImpl.getInstance().findFile(uuid, this);
	}

	
	@Override
	public void gettingFileFailed() {
		((ReadOnlyAnalysisTable)analysisTable).displayError();
		
	}

	@Override
	public void fileReceived(DataModel daneModel) {
		((ReadOnlyAnalysisTable)analysisTable).gotData((DocumentsDataModel) daneModel);
		
	}
	
	@Override
	public void databaseProblemOccured() {
		analysisTable.databaseProblemOccured();
		
	}

}
