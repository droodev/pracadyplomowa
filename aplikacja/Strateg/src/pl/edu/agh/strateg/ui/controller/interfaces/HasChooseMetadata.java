package pl.edu.agh.strateg.ui.controller.interfaces;

import pl.edu.agh.strateg.ui.controller.component.common.ChooseMetadataController;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;

public interface HasChooseMetadata {
	
	void setChooseMetadataController(ChooseMetadataController wybierzMetadaneController);
	void chosenData(MetadatumModel metadana);
}
