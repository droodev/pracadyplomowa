package pl.edu.agh.strateg.ui.controller.interfaces.listeners;

public interface FileDeletedListener extends DatabaseProblemListener{
	
	void fileDeletedSuccessfully();
	void fileDeletingFailed();

}
