package pl.edu.agh.strateg.ui.view.components.common.insert;


import pl.edu.agh.strateg.ui.controller.component.common.InsertDatumValueController;

import com.vaadin.data.validator.IntegerValidator;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.TextField;

@SuppressWarnings("deprecation")
public class InsertInteger implements InsertComponent{
	
	private static final String NIEPOPRAWNA_LICZBA_CAPTION = "Niepoprawna liczba";
	private static final String NIEPOPRAWNY_TYP_ERROR = "Niepoprawny typ";
	
	private TextField field;

	public InsertInteger(InsertDatumValueController controller) {
		field = new TextField();
		field.addValidator(new IntegerValidator(NIEPOPRAWNA_LICZBA_CAPTION));
		field.setNullRepresentation("");
		field.setConversionError(NIEPOPRAWNA_LICZBA_CAPTION);
		field.setRequired(true);
		field.setImmediate(true);
		field.addValueChangeListener(controller);
	}

	@Override
	public Object getValue() throws NumberFormatException{
		return Integer.valueOf(field.getValue());
	}

	@Override
	public AbstractField<?> getField() {
		return field;
	}
	
	@Override
	public boolean isValid() {
		return field.isValid();
	}
	
	@Override
	public void setValue(Object object) {
		if (!(object instanceof Integer)) {
			throw new RuntimeException(NIEPOPRAWNY_TYP_ERROR);
		}
		field.setValue(String.valueOf(object));
		
	}

}
