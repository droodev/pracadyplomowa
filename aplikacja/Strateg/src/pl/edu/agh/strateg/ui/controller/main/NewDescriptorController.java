package pl.edu.agh.strateg.ui.controller.main;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pl.edu.agh.strateg.ui.Views;
import pl.edu.agh.strateg.ui.controller.component.common.ChooseMetadataController;
import pl.edu.agh.strateg.ui.controller.component.common.ChosenMetadataController;
import pl.edu.agh.strateg.ui.controller.interfaces.HasChooseMetadata;
import pl.edu.agh.strateg.ui.controller.interfaces.HasChosenMetadata;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.GotAllMetadaneListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.DescriptorCreatedListener;
import pl.edu.agh.strateg.ui.model.common.SimpleDatumModel;
import pl.edu.agh.strateg.ui.model.common.DataTypes;
import pl.edu.agh.strateg.ui.model.communication.Gateway;
import pl.edu.agh.strateg.ui.model.communication.GatewayImpl;
import pl.edu.agh.strateg.ui.model.metadata.MetadataGroup;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;
import pl.edu.agh.strateg.ui.model.metadata.RequiredMetadatum;
import pl.edu.agh.strateg.ui.view.components.common.TitlePanel;
import pl.edu.agh.strateg.ui.view.views.NewDescriptorView;

import com.vaadin.navigator.Navigator;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public class NewDescriptorController implements HasChooseMetadata, HasChosenMetadata, ClickListener, 
							DescriptorCreatedListener, GotAllMetadaneListener, Serializable {
	
	private Navigator navigator;
	
	private ChooseMetadataController chooseMetadataController;
	private ChosenMetadataController chosenMetadataController;
	
	private NewDescriptorView newDescriptorView;
	
	private Gateway gateway;
	
	
	public NewDescriptorController(Navigator navigator) {
		gateway = GatewayImpl.getInstance();
		this.navigator = navigator;
	}
	
	public void setNewDescriptorView(NewDescriptorView nowaMetadanaView) {
		this.newDescriptorView = nowaMetadanaView;
	}
	
	public void setChooseMetadataController(
			ChooseMetadataController wybierzMetadaneController) {
		this.chooseMetadataController = wybierzMetadaneController;
	}
	
	public void setChosenMetadataController(ChosenMetadataController wybraneMetadaneController) {
		this.chosenMetadataController = wybraneMetadaneController;
	}
	
	
	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		if (button.getCaption().equals(NewDescriptorView.STRONA_GLOWNA_BUTTON_CAPTION)) {
			mainPageButtonClicked();
		}
		else if (button.getCaption().equals(NewDescriptorView.POTWIERDZ_BUTTON_CAPTION)) {
			confirmButtonClicked();
		}
		else if (button.getCaption().equals(NewDescriptorView.ANULUJ_BUTTON_CAPTION)) {
			cancelButtonClicked();
		}
		
	}

	
	public void mainPageButtonClicked() {
		navigator.navigateTo(Views.MAIN_VIEW);
	}
	
	public void confirmButtonClicked() {
		String nazwa = newDescriptorView.getNewName();
		if (nazwa == null || nazwa.trim().length() == 0) {
			newDescriptorView.displayEmptyNameWarning();
			return;
		}
		if (!chosenMetadataController.areValuesValid()) {
			newDescriptorView.displayNotValidValuesWarning();
			return;
		}
		List<MetadatumModel> metadane = chosenMetadataController.getAllValues();
		Class<?> typ = newDescriptorView.getType();
		if(newDescriptorView.isEnum()) {
			List<Object> mozliweWartosci = newDescriptorView.getPossibleValues();
			if (mozliweWartosci.isEmpty()) {
				newDescriptorView.displayNotValidValuesWarning();
				return;
			}
			addDescriptor(nazwa, mozliweWartosci, metadane);
		}
		else {
			addDescriptor(nazwa, typ, metadane);
		}
		
	}
	
	public void cancelButtonClicked() {
		navigator.navigateTo(Views.DESCRIPTORS_VIEW);
	}
	
	@Override
	public void chosenData(MetadatumModel metadana) {
		chosenMetadataController.datumChosen(metadana);
		chooseMetadataController.deleteDatum(metadana);
		addAllRequired(metadana);
	}
	
	@Override
	public void datumValueChanged(MetadatumModel metadana) {
		addAllRequired(metadana);
	}

	
	private void addAllRequired(MetadatumModel metadana) {
		if (metadana.getRequirements(DataTypes.METADANA) == null) {
			return;
		}
		for (RequiredMetadatum wymaganaMetadana : metadana.getRequirements(DataTypes.METADANA)) {
			if (!chosenMetadataController.contains(wymaganaMetadana.getDescriptorName())) {
				if (wymaganaMetadana.getValue() == null || 
						wymaganaMetadana.getValue().equals(metadana.getSimpleDatumModel().getValue())) {
					MetadatumModel wymagana = chooseMetadataController.deleteDatum(wymaganaMetadana.getDescriptorName());
					chosenMetadataController.datumChosen(wymagana);
				}
			}
		}
		
	}

	private void addDescriptor(String nazwa, Class<?> typ, List<MetadatumModel> metadane) {
		MetadatumModel metadana = new MetadatumModel(new SimpleDatumModel(nazwa, typ), metadane);
		gateway.saveDescriptor(metadana, this);
		
	}
	
	private void addDescriptor(String nazwa, List<Object> mozliweWartosci, List<MetadatumModel> metadane) {
		MetadatumModel metadana = new MetadatumModel(new SimpleDatumModel(nazwa, String.class), metadane, mozliweWartosci);
		gateway.saveDescriptor(metadana, this);
		
	}

	
	public void fillChooseMetadataPanel() {
		GatewayImpl.getInstance().getAllDescriptors(this);
	}

	@Override
	public void descriptorCreatedSuccessfully() {
		newDescriptorView.successfullyAdded();
	}

	@Override
	public void descriptorCreationFailed() {
		newDescriptorView.addingFailed();
		
	}

	@Override
	public void gettingDescriptorsFailed() {
		System.out.println("Getting descriptors failed");
		
	}

	@Override
	public void descriptorsReceived(MetadataGroup grupaMetadanych) {
		chooseMetadataController.fillPanelWithData(grupaMetadanych);
		List<MetadatumModel> wymaganeMetadane = new ArrayList<>();
		getRequiredMetadata(grupaMetadanych, wymaganeMetadane);
		for (MetadatumModel metadana : wymaganeMetadane) {
			chosenData(metadana);
		}
	}
	
	private void getRequiredMetadata(MetadataGroup grupaMetadanych, List<MetadatumModel> wymaganeMetadane) {
		if (grupaMetadanych.getData() != null) {
			for (MetadatumModel metadana : grupaMetadanych.getData()) {
				if (metadana.isRequired(DataTypes.METADANA)) {
					wymaganeMetadane.add(metadana);
				}
			}
		}
		if (grupaMetadanych.getGroups() != null) {
			for (MetadataGroup grupa : grupaMetadanych.getGroups()) {
				getRequiredMetadata(grupa, wymaganeMetadane);
			}
		}
	}

	@Override
	public boolean canBeDeleted(MetadatumModel metadana) {
		if (metadana.isRequired(DataTypes.METADANA)) {
			return false;
		}
		for (MetadatumModel m : chosenMetadataController.getAllValues()) {
			if (m.getRequirements(DataTypes.METADANA) == null) {
				continue;
			}
			for (RequiredMetadatum wymaganaMetadana : m.getRequirements(DataTypes.METADANA)) {
				if (wymaganaMetadana.getDescriptorName().equals(metadana.getSimpleDatumModel().getName())) {
					if (wymaganaMetadana.getValue() == null || 
						wymaganaMetadana.getValue().equals(m.getSimpleDatumModel().getValue())) {
						return false;
					}
				}
			}
		}
		return true;
	}

	@Override
	public void restoreDatum(MetadatumModel metadana) {
		chooseMetadataController.restoreDatum(metadana);
	}

	@Override
	public void databaseProblemOccured() {
		newDescriptorView.databaseProblemOccured();
		
	}
	
	public void addNavigator(TitlePanel panel) {
		panel.setNavigator(navigator);
	}
	

}
