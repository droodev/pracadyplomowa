package pl.edu.agh.strateg.ui.controller.main;

import pl.edu.agh.strateg.ui.Views;
import pl.edu.agh.strateg.ui.controller.component.common.EditController;
import pl.edu.agh.strateg.ui.controller.component.documents.EditDocumentController;
import pl.edu.agh.strateg.ui.controller.interfaces.HasEditWindow;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.FileDeletedListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.FileUpdatedListener;
import pl.edu.agh.strateg.ui.model.communication.GatewayImpl;
import pl.edu.agh.strateg.ui.model.documents.DocumentsDataModel;
import pl.edu.agh.strateg.ui.view.components.common.ConfirmWindow;
import pl.edu.agh.strateg.ui.view.components.documents.EditDocumentWindow;
import pl.edu.agh.strateg.ui.view.views.DocumentsView;

import com.vaadin.navigator.Navigator;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;

public class DocumentsController extends AbstractDocumentsController implements HasEditWindow, 
									FileUpdatedListener, FileDeletedListener {

	private EditController editController;
	
	public DocumentsController(Navigator navigator) {
		super(navigator);
	}
	
	public void setEditController(EditController edytujController) {
		this.editController = edytujController;
		
	}

	
	protected void newDocumentButtonClicked() {
		navigator.navigateTo(Views.NEW_DOCUMENT_VIEW);
	}
	

	public void addEditWindow() {
		((DocumentsView)documentsView).displayEditWindow(currentData);
	}




	@Override
	public void windowClose(CloseEvent e) {
		Window window = e.getWindow();
		if (window instanceof EditDocumentWindow) {
			if (!editController.isChangeConfirmed()) {
				((DocumentsView)documentsView).modificationCancelled();
			}
			else {
				DocumentsDataModel daneDokumentu = ((EditDocumentController)editController).getDocumentData();
				GatewayImpl.getInstance().updateFile(daneDokumentu, this);
			}
		}
		else if (window instanceof ConfirmWindow) {
			ConfirmWindow pWindow = (ConfirmWindow)window;
			if (pWindow.isConfirmed()) {
				if (pWindow.getQuestion().equals(DocumentsView.CZY_USUNAC)) {
					GatewayImpl.getInstance().deleteFile(currentData, this);
				}
				else {
					sendFindQuery();
				}
			}
		}
		
	}

	@Override
	public void fileUpdatedSuccessfully() {
		((DocumentsView)documentsView).fileUpdatedSuccessfully();
		sendFindQuery();
	}

	@Override
	public void fileUpdatingFailed() {
		((DocumentsView)documentsView).fileUpdatingFailed();
		sendFindQuery();
		
	}

	@Override
	public void deleteDatum() {
		((DocumentsView)documentsView).displayConfirmDeletingWindow();
		
	}

	@Override
	public void fileDeletedSuccessfully() {
		((DocumentsView)documentsView).fileDeletedSuccessfully();
		sendFindQuery();
		
	}

	@Override
	public void fileDeletingFailed() {
		((DocumentsView)documentsView).fileDeletingFailed();
		sendFindQuery();
		
	}


}
