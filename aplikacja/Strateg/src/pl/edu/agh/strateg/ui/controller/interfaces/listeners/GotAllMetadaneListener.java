package pl.edu.agh.strateg.ui.controller.interfaces.listeners;

import pl.edu.agh.strateg.ui.model.metadata.MetadataGroup;

public interface GotAllMetadaneListener extends DatabaseProblemListener {

	void gettingDescriptorsFailed();
	void descriptorsReceived(MetadataGroup grupaMetadanych);
	
}
