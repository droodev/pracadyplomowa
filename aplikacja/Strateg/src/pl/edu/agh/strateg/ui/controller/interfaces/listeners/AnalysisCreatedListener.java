package pl.edu.agh.strateg.ui.controller.interfaces.listeners;

public interface AnalysisCreatedListener extends DatabaseProblemListener{
	
	void analysisCreatedSuccessfully();
	void analysisCreationFailed();

}
