package pl.edu.agh.strateg.ui.view.components.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import pl.edu.agh.strateg.ui.controller.component.common.ChosenMetadataController;
import pl.edu.agh.strateg.ui.controller.interfaces.HasChosenMetadata;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;
import pl.edu.agh.strateg.ui.view.components.common.insert.InsertDatumValuePanel;
import pl.edu.agh.strateg.utils.Icons;

import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public class ChosenMetadataPanel extends Panel {
	
	private static final String PANEL_CAPTION = "Wybrane filtry";
	private static final String USUN_CAPTION = "Usu�";
	private static final String NIE_MOZNA_USUNAC_CAPTION = "Nie mo�na usun�� wybranej metadanej";
	
	private VerticalLayout mainLayout = new VerticalLayout();
	private Map<MetadatumModel,InsertDatumValuePanel> components;
	private Map<MetadatumModel,Layout> layouts = new HashMap<>();
	private Map<String, MetadatumModel> metadane;
	private ChosenMetadataController controller;
	
	private boolean canBeDeleted = false;
	
	
	public ChosenMetadataPanel() {
		this(PANEL_CAPTION);
	}
	
	public ChosenMetadataPanel(String panelCaption, boolean canBeDeleted) {
		this(panelCaption);
		this.canBeDeleted = canBeDeleted;
	}
	
	public ChosenMetadataPanel(String panelCaption) {
		mainLayout.setMargin(new MarginInfo(false, true, false, false));
		setCaption(panelCaption);
		setSizeFull();
		setContent(mainLayout);
		components = new LinkedHashMap<MetadatumModel, InsertDatumValuePanel>();
		metadane = new HashMap<>();
		controller = new ChosenMetadataController(this);
		
	}
	
	public void setMainController(HasChosenMetadata mainController) {
		controller.setMainController(mainController);
	}

	
	public void addFilters(List<MetadatumModel> wybrane) {
		for (MetadatumModel dana : wybrane) {
			addFilter(dana);
		}
	}
	
	public void addFilter(MetadatumModel dana) {
		if (!metadane.containsKey(dana.getSimpleDatumModel().getName())) {
			HorizontalLayout layout = new HorizontalLayout();
			layout.setSizeFull();
			layout.setSpacing(true);
			
			InsertDatumValuePanel	panel = new InsertDatumValuePanel(dana.getSimpleDatumModel(), 
																		dana.getPossibleValues(), controller);
			components.put(dana, panel);
			metadane.put(dana.getSimpleDatumModel().getName(), dana);
			
			Button button = null;
			
			if (canBeDeleted) {
				button = new Button(USUN_CAPTION);
				button.setIcon(new ThemeResource(Icons.DELETE));
				button.addClickListener(controller);
				button.setData(dana);
				
			}
			
			layout.addComponent(panel);
			
			if (canBeDeleted) {
				layout.addComponent(button);
				layout.setComponentAlignment(button, Alignment.MIDDLE_RIGHT);
				layout.setExpandRatio(panel, 0.9f);
				layout.setExpandRatio(button, 0.1f);
			}
			
			mainLayout.addComponent(layout);
			layouts.put(dana, layout);
		}
	}
	
	public void removeFilter(MetadatumModel metadana) {
		String nazwaMetadanej = metadana.getSimpleDatumModel().getName();
		mainLayout.removeComponent(layouts.get(metadana));
		metadane.remove(nazwaMetadanej);
		components.remove(metadana);

	}
	
	public void removeAllFilters() {
		components.clear();
		metadane.clear();
		mainLayout.removeAllComponents();
	}
	
	public void setValue(MetadatumModel metadana, Object wartosc) {
		MetadatumModel m = metadane.get(metadana.getSimpleDatumModel().getName());
		components.get(m).setValue(wartosc);
	}
	
	public List<MetadatumModel> getAllValues() {
		List<MetadatumModel> wartosci = new ArrayList<MetadatumModel>();
		for (MetadatumModel dana : components.keySet()) {
			InsertDatumValuePanel panel = components.get(dana);
			Object wartosc;
			if (panel.getValue() == null) {
				wartosc = null;
			}
			else {
				wartosc = panel.getValue().getValue();
			}
			dana.getSimpleDatumModel().setValue(wartosc);
			wartosci.add(dana);
		}
		return wartosci;
	}
	
	public boolean areFieldsValid() {
		for (InsertDatumValuePanel panel : components.values()) {
			if (!panel.isValueValid()) {
				return false;
			}
		}
		return true;
	}
	
	public boolean contains(String nazwaMetadanej) {
		return metadane.containsKey(nazwaMetadanej);
	}
	
	public MetadatumModel getMetadatum(String nazwa) {
		return metadane.get(nazwa);
	}
	
	public void cannotBeDeleted() {
		Notification.show(NIE_MOZNA_USUNAC_CAPTION, Type.ERROR_MESSAGE);
	}

}
