package pl.edu.agh.strateg.ui.controller.interfaces;

import pl.edu.agh.strateg.ui.controller.component.documents.SearchResultsController;
import pl.edu.agh.strateg.ui.model.common.DataModel;

public interface HasSearchResults {
	
	void setSearchResultsController(SearchResultsController controller);
	void previewDatum(DataModel model);
	void chooseData(DataModel model);

}
