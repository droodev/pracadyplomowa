package pl.edu.agh.strateg.ui.controller.main;

import java.io.File;
import java.util.List;

import pl.edu.agh.strateg.ui.Views;
import pl.edu.agh.strateg.ui.model.common.DataTypes;
import pl.edu.agh.strateg.ui.model.communication.GatewayImpl;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;

import com.vaadin.navigator.Navigator;


public class NewReportController extends AbstractNewDocumentController {
	
	public NewReportController(Navigator navigator) {
		super(navigator);
		dataTypes = DataTypes.RAPORT;
	}
	
	
	protected void addNewDocument(List<MetadatumModel> metadane) {
		GatewayImpl.getInstance().addNewFile(createNewReport(), metadane, this);
		
	}
	
	private File createNewReport() {
		return null;
	}

	protected void cancelButtonClicked() {
		navigator.navigateTo(Views.REPORTS_VIEW);
	}


	@Override
	public void databaseProblemOccured() {
		newDocumentView.databaseProblemOccured();
		
	}

}
