package pl.edu.agh.strateg.ui.controller.interfaces.listeners;

import java.util.List;

import pl.edu.agh.strateg.ui.model.common.DataModel;

public interface FoundFilesListener extends DatabaseProblemListener {

	void findingFilesFailed();
	void filesReceived(List<DataModel> daneModel);
	
}
