package pl.edu.agh.strateg.ui.view.components.analysis;

import pl.edu.agh.strateg.security.Roles;
import pl.edu.agh.strateg.ui.model.analysis.AnalysisDataModel;
import pl.edu.agh.strateg.ui.view.components.common.PreviewWindow;

public class PreviewAnalysisWindow extends PreviewWindow{
	
	private static final String WINDOW_CAPTION = "Podgl�d analizy";
	private AnalysisDataModel daneAnalizy;
	private AbstractAnalysisTable analizaTable;
	
	public PreviewAnalysisWindow() {
		super(WINDOW_CAPTION, false);
		super.setEditRole(Roles.EDIT_ANALYSIS);
		super.setDeleteRole(Roles.DELETE_ANALYSIS);
	}
	
	protected void initComponents() {
		super.initComponents();
		analizaTable = new ReadOnlyAnalysisTable();
	}
	
	protected void initLayouts() {
		super.initLayouts();
		customLayout.addComponent(analizaTable);

	}
	
	public void changeAnalysisData(AnalysisDataModel daneAnalizy) {
		this.daneAnalizy = daneAnalizy;
		fillTable(daneAnalizy.getMetadata());
		((ReadOnlyAnalysisTable)analizaTable).fillTable(daneAnalizy.getAnalysis());
	}
	
	public AnalysisDataModel getAnalysisData() {
		return daneAnalizy;
	}

}
