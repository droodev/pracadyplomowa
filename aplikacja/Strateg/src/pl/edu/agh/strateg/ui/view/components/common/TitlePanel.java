package pl.edu.agh.strateg.ui.view.components.common;

import pl.edu.agh.strateg.ui.controller.component.common.TitleController;
import pl.edu.agh.strateg.utils.Icons;

import com.vaadin.navigator.Navigator;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;

public class TitlePanel extends Panel{
	
	public static final String LOGIN = "Strona logowania";
	public static final String LOGOUT = "Wyloguj";
	
	private Label titleLabel;
	private Label iconLabel;
	
	private HorizontalLayout mainLayout;
	
	private TitleController controller;
	
	public TitlePanel(String title, String icon) {
		controller = new TitleController();
		
		titleLabel = new Label(title);
		iconLabel = new Label();
		titleLabel.setStyleName("title");
		HorizontalLayout layout = new HorizontalLayout();
		iconLabel.setIcon(new ThemeResource(icon));
		
		layout.addComponent(titleLabel);
		layout.addComponent(iconLabel);
		
		layout.setComponentAlignment(titleLabel, Alignment.MIDDLE_LEFT);
		
		mainLayout = new HorizontalLayout();
		mainLayout.setSpacing(true);
		mainLayout.setSizeFull();
		mainLayout.setMargin(true);
		mainLayout.addComponent(layout);
		setContent(mainLayout);
	}
	
	public TitlePanel(String title, String icon, boolean isLoggedIn) {
		this(title, icon);
		Button button;
		if (isLoggedIn) {
			button = new Button(LOGOUT);
			button.setIcon(new ThemeResource(Icons.LOGOUT));
		}
		else {
			button = new Button(LOGIN);
			button.setIcon(new ThemeResource(Icons.LOGIN));
		}
		button.addClickListener(controller);
		mainLayout.addComponent(button);
		mainLayout.setComponentAlignment(button, Alignment.MIDDLE_RIGHT);
	}
	
	public void setNavigator(Navigator navigator) {
		controller.setNavigator(navigator);
	}

}
