package pl.edu.agh.strateg.ui.model.communication;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import pl.edu.agh.strateg.backbone.InitListener;
import pl.edu.agh.strateg.communication.LogicBus;
import pl.edu.agh.strateg.logic.async.OperationListener;
import pl.edu.agh.strateg.logic.events.AnalysesEvents;
import pl.edu.agh.strateg.logic.events.DescriptorEvents;
import pl.edu.agh.strateg.logic.events.DocumentsEvents;
import pl.edu.agh.strateg.logic.verification.VerificationStatus;
import pl.edu.agh.strateg.model.Document;
import pl.edu.agh.strateg.model.analysis.Analysis;
import pl.edu.agh.strateg.model.analysis.SimpleAnalysis;
import pl.edu.agh.strateg.model.metadata.description.Descriptor;
import pl.edu.agh.strateg.persistence.PersistenceException;
import pl.edu.agh.strateg.persistence.querying.MetadatumQuery;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.AnalysisCreatedListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.AnalysisDeletedListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.AnalysisUpdatedListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.FileCreatedListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.FileDeletedListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.FileUpdatedListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.FoundAnalysisListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.FoundFilesListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.GotAllMetadaneListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.GotFileListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.DescriptorCreatedListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.DescriptorDeletedListener;
import pl.edu.agh.strateg.ui.controller.interfaces.listeners.DescriptorUpdatedListener;
import pl.edu.agh.strateg.ui.model.analysis.AnalysisDataModel;
import pl.edu.agh.strateg.ui.model.common.DataModel;
import pl.edu.agh.strateg.ui.model.documents.DocumentsDataModel;
import pl.edu.agh.strateg.ui.model.metadata.MetadatumModel;

import com.google.common.base.Optional;

public class GatewayImpl implements Gateway {

	private Adapter adapter = new Adapter();

	private static GatewayImpl gatewayImpl;

	@Inject
	private LogicBus bus;

	public static GatewayImpl getInstance() {
		if (gatewayImpl == null) {
			gatewayImpl = InitListener.getInjectorTemp().getInstance(
					GatewayImpl.class);
		}
		return gatewayImpl;
	}

	@Override
	public void saveDescriptor(MetadatumModel metadana,
			final DescriptorCreatedListener listener) {
		Descriptor mainDescriptor = adapter.metadatumModelToDescriptor(metadana);
		bus.post(new DescriptorEvents.SaveIfNotExistDescriptorEvent(
				mainDescriptor, new OperationListener<Boolean>() {

					@Override
					public void preOperation() {
					}

					@Override
					public void postVerification(VerificationStatus status) {
						if (!status.isOK()) {
							listener.descriptorCreationFailed();
						}
					}

					@Override
					public void postOperation(Boolean returnValue) {
						if (returnValue.booleanValue()) {
							listener.descriptorCreatedSuccessfully();
						} else {
							listener.descriptorCreationFailed();
						}
					}

					@Override
					public void databaseProblemOccured(PersistenceException e) {
						listener.databaseProblemOccured();

					}

				}));

	}

	@Override
	public void updateDescriptor(MetadatumModel metadana,
			final DescriptorUpdatedListener listener) {

		Descriptor mainDescriptor = adapter.metadatumModelToDescriptor(metadana);
		bus.post(new DescriptorEvents.UpdateIfExistsDescriptorEvent(
				mainDescriptor, new OperationListener<Boolean>() {

					@Override
					public void preOperation() {

					}

					@Override
					public void postVerification(VerificationStatus status) {
						if (!status.isOK()) {
							listener.descriptorUpdatingFailed();
						}
					}

					@Override
					public void postOperation(Boolean returnValue) {
						if (returnValue.booleanValue()) {
							listener.descriptorUpdatedSuccessfully();
						} else {
							listener.descriptorUpdatingFailed();
						}

					}

					@Override
					public void databaseProblemOccured(PersistenceException e) {
						listener.databaseProblemOccured();

					}

				}));

	}
	
	@Override
	public void deleteDescriptor(MetadatumModel metadana, final DescriptorDeletedListener listener) {
		
		Descriptor mainDescriptor = adapter.metadatumModelToDescriptor(metadana);
		bus.post(new DescriptorEvents.DeleteIfExistsDescriptorEvent(
				mainDescriptor, new OperationListener<Boolean>() {

					@Override
					public void preOperation() {

					}

					@Override
					public void postVerification(VerificationStatus status) {
						if (!status.isOK()) {
							listener.descriptorDeletingFailed();
						}
					}

					@Override
					public void postOperation(Boolean returnValue) {
						if (returnValue.booleanValue()) {
							listener.descriptorDeletedSuccessfully();
						} else {
							listener.descriptorDeletingFailed();
						}

					}

					@Override
					public void databaseProblemOccured(PersistenceException e) {
						listener.databaseProblemOccured();

					}

				}));
		
	}

	@Override
	public void getAllDescriptors(final GotAllMetadaneListener listener) {

		bus.post(new DescriptorEvents.GetAllDescriptorsEvent(
				new OperationListener<List<Descriptor>>() {

					@Override
					public void preOperation() {

					}

					@Override
					public void postVerification(VerificationStatus status) {
						if (!status.isOK()) {
							listener.gettingDescriptorsFailed();
						}
					}

					@Override
					public void postOperation(List<Descriptor> returnValue) {
						listener.descriptorsReceived(adapter
								.createMetadataGroup(returnValue));
					}

					@Override
					public void databaseProblemOccured(PersistenceException e) {
						listener.databaseProblemOccured();

					}

				}));
	}

	@Override
	public void addNewFile(File file, List<MetadatumModel> metadane,
			final FileCreatedListener listener) {
		Document document = adapter.createDocument(file, metadane);
		bus.post(new DocumentsEvents.SaveIfNotExistsDocumentEvent(document,
				new OperationListener<Boolean>() {

					@Override
					public void preOperation() {

					}

					@Override
					public void postVerification(VerificationStatus status) {
						if (!status.isOK()) {
							listener.fileCreationFailed();
						}
					}

					@Override
					public void postOperation(Boolean returnValue) {
						if (!returnValue.booleanValue()) {
							listener.fileCreationFailed();
						} else {
							listener.fileCreatedSuccessfully();
						}
					}

					@Override
					public void databaseProblemOccured(PersistenceException e) {
						listener.databaseProblemOccured();

					}

				}));
	}

	@Override
	public void updateFile(DocumentsDataModel daneDokumentuModel,
			final FileUpdatedListener listener) {
		Document document = adapter
				.documentsDataModelToData(daneDokumentuModel);
		bus.post(new DocumentsEvents.UpdateIfExistsDocumentEvent(document,
				new OperationListener<Boolean>() {

					@Override
					public void preOperation() {

					}

					@Override
					public void postVerification(VerificationStatus status) {
						if (!status.isOK()) {
							listener.fileUpdatingFailed();
						}
					}

					@Override
					public void postOperation(Boolean returnValue) {
						if (!returnValue.booleanValue()) {
							listener.fileUpdatingFailed();
						} else {
							listener.fileUpdatedSuccessfully();
						}
					}

					@Override
					public void databaseProblemOccured(PersistenceException e) {
						listener.databaseProblemOccured();

					}

				}));

	}
	
	@Override
	public void deleteFile(DocumentsDataModel daneDokumentuModel, final FileDeletedListener listener) {
		Document document = adapter
				.documentsDataModelToData(daneDokumentuModel);
		bus.post(new DocumentsEvents.DeleteIfExistsDocumentEvent(document,
				new OperationListener<Boolean>() {

					@Override
					public void preOperation() {

					}

					@Override
					public void postVerification(VerificationStatus status) {
						if (!status.isOK()) {
							listener.fileDeletingFailed();
						}
					}

					@Override
					public void postOperation(Boolean returnValue) {
						if (!returnValue.booleanValue()) {
							listener.fileDeletingFailed();
						} else {
							listener.fileDeletedSuccessfully();
						}
					}

					@Override
					public void databaseProblemOccured(PersistenceException e) {
						listener.databaseProblemOccured();

					}

				}));
		
	}

	@Override
	public void findFiles(List<MetadatumModel> metadane,
			final FoundFilesListener listener) {
		Optional<MetadatumQuery> metadatumQuery = Optional.of(adapter
				.createMetadatumQuery(metadane));
		bus.post(new DocumentsEvents.QueryDocumentsByMetadataEvent(
				metadatumQuery, new OperationListener<List<Document>>() {

					@Override
					public void preOperation() {

					}

					@Override
					public void postVerification(VerificationStatus status) {
						if (!status.isOK()) {
							listener.findingFilesFailed();
						}

					}

					@Override
					public void postOperation(List<Document> returnValue) {

						listener.filesReceived(adapter
								.dataListToDocumentsDataModelList(returnValue));

					}

					@Override
					public void databaseProblemOccured(PersistenceException e) {
						listener.databaseProblemOccured();

					}

				}));
	}

	@Override
	public void findFile(UUID uuid, final GotFileListener listener) {
		bus.post(new DocumentsEvents.GetDocumentEvent(uuid,
				new OperationListener<Optional<Document>>() {

					@Override
					public void preOperation() {

					}

					@Override
					public void postVerification(VerificationStatus status) {
						if (!status.isOK()) {
							listener.gettingFileFailed();
						}

					}

					@Override
					public void postOperation(Optional<Document> returnValue) {

						listener.fileReceived(adapter
								.dataToDocumentsDataModel(returnValue.get()));

					}

					@Override
					public void databaseProblemOccured(PersistenceException e) {
						listener.databaseProblemOccured();

					}

				}));
	}

	@Override
	public void addNewAnalysis(AnalysisDataModel analiza,
			final AnalysisCreatedListener listener) {

		SimpleAnalysis analysis = adapter.createAnalysis(analiza);
		bus.post(new AnalysesEvents.SaveIfNotExistsAnalysisEvent(analysis,
				new OperationListener<Boolean>() {

					@Override
					public void preOperation() {

					}

					@Override
					public void postVerification(VerificationStatus status) {
						if (!status.isOK()) {
							listener.analysisCreationFailed();
						}
					}

					@Override
					public void postOperation(Boolean returnValue) {
						if (!returnValue.booleanValue()) {
							listener.analysisCreationFailed();
						} else {
							listener.analysisCreatedSuccessfully();
						}
					}

					@Override
					public void databaseProblemOccured(PersistenceException e) {
						listener.databaseProblemOccured();

					}

				}));

	}

	@Override
	public void findAnalyses(List<MetadatumModel> metadane,
			final FoundAnalysisListener listener) {
		Optional<MetadatumQuery> metadatumQuery = Optional.of(adapter
				.createMetadatumQuery(metadane));
		bus.post(new AnalysesEvents.QueryAnalysesByMetadataEvent(
				metadatumQuery, new OperationListener<List<Analysis>>() {

					@Override
					public void preOperation() {

					}

					@Override
					public void postVerification(VerificationStatus status) {
						if (!status.isOK()) {
							listener.findingAnalysisFailed();
						}

					}

					@Override
					public void postOperation(List<Analysis> returnValue) {

						listener.analysesReceived(adapter
								.analysisListToAnalysisDataModelList(returnValue));

					}

					@Override
					public void databaseProblemOccured(PersistenceException e) {
						listener.databaseProblemOccured();

					}

				}));

	}

	@Override
	public void updateAnalysis(AnalysisDataModel analiza,
			final AnalysisUpdatedListener listener) {
		SimpleAnalysis analysis = adapter.createAnalysis(analiza);
		bus.post(new AnalysesEvents.UpdateIfExistsAnalysisEvent(analysis,
				new OperationListener<Boolean>() {

					@Override
					public void preOperation() {

					}

					@Override
					public void postVerification(VerificationStatus status) {
						if (!status.isOK()) {
							listener.analysisUpdatingFailed();
						}
					}

					@Override
					public void postOperation(Boolean returnValue) {
						if (!returnValue.booleanValue()) {
							listener.analysisUpdatingFailed();
						} else {
							listener.analysisUpdatedSuccessfully();
						}
					}

					@Override
					public void databaseProblemOccured(PersistenceException e) {
						listener.databaseProblemOccured();

					}

				}));

	}
	
	@Override
	public void deleteAnalysis(AnalysisDataModel analiza, final AnalysisDeletedListener listener) {

		SimpleAnalysis analysis = adapter.createAnalysis(analiza);
		bus.post(new AnalysesEvents.DeleteIfExistsAnalysisEvent(analysis,
				new OperationListener<Boolean>() {

					@Override
					public void preOperation() {

					}

					@Override
					public void postVerification(VerificationStatus status) {
						if (!status.isOK()) {
							listener.analysisDeletingFailed();
						}
					}

					@Override
					public void postOperation(Boolean returnValue) {
						if (!returnValue.booleanValue()) {
							listener.analysisDeletingFailed();
						} else {
							listener.analysisDeletedSuccessfully();
						}
					}

					@Override
					public void databaseProblemOccured(PersistenceException e) {
						listener.databaseProblemOccured();

					}

				}));

		
	}

	@Override
	public void addNewReport(File file, List<MetadatumModel> metadane,
			FileCreatedListener listener) {
		
		throw new RuntimeException("Not implemented");

	}

	@Override
	public void findReports(List<MetadatumModel> metadane, FoundFilesListener listener) {
		List<DataModel> documents = new ArrayList<>();
		documents.add(adapter.createDummyReport());
		listener.filesReceived(documents);
	}



}
