package pl.edu.agh.strateg.ui.view.components.common.insert;

import java.io.Serializable;
import java.text.ParseException;

import com.vaadin.ui.Component;


public interface InsertComponent extends Serializable {
	
	Object getValue() throws ParseException;
	void setValue(Object object);
	Component getField();
	boolean isValid();

}
