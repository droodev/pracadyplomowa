package pl.edu.agh.strateg.ui.controller.interfaces;

import pl.edu.agh.strateg.ui.controller.component.documents.PreviewController;

public interface HasPreviewWindow {
	
	void setPreviewController(PreviewController controller);
	void deleteDatum();

}
