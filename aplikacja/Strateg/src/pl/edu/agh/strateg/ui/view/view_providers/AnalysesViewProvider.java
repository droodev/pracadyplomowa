package pl.edu.agh.strateg.ui.view.view_providers;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import pl.edu.agh.strateg.security.Roles;
import pl.edu.agh.strateg.ui.Views;
import pl.edu.agh.strateg.ui.controller.main.AnalysesController;
import pl.edu.agh.strateg.ui.view.views.AnalysesView;
import pl.edu.agh.strateg.ui.view.views.AccessDeniedView;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewProvider;

public class AnalysesViewProvider implements ViewProvider {
	private AnalysesController analizaController;

	private Navigator.ClassBasedViewProvider provider = new Navigator.ClassBasedViewProvider(
			Views.ANALYSES_VIEW, AnalysesView.class);

	private Navigator.ClassBasedViewProvider noPermissionProvider = new Navigator.ClassBasedViewProvider(
			Views.ANALYSES_VIEW, AccessDeniedView.class);

	public AnalysesViewProvider(AnalysesController analizaController) {
		this.analizaController = analizaController;
	}

	@Override
	public String getViewName(String viewAndParameters) {
		Subject currentUser = SecurityUtils.getSubject();
		if (currentUser.isAuthenticated() && currentUser.hasRole(Roles.SEE_ANALYSES)) {
			return provider.getViewName(viewAndParameters);
		} else {
			return noPermissionProvider.getViewName(viewAndParameters);
		}
	}

	@Override
	public View getView(String viewName) {
		Subject currentUser = SecurityUtils.getSubject();
		if (currentUser.isAuthenticated() && currentUser.hasRole(Roles.SEE_ANALYSES)) {
			AnalysesView view = (AnalysesView) provider.getView(viewName);
			view.setControllerToAllComponents(analizaController);
			return view;
		} else {
			AccessDeniedView view = (AccessDeniedView) noPermissionProvider
					.getView(viewName);
			return view;
		}
	}

}
