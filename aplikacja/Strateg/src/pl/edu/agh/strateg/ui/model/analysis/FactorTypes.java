package pl.edu.agh.strateg.ui.model.analysis;

public enum FactorTypes {
	
	SZANSA("Szansa"),
	ZAGROZENIE("Zagro�enie"),
	SILA("Si�a"),
	SLABOSC("S�abo��");
	
	private String caption;
	
	private FactorTypes(String caption) {
		this.caption = caption;
	}
	
	public String getCaption() {
		return caption;
	}

}
