/**
	@author drew
 */

package pl.edu.agh.strateg.model.analysis;

import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class AnalysisHeaderBaseTest {

	OpportunityAnalysisHeader firstLower = new OpportunityAnalysisHeader(
			"First", 4);
	OpportunityAnalysisHeader firstUpper = new OpportunityAnalysisHeader(
			"First", 9);

	OpportunityAnalysisHeader second = new OpportunityAnalysisHeader("Second",
			6);

	ThreatAnalysisHeader threat = new ThreatAnalysisHeader("Header", 1);

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void equalityAndHashTest() {
		Assert.assertThat(firstLower, CoreMatchers.equalTo(firstUpper));
		Assert.assertThat(firstLower.hashCode(),
				CoreMatchers.equalTo(firstUpper.hashCode()));
	}

	@Test
	public void comparingOneTypeTest() {
		Assert.assertThat(firstLower.compareTo(second) < 0,
				CoreMatchers.equalTo(true));
		Assert.assertThat(firstUpper.compareTo(second) > 0,
				CoreMatchers.equalTo(true));
		Assert.assertThat(firstLower.compareTo(firstUpper) == 0,
				CoreMatchers.equalTo(true));
	}

	@Test
	public void comparingTwoTypesTest() {
		Assert.assertThat(firstLower.compareTo(threat) < 0,
				CoreMatchers.equalTo(true));
	}
}
