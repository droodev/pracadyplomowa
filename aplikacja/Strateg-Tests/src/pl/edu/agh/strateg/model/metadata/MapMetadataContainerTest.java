/**
 * @author drew
 */
package pl.edu.agh.strateg.model.metadata;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import pl.edu.agh.strateg.model.metadata.description.Descriptor;
import pl.edu.agh.strateg.model.metadata.description.SimpleDescriptor;

import com.google.common.base.Optional;

public class MapMetadataContainerTest {

	private static final String DESC_ONE_NAME = "DescOneName";
	private static final String DESC_TWO_NAME = "DescTwoName";
	private static final String DESC_ONE_VAL = "DescOneVal";
	private static final int DESC_TWO_VAL = 2;

	private Descriptor descOne;
	private Descriptor descTwo;

	private SimpleMetadatum metaOne;
	private SimpleMetadatum metaTwo;

	Collection<Metadatum> metadataCollection;
	MapMetadataContainer testedContainer;

	@Before
	public void setUp() throws Exception {
		settingMetadataDeps();
		metadataCollection = new ArrayList<Metadatum>();
		metadataCollection.add(metaOne);

		testedContainer = new MapMetadataContainer(metadataCollection);
	}

	private void settingMetadataDeps() {
		// Mockito cannot mock methods of superclass. Can't mocking
		descOne = SimpleDescriptor.getCreator().createString(DESC_ONE_NAME);
		descTwo = SimpleDescriptor.getCreator().createInteger(DESC_TWO_NAME);

		metaOne = new SimpleMetadatum(descOne, DESC_ONE_VAL);
		metaTwo = new SimpleMetadatum(descTwo, DESC_TWO_VAL);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void constructorValid() throws Exception {
		metadataCollection.add(metaTwo);
		MapMetadataContainer cont = new MapMetadataContainer(metadataCollection);
		assertThat(cont.getSize(), is(2));
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructorNull() throws Exception {
		new MapMetadataContainer(null);
	}

	@Test
	public void addingValid() throws Exception {
		testedContainer.addMetadatum(metaTwo);
		assertThat(testedContainer.getSize(), is(2));
	}

	@Test(expected = IllegalArgumentException.class)
	public void addingNull() throws Exception {
		testedContainer.addMetadatum(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void addingExisting() throws Exception {
		testedContainer.addMetadatum(metaOne);
	}

	@Test(expected = IllegalArgumentException.class)
	public void removingNull() throws Exception {
		testedContainer.removeMetadatum(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void removingNonexisting() throws Exception {
		testedContainer.removeMetadatum(descTwo);
	}

	@Test
	public void removingValid() throws Exception {
		testedContainer.removeMetadatum(descOne);
		assertThat(testedContainer.getSize(), is(0));
	}

	@Test
	public void gettingValid() throws Exception {
		Optional<Metadatum> got = testedContainer.getMetadatum(descOne);
		assertThat(got.isPresent(), is(true));
		assertThat(got.get(), is((Metadatum) metaOne));
	}

	@Test
	public void gettingNonexisting() throws Exception {
		Optional<Metadatum> got = testedContainer.getMetadatum(descTwo);
		assertThat(got.isPresent(), is(false));
	}

	/**
	 * Watch out - assuming that getting metadatum checks equality on name only!
	 * THIS COULD BE CHANGED
	 */
	@Test(expected = IllegalArgumentException.class)
	public void gettingExistingWithBadType() throws Exception {
		Descriptor newDesc = SimpleDescriptor.getCreator().createInteger(
				DESC_ONE_NAME);
		testedContainer.getMetadatum(newDesc);
	}

	@Test(expected = IllegalArgumentException.class)
	public void gettingNull() throws Exception {
		testedContainer.getMetadatum(null);
	}

	@Test
	public void updateWithSameType() throws Exception {
		SimpleMetadatum newMetadatum = new SimpleMetadatum(descOne, "newVal");
		testedContainer.updateMetadatum(newMetadatum);
		assertThat(testedContainer.getSize(), is(1));
		Optional<Metadatum> gotMetadatum = testedContainer
				.getMetadatum(descOne);
		assertThat(gotMetadatum.get(), is((Metadatum) newMetadatum));
	}

	/**
	 * Watch out - assuming that getting metadatum checks equality on name only!
	 * THIS COULD BE CHANGED
	 */
	@Test
	public void updateWithOtherType() throws Exception {
		Descriptor newDesc = SimpleDescriptor.getCreator().createInteger(
				DESC_ONE_NAME);
		SimpleMetadatum newMetadatum = new SimpleMetadatum(newDesc,
				DESC_TWO_VAL);
		testedContainer.updateMetadatum(newMetadatum);
		assertThat(testedContainer.getSize(), is(1));
		Optional<Metadatum> gotMetadatum = testedContainer
				.getMetadatum(newDesc);
		assertThat(gotMetadatum.get(), is((Metadatum) newMetadatum));
	}

	@Test
	public void updateTheSame() throws Exception {
		testedContainer.updateMetadatum(metaOne);
		assertThat(testedContainer.getSize(), is(1));
		Optional<Metadatum> gotMetadatum = testedContainer
				.getMetadatum(descOne);
		assertThat(gotMetadatum.get(), is((Metadatum) metaOne));
	}

	@Test(expected = IllegalArgumentException.class)
	public void updateNonexisting() throws Exception {
		testedContainer.updateMetadatum(metaTwo);
	}

	@Test(expected = IllegalArgumentException.class)
	public void updateNull() throws Exception {
		testedContainer.updateMetadatum(null);
	}

}
