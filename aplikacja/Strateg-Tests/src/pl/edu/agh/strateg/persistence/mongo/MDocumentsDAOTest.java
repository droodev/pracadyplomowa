/**
	@author drew
 */

package pl.edu.agh.strateg.persistence.mongo;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import pl.edu.agh.strateg.helpers.ByteOperationsHelper;
import pl.edu.agh.strateg.model.Document;
import pl.edu.agh.strateg.model.SimpleDocument;
import pl.edu.agh.strateg.model.metadata.MapMetadataContainer;
import pl.edu.agh.strateg.model.metadata.MetadataContainer;
import pl.edu.agh.strateg.model.metadata.Metadatum;
import pl.edu.agh.strateg.model.metadata.SimpleMetadatum;
import pl.edu.agh.strateg.model.metadata.description.Descriptor;
import pl.edu.agh.strateg.model.metadata.description.Domain;
import pl.edu.agh.strateg.model.metadata.description.SimpleDescriptor;
import pl.edu.agh.strateg.persistence.DescriptorsDAO;
import pl.edu.agh.strateg.persistence.DocumentsDAO;
import pl.edu.agh.strateg.persistence.mongo.proxy.DescriptorProxy;
import pl.edu.agh.strateg.persistence.mongo.proxy.DocumentProxy;
import pl.edu.agh.strateg.persistence.querying.MetadatumQuery;

import com.google.code.morphia.Datastore;
import com.google.code.morphia.Morphia;
import com.google.code.morphia.dao.BasicDAO;
import com.google.common.collect.Lists;
import com.mongodb.MongoClient;

public class MDocumentsDAOTest {

	private static final String ACCIDENT_META_VALUE1 = "Football penalty";
	private static final String ACCIDENT_DESC_NAME = "Accident category";
	private static final String TEST_DB_NAME = "DataTests";
	private static MongoClient mc;
	private static Morphia morphia;

	private Descriptor causeDesc = SimpleDescriptor.getCreator().createString(
			"Cause");

	private SimpleMetadatum causeMeta;
	private SimpleMetadatum accidentMeta;
	private SimpleMetadatum accidentMeta2;

	private Descriptor accidentDesc;

	private DocumentsDAO dataDAO;
	private DescriptorsDAO descDAO;

	private SimpleDocument firstDocument;
	private SimpleDocument secondDocument;
	private MapMetadataContainer firstContainer;
	private MapMetadataContainer secondContainer;

	private static ByteArrayOutputStream testBytes = new ByteArrayOutputStream();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		mc = new MongoClient();
		morphia = new Morphia();
		int i = 0;
		while (i < 4024) {
			testBytes.write(i);
			++i;
		}
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		mc.close();
	}

	@Before
	public void setUp() throws Exception {
		mc.dropDatabase(TEST_DB_NAME);
		Datastore ds = morphia.createDatastore(mc, TEST_DB_NAME);
		dataDAO = new MDocumentsDAO(new BasicDAO<DocumentProxy, UUID>(
				DocumentProxy.class, ds));
		descDAO = new MDescriptorsDAO(new BasicDAO<DescriptorProxy, String>(
				DescriptorProxy.class, ds));
		causeMeta = new SimpleMetadatum(causeDesc, "Because of");

		MetadataContainer container = new MapMetadataContainer(
				Lists.newArrayList(causeMeta));
		accidentDesc = SimpleDescriptor
				.getCreator()
				.withDescribingMetada(container)
				.withObligatories(
						Lists.newArrayList(Domain.ANALYSIS, Domain.DOCUMENT))
				.createString(ACCIDENT_DESC_NAME);

		accidentMeta = new SimpleMetadatum(accidentDesc, ACCIDENT_META_VALUE1);
		accidentMeta2 = new SimpleMetadatum(accidentDesc,
				"Unfortunately accident");

		firstContainer = new MapMetadataContainer(
				Lists.newArrayList(accidentMeta));
		secondContainer = new MapMetadataContainer(
				Lists.newArrayList(accidentMeta2));

		firstDocument = new SimpleDocument();
		firstDocument
		// .setData("IKonkaSobieSkaczeWeso³IKonkaSobieSkaczeWeso³IKonkaSobieSkaczeWeso³IKonkaSobieSkaczeWeso³IKonkaSobieSkaczeWeso³IKonkaSobieSkaczeWeso³IKonkaSobieSkaczeWeso³IKonkaSobieSkaczeWeso³IKonkaSobieSkaczeWeso³IKonkaSobieSkaczeWeso³IKonkaSobieSkaczeWeso³IKonkaSobieSkaczeWeso³IKonkaSobieSkaczeWeso³IKonkaSobieSkaczeWeso³IKonkaSobieSkaczeWeso³pppppo"
		// .getBytes());
				.setData(testBytes.toByteArray());
		firstDocument.setDescribingMetadata(firstContainer);

		secondDocument = new SimpleDocument();
		secondDocument.setData("someString".getBytes());
		secondDocument.setDescribingMetadata(secondContainer);

		descDAO.saveIfNotExists(accidentDesc);
		descDAO.saveIfNotExists(causeDesc);
	}

	@Test
	public void findingByDescriptorValue() throws Exception {
		dataDAO.saveIfNotExists(firstDocument);
		List<Document> found = dataDAO.queryByMetadata(MetadatumQuery
				.getBuilder().metadatumName(ACCIDENT_DESC_NAME)
				.equalTo(ACCIDENT_META_VALUE1).build());
		assertThat(found.size(), equalTo(1));
		assertDataEquality(found.get(0), firstDocument);
	}

	@Test
	public void findingByDescriptorExistence() throws Exception {
		dataDAO.saveIfNotExists(firstDocument);
		List<Document> found = dataDAO.queryByMetadata(MetadatumQuery
				.getBuilder().metadatumName(ACCIDENT_DESC_NAME).exists()
				.build());
		assertThat(found.size(), equalTo(1));
		assertDataEquality(found.get(0), firstDocument);
	}

	@Test
	public void findingByExisitenceNonExiting() throws Exception {
		dataDAO.saveIfNotExists(firstDocument);
		List<Document> found = dataDAO.queryByMetadata(MetadatumQuery
				.getBuilder().metadatumName("d").exists().build());
		assertThat(found.size(), equalTo(0));
	}

	@Test
	public void findingByNonExisitngDescriptorValue() throws Exception {
		dataDAO.saveIfNotExists(firstDocument);
		List<Document> found = dataDAO.queryByMetadata(MetadatumQuery
				.getBuilder().metadatumName(ACCIDENT_DESC_NAME).equalTo("xyz")
				.build());
		assertThat(found.size(), equalTo(0));
	}

	@Test
	public void savingAndGetting() throws Exception {
		dataDAO.saveIfNotExists(firstDocument);
		assertThat(dataDAO.getAll().size(), equalTo(1));
		assertDataEquality(dataDAO.get(firstDocument.getUUID()).get(),
				firstDocument);
	}

	@Test
	public void savingKeyCorrectness() throws Exception {
		assertThat(dataDAO.saveIfNotExists(firstDocument), is(true));
		assertThat(dataDAO.getAll().size(), equalTo(1));
		Document read = dataDAO.get(firstDocument.getUUID()).get();
		assertDataEquality(firstDocument, read);
	}

	@Test
	public void savingTwoTheSame() throws Exception {
		dataDAO.saveIfNotExists(firstDocument);
		assertThat(dataDAO.saveIfNotExists(firstDocument), equalTo(false));
	}

	@Test
	public void deleting() throws Exception {
		dataDAO.saveIfNotExists(firstDocument);
		assertThat(dataDAO.getAll().size(), equalTo(1));
		dataDAO.deleteIfExists(firstDocument);
		assertThat(dataDAO.getAll().size(), equalTo(0));
	}

	@Test
	public void updating() throws Exception {
		dataDAO.saveIfNotExists(firstDocument);
		assertThat(dataDAO.getAll().size(), equalTo(1));
		firstDocument.setData("new".getBytes());
		dataDAO.updateIfExists(firstDocument);
		assertThat(dataDAO.getAll().size(), equalTo(1));
		SimpleDocument document = (SimpleDocument) dataDAO.get(
				firstDocument.getUUID()).get();
		assertThat(document, equalTo(firstDocument));
		// TODO short equality test
		assertThat(document.getData().read(), equalTo(firstDocument.getData()
				.read()));
	}

	private static void assertDataEquality(Document expected, Document actual)
			throws IOException {
		assertThat(expected, equalTo(actual));
		MetadataContainer expectedContainer = expected.getDescribingMetadata();
		MetadataContainer actualContainer = actual.getDescribingMetadata();
		assertThat(expectedContainer.getSize(),
				equalTo(actualContainer.getSize()));
		Iterator<Metadatum> expectedIterator = expectedContainer.getAll()
				.iterator();
		Iterator<Metadatum> actualIterator = actualContainer.getAll()
				.iterator();
		while (expectedIterator.hasNext()) {
			Metadatum expectedMetadatum = expectedIterator.next();
			Metadatum actualMetadatum = actualIterator.next();
			assertThat(expectedMetadatum, equalTo(actualMetadatum));
		}

		ByteArrayOutputStream expectedBytes = ByteOperationsHelper
				.copyToByteArray(expected.getData());
		ByteArrayOutputStream actualBytes = ByteOperationsHelper
				.copyToByteArray(actual.getData());

		assertArrayEquals(expectedBytes.toByteArray(),
				actualBytes.toByteArray());

	}
}
