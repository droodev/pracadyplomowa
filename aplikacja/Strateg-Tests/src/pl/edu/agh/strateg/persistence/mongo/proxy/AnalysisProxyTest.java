/**
	@author drew
 */

package pl.edu.agh.strateg.persistence.mongo.proxy;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import pl.edu.agh.strateg.model.analysis.Analysis;
import pl.edu.agh.strateg.model.analysis.ColumnAnalysisHeader;
import pl.edu.agh.strateg.model.analysis.RowAnalysisHeader;
import pl.edu.agh.strateg.model.analysis.SWOTField;
import pl.edu.agh.strateg.model.analysis.SimpleAnalysis;
import pl.edu.agh.strateg.model.metadata.MapMetadataContainer;
import pl.edu.agh.strateg.model.metadata.MetadataContainer;
import pl.edu.agh.strateg.model.metadata.SimpleMetadatum;
import pl.edu.agh.strateg.model.metadata.description.Descriptor;
import pl.edu.agh.strateg.model.metadata.description.SimpleDescriptor;

public class AnalysisProxyTest {

	private static final String THREAT_NAME = "Threat1";
	private static final String OPP_NAME = "Opp1";
	private static final String WEAK_NAME = "Weak1";
	private static final String STRENGT_NAME = "Strength1";
	private static final int WO_WEIGHT = 10;
	private static final int WT_WEIGHT = 3;
	private Analysis analysis;
	private RowAnalysisHeader weak1Head;
	private ColumnAnalysisHeader thr1Head;
	private RowAnalysisHeader str1Head;
	private ColumnAnalysisHeader opp1Head;
	private Descriptor descriptor;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		analysis = SimpleAnalysis.createEmptyAnalysisWithNoMetadata();
		opp1Head = analysis.addOpportunity(OPP_NAME);
		str1Head = analysis.addStrength(STRENGT_NAME);
		thr1Head = analysis.addThreat(THREAT_NAME);
		weak1Head = analysis.addWeakness(WEAK_NAME);
		analysis.setCell(weak1Head, thr1Head,
				SWOTField.createSWOTField(WT_WEIGHT));
		analysis.setCell(weak1Head, opp1Head,
				SWOTField.createSWOTField(WO_WEIGHT));

		MetadataContainer container = new MapMetadataContainer();
		descriptor = SimpleDescriptor.getCreator().createInteger("Int");
		SimpleMetadatum describingMetadatum = new SimpleMetadatum(descriptor,
				125);
		container.addMetadatum(describingMetadatum);
		analysis.setDescribingMetadata(container);

	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void proxifyAndDeproxify() {
		AnalysisProxy analysisProxy = AnalysisProxy.proxify(analysis);
		Analysis deproxifiedAnalysis = analysisProxy.deproxify();
		assertThat(deproxifiedAnalysis.getCell(weak1Head, thr1Head).get()
				.getWeight(), is(WT_WEIGHT));
		assertThat(deproxifiedAnalysis.getCell(weak1Head, opp1Head).get()
				.getWeight(), is(WO_WEIGHT));
		assertThat(deproxifiedAnalysis.getCell(str1Head, opp1Head).isPresent(),
				is(false));
		Set<ColumnAnalysisHeader> columns = deproxifiedAnalysis
				.getColumns();
		assertThat(columns.size(), is(2));
		Iterator<ColumnAnalysisHeader> iterator = columns.iterator();
		assertThat(iterator.next().getName(), is(OPP_NAME));
		assertThat(iterator.next().getName(), is(THREAT_NAME));
		assertThat(
				deproxifiedAnalysis.getDescribingMetadata()
						.contains(descriptor), is(true));

	}
}
