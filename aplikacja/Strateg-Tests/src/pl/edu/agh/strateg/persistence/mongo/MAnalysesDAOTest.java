/**
	@author drew
 */

package pl.edu.agh.strateg.persistence.mongo;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.UUID;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import pl.edu.agh.strateg.model.analysis.Analysis;
import pl.edu.agh.strateg.model.analysis.ColumnAnalysisHeader;
import pl.edu.agh.strateg.model.analysis.RowAnalysisHeader;
import pl.edu.agh.strateg.model.analysis.SWOTField;
import pl.edu.agh.strateg.model.analysis.SimpleAnalysis;
import pl.edu.agh.strateg.model.metadata.MapMetadataContainer;
import pl.edu.agh.strateg.model.metadata.MetadataContainer;
import pl.edu.agh.strateg.model.metadata.SimpleMetadatum;
import pl.edu.agh.strateg.model.metadata.description.Descriptor;
import pl.edu.agh.strateg.model.metadata.description.SimpleDescriptor;
import pl.edu.agh.strateg.persistence.DescriptorsDAO;
import pl.edu.agh.strateg.persistence.PersistenceException;
import pl.edu.agh.strateg.persistence.mongo.proxy.AnalysisProxy;
import pl.edu.agh.strateg.persistence.mongo.proxy.DescriptorProxy;

import com.google.code.morphia.Datastore;
import com.google.code.morphia.Morphia;
import com.google.code.morphia.dao.BasicDAO;
import com.google.common.base.Optional;
import com.mongodb.MongoClient;

public class MAnalysesDAOTest {

	private static final String THREAT_NAME = "Threat1";
	private static final String OPP_NAME = "Opp1";
	private static final String WEAK_NAME = "Weak1";
	private static final String STRENGT_NAME = "Strength1";
	private static final int WO_WEIGHT = 10;
	private static final int WT_WEIGHT = 3;
	private Analysis analysis;
	private RowAnalysisHeader weak1Head;
	private ColumnAnalysisHeader thr1Head;
	private RowAnalysisHeader str1Head;
	private ColumnAnalysisHeader opp1Head;

	private static final String TEST_DB_NAME = "AnalysisTest";
	private static MongoClient mc;
	private static Morphia morphia;

	private DescriptorsDAO descDAO;

	MAnalysesDAO dao;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		mc = new MongoClient();
		morphia = new Morphia();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		mc.close();
	}

	@Before
	public void setUp() throws Exception {
		Datastore ds = morphia.createDatastore(mc, TEST_DB_NAME);
		dao = new MAnalysesDAO(new BasicDAO<AnalysisProxy, UUID>(
				AnalysisProxy.class, ds));

		descDAO = new MDescriptorsDAO(new BasicDAO<DescriptorProxy, String>(
				DescriptorProxy.class, ds));

		mc.dropDatabase(TEST_DB_NAME);
		analysis = SimpleAnalysis.createEmptyAnalysisWithNoMetadata();
		opp1Head = analysis.addOpportunity(OPP_NAME);
		str1Head = analysis.addStrength(STRENGT_NAME);
		thr1Head = analysis.addThreat(THREAT_NAME);
		weak1Head = analysis.addWeakness(WEAK_NAME);
		analysis.setCell(weak1Head, thr1Head,
				SWOTField.createSWOTField(WT_WEIGHT));
		analysis.setCell(weak1Head, opp1Head,
				SWOTField.createSWOTField(WO_WEIGHT));

		MetadataContainer container = new MapMetadataContainer();
		Descriptor descriptor = SimpleDescriptor.getCreator().createInteger(
				"Int");
		SimpleMetadatum describingMetadatum = new SimpleMetadatum(descriptor,
				125);
		container.addMetadatum(describingMetadatum);
		analysis.setDescribingMetadata(container);
		descDAO.saveIfNotExists(descriptor);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void saving() throws PersistenceException {
		assertThat(dao.saveIfNotExists(analysis), is(true));
		assertThat(dao.saveIfNotExists(analysis), is(false));
		assertThat(dao.get(analysis.getUUID()).isPresent(), is(true));
	}

	@Test
	public void deleting() throws PersistenceException {
		assertThat(dao.saveIfNotExists(analysis), is(true));
		assertThat(dao.deleteIfExists(analysis), is(true));
		assertThat(dao.get(analysis.getUUID()).isPresent(), is(false));
		assertThat(dao.deleteIfExists(analysis), is(false));
	}

	@Test
	public void updating() throws PersistenceException {
		assertThat(dao.updateIfExists(analysis), is(false));
		assertThat(dao.saveIfNotExists(analysis), is(true));
		analysis.addOpportunity("NewOpp");
		assertThat(dao.updateIfExists(analysis), is(true));
		Optional<Analysis> gotAnalysisOpt = dao.get(analysis.getUUID());
		assertThat(gotAnalysisOpt.isPresent(), is(true));
		assertThat(gotAnalysisOpt.get().getColumns().size(), is(3));
	}

}
