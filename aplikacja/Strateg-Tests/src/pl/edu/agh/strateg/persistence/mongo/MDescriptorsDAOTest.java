/**
	@author drew
 */

package pl.edu.agh.strateg.persistence.mongo;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import pl.edu.agh.strateg.model.metadata.MapMetadataContainer;
import pl.edu.agh.strateg.model.metadata.MetadataContainer;
import pl.edu.agh.strateg.model.metadata.SimpleMetadatum;
import pl.edu.agh.strateg.model.metadata.description.CollectionLimitedDescriptor;
import pl.edu.agh.strateg.model.metadata.description.Descriptor;
import pl.edu.agh.strateg.model.metadata.description.Domain;
import pl.edu.agh.strateg.model.metadata.description.Requirements;
import pl.edu.agh.strateg.model.metadata.description.SimpleDescriptor;
import pl.edu.agh.strateg.persistence.DescriptorsDAO;
import pl.edu.agh.strateg.persistence.PersistenceException;
import pl.edu.agh.strateg.persistence.mongo.proxy.DescriptorProxy;
import pl.edu.agh.strateg.persistence.querying.MetadatumQuery;

import com.google.code.morphia.Datastore;
import com.google.code.morphia.Morphia;
import com.google.code.morphia.dao.BasicDAO;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.mongodb.MongoClient;

public class MDescriptorsDAOTest {
	private static final String REQ_OBJ = "req";
	private static final String INNER_VAL = "IUnnerVal";
	private static final String INNER_NAME = "Inner";
	private static final ArrayList<String> LIMITING_LIST = Lists
			.newArrayList("Abc");
	private static final String REQUIRED_DESC_NAME = "Req";
	private static final String REQUIRED_DESC_NAME_COND = "ReqCond";
	private static final String TESTED_DESCRIPTOR_NAME = "TestedDescriptor";
	private static final String TEST_DB_NAME = "DescTest";
	private static MongoClient mc;
	private static Morphia morphia;

	private static Requirements testedReq = Requirements.getBuilder()
			.addRequirement(Domain.DESCRIPTOR)
			.unconditional(REQUIRED_DESC_NAME).addRequirement(Domain.ANALYSIS)
			.conditional(REQUIRED_DESC_NAME_COND, REQ_OBJ).build();

	private static Descriptor innerDesc = SimpleDescriptor.getCreator()
			.withObligatories(Lists.newArrayList(Domain.REPORT))
			.withRequirements(testedReq).createString(INNER_NAME);

	private static SimpleMetadatum innerMetadatum = new SimpleMetadatum(
			innerDesc, INNER_VAL);

	private static MapMetadataContainer descContainer = new MapMetadataContainer();
	static {
		descContainer.addMetadatum(innerMetadatum);
	}

	private Descriptor descToAdd = SimpleDescriptor.getCreator()
			.withObligatories(Lists.newArrayList(Domain.REPORT))
			.withRequirements(testedReq).withDescribingMetada(descContainer)
			.createString(TESTED_DESCRIPTOR_NAME);

	private Descriptor limitedDesc = CollectionLimitedDescriptor
			.createLimitedDescriptor(LIMITING_LIST, descToAdd);

	private DescriptorsDAO testedDao;
	private Descriptor mockedDescriptor;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		mc = new MongoClient();
		morphia = new Morphia();

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		mc.close();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Before
	public void setUp() throws Exception {
		mc.dropDatabase(TEST_DB_NAME);
		Datastore ds = morphia.createDatastore(mc, TEST_DB_NAME);

		testedDao = new MDescriptorsDAO(new BasicDAO<DescriptorProxy, String>(
				DescriptorProxy.class, ds));
		// This descriptor is in db at start and till end - it's needed
		testedDao.saveIfNotExists(innerDesc);
		mockedDescriptor = Mockito.mock(Descriptor.class);
		Mockito.when(mockedDescriptor.getName()).thenReturn("Name");
		Mockito.when(mockedDescriptor.getDescribingMetadata()).thenReturn(
				MetadataContainer.EMPTY);
		Mockito.when(mockedDescriptor.getValueType()).thenReturn(
				(Class) String.class);
		Mockito.when(mockedDescriptor.getObligatories()).thenReturn(
				Lists.newArrayList(Domain.ANALYSIS));
		Mockito.when(mockedDescriptor.getRequirements()).thenReturn(
				Requirements.NONE);

	}

	@SuppressWarnings("unchecked")
	@Test
	public void savingLimitedCorrectness() throws Exception {
		testedDao.saveIfNotExists(limitedDesc);
		List<Descriptor> all = testedDao.getAll();
		assertThat(all.size(), CoreMatchers.equalTo(2));
		Descriptor limitedDesc = all.get(1);
		assertThat(limitedDesc.isLimited(), is(true));
		Collection<?> relevantValues = limitedDesc.toLimited()
				.getRelevantValues();
		assertThat(relevantValues.iterator().next(),
				(Matcher<Object>) (Object) equalTo(LIMITING_LIST.get(0)));

	}

	@Test
	public void queryingByDescriptorExistenceNonExisting() throws Exception {
		testedDao.saveIfNotExists(descToAdd);
		MetadatumQuery criterion = MetadatumQuery.getBuilder()
				.metadatumName("as").exists().build();
		assertThat(testedDao.queryByMetadata(criterion).size(), equalTo(0));
	}

	@Test
	public void queryingByDescriptorEqualityNonEqual() throws Exception {
		testedDao.saveIfNotExists(descToAdd);
		MetadatumQuery criterion = MetadatumQuery.getBuilder()
				.metadatumName("as").equalTo("o").build();
		assertThat(testedDao.queryByMetadata(criterion).size(), equalTo(0));
	}

	@Test
	public void queryingByDescriptorEquality() throws Exception {
		testedDao.saveIfNotExists(descToAdd);
		MetadatumQuery criterion = MetadatumQuery.getBuilder()
				.metadatumName(INNER_NAME).equalTo(INNER_VAL).build();
		assertThat(testedDao.queryByMetadata(criterion).size(), equalTo(1));
	}

	@Test
	public void queryingByDescriptorExistence() throws Exception {
		testedDao.saveIfNotExists(descToAdd);
		MetadatumQuery criterion = MetadatumQuery.getBuilder()
				.metadatumName(INNER_NAME).exists().build();
		assertThat(testedDao.queryByMetadata(criterion).size(), equalTo(1));
	}

	@Test
	public void savingCountCorrectness() throws Exception {
		testedDao.saveIfNotExists(descToAdd);
		assertThat(testedDao.getAll().size(), CoreMatchers.equalTo(2));
	}

	@Test
	public void savingKeyCorrectness() throws Exception {
		assertThat(testedDao.saveIfNotExists(descToAdd), is(true));
	}

	@Test
	public void savingEqualityCorrectness() throws Exception {
		testedDao.saveIfNotExists(descToAdd);
		assertThat(testedDao.getAll().size(), CoreMatchers.equalTo(2));
		Descriptor gotDescriptor = testedDao.getAll().get(1);
		assertThat(gotDescriptor, CoreMatchers.equalTo((Descriptor) descToAdd));
		assertThat(gotDescriptor.getObligatories().size(), equalTo(descToAdd
				.getObligatories().size()));
		assertThat(gotDescriptor.getObligatories().iterator().next(),
				equalTo(descToAdd.getObligatories().iterator().next()));
		List<String> requiredForDescriptor = gotDescriptor.getRequirements()
				.getUnconditionalRequirements(Domain.DESCRIPTOR);
		assertThat(requiredForDescriptor.size(), is(1));
		assertThat(requiredForDescriptor.get(0), is(REQUIRED_DESC_NAME));
		requiredForDescriptor = gotDescriptor.getRequirements()
				.getRequirementsForValueAndDomain(Domain.ANALYSIS, REQ_OBJ);
		assertThat(requiredForDescriptor.size(), is(1));
		assertThat(requiredForDescriptor.get(0), is(REQUIRED_DESC_NAME_COND));
	}

	@Test
	public void savingTwoTimes() throws PersistenceException {
		testedDao.saveIfNotExists(descToAdd);
		assertThat(testedDao.saveIfNotExists(descToAdd), equalTo(false));
	}

	@Test(expected = PersistenceException.class)
	public void savingWithNullContainerRef() throws PersistenceException {
		Descriptor withNullMock = Mockito.mock(Descriptor.class);
		Mockito.when(withNullMock.getDescribingMetadata()).thenReturn(null);
		testedDao.saveIfNotExists(withNullMock);
	}

	@Test(expected = PersistenceException.class)
	public void savingWithNullValueType() throws Exception {
		Descriptor withNullMock = Mockito.mock(Descriptor.class);
		Mockito.when(withNullMock.getValueType()).thenReturn(null);
		testedDao.saveIfNotExists(withNullMock);
	}

	@Test
	public void deletingWithDescRef() throws Exception {
		testedDao.saveIfNotExists(mockedDescriptor);
		testedDao.saveIfNotExists(descToAdd);
		assertThat(testedDao.deleteIfExists(mockedDescriptor), is(true));
		List<Descriptor> all = testedDao.getAll();
		assertThat(all.size(), CoreMatchers.is(2));
		// org.hamcrest.MatcherAssert.assertThat(all,
		// org.hamcrest.Matchers.containsInAnyOrder(descToAdd));
		// hamcrest doesn't work:/
		assertThat(all.get(1), is(descToAdd));

	}

	@Test
	public void deletingNonExistent() throws Exception {
		assertThat(testedDao.deleteIfExists(descToAdd), is(false));
	}

	@Test
	public void findingAll() throws Exception {
		testedDao.saveIfNotExists(descToAdd);
		testedDao.saveIfNotExists(mockedDescriptor);
		assertThat(testedDao.getAll().size(), CoreMatchers.is(3));
	}

	/**
	 * Not exactly empty - inner descriptor is always in db
	 * 
	 * @throws Exception
	 */
	@Test
	public void findingAllfromEmpty() throws Exception {
		assertThat(testedDao.getAll().size(), CoreMatchers.is(1));
	}

	@Test
	public void updatingNonExistent() throws PersistenceException {
		assertThat(testedDao.updateIfExists(mockedDescriptor), is(false));
	}

	@Test
	public void updating() throws PersistenceException {
		Descriptor toUpdateDescriptor = SimpleDescriptor.getCreator()
				.createUntyped(TESTED_DESCRIPTOR_NAME);
		testedDao.saveIfNotExists(toUpdateDescriptor);
		assertThat(testedDao.updateIfExists(descToAdd), is(true));
		Optional<Descriptor> updatedDescriptor = testedDao
				.get(TESTED_DESCRIPTOR_NAME);
		assertThat(updatedDescriptor.isPresent(), is(true));
		assertThat(updatedDescriptor.get(),
				CoreMatchers.equalTo((Descriptor) descToAdd));
	}

}
