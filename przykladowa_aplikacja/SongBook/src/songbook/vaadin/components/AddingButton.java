package songbook.vaadin.components;

import songbook.vaadin.components.inner.SongWriterController;
import songbook.vaadin.model.SongPart;

import com.vaadin.ui.Button;


public class AddingButton extends Button {

	public AddingButton(final SongPart managedPart,
			final SongWriterController songWriterController,
			String caption) {
		super(caption);
		
		addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				songWriterController.fieldAdded(managedPart);
			}
		});
		
		
	}
}
