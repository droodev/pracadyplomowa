package songbook.vaadin.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import org.apache.bcel.generic.IndexedInstruction;

import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.ObjectProperty;

public class SongItem implements Item{
	private EnumMap<SongPart, List<Property<String>>> songParts= new EnumMap<SongPart, List<Property<String>>>(SongPart.class);  

	private Item indexedItem;
	
	public SongItem() {
		initializeEnumMap();
	}

	private void initializeEnumMap() {
		for(SongPart part: SongPart.values()){
			songParts.put(part, new ArrayList<Property<String>>());
		}
		songParts.get(SongPart.TITLE).add(new ObjectProperty<String>("Tytu�"));
	}
	
	@Override
	public Property<String> getItemProperty(Object id) {
		checkIndexValidity(id);
		SongQualifiedPart qualifiedPart = (SongQualifiedPart) id;
		SongPart songPart = qualifiedPart.getPart();
		int partOrdinalNumber = qualifiedPart.getOrdinalNumber();
		List<Property<String>> concretePartsList = songParts.get(songPart);
		if(concretePartsList == null){
			return null;
		}
		try {
			return concretePartsList.get(partOrdinalNumber);
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace(System.err);
			return null;
		}
	}

	@Override
	public Collection<?> getItemPropertyIds() {
		ArrayList<SongQualifiedPart> allParts = new ArrayList<SongQualifiedPart>();
		for(Map.Entry<SongPart, List<Property<String>>> entry: songParts.entrySet()){
			for(int partOrdinal = 0; partOrdinal < entry.getValue().size(); ++partOrdinal){
				allParts.add(new SongQualifiedPart(partOrdinal, entry.getKey()));
			}
		}
		return allParts;
	}

	@Override
	public boolean addItemProperty(Object id, @SuppressWarnings("rawtypes") Property property)
			throws UnsupportedOperationException {
		checkIndexValidity(id);
		checkPropertyValidity(property);
		@SuppressWarnings("unchecked")
		Property<String> validatedProperty = (Property<String>)property;
		SongQualifiedPart qualifiedPart = (SongQualifiedPart) id;
		SongPart songPart = qualifiedPart.getPart();
		int partOrdinalNumber = qualifiedPart.getOrdinalNumber();
		List<Property<String>> concretePartsList = songParts.get(songPart);
		if(partOrdinalNumber < concretePartsList.size()){
			concretePartsList.add(partOrdinalNumber, validatedProperty);
		}else{
			return concretePartsList.add(validatedProperty);
		}
		return true;
	}

	private void checkPropertyValidity(@SuppressWarnings("rawtypes") Property property) {
		if(!property.getType().equals(String.class)){
			throw new UnsupportedOperationException("Bad property type");
		}
	}

	@Override
	public boolean removeItemProperty(Object id)
			throws UnsupportedOperationException {
		checkIndexValidity(id);
		SongQualifiedPart qualifiedPart = (SongQualifiedPart) id;
		SongPart songPart = qualifiedPart.getPart();
		int partOrdinalNumber = qualifiedPart.getOrdinalNumber();
		List<Property<String>> concretePartsList = songParts.get(songPart);
		try {
			concretePartsList.remove(partOrdinalNumber);
		} catch (IndexOutOfBoundsException e) {
			System.err.println("Cannot remove property cause it's not in database");
			return false;
		}
		return true;
	}
	
	private void checkIndexValidity(Object id){
		if(id.getClass() != SongQualifiedPart.class){
			throw new IllegalArgumentException("Bad index type");
		}
	}
	
	public SongQualifiedPart getNextPart(SongPart part){
		int ordinalPart = songParts.get(part).size();
		return new SongQualifiedPart(ordinalPart, part);
	}
	
	public void bindWithIndexedItem(Item indexedItem){
		this.indexedItem = indexedItem;
		changeCommited();
	}
	
	@SuppressWarnings("unchecked")
	public void changeCommited(){
		SongQualifiedPart id = new SongQualifiedPart(0, SongPart.TITLE);
		indexedItem.getItemProperty(id).setValue(getItemProperty(id).getValue());
	}
	
	
}
