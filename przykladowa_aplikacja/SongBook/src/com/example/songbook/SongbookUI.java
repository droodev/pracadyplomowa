package com.example.songbook;

import javax.servlet.annotation.WebServlet;

import songbook.vaadin.components.SongTable;
import songbook.vaadin.components.SongWriter;
import songbook.vaadin.model.SongItem;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
@Theme("songbook")
public class SongbookUI extends UI {

	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = SongbookUI.class, closeIdleSessions = true, heartbeatInterval = 4)
	public static class Servlet extends VaadinServlet {
	}

	@Override
	protected void init(VaadinRequest request) {
		final HorizontalLayout layout = new HorizontalLayout();
		layout.setSpacing(true);
		layout.setMargin(true);
		setContent(layout);
		
		SongItem songItem = new SongItem();
		
		
		final SongWriter songW = new SongWriter(songItem);
		final SongTable songTable = new SongTable(songW);
		//songItem.bindWithIndexedItem(songTable.getNewItem());
		
		layout.addComponent(songW);
		layout.addComponent(songTable);
		
		
		getSession().getSession().setMaxInactiveInterval(5);
		//		Button refBut = new Button("OD�WIE�");
//		refBut.addClickListener(new ClickListener() {
//			
//			@Override
//			public void buttonClick(ClickEvent event) {
//				SongItem songIt = new SongItem();
//				songW.reload(songIt);
//				songIt.bindWithIndexedItem(songTable.getNewItem());
//			}
//		});
//		layout.addComponent(refBut);
		//songTable.addItem(songItem);
		System.out.println(VaadinSession.getCurrent().hashCode());
		System.out.println(VaadinSession.getCurrent().getSession().getMaxInactiveInterval());
	}

}